/**
 * Internal imports
 */
import { Role } from './common';

/**
 * TypeScript entities and constants
 */
export const CAN_CREATE_REPORT_ROLES: Role[] = [
  Role.Mayor,
  Role.ProjectTeam,
  Role.ANCT,
  Role.EPCI
];
