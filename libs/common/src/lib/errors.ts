/**
 * TypeScript entities and constants
 */
export const EMAIL_ALREADY_EXISTS_ERROR: string = 'Email already exists';
export const ALLOWED_ACCOUNT_NOT_FOUND: string = 'Allowed account not found';
export const INVALID_JWT_TOKEN_ERROR: string = 'Invalid token';
export const EXPIRED_JWT_TOKEN_ERROR: string = 'Expired token';
export const WEAK_PASSWORD_ERROR: string = 'Password too weak';
export const EMAIL_ALLOWED_ACCOUNT_NOT_FOUND_ERROR: string = 'L’adresse électronique utilisée n’est pas connue de l’Agence nationale de la cohésion des territoires';
export const ACCOUNT_ALREADY_EXISTS_ERROR: string = 'Un compte a déjà été créé pour cette adresse électronique.';
export const EMAIL_TEMPLATE_NOT_FOUND = 'Email template not found';
export const INVALID_FORMAT_EMAIL_ERROR: string = 'Veuillez utiliser une adresse électronique valide';
export const INVALID_LOGIN_CREDENTIALS: string = 'L\'adresse électronique ou le mot de passe est invalide.';
export const ACCOUNT_NOT_CONFIRMED: string = 'La création de votre compte n’est pas finalisée. Pour la terminer, veuillez cliquer sur le lien qui vous a été envoyé dans le courriel de confirmation lors de votre inscription.';
export const ACCOUNT_DEACTIVATED: string = 'Votre compte a été désactivé, car nous avons été informés de la fin de votre activité concernant les sujets d’infrastructures numériques. Si vous pensez que c’est une erreur, merci de nous contacter à tous-connectes@anct.gouv.fr.';
export const INTERNAL_SERVER_ERROR: string = 'Une erreur est survenue, veuillez réessayer plus tard.'
