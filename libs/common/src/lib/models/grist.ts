/**
 * Internal imports
 */
import { Role } from '../common';

/**
 * TypeScript entities and constants
 */

/*
 * These interfaces depend on the Grist Contacts document and its
 * source data.
 * If any of the columns involved changes, the according interface must correspond
 */
export interface GristContact {
  id: number;
  fields: GristContactFields;
}

export interface GristContactFields {
  Sous_categorie: number;
  Derniere_modification: string;
  Civilite: string;
  Structure: string;
  Juridiction: (number | string)[];
  Programme2: string[];
  Comite: string[];
  Prenom: string;
  Telephone: string;
  Adresse_courriel: string;
  Role: (number | string)[];
  Fonction: string;
  Commentaire: string;
  Nom: string;
  Actif: boolean;
  Referent_principal: boolean;
  Compte_cree: boolean;
  Doublon: boolean;
  Referent_pour_Pilote: boolean;
  Ministere_porteur: string;
  Chantier_prioritaire: string;
  Categorie: string;
  Departement : string[],
  Ecrire: string;
  Region: string[];
  authWebhook: string;
}

export interface GristJurisdiction {
  id: number;
  fields: {
    lib_desambig: string;
    siren_groupement: number,
    lib_groupement: string;
    nature_juridique: string;
    cheflieu: string;
    insee_geo: string;
    echelon_geo: string;
    insee_dep: string[];
    insee_reg: string;
  }
}

export interface GristRole {
  id: number;
  fields: GristRoleFields;
}

export interface GristRoleFields {
  Role: string,
  Ecrire: string,
  Name: Role;
}