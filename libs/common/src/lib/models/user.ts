/**
 * 3rd Party imports
 */
import { Types } from 'mongoose';

/**
 * Internal imports
 */
import { JurisdictionType, PossibleCoordinates, Role } from '../common';
import { CityHallContact } from './report';

/**
 * TypeScript entities and constants
 */
export interface Jurisdiction {
  type: JurisdictionType;
  name: string;
  initialZoom: number;
  code?: string;
  epciCities?: Jurisdiction[] | Types.ObjectId[];
  referenceCoordinates: [number, number];
}

export interface SafeUser {
  _id: Types.ObjectId;
  email: string;
  roles: Role[];
  jurisdictions: Jurisdiction[];
  preferredCoordinates: PossibleCoordinates;
  hasSubscribedToNewsletter: boolean;
  lastLoginDate?: Date;
  preferences?: UserPreferences;
}

export interface UserPreferences {
  defaultReportContact: CityHallContact;
}

export interface UserToCreate {
  email: string;
  password: string;
  hasSubscribedToNewsletter: boolean;
  roles?: Role[];
  jurisdictions?: Jurisdiction[] | Types.ObjectId[];
}

export interface AccessToken {
  access_token: string;
}

export interface AllowedAccount {
  _id?: Types.ObjectId;
  email: string;
  fullName?: string;
  roles: Role[];
  jurisdictions: Jurisdiction[] | Types.ObjectId[];
  isActive: boolean;
  pointOfContact: boolean;
}
