/**
 * 3rd-party imports
 */
import { Types } from "mongoose";

/**
 * Internal imports
 */
import { MobileOperator } from '../common';
import { Report } from "./report";

/**
 * TypeScript entities and constants
 */
export enum CoverStatus {
  TBC = 'TBC',
  BC = 'BC',
  CL = 'CL',
  NC = 'NC'
}

export interface StudyPoint {
  batch: string;
  zoneId: string;
  zoneName: string;
  pointId: string;
  pointName: string;
  latitude: string;
  longitude: string;
  department: string;
  region: string;
  BYTCoverStatus: CoverStatus;
  FRMCoverStatus: CoverStatus;
  ORFCoverStatus: CoverStatus;
  SFRCoverStatus: CoverStatus;
  operatorsConcerned: MobileOperator[];
  sitesNeededForZone: number;
  siteId?: string;
  comment?: string;
  validityLimitDate: Date;
  creationDate: Date;
  modificationDate?: Date;
  report?: Report | Types.ObjectId;
}
