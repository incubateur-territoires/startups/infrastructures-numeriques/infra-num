/**
 * TypeScript entities and constants
 */
export interface ServicePublicApiResult {
  total_count: number;
  results: ServicePublicApiProperties[];
}

export interface ServicePublicApiProperties {
  id: string;
  plage_ouverture?: string;
  site_internet?: string;
  copyright: string;
  siren?: string;
  ancien_code_pivot?: string;
  reseau_social?: string;
  texte_reference?: string;
  partenaire?: string;
  telecopie?: string;
  nom: string;
  siret?: string;
  itm_identifiant?: string;
  sigle?: string;
  affectation_personne?: string;
  date_modification: Date;
  adresse_courriel?: string;
  service_disponible?: string;
  organigramme?: string;
  pivot?: string;
  partenaire_identifiant?: string;
  ancien_identifiant?: string;
  ancien_nom?: string;
  commentaire_plage_ouverture?: string;
  annuaire?: string;
  tchat?: string;
  hierarchie?: string;
  categorie: 'SL' | 'SI' | 'SIL';
  sve?: string;
  telephone_accessible?: string;
  application_mobile?: string;
  version_type: 'Publiable',
  type_repertoire?: string;
  telephone?: string;
  version_etat_modification?: string;
  date_creation: string;
  partenaire_date_modification?: string;
  mission?: string;
  formulaire_contact?: string;
  version_source?: string;
  type_organisme?: string;
  code_insee_commune?: string;
  statut_de_diffusion: 'true';
  adresse?: string;
  url_service_public?: string;
  information_complementaire?: string;
  date_diffusion?: Date;
}
