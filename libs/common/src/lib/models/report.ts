/**
 * 3rd Party imports
 */
import { Types } from 'mongoose';

/**
 * Internal imports
 */
import { Jurisdiction, SafeUser } from './user';
import { JurisdictionType, MobileOperator, Role } from '../common';

export enum ReportLocation {
  Outside = 'Outside',
  Inside = 'Inside'
}

export enum ReportUnavailability {
  CallOrSendSms = 'CallOrSendSms',
  InternetAccess = 'InternetAccess'
}

export enum ReportStatus {
  NotTreated = 'NotTreated',  // En attente
  Treated = 'Treated',        // Etude radio demandée
  Validated = 'Validated',    // Etude radio réalisée
  Desultory = 'Desultory',    // Sans suite
  Obsolete = 'Obsolete',      // Obsolète
  Deleted = 'Deleted'         // Supprimé
}

export interface Report {
  latitude: string;
  longitude: string;
  zipCode: string;
  city: string;
  department: string;
  region: string;
  locations: ReportLocation[];
  unavailabilities: ReportUnavailability[];
  operators: MobileOperator[];
  description: string;
  statusInfo?: ReportStatusInfo[];
  status: ReportStatus;
  author: SafeUser | Types.ObjectId;
  creationDate?: Date;
  modification?: ReportModification;
  cityHallContact?: CityHallContact;
  oldPlatformContact?: string;
}

export interface ReportModification {
  date: Date;
  author: SafeUser | Types.ObjectId;
}

export interface ReportStatusInfo {
  status: ReportStatus;
  oldStatus: ReportStatus;
  changeDate: Date;
  comment?: string;
  changeAuthor: SafeUser | Types.ObjectId;
}

export interface CityHallContact {
  fullName: string;
  phone: string;
  email: string;
}

export interface ReportWithId extends Report {
  _id: Types.ObjectId;
}

export interface ReportDownload {
  region: string;
  department: string;
  city: string;
  creationDate: string;
  author: string;
  status: string;
  operators: string;
  issues: string;
  description: string;
  xLambert: string;
  yLambert: string;
  latitude: string;
  longitude: string;
}

export const setInstitutionByAuthor = (author: SafeUser): string => {
  let authorInstitution: string;

  if (author?.roles?.includes(Role.Mayor)) {
    const eligibleJurisdictions = author.jurisdictions.filter(
      (jurisdiction: Jurisdiction) => {
        return jurisdiction.type === JurisdictionType.City;
      }
    );
    authorInstitution = 'mairie de ' + eligibleJurisdictions[0].name;
  } else if (author?.roles?.includes(Role.EPCI)) {
    const eligibleJurisdictions = author.jurisdictions.filter(
      (jurisdiction: Jurisdiction) => {
        return jurisdiction.type === JurisdictionType.EPCI;
      }
    );
    authorInstitution = 'EPCI de ' + eligibleJurisdictions[0].name;
  } else if (author?.roles?.includes(Role.ProjectTeam)) {
    const eligibleJurisdictions = author.jurisdictions.filter(
      (jurisdiction: Jurisdiction) => {
        return (
          jurisdiction.type === JurisdictionType.Department ||
          jurisdiction.type === JurisdictionType.Region
        );
      }
    );
    authorInstitution =
      'équipe projet de ' +
      eligibleJurisdictions.map(({name}) => name).join(', ');
  } else {
    authorInstitution = 'l\'ANCT';
  }

  return authorInstitution;
}