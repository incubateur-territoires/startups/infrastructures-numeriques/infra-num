/**
 * Internal imports
 */
import { PointGeometry } from '../common';

/**
 * TypeScript entities and constants
 */
export interface AdresseApiResult {
  type: 'FeatureCollection';
  features: AdresseApiFeature[];
}

interface AdresseApiFeature {
  type: 'Feature';
  geometry: PointGeometry;
  properties: AdresseApiProperties;
}

export interface AdresseApiProperties {
  id: string;
  label: string;
  score: number;
  housenumber?: string;
  street?: string;
  name: string;
  postcode: string;
  citycode: string;
  city: string;
  district?: string;
  oldcitycode?: string;
  oldcity?: string;
  context: string;
  x: number;
  y: number;
  type: AdresseApiType;
  importance: number;
  municipality?: string;
  locality?: string;
  population?: number;
  distance?: number;
}

export type AdresseApiType =
  | 'housenumber'
  | 'street'
  | 'locality'
  | 'municipality';

export interface Address {
  id: string;
  label: string;
  housenumber?: string;
  street: string;
  codePostal: string;
  city: string;
  type: 'address';
  coordinates: [number, number];
  zoom: 17;
}
