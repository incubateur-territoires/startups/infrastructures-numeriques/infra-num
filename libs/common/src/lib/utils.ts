/**
 * 3rd-party imports
 */
import proj4 from 'proj4';

/**
 * Useful functions
 */
const lambertProjection =
  '+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs';
const gpsProjection = 'WGS84';

export const isNumeric = (numberStr: string): boolean =>
  !isNaN(parseFloat(numberStr));

export const lambert93ToGPS = (
  lambertE: number,
  lambertN: number
): { lat: number; lng: number } => {
  const [lng, lat] = proj4(lambertProjection, gpsProjection, [
    lambertE,
    lambertN
  ]);
  return { lng: +lng.toFixed(5), lat: +lat.toFixed(5) };
};

export const GPSToLambert93 = (
  lng: number,
  lat: number
): { lambertE: number; lambertN: number } => {
  const [lambertE, lambertN] = proj4(gpsProjection, lambertProjection, [
    lng,
    lat
  ]);
  return { lambertE: Math.trunc(lambertE), lambertN: Math.trunc(lambertN) };
};
