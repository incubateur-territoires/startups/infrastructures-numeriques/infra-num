/**
 * Internal imports
 */
import { GPSToLambert93, isNumeric, lambert93ToGPS } from './utils';

describe('Utils', () => {
  describe('isNumeric', () => {
    it('should return true when given a string of only numbers', () => {
      expect(isNumeric('12345')).toEqual(true);
    });
    it('should return false when given a string with letters', () => {
      expect(isNumeric('azerty')).toEqual(false);
    });
  });

  describe('lambert93ToGPS', () => {
    const testLambertE = 646111.587950;
    const testLambertN = 6876272.601465;

    it('should return the gps coordinates values', () => {
      const {lat, lng} = lambert93ToGPS(testLambertE, testLambertN);
      const {lambertE, lambertN} = GPSToLambert93(lng, lat);
      expect(lambertE).toBeCloseTo(testLambertE);
      expect(lambertN).toBeCloseTo(testLambertN);
    });
  });

  describe('GPSToLambert93', () => {
    const testLng = 2.2637500000042587;
    const testLat = 48.984125999985885;

    it('should return the Lambert93 coordinates values', () => {
      const {lambertE, lambertN} = GPSToLambert93(testLng, testLat);
      const {lat, lng} = lambert93ToGPS(lambertE, lambertN);
      expect(testLng).toBeCloseTo(lng);
      expect(testLat).toBeCloseTo(lat);
    });
  });
});
