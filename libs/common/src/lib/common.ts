import { Address } from './models/adresse';

/**
 * TypeScript entities and constants
 */
export enum Role {
  Mayor = 'Mayor',
  ProjectTeam = 'ProjectTeam',
  ANCT = 'ANCT',
  MobileNetworkProvider = 'MobileNetworkProvider',
  Guest = 'Guest',
  ProjectHolder = 'ProjectHolder',
  EPCI = 'EPCI'
}

export enum MobileOperator {
  BouyguesTelecom = 'BouyguesTelecom',
  Free = 'Free',
  Orange = 'Orange',
  SFR = 'SFR'
}

export enum PossibleCoordinates {
  GPS = 'GPS',
  Lambert93 = 'Lambert93'
}

export enum JurisdictionType {
  City = 'City',
  EPCI = 'EPCI',
  Region = 'Region',
  Department = 'Department',
  France = 'France',
  Territory = 'Territory',
  Other = 'Other'
}

export interface City {
  nom: string;
  centre?: {
    type: 'Point';
    coordinates: [number, number];
  };
  type: 'city';
  codePostal: string;
  code: string;
  codeDepartement?: string;
  nomDepartement?: string;
  codeRegion?: string;
  nomRegion?: string;
  zoom: 13;
}

export interface Department {
  nom: string;
  region?: {
    code: string;
    nom: string;
  };
  codeRegion: string;
  type: 'department';
  code: string;
  centre?: {
    type: 'Point';
    coordinates: [number, number];
  };
  zoom: 8;
}

export interface Region {
  nom: string;
  type: 'region';
  code: string;
  centre?: {
    type: 'Point';
    coordinates: [number, number];
  };
  zoom: 6.8;
}

export interface Epci {
  code: string;
  nom: string;
  type: 'epci';
  zoom: 10;
  surface?: number;
  centre?: {
    type: 'Point';
    coordinates: [number, number];
  };
}

export interface TokenObject {
  token: string;
}

export interface PointGeometry {
  type: 'Point';
  coordinates: [number, number];
}

export type GeographicSearchResult = (Region | Department | City | Address)[];
