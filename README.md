# InfraNum

⚠️ Ce projet est désormais obsolète et n'est plus maintenu. Il reste disponible à des fins d'archivage. Veuillez utiliser le nouveau projet "Toutes et Tous Connectés," disponible ici : [Toutes et Tous Connectés](https://gitlab.com/incubateur-territoires/startups/Toutes-et-Tous-Connectes/ttc).

This project was generated using [Nx](https://nx.dev).

## Prerequisites

1. Make sure you have the latest stable version of [Node.js and NPM](https://nodejs.org/en/download/) installed on your computer
2. Install `Nx` globally:
   ```bash
     npm install -g nx
   ```

## How to setup Serverless to work on this project ?

1. Install `serverless` [globally](https://www.serverless.com/framework/docs/getting-started)

   ```bash
   npm install -g serverless
   ```

2. Configure `serverless` with the AWS Credentials of the IAM user, **_sls-infra-num_**, allowed to deploy the app on AWS Lambda

   ```bash
   serverless config credentials \
     --provider aws \
     --key <aws-public-key-for-the-profile> \
     --secret <aws-secret-key-for-the-profile> \
     --profile sls-infra-num
   ```

   It should create a `~/.aws/credentials` file with AWS credentials for this profile.

    <blockquote>
    💡 To get the <span style="font-weight: bold;font-style: italic">real</span> key & secret values for this profile, please contact <a href="mailto:dev-infrastructures-numeriques@anct.gouv.fr">dev-infrastructures-numeriques@anct.gouv.fr</a>
    </blockquote>

## How to develop locally and test on an iPhone
1. [Find out your local IP address](https://www.avg.com/en/signal/find-ip-address), the one that is given to your computer
   **inside** your WIFI network ( _not your **public** IP, which is the one your
   Router assigns to your home when facing the Internet_ )
2. **Update the Back-End to make it work** : Go to `apps/infra-num-api/src/environments/environment.ts` and change the `corsOrigin` property
   to `'http://<your-local-IP>:4200'`
3. **Launch the Back-End** : Tap this in your Terminal, _while being at the MonoRepo's root_:
```shell
nx serve infra-num-api --host 0.0.0.0
```
4. **Update the Front-End to make it work** : Go to `apps/infra-num-front/src/environments/environment.ts` and change the `apiUrl` property
   to `'http://<your-local-IP>:3333/api'`
5. **Launch the Front-End** : Tap this in your Terminal, _while being at the MonoRepo's root_:
```shell
nx serve infra-num-front --host 0.0.0.0
```
6. Connect your iPhone to your computer via USB cable and go to `http://<your-local-IP>:4200` in your
favorite browser
7. If you have a Mac, go to Safari on your computer, then [follow these steps](https://dev.to/pahund/testing-web-applications-running-on-localhost-with-an-iphone-10p3) to be able to
have a Chrome Dev Tools equivalent displayed on your computer and interacting with your phone's browser

<blockquote>
  🚨 Once it's done, remember to revert the changes in the <code>environment.ts</code> files, both in the Front and the Back-End 🚨
</blockquote>

## Adding capabilities to your workspace

Nx supports many plugins which add capabilities for developing different types of applications and different tools.

These capabilities include generating applications, libraries, etc as well as the devtools to test, and build projects as well.

Below are our core plugins:

- [React](https://reactjs.org)
  - `npm install --save-dev @nrwl/react`
- Web (no framework frontends)
  - `npm install --save-dev @nrwl/web`
- [Angular](https://angular.io)
  - `npm install --save-dev @nx/angular`
- [Nest](https://nestjs.com)
  - `npm install --save-dev @nx/nest`
- [Express](https://expressjs.com)
  - `npm install --save-dev @nrwl/express`
- [Node](https://nodejs.org)
  - `npm install --save-dev @nx/node`

There are also many [community plugins](https://nx.dev/community) you could add.

## Generate an application

Run `nx g @nrwl/react:app my-app` to generate an application.

> You can use any of the plugins above to generate applications as well.

When using Nx, you can create multiple applications and libraries in the same workspace.

## Generate a library

Run `nx g @nrwl/react:lib my-lib` to generate a library.

> You can also use any of the plugins above to generate libraries as well.

Libraries are shareable across libraries and applications. They can be imported from `@infra-num/mylib`.

## Development server

Run `nx serve my-app` for a dev server. Navigate to http://localhost:4200/. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `nx g @nrwl/react:component my-component --project=my-app` to generate a new component.

## Build

Run `nx build my-app` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `nx test my-app` to execute the unit tests via [Jest](https://jestjs.io).

Run `nx affected:test` to execute the unit tests affected by a change.

## Running end-to-end tests

Run `nx e2e my-app` to execute the end-to-end tests via [Cypress](https://www.cypress.io).

Run `nx affected:e2e` to execute the end-to-end tests affected by a change.

## Understand your workspace

Run `nx graph` to see a diagram of the dependencies of your projects.

## Further help

Visit the [Nx Documentation](https://nx.dev) to learn more.

## ☁ Nx Cloud

### Distributed Computation Caching & Distributed Task Execution

<p style="text-align: center;"><img src="https://raw.githubusercontent.com/nrwl/nx/master/images/nx-cloud-card.png"></p>

Nx Cloud pairs with Nx in order to enable you to build and test code more rapidly, by up to 10 times. Even teams that are new to Nx can connect to Nx Cloud and start saving time instantly.

Teams using Nx gain the advantage of building full-stack applications with their preferred framework alongside Nx’s advanced code generation and project dependency graph, plus a unified experience for both frontend and backend developers.

Visit [Nx Cloud](https://nx.app/) to learn more.
