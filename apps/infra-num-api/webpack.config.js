const { composePlugins, withNx } = require('@nx/webpack');
const { resolve } = require('path');

// Nx plugins for webpack
module.exports = composePlugins(withNx(), (config) => {
  // Update the webpack config as needed here
  config = {
    ...config,
    ignoreWarnings: [
      /source-map-loader/,
      /Module not found/,
      /Critical dependency/
    ],
    output: {
      path: resolve(process.cwd(), 'dist/apps/infra-num-api'),
      filename: '[name].js',
      library: {
        type: 'commonjs2'
      }
    }
  };

  return config;
});
