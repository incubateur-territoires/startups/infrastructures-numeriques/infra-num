/**
 * 3rd-party imports
 */
import type { IamRole, Serverless } from 'serverless/aws';

/**
 * TypeScript entities and constants
 */
export const AWS_REGION: string = 'eu-west-3';

const awsSesIamRole: IamRole = {
  statements: [
    {
      Effect: 'Allow',
      Action: ['ses:sendEmail', 'ses:sendRawEmail'],
      Resource: '*',
      Condition: {
        StringEquals: {'ses:ApiVersion': '2'}
      }
    }
  ]
};

const serverlessConfiguration: Serverless = {
  provider: {
    name: 'aws',
    region: AWS_REGION,
    profile: 'sls-infra-num',
    runtime: 'nodejs18.x',
    timeout: 30,
    architecture: 'arm64',
    iam: {
      role: awsSesIamRole
    }
  },
  service: 'infra-num-api',
  custom: {
    dev: {
      layerName: 'mongoDBCerts',
      dbEnvVars: {
        user: process.env.MONGODB_INFRANUM_ROOT_USER,
        password: process.env.MONGODB_INFRANUM_ROOT_PASSWORD_URLENCODED,
        host: process.env.MONGODB_INFRANUM_HOST,
        caFile: process.env.MONGODB_INFRANUM_TLS_CA_FILE_AWS,
        certFile: process.env.MONGODB_INFRANUM_TLS_CERT_FILE_AWS
      },
      jwtSecret: process.env.INFRANUM_JWT_SECRET,
      listmonkVars: {
        username: process.env.LISTMONK_INFRANUM_USERNAME,
        password: process.env.LISTMONK_INFRANUM_PASSWORD
      },
      gristApiVars: {
        bearerAuthorization: process.env.GRIST_INFRANUM_AUTHORIZATION_BEARER,
        contactsDocId: process.env.GRIST_INFRANUM_CONTACTS_DOC_ID,
        authWebhook: process.env.GRIST_INFRANUM_AUTH_WEBHOOK
      },
      apiKey: process.env.INFRANUM_API_KEY,
      technicalEmail: process.env.INFRANUM_TECHNICAL_EMAIL
    },
    prod: {
      layerName: 'mongoDBCertsProd',
      dbEnvVars: {
        user: process.env.MONGODB_INFRANUM_PROD_ROOT_USER,
        password: process.env.MONGODB_INFRANUM_PROD_ROOT_PASSWORD_URLENCODED,
        host: process.env.MONGODB_INFRANUM_PROD_HOST,
        caFile: process.env.MONGODB_INFRANUM_PROD_TLS_CA_FILE_AWS,
        certFile: process.env.MONGODB_INFRANUM_PROD_TLS_CERT_FILE_AWS
      },
      jwtSecret: process.env.INFRANUM_PROD_JWT_SECRET,
      listmonkVars: {
        username: process.env.LISTMONK_INFRANUM_PROD_USERNAME,
        password: process.env.LISTMONK_INFRANUM_PROD_PASSWORD
      },
      gristApiVars: {
        bearerAuthorization: process.env.GRIST_INFRANUM_AUTHORIZATION_BEARER,
        contactsDocId: process.env.GRIST_INFRANUM_CONTACTS_DOC_ID,
        authWebhook: process.env.GRIST_INFRANUM_AUTH_WEBHOOK
      },
      apiKey: process.env.INFRANUM_PROD_API_KEY,
      technicalEmail: process.env.INFRANUM_TECHNICAL_EMAIL
    }
  },
  functions: {
    main: {
      handler: 'dist/apps/infra-num-api/main.handler',
      events: [
        {
          http: {
            method: 'ANY',
            path: '/'
          }
        },
        {
          http: {
            method: 'ANY',
            path: '{proxy+}'
          }
        }
      ],
      layers: [
        "arn:aws:lambda:eu-west-3:608649312311:layer:${self:custom.${opt:stage, 'dev'}.layerName}:1"
      ],
      environment: {
        MONGODB_INFRANUM_ROOT_USER:
          "${self:custom.${opt:stage, 'dev'}.dbEnvVars.user}",
        MONGODB_INFRANUM_ROOT_PASSWORD_URLENCODED:
          "${self:custom.${opt:stage, 'dev'}.dbEnvVars.password}",
        MONGODB_INFRANUM_HOST:
          "${self:custom.${opt:stage, 'dev'}.dbEnvVars.host}",
        MONGODB_INFRANUM_TLS_CA_FILE:
          "${self:custom.${opt:stage, 'dev'}.dbEnvVars.caFile}",
        MONGODB_INFRANUM_TLS_CERT_FILE:
          "${self:custom.${opt:stage, 'dev'}.dbEnvVars.certFile}",
        INFRANUM_JWT_SECRET: "${self:custom.${opt:stage, 'dev'}.jwtSecret}",
        LISTMONK_INFRANUM_USERNAME: "${self:custom.${opt:stage, 'dev'}.listmonkVars.username}",
        LISTMONK_INFRANUM_PASSWORD: "${self:custom.${opt:stage, 'dev'}.listmonkVars.password}",
        GRIST_INFRANUM_AUTHORIZATION_BEARER: "${self:custom.${opt:stage, 'dev'}.gristApiVars.bearerAuthorization}",
        GRIST_INFRANUM_CONTACTS_DOC_ID: "${self:custom.${opt:stage, 'dev'}.gristApiVars.contactsDocId}",
        GRIST_INFRANUM_AUTH_WEBHOOK: "${self:custom.${opt:stage, 'dev'}.gristApiVars.authWebhook}",
        INFRANUM_API_KEY: "${self:custom.${opt:stage, 'dev'}.apiKey}",
        INFRANUM_TECHNICAL_EMAIL:
          "${self:custom.${opt:stage, 'dev'}.technicalEmail}"
      }
    }
  },
  plugins: ['serverless-offline']
};

module.exports = serverlessConfiguration;
