/* eslint-disable */
export default {
  displayName: 'infra-num-api',
  preset: '../../jest.preset.js',
  globals: {},
  testEnvironment: 'node',
  testMatch: [
    '**/__tests__/**/*.[jt]s?(x)',
    '**/?(*.)+(spec|test|e2e).[jt]s?(x)'
  ],
  transform: {
    '^.+\\.[tj]s$': [
      'ts-jest',
      {
        tsconfig: '<rootDir>/tsconfig.spec.json'
      }
    ]
  },
  moduleFileExtensions: ['ts', 'js', 'html'],
  coverageDirectory: '../../coverage/apps/infra-num-api',
  coveragePathIgnorePatterns: ['.schema.ts'],
  clearMocks: true,
  resetMocks: true
};
