/**
 * Nest.js imports
 */
import { NestApplication, NestFactory } from '@nestjs/core';

/**
 * 3rd-party imports
 */
import { Callback, Context, Handler } from 'aws-lambda';
import serverlessExpress from '@vendia/serverless-express';
import cookieParser from 'cookie-parser';

/**
 * Internal imports
 */
import { AppModule } from './app/app.module';

let cachedServer: Handler;

async function bootstrap(): Promise<Handler> {
  const nestApp: NestApplication = await NestFactory.create(AppModule);
  nestApp.use(cookieParser());
  await nestApp.init();

  cachedServer = serverlessExpress({
    app: nestApp.getHttpAdapter().getInstance()
  });

  return cachedServer;
}

export const handler: Handler = async (
  event: unknown,
  context: Context,
  callback: Callback
) => {
  const server: Handler = cachedServer ?? (await bootstrap());

  return server(event, context, callback);
};
