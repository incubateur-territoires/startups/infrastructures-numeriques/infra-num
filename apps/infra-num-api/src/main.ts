/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

/**
 * Nest.js imports
 */
import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';

/**
 * 3rd-party imports
 */
import cookieParser from 'cookie-parser';
/**
 * Internal imports
 */
import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    cors: {
      origin: environment.corsOrigin,
      credentials: true
    }
  });
  const globalPrefix = 'api';
  app.setGlobalPrefix(globalPrefix);
  app.use(cookieParser());
  const port = process.env.PORT || 3333;
  await app.listen(port);
  Logger.log(
    `🚀 Application is running on: http://localhost:${port}/${globalPrefix}`
  );
}

bootstrap();
