/**
 * Nest.js imports
 */
import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios'

/**
 * Internal imports
 */
import { ServicePublicApiController } from './service-public-api.controller';
import { ServicePublicApiService } from './service-public-api.service';

@Module({
  imports: [HttpModule],
  controllers: [ServicePublicApiController],
  providers: [ServicePublicApiService]
})
export class ServicePublicApiModule { }
