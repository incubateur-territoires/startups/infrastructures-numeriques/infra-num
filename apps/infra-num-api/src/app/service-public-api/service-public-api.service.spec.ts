/**
 * Nest.js imports
 */
import { HttpService } from '@nestjs/axios';
import { Test, TestingModule } from '@nestjs/testing';
import { BadRequestException } from '@nestjs/common';


/**
 * 3rd-party imports
 */
import { ServicePublicApiProperties } from '@infra-num/common';
import { of } from 'rxjs';

/**
 * Internal imports
 */
import { INSEE_CODE_FORMAT_ERROR, INSEE_CODE_NOT_FOUND, ServicePublicApiService } from './service-public-api.service';
import {
  CITY_HALL_INFORMATION_EMPTY,
  CITY_HALL_INFORMATION_NORMAL_CITY_HALL,
  CITY_HALL_INFORMATION_DELEGATED_CITY_HALL
} from './tests/city-hall-already-saved.mock';


class HttpServiceMock {
  get: jest.Mock = jest.fn();
}

describe('ServicePublicApiService', () => {
  let servicePublicApiService: ServicePublicApiService;
  let httpService: HttpServiceMock;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ServicePublicApiService,
        {
          provide: HttpService,
          useClass: HttpServiceMock
        },
      ]
    }).compile();

    servicePublicApiService = module.get<ServicePublicApiService>(ServicePublicApiService);
    httpService = module.get<HttpServiceMock>(HttpService);
  });

  it('should be defined', () => {
    expect(servicePublicApiService).toBeDefined();
  });

  it('should return  a ServicePublicApiProperties object when everything goes right', async () => {
    const response = {
      data: CITY_HALL_INFORMATION_NORMAL_CITY_HALL,
      headers: {},
      config: { url: 'http://localhost:3333/api' },
      status: 200,
      statusText: 'OK',
    };

    httpService.get.mockReturnValue(of(response));

    const cityHallInformation = await servicePublicApiService.getCityHallInformation('87131');
    const isServicePublicApiProperties = (data: ServicePublicApiProperties): data is ServicePublicApiProperties => {
      return (<ServicePublicApiProperties>data).id !== undefined;
    };

    expect(httpService.get).toHaveBeenCalled();
    expect(isServicePublicApiProperties(cityHallInformation)).toBeTruthy();
  });

  it('should not return anything when asking for a delegated city hall', async () => {
    const response = {
      data: CITY_HALL_INFORMATION_DELEGATED_CITY_HALL,
      headers: {},
      config: { url: 'http://localhost:3333/api' },
      status: 200,
      statusText: 'OK',
    };

    httpService.get.mockReturnValue(of(response));
    const cityHallInformation = await servicePublicApiService.getCityHallInformation('01033');

    expect(httpService.get).toHaveBeenCalled();
    expect(cityHallInformation).toBeUndefined();
  });

  it('should throw an error if the insee code is not a string of 5 numbers', async () => {
    try {
      await servicePublicApiService.getCityHallInformation('test');
    } catch (error) {
      expect(error).toEqual(new BadRequestException(INSEE_CODE_FORMAT_ERROR));
    }
  });

  it('should throw an error if the insee code does not exist', async () => {
    const response = {
      data: CITY_HALL_INFORMATION_EMPTY,
      headers: {},
      config: { url: 'http://localhost:3333/api' },
      status: 200,
      statusText: 'OK',
    };

    httpService.get.mockReturnValue(of(response));

    try {
      await servicePublicApiService.getCityHallInformation('10000');
    } catch (error) {
      expect(error).toEqual(new BadRequestException(INSEE_CODE_NOT_FOUND));
    }
  });
});
