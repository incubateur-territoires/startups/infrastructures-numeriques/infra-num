/**
 * Nest.js imports
 */
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';

/**
 * 3rd-party imports
 */
import request from 'supertest';

/**
 * Internal imports
 */
import { ServicePublicApiService } from '../service-public-api.service';
import { ServicePublicApiModule } from '../service-public-api.module';
import { ServicePublicApiServiceMock } from './service-public-api.service.mock';
import { CITY_HALL_INFORMATION_ALREADY_SAVED } from './city-hall-already-saved.mock';

describe('ServicePublicApi', () => {
  let app: INestApplication;
  let servicePublicService: ServicePublicApiServiceMock;

  beforeAll(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [ServicePublicApiModule]
    })
      .overrideProvider(ServicePublicApiService)
      .useValue(ServicePublicApiServiceMock)
      .compile();

    servicePublicService = moduleRef.get<ServicePublicApiServiceMock>(ServicePublicApiService);
    app = moduleRef.createNestApplication();
    await app.init();
  });

  it('/GET /city-hall/10002', () => {
    const cityHallInformationMock = CITY_HALL_INFORMATION_ALREADY_SAVED.features[0].properties;
    servicePublicService.getCityHallInformation = jest.fn().mockResolvedValue(cityHallInformationMock);

    return request(app.getHttpServer())
      .get('/city-hall/10002')
      .expect(200)
      .expect(cityHallInformationMock);
  });

  afterAll(async () => {
    await app.close();
  });
});
