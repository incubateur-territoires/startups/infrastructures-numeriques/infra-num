/**
 * 3rd-party imports
 */
import { ServicePublicApiResult } from '@infra-num/common';

/**
 * TypeScript entities and constants
 */
export const CITY_HALL_INFORMATION_EMPTY: ServicePublicApiResult = {
  total_count: 0,
  results: []
};

export const CITY_HALL_INFORMATION_NORMAL_CITY_HALL: ServicePublicApiResult = {
  total_count: 1,
  results: [{
    plage_ouverture: '[{"nom_jour_debut": "Lundi", "nom_jour_fin": "Lundi", "valeur_heure_debut_1": "09:00:00", "valeur_heure_fin_1": "12:00:00", "valeur_heure_debut_2": "", "valeur_heure_fin_2": "", "commentaire": ""}, {"nom_jour_debut": "Mardi", "nom_jour_fin": "Vendredi", "valeur_heure_debut_1": "08:30:00", "valeur_heure_fin_1": "12:00:00", "valeur_heure_debut_2": "13:30:00", "valeur_heure_fin_2": "18:00:00", "commentaire": ""}, {"nom_jour_debut": "Samedi", "nom_jour_fin": "Samedi", "valeur_heure_debut_1": "09:00:00", "valeur_heure_fin_1": "12:00:00", "valeur_heure_debut_2": "", "valeur_heure_fin_2": "", "commentaire": ""}]',
    site_internet: null,
    copyright: "Direction de l'information légale et administrative (Premier ministre)",
    siren: null,
    ancien_code_pivot: "mairie-87131-01",
    reseau_social: null,
    texte_reference: null,
    partenaire: null,
    telecopie: "05 55 03 49 75",
    nom: "Mairie - Saillat-sur-Vienne",
    siret: "21871310500011",
    itm_identifiant: "424486",
    sigle: null,
    affectation_personne: null,
    date_modification: new Date(),
    adresse_courriel: "mairie@saillat.fr",
    service_disponible: null,
    organigramme: null,
    pivot: '[{"type_service_local": "mairie", "code_insee_commune": ["87131"]}]',
    partenaire_identifiant: null,
    ancien_identifiant: null,
    id: "9ce09d50-5ad4-4243-aef0-b8ace2f4864e",
    ancien_nom: null,
    commentaire_plage_ouverture: null,
    annuaire: null,
    tchat: null,
    hierarchie: null,
    categorie: "SL",
    sve: null,
    telephone_accessible: null,
    application_mobile: null,
    version_type: "Publiable",
    type_repertoire: null,
    telephone: '[{"valeur": "05 55 03 41 82", "description": ""}]',
    version_etat_modification: null,
    date_creation: "18/01/2012 00:00:00",
    partenaire_date_modification: null,
    mission: null,
    formulaire_contact: null,
    version_source: null,
    type_organisme: null,
    code_insee_commune: "87131",
    statut_de_diffusion: "true",
    adresse: '[{"type_adresse": "Adresse", "complement1": "", "complement2": "", "numero_voie": "1 place de la Mairie", "service_distribution": "", "code_postal": "87720", "nom_commune": "Saillat-sur-Vienne", "pays": "", "continent": "", "longitude": "0.818121016026", "latitude": "45.8730125427", "accessibilite": "ACC", "note_accessibilite": ""}]',
    url_service_public: "https://lannuaire.service-public.fr/nouvelle-aquitaine/haute-vienne/9ce09d50-5ad4-4243-aef0-b8ace2f4864e",
    information_complementaire: null,
    date_diffusion: null
  }]
};

export const CITY_HALL_INFORMATION_DELEGATED_CITY_HALL: ServicePublicApiResult = {
  total_count: 1,
  results: [{
    plage_ouverture: '[{"nom_jour_debut": "Mercredi", "nom_jour_fin": "Mercredi", "valeur_heure_debut_1": "08:30:00", "valeur_heure_fin_1": "12:00:00", "valeur_heure_debut_2": "", "valeur_heure_fin_2": "", "commentaire": ""}]',
    site_internet: '[{"libelle": "", "valeur": "http://www.valserhone.fr"}]',
    copyright: 'Direction de l\'information légale et administrative (Premier ministre)',
    siren: null,
    ancien_code_pivot: null,
    reseau_social: null,
    texte_reference: null,
    partenaire: null,
    telecopie: null,
    nom: "Mairie déléguée - Lancrans",
    siret: null,
    itm_identifiant: null,
    sigle: null,
    affectation_personne: null,
    date_modification: new Date(),
    adresse_courriel: "mairie@valserhone.fr",
    service_disponible: null,
    organigramme: null,
    pivot: '[{"type_service_local": "mairie", "code_insee_commune": ["01033"]}]',
    partenaire_identifiant: null,
    ancien_identifiant: null,
    id: "5362da9b-1ee9-48ab-9f7a-2798a56be976",
    ancien_nom: null,
    commentaire_plage_ouverture: null,
    annuaire: null,
    tchat: null,
    hierarchie: null,
    categorie: "SL",
    sve: null,
    telephone_accessible: null,
    application_mobile: null,
    version_type: "Publiable",
    type_repertoire: null,
    telephone: '[{"valeur": "04 50 48 15 88", "description": ""}]',
    version_etat_modification: null,
    date_creation: "08/06/2023 12:05:26",
    partenaire_date_modification: null,
    mission: null,
    formulaire_contact: null,
    version_source: null,
    type_organisme: null,
    code_insee_commune: "01033",
    statut_de_diffusion: "true",
    adresse: '[{"type_adresse": "Adresse", "complement1": "", "complement2": "", "numero_voie": "25 Grande-rue", "service_distribution": "", "code_postal": "01200", "nom_commune": "Valserhône", "pays": "", "continent": "", "longitude": "5.833761", "latitude": "46.124801", "accessibilite": "", "note_accessibilite": ""}]',
    url_service_public: "https://lannuaire.service-public.fr/auvergne-rhone-alpes/ain/5362da9b-1ee9-48ab-9f7a-2798a56be976",
    information_complementaire: null,
    date_diffusion: null
  }]
};
