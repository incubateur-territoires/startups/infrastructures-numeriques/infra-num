/**
 * Nest.js imports
 */
import { Controller, Get, Param } from '@nestjs/common';

/**
 * Internal imports
 */
import { ServicePublicApiService } from './service-public-api.service';

/**
 * 3rd-party imports
 */
import { ServicePublicApiProperties } from '@infra-num/common';

@Controller()
export class ServicePublicApiController {

  constructor(private readonly servicePublicApiService: ServicePublicApiService) { }

  @Get('/epci/:siren')
  async getEpciInformation(@Param('siren') siren: string): Promise<ServicePublicApiProperties> {
    return await this.servicePublicApiService.getEpciInformation(siren);
  }

  @Get('/city-hall/:codeInsee')
  async getCityHallInformation(@Param('codeInsee') codeInsee: string): Promise<ServicePublicApiProperties> {
    return await this.servicePublicApiService.getCityHallInformation(codeInsee);
  }
}
