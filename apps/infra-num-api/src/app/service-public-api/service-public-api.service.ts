/**
 * Nest.js imports
 */
import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';

/**
 * 3rd-party imports
 */
import {
  ServicePublicApiResult,
  ServicePublicApiProperties
} from '@infra-num/common';
import { catchError, firstValueFrom, tap } from 'rxjs';
import { AxiosError } from 'axios';

export const INSEE_CODE_FORMAT_ERROR: string =
  'Insee code must be a string of 5 numbers';
export const INSEE_CODE_NOT_FOUND: string =
  "Insee code not found. Don't confuse them with zip codes";
export const SIREN_CODE_NOT_FOUND: string =
  "Siren code not found. Don't confuse them with siret or INSEE codes";

@Injectable()
export class ServicePublicApiService {
  private readonly logger = new Logger(ServicePublicApiService.name);
  private servicePublicApiUrl: string =
    'https://api-lannuaire.service-public.fr/api/explore/v2.1/catalog/datasets/api-lannuaire-administration/records';

  constructor(private readonly httpService: HttpService) {}

  private getInformation(
    requestParams: string,
    notFoundError: string
  ): Promise<{ data: ServicePublicApiResult }> {
    return firstValueFrom(
      this.httpService
        .get<ServicePublicApiResult>(
          `${this.servicePublicApiUrl}?where=${requestParams}`
        )
        .pipe(
          tap((response) => {
            if (!response.data.total_count) {
              throw new BadRequestException(notFoundError);
            }
            return response;
          }),
          catchError((error: AxiosError) => {
            this.logger.error(error.response.data);
            throw error;
          })
        )
    );
  }

  async getEpciInformation(siren: string): Promise<ServicePublicApiProperties> {
    const encodedParamsURI = encodeURIComponent(
      `siren LIKE "${siren}" AND (pivot LIKE "epci")`
    );
    const { data } = await this.getInformation(
      encodedParamsURI,
      SIREN_CODE_NOT_FOUND
    );

    return data.results[0];
  }


  async getCityHallInformation(
    codeInsee: string
  ): Promise<ServicePublicApiProperties> {
    if (codeInsee.length !== 5) {
      throw new BadRequestException(INSEE_CODE_FORMAT_ERROR);
    }

    const encodedParamsURI = encodeURIComponent(`code_insee_commune LIKE "${codeInsee}" AND (pivot LIKE "mairie")`);
    const { data } = await this.getInformation(
      encodedParamsURI,
      INSEE_CODE_NOT_FOUND
    );


    /*
     * Some villages are regrouped administratively under one city.
     * That's why we might have several features for a specific value
     * of codeInsee.
     *
     * Here, we're discriminating with this rule "not being a delegated
     * city hall".
     */
    return data.results.filter((cityHall) => {
      return !cityHall.nom.includes('Mairie déléguée');
    })[0];
  }
}
