/**
 * Nest.js imports
 */
import { Test, TestingModule } from '@nestjs/testing';

/**
 * Internal imports
 */
import { ServicePublicApiController } from './service-public-api.controller';
import { ServicePublicApiService } from './service-public-api.service';
import { ServicePublicApiServiceMock } from './tests/service-public-api.service.mock';
import { CITY_HALL_INFORMATION_NORMAL_CITY_HALL } from './tests/city-hall-already-saved.mock';

describe('ServicePublicApiController', () => {
  let controller: ServicePublicApiController;
  let servicePublicService: ServicePublicApiServiceMock;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ServicePublicApiController],
      providers: [{
        provide: ServicePublicApiService,
        useClass: ServicePublicApiServiceMock
      }]
    }).compile();

    controller = module.get<ServicePublicApiController>(ServicePublicApiController);
    servicePublicService = module.get<ServicePublicApiServiceMock>(ServicePublicApiService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should return an object with city hall information', async () => {
    const cityHallInformationMock = CITY_HALL_INFORMATION_NORMAL_CITY_HALL.results[0];
    servicePublicService.getCityHallInformation = jest.fn().mockResolvedValue(cityHallInformationMock);
    const cityHallInformation = await controller.getCityHallInformation('87131');
    expect(servicePublicService.getCityHallInformation).toHaveBeenCalled();
    expect(cityHallInformation).toEqual(cityHallInformationMock);
  });
});
