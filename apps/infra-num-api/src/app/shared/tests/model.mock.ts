/**
 * 3rd-party imports
 */
import { Types } from 'mongoose';

export class ModelMock {
  _id: Types.ObjectId;

  static find: jest.Mock = jest.fn();
  static findOne: jest.Mock = jest.fn();

  constructor(docDefinition: Record<string, any>) {
    for (const docDefinitionKey in docDefinition) {
      this[docDefinitionKey as string] = docDefinition[docDefinitionKey];
    }
    this._id = new Types.ObjectId(1234);
  }

  async save(): Promise<typeof this> {
    return this;
  }
}
