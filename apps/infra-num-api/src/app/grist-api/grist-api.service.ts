/**
 * Nest.js imports
 */
import { BadRequestException, forwardRef, Inject, Injectable, Logger } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';

/**
 * 3rd-party imports
 */
import { catchError, firstValueFrom } from 'rxjs';
import { AxiosError } from 'axios';
import {
  JurisdictionType,
  Role,
  GristContactFields,
  GristJurisdiction,
  GristRole,
  AllowedAccount
} from '@infra-num/common';
import { Types } from 'mongoose';

/**
 * Internal imports
 */
import { AllowedAccountsService } from '../users/allowed-accounts/allowed-accounts.service';
import { UsersService } from '../users/users.service';
import { UserWithId } from '../users/user.schema';
import { JurisdictionsService } from '../users/jurisdictions/jurisdictions.service';
import { JurisdictionWithId } from '../users/jurisdictions/jurisdiction.schema';
import { DEPARTMENTS } from '../users/jurisdictions/data/departements';
import { REGIONS } from '../users/jurisdictions/data/regions';
import { TERRITORIES } from '../users/jurisdictions/data/territoires';

/**
 * TypeScript entities and constants
 */
const EMAIL_REGEX: RegExp = new RegExp(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, 'gi');

/**
 * We use this class to handle a bidirectional communication
 * between this API and Grist API
 * @link Doc Grist API : https://support.getgrist.com/api/#tag/records
 */
@Injectable()
export class GristApiService {

  private readonly logger = new Logger(GristApiService.name);
  private gristApiUrl: string = `https://grist.incubateur.anct.gouv.fr/api/docs/${process.env.GRIST_INFRANUM_CONTACTS_DOC_ID}`;
  private gristHeaders = {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${process.env.GRIST_INFRANUM_AUTHORIZATION_BEARER}`,
    }
  };

  constructor(
    private readonly httpService: HttpService,
    private readonly allowedAccountsService: AllowedAccountsService,
    @Inject(forwardRef(() => UsersService)) private readonly usersService: UsersService,
    private readonly jurisdictionsService: JurisdictionsService
  ) { }

  /**
   * This method is used to communicate in this direction :
   * this API => Grist API
   * It enables us to keep Grist aware of when a Contact has actually
   * created their account on the app
   */
  async updateGristContactAccountCreatedField(email: string): Promise<void> {
    const records = {
      "records" : [
        {
          require: {
            "Adresse_courriel" : email,
          },
          fields: {
            "Adresse_courriel" : email,
            "Compte_cree": true
          }
        }
      ]
    };

    await firstValueFrom(
      this.httpService
        .put(
          `${this.gristApiUrl}/tables/Contact/records`,
          records,
          this.gristHeaders
        ).pipe(
        catchError((error: AxiosError) => {
          this.logger.error(error.response.data);
          throw error;
        })
      )
    );

    return;
  }

  async updateContacts(contactsList: GristContactFields[]): Promise<void> {

    if (
      !contactsList ||
      !contactsList.length ||
      contactsList[0].authWebhook !== process.env.GRIST_INFRANUM_AUTH_WEBHOOK
    ) {
      /*
       * We do not throw an error here, otherwise Grist will keep this
       * webhook attempt in its queue and retry it
       */
      await this.deleteWebhookQueues();

      return;
    }

    for (const contact of contactsList) {
      if (
        contact.Adresse_courriel.length &&
        contact.Adresse_courriel.match(EMAIL_REGEX)
      ) {
        const existingAllowedAccount: AllowedAccount = await this.allowedAccountsService.findOneWithoutIsActive(contact.Adresse_courriel.toLowerCase());
        const existingUser: UserWithId = await this.usersService.findOne(contact.Adresse_courriel.toLowerCase());

        const currentContactJurisdictions: GristJurisdiction[] = await this.getGristJurisdictionsById(contact.Juridiction);

        let fullName: string = ``;
        if (contact.Prenom) {
          fullName += `${contact.Prenom}`;
        }
        if (contact.Nom) {
          fullName += ` ${contact.Nom.toUpperCase()}`;
        }

        const contactToCreateOrUpdate: AllowedAccount = {
          email: contact.Adresse_courriel.toLowerCase(),
          roles: await this.getRolesByGristContact(contact),
          isActive: contact.Actif,
          pointOfContact: contact.Referent_principal,
          fullName: fullName.length ? fullName : undefined,
          jurisdictions: await this.createAndOrGetJurisdictions(currentContactJurisdictions)
        }

        if (existingAllowedAccount) {
          await this.allowedAccountsService.updateOne(contactToCreateOrUpdate);
        }

        if (existingUser) {
          await this.usersService.updateOne(contactToCreateOrUpdate);
        }

        if (!existingAllowedAccount && !existingUser) {
          await this.allowedAccountsService.create(contactToCreateOrUpdate);
        }
      }
    }

    await this.deleteWebhookQueues();

    return;
  }

  private async getGristJurisdictionsById(jurisdictionsIds: (number | string)[]): Promise<GristJurisdiction[]> {

    if (!jurisdictionsIds || !jurisdictionsIds.length) {
      throw new BadRequestException();
    }

    let jurisdictionsIdsStringified: string = '';
    jurisdictionsIds = jurisdictionsIds.filter(id => typeof id === 'number');
    jurisdictionsIds.forEach((jurisdictionId: number, index: number) => {
      jurisdictionsIdsStringified += (index === jurisdictionsIds.length - 1) ? `${jurisdictionId}` : `${jurisdictionId},`;
    });

    const jurisdictionsFilterIdEncoded = encodeURIComponent(`{"id":[${jurisdictionsIdsStringified}]}`);
    const { data } = await firstValueFrom(
      this.httpService
        .get(
          `${this.gristApiUrl}/tables/Regions_departements/records?filter=${jurisdictionsFilterIdEncoded}`,
          this.gristHeaders
        )
    );

    return data.records;
  }

  private async getRolesByGristContact(gristContact: GristContactFields): Promise<Role[]> {
    const roles: Role[] = [];
    const gristRolesIds  = gristContact.Role.filter(id => typeof id === 'number');
    let roleIdsStringified: string = '';
    gristRolesIds.forEach((roleId: number, index: number) => {
      roleIdsStringified += (index === gristRolesIds.length - 1) ? `${roleId}` : `${roleId},`;
    });
    const rolesFilterEncoded =  encodeURIComponent(`{"id":[${roleIdsStringified}]}`);
    const { data } = await firstValueFrom(
      this.httpService
        .get(
          `${this.gristApiUrl}/tables/Roles/records?filter=${rolesFilterEncoded}`,
          this.gristHeaders
        )
    );
    data.records.forEach((gristRole: GristRole) => roles.push(gristRole.fields.Name));

    return roles;
  }

  private async createAndOrGetJurisdictions(gristJurisdictions: GristJurisdiction[]): Promise<Types.ObjectId[]> {
    const jurisdictions: Types.ObjectId[] = [];

      for(const gristJurisdiction of gristJurisdictions) {
        let currentJurisdiction: JurisdictionWithId = await this.jurisdictionsService.findOne(gristJurisdiction.fields.lib_groupement.trim());

        if (!currentJurisdiction) {
          currentJurisdiction = await this.jurisdictionsService.create({
            type: this.formatJurisdictionType(gristJurisdiction),
            name: gristJurisdiction.fields.lib_groupement.trim(),
            referenceCoordinates: this.getJurisdictionReferenceCoordinates(gristJurisdiction),
            initialZoom: this.setInitialZoomByJurisdiction(gristJurisdiction)
          });
        }

        jurisdictions.push(currentJurisdiction._id);
    }

    return jurisdictions;
  }

  private formatJurisdictionType(gristJurisdiction: GristJurisdiction): JurisdictionType {
    if (!gristJurisdiction) {
      return;
    }

    switch(gristJurisdiction.fields.echelon_geo) {
      case '' :
        return JurisdictionType.France;
      case 'region' :
        return JurisdictionType.Region;
      case 'departement' :
        return JurisdictionType.Department;
      case 'territoire' :
        return JurisdictionType.Territory;
    }
  }

  private getJurisdictionReferenceCoordinates(gristJurisdiction: GristJurisdiction): [number, number] {
    let currentJurisdictionCoordinates : [number, number];

    if (gristJurisdiction.fields.echelon_geo === 'departement') {
      currentJurisdictionCoordinates = DEPARTMENTS.filter(
        (department: any) =>
          department.dep_name === gristJurisdiction.fields.lib_groupement.trim())[0].coordinates as [number, number];
    }

    if (gristJurisdiction.fields.echelon_geo === 'region') {
      currentJurisdictionCoordinates = REGIONS.filter(
        (region: any) =>
          region.nom === gristJurisdiction.fields.lib_groupement.trim())[0].coordinates as [number, number];
    }

    if (gristJurisdiction.fields.echelon_geo === 'territoire') {
      currentJurisdictionCoordinates = TERRITORIES.filter(
        (territory: any) =>
          territory.nom === gristJurisdiction.fields.lib_groupement.trim())[0].coordinates as [number, number];
    }

    return currentJurisdictionCoordinates;
  }

  private setInitialZoomByJurisdiction(gristJurisdiction: GristJurisdiction): number {
    if (!gristJurisdiction) {
      return;
    }

    switch(gristJurisdiction.fields.echelon_geo) {
      case 'region' :
        return 7;
      case 'departement' :
      case 'territoire' :
        return 9;
    }
  }

  private async deleteWebhookQueues(): Promise<null> {
    const { data } = await firstValueFrom(
      this.httpService.delete(`${this.gristApiUrl}/webhooks/queue`, this.gristHeaders)
        .pipe(
          catchError((error: AxiosError) => {
            this.logger.error(error.response.data);
            throw error;
          })
        )
    );
    return data;
  }

}
