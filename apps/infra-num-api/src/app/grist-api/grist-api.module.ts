/**
 * Nest.js imports
 */
import { forwardRef, Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';

/**
 * Internal imports
 */
import { GristApiService } from './grist-api.service';
import { GristApiController } from './grist-api.controller';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [
    HttpModule,
    forwardRef(() => UsersModule),
  ],
  controllers: [GristApiController],
  providers: [GristApiService],
  exports: [GristApiService]
})
export class GristApiModule {}
