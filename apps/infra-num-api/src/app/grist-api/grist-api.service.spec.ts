/**
 * Nest.js imports
 */
import { Test, TestingModule } from '@nestjs/testing';
import { HttpService } from '@nestjs/axios';

/**
 * Internal imports
 */
import { GristApiService } from './grist-api.service';
import { JurisdictionsService } from '../users/jurisdictions/jurisdictions.service';
import { UsersService } from '../users/users.service';
import { AllowedAccountsService } from '../users/allowed-accounts/allowed-accounts.service';
import { JurisdictionsServiceMock } from '../users/tests/jurisdictions-service.mock';
import { AllowedAccountsServiceMock } from '../users/tests/allowed-accounts.service.mock';
import { UsersServiceMock } from '../users/tests/users-service.mock';

/**
 * Creating mocks
 */
class HttpServiceMock {
  get: jest.Mock = jest.fn();
}

describe('GristApiService', () => {
  let gristApiService: GristApiService;
  let httpService: HttpServiceMock;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      providers: [
        GristApiService,
        {
          provide: UsersService,
          useClass: UsersServiceMock
        },
        {
          provide: JurisdictionsService,
          useClass: JurisdictionsServiceMock
        },
        {
          provide: AllowedAccountsService,
          useClass: AllowedAccountsServiceMock
        },
        {
          provide: HttpService,
          useClass: HttpServiceMock
        }
      ]
    }).compile();

    gristApiService = module.get<GristApiService>(GristApiService);
    httpService = module.get<HttpServiceMock>(HttpService);
  });

  it('should be defined', () => {
    expect(GristApiService).toBeDefined();
  });
  
});
