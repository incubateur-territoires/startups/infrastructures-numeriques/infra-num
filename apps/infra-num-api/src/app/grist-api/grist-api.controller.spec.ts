/**
 * Nest.js imports
 */
import { Test, TestingModule } from '@nestjs/testing';

/**
 * Internal imports
 */
import { GristApiController } from './grist-api.controller';
import { GristApiService } from './grist-api.service';
import { GristApiServiceMock } from './tests/grist-api.service.mock';

describe('GristApiController', () => {
  let controller: GristApiController;
  let gristApiService: GristApiServiceMock;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GristApiController],
      providers: [
        {
          provide: GristApiService,
          useClass: GristApiServiceMock
        }
      ]
    }).compile();

    controller = module.get<GristApiController>(GristApiController);
    gristApiService = module.get<GristApiServiceMock>(GristApiService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
