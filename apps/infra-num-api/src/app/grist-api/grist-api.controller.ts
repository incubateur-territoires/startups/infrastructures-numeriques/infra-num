/**
 * Nest.js imports
 */
import { Controller, Post, Body } from '@nestjs/common';

/**
 * Internal imports
 */
import { GristApiService } from './grist-api.service';

/**
 * 3rd-party imports
 */
import { GristContactFields } from '@infra-num/common';

@Controller('grist')
export class GristApiController {

  constructor(
    private readonly gristApiService: GristApiService
  ) {}

  @Post('handleMirroringAllowedAccounts')
  async updateContacts(@Body() body: GristContactFields[]): Promise<void> {
    return await this.gristApiService.updateContacts(body);
  }

}
