/**
 * Nest.js imports
 */
import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';

/**
 * 3rd-party imports
 */
import { JurisdictionType } from '@infra-num/common';

/**
 * Internal imports
 */
import { JurisdictionsService } from './jurisdictions.service';
import { ModelMock } from '../../shared/tests/model.mock';
import { Jurisdiction, JurisdictionDocument } from './jurisdiction.schema';
import { JURISDICTIONS_ALREADY_SAVED } from '../tests/already-saved.mock';

describe('JurisdictionsService', () => {
  let service: JurisdictionsService;
  let jurisdictionModel: typeof ModelMock;

  beforeEach(async () => {
    const modelToken: string = getModelToken(Jurisdiction.name);
    const module: TestingModule = await Test.createTestingModule({
      providers: [JurisdictionsService,
        {
          provide: modelToken,
          useValue: ModelMock
        }]
    }).compile();

    service = module.get<JurisdictionsService>(JurisdictionsService);
    jurisdictionModel = module.get<typeof ModelMock>(modelToken);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return the Jurisdiction we want it to create', async () => {
    const jurisdictionToCreate: Jurisdiction = {
      type: JurisdictionType.City,
      name: 'Toulouse',
      initialZoom: 12,
      referenceCoordinates: [42, 42]
    };

    const createdJurisdiction: JurisdictionDocument = await service.create(jurisdictionToCreate);

    expect(createdJurisdiction).toMatchObject(jurisdictionToCreate);
  });

  it('should return the expected list of Jurisdictions when readAll is called', async () => {
    const jurisdictionsAlreadySaved: JurisdictionDocument[] = JURISDICTIONS_ALREADY_SAVED;
    jurisdictionModel.find.mockResolvedValue(jurisdictionsAlreadySaved);

    const jurisdictionsRetrieved: JurisdictionDocument[] = await service.readAll();

    expect(jurisdictionModel.find).toHaveBeenCalled();
    expect(jurisdictionsRetrieved).toEqual(jurisdictionsAlreadySaved);
  });
});
