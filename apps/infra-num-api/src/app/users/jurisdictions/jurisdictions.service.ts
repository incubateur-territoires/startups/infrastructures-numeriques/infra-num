/**
 * Nest.js imports
 */
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

/**
 * 3rd-party imports
 */
import { Model, Types } from 'mongoose';
import { Jurisdiction, JurisdictionType } from '@infra-num/common';

/**
 * Internal imports
 */
import {
  Jurisdiction as JurisdictionSchemaClass,
  JurisdictionDocument,
  JurisdictionWithId
} from './jurisdiction.schema';

@Injectable()
export class JurisdictionsService {
  constructor(
    @InjectModel(JurisdictionSchemaClass.name) private jurisdictionModel: Model<JurisdictionDocument>
  ) {}

  async create(jurisdictionDefinition: Jurisdiction): Promise<JurisdictionDocument> {
    const jurisdictionToCreate = new this.jurisdictionModel(jurisdictionDefinition);

    return jurisdictionToCreate.save();
  }

  async createManyAndOrReturnIds(jurisdictionsDefinition: Jurisdiction[]): Promise<Types.ObjectId[]> {
    let ids = [];
    for (const jurisdictionDefinition of jurisdictionsDefinition) {
      const jurisdictionId = await this.createAndOrReturnId(jurisdictionDefinition);
      ids.push(jurisdictionId);
    }
    return ids;
  }

  async createAndOrReturnId(jurisdictionDefinition: Jurisdiction): Promise<Types.ObjectId> {
    const existingJurisdiction = await this.findOne(jurisdictionDefinition.name);

    let jurisdictionId: Types.ObjectId;

    if (existingJurisdiction) {
      jurisdictionId = existingJurisdiction._id;
    } else {
      const createdJurisdiction: JurisdictionDocument = await this.create(jurisdictionDefinition);
      jurisdictionId = createdJurisdiction._id;
    }

    return jurisdictionId;
  }

  async findOne(name: string): Promise<JurisdictionWithId> {
    return this.jurisdictionModel
      .findOne({name})
      .populate({ path: 'epciCities' })
      .lean();
  }

  async readAll(): Promise<JurisdictionDocument[]> {
    return this.jurisdictionModel.find();
  }

  async updateManyDepartments(departments: { name: string, code: string }[]): Promise<void> {
    await this.jurisdictionModel.bulkWrite(
      departments.map(({name, code}) => ({
        updateOne: {
          filter: {name, type: JurisdictionType.Department},
          update: {$set: {code}}
        }
      })),
      {ordered: false}
    );
  }
}
