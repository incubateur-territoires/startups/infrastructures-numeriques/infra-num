export const TERRITORIES: any = [
  {
    "nom": "Saint-Pierre-et-Miquelon",
    "code": "975",
    "coordinates": [
      46.8852,
      -56.3159
    ]
  },
  {
    "nom": "Nouvelle-Calédonie",
    "code": "988",
    "coordinates": [
      -20.9043,
      165.6180
    ]
  },
  {
    "nom": "Polynésie française",
    "code": "987",
    "coordinates": [
      -17.6797,
      -149.4068
    ]
  },
  {
    "nom": "Wallis et Futuna",
    "code": "986",
    "coordinates": [
      -13.289402,
      -176.204224
    ]
  },
  {
    "nom": "Saint-Martin ",
    "code": "97150",
    "coordinates": [
      18.05406346,
      -63.05658458
    ]
  },
  {
    "nom": "Saint-Barthélemy ",
    "code": "97133",
    "coordinates": [
      17.897903410,
      -62.82548141
    ]
  }
];