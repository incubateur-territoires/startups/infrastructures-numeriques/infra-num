export const REGIONS: any = [
  {
    "nom": "Île-de-France",
    "code": "11",
    "coordinates": [
      48.6443057,
      2.7537863
    ]
  },
  {
    "nom": "Centre-Val de Loire",
    "code": "24",
    "coordinates": [
      47.5490251,
      1.7324062
    ]
  },
  {
    "nom": "Bourgogne-Franche-Comté",
    "code": "27",
    "coordinates": [
      47.0510946,
      5.0740568
    ]
  },
  {
    "nom": "Normandie",
    "code": "28",
    "coordinates": [
      49.0677708,
      0.3138532
    ]
  },
  {
    "nom": "Hauts-de-France",
    "code": "32",
    "coordinates": [
      50.1024606,
      2.7247515
    ]
  },
  {
    "nom": "Grand Est",
    "code": "44",
    "coordinates": [
      48.4845157,
      6.113035
    ]
  },
  {
    "nom": "Pays de la Loire",
    "code": "52",
    "coordinates": [
      47.6594864,
      -0.8186143
    ]
  },
  {
    "nom": "Bretagne",
    "code": "53",
    "coordinates": [
      48.2640845,
      -2.9202408
    ]
  },
  {
    "nom": "Nouvelle-Aquitaine",
    "code": "75",
    "coordinates": [
      45.4039367,
      0.3756199
    ]
  },
  {
    "nom": "Occitanie",
    "code": "76",
    "coordinates": [
      43.6487851,
      2.3435684
    ]
  },
  {
    "nom": "Auvergne-Rhône-Alpes",
    "code": "84",
    "coordinates": [
      45.2968119,
      4.6604809
    ]
  },
  {
    "nom": "Provence-Alpes-Côte d'Azur",
    "code": "93",
    "coordinates": [
      44.0580563,
      6.0638506
    ]
  },
  {
    "nom": "Corse",
    "code": "94",
    "coordinates": [
      42.1880896,
      9.0684138
    ]
  },
  {
    "nom": "Guadeloupe",
    "code": "01",
    "coordinates": [
      16.2528827,
      -61.5686855
    ]
  },
  {
    "nom": "Martinique",
    "code": "02",
    "coordinates": [
      14.6367927,
      -61.0158269
    ]
  },
  {
    "nom": "Guyane",
    "code": "03",
    "coordinates": [
      4.0039882,
      -52.999998
    ]
  },
  {
    "nom": "La Réunion",
    "code": "04",
    "coordinates": [
      -21.1307379,
      55.5364801
    ]
  },
  {
    "nom": "Mayotte",
    "code": "06",
    "coordinates": [
      -12.823048,
      45.1520755
    ]
  }
]
