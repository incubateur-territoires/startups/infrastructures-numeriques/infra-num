export const DEPARTMENTS: any = [
  {
    "num_dep": "01",
    "dep_name": "Ain",
    "region_name": "Auvergne-Rhône-Alpes",
    "coordinates": [
      49.4532854,
      3.606899
    ]
  },
  {
    "num_dep": "02",
    "dep_name": "Aisne",
    "region_name": "Hauts-de-France",
    "coordinates": [
      49.4532854,
      3.606899
    ]
  },
  {
    "num_dep": "03",
    "dep_name": "Allier",
    "region_name": "Auvergne-Rhône-Alpes",
    "coordinates": [
      46.3674641,
      3.1638828
    ]
  },
  {
    "num_dep": "04",
    "dep_name": "Alpes-de-Haute-Provence",
    "region_name": "Provence-Alpes-Côte d'Azur",
    "coordinates": [
      44.1640832,
      6.1878515
    ]
  },
  {
    "num_dep": "05",
    "dep_name": "Hautes-Alpes",
    "region_name": "Provence-Alpes-Côte d'Azur",
    "coordinates": [
      44.6564666,
      6.3520246
    ]
  },
  {
    "num_dep": "06",
    "dep_name": "Alpes-Maritimes",
    "region_name": "Provence-Alpes-Côte d'Azur",
    "coordinates": [
      43.9210587,
      7.1790785
    ]
  },
  {
    "num_dep": "07",
    "dep_name": "Ardèche",
    "region_name": "Auvergne-Rhône-Alpes",
    "coordinates": [
      44.815194,
      4.3986525
    ]
  },
  {
    "num_dep": "08",
    "dep_name": "Ardennes",
    "region_name": "Grand Est",
    "coordinates": [
      49.6980117,
      4.6716005
    ]
  },
  {
    "num_dep": "09",
    "dep_name": "Ariège",
    "region_name": "Occitanie",
    "coordinates": [
      42.9455368,
      1.4065544
    ]
  },
  {
    "num_dep": "10",
    "dep_name": "Aube",
    "region_name": "Grand Est",
    "coordinates": [
      48.3201921,
      4.1905397
    ]
  },
  {
    "num_dep": "11",
    "dep_name": "Aude",
    "region_name": "Occitanie",
    "coordinates": [
      43.0542733,
      2.5124715
    ]
  },
  {
    "num_dep": "12",
    "dep_name": "Aveyron",
    "region_name": "Occitanie",
    "coordinates": [
      44.3158574,
      2.5065697
    ]
  },
  {
    "num_dep": "13",
    "dep_name": "Bouches-du-Rhône",
    "region_name": "Provence-Alpes-Côte d'Azur",
    "coordinates": [
      43.5424182,
      5.0343236
    ]
  },
  {
    "num_dep": "14",
    "dep_name": "Calvados",
    "region_name": "Normandie",
    "coordinates": [
      49.0907648,
      -0.2413951
    ]
  },
  {
    "num_dep": "15",
    "dep_name": "Cantal",
    "region_name": "Auvergne-Rhône-Alpes",
    "coordinates": [
      45.0497701,
      2.6997176
    ]
  },
  {
    "num_dep": "16",
    "dep_name": "Charente",
    "region_name": "Nouvelle-Aquitaine",
    "coordinates": [
      45.6667902,
      0.097305
    ]
  },
  {
    "num_dep": "17",
    "dep_name": "Charente-Maritime",
    "region_name": "Nouvelle-Aquitaine",
    "coordinates": [
      45.7302267,
      -0.7212876
    ]
  },
  {
    "num_dep": "18",
    "dep_name": "Cher",
    "region_name": "Centre-Val de Loire",
    "coordinates": [
      47.0248824,
      2.5753334
    ]
  },
  {
    "num_dep": "19",
    "dep_name": "Corrèze",
    "region_name": "Nouvelle-Aquitaine",
    "coordinates": [
      45.3429047,
      1.8176424
    ]
  },
  {
    "num_dep": "21",
    "dep_name": "Côte-d'Or",
    "region_name": "Bourgogne-Franche-Comté",
    "coordinates": [
      47.4655034,
      4.7481223
    ]
  },
  {
    "num_dep": "22",
    "dep_name": "Côtes-d'Armor",
    "region_name": "Bretagne",
    "coordinates": [
      48.4614877,
      -2.7514328
    ]
  },
  {
    "num_dep": "23",
    "dep_name": "Creuse",
    "region_name": "Nouvelle-Aquitaine",
    "coordinates": [
      46.0593485,
      2.048901
    ]
  },
  {
    "num_dep": "24",
    "dep_name": "Dordogne",
    "region_name": "Nouvelle-Aquitaine",
    "coordinates": [
      45.1429198,
      0.6321258
    ]
  },
  {
    "num_dep": "25",
    "dep_name": "Doubs",
    "region_name": "Bourgogne-Franche-Comté",
    "coordinates": [
      47.0669915,
      6.2356228
    ]
  },
  {
    "num_dep": "26",
    "dep_name": "Drôme",
    "region_name": "Auvergne-Rhône-Alpes",
    "coordinates": [
      44.7295347,
      5.2046372
    ]
  },
  {
    "num_dep": "27",
    "dep_name": "Eure",
    "region_name": "Normandie",
    "coordinates": [
      49.0756358,
      0.9652026
    ]
  },
  {
    "num_dep": "28",
    "dep_name": "Eure-et-Loir",
    "region_name": "Centre-Val de Loire",
    "coordinates": [
      48.4474102,
      1.399882
    ]
  },
  {
    "num_dep": "29",
    "dep_name": "Finistère",
    "region_name": "Bretagne",
    "coordinates": [
      48.2451152,
      -4.0440902
    ]
  },
  {
    "num_dep": "2A",
    "dep_name": "Corse-du-Sud",
    "region_name": "Corse",
    "coordinates": [
      41.8734082,
      9.0087052
    ]
  },
  {
    "num_dep": "2B",
    "dep_name": "Haute-Corse",
    "region_name": "Corse",
    "coordinates": [
      42.4219698,
      9.1009065
    ]
  },
  {
    "num_dep": "30",
    "dep_name": "Gard",
    "region_name": "Occitanie",
    "coordinates": [
      43.95995,
      4.297637
    ]
  },
  {
    "num_dep": "31",
    "dep_name": "Haute-Garonne",
    "region_name": "Occitanie",
    "coordinates": [
      43.3054546,
      0.9716792
    ]
  },
  {
    "num_dep": "32",
    "dep_name": "Gers",
    "region_name": "Occitanie",
    "coordinates": [
      43.6955276,
      0.4101019
    ]
  },
  {
    "num_dep": "33",
    "dep_name": "Gironde",
    "region_name": "Nouvelle-Aquitaine",
    "coordinates": [
      44.883746,
      -0.6051264
    ]
  },
  {
    "num_dep": "34",
    "dep_name": "Hérault",
    "region_name": "Occitanie",
    "coordinates": [
      43.591422,
      3.3553309
    ]
  },
  {
    "num_dep": "35",
    "dep_name": "Ille-et-Vilaine",
    "region_name": "Bretagne",
    "coordinates": [
      48.1727681,
      -1.6498092
    ]
  },
  {
    "num_dep": "36",
    "dep_name": "Indre",
    "region_name": "Centre-Val de Loire",
    "coordinates": [
      46.8121056,
      1.5382052
    ]
  },
  {
    "num_dep": "37",
    "dep_name": "Indre-et-Loire",
    "region_name": "Centre-Val de Loire",
    "coordinates": [
      47.2232046,
      0.6866703
    ]
  },
  {
    "num_dep": "38",
    "dep_name": "Isère",
    "region_name": "Auvergne-Rhône-Alpes",
    "coordinates": [
      45.2897932,
      5.6343825
    ]
  },
  {
    "num_dep": "39",
    "dep_name": "Jura",
    "region_name": "Bourgogne-Franche-Comté",
    "coordinates": [
      46.7833625,
      5.7832857
    ]
  },
  {
    "num_dep": "40",
    "dep_name": "Landes",
    "region_name": "Nouvelle-Aquitaine",
    "coordinates": [
      44.0099694,
      -0.6433872
    ]
  },
  {
    "num_dep": "41",
    "dep_name": "Loir-et-Cher",
    "region_name": "Centre-Val de Loire",
    "coordinates": [
      47.6597752,
      1.2971835
    ]
  },
  {
    "num_dep": "42",
    "dep_name": "Loire",
    "region_name": "Auvergne-Rhône-Alpes",
    "coordinates": [
      45.7538536,
      4.0454737
    ]
  },
  {
    "num_dep": "43",
    "dep_name": "Haute-Loire",
    "region_name": "Auvergne-Rhône-Alpes",
    "coordinates": [
      45.0857249,
      3.8338261
    ]
  },
  {
    "num_dep": "44",
    "dep_name": "Loire-Atlantique",
    "region_name": "Pays de la Loire",
    "coordinates": [
      47.3481614,
      -1.8727461
    ]
  },
  {
    "num_dep": "45",
    "dep_name": "Loiret",
    "region_name": "Centre-Val de Loire",
    "coordinates": [
      47.9138724,
      2.3075036
    ]
  },
  {
    "num_dep": "46",
    "dep_name": "Lot",
    "region_name": "Occitanie",
    "coordinates": [
      44.6249918,
      1.6657742
    ]
  },
  {
    "num_dep": "47",
    "dep_name": "Lot-et-Garonne",
    "region_name": "Nouvelle-Aquitaine",
    "coordinates": [
      44.3691703,
      0.4539158
    ]
  },
  {
    "num_dep": "48",
    "dep_name": "Lozère",
    "region_name": "Occitanie",
    "coordinates": [
      44.5425706,
      3.5211146
    ]
  },
  {
    "num_dep": "49",
    "dep_name": "Maine-et-Loire",
    "region_name": "Pays de la Loire",
    "coordinates": [
      47.3886305,
      -0.3909097
    ]
  },
  {
    "num_dep": "50",
    "dep_name": "Manche",
    "region_name": "Normandie",
    "coordinates": [
      49.0918952,
      -1.2454952
    ]
  },
  {
    "num_dep": "51",
    "dep_name": "Marne",
    "region_name": "Grand Est",
    "coordinates": [
      48.961264,
      4.3122436
    ]
  },
  {
    "num_dep": "52",
    "dep_name": "Haute-Marne",
    "region_name": "Grand Est",
    "coordinates": [
      48.1329414,
      5.2529108
    ]
  },
  {
    "num_dep": "53",
    "dep_name": "Mayenne",
    "region_name": "Pays de la Loire",
    "coordinates": [
      48.1507819,
      -0.6491274
    ]
  },
  {
    "num_dep": "54",
    "dep_name": "Meurthe-et-Moselle",
    "region_name": "Grand Est",
    "coordinates": [
      48.9559682,
      5.9870383
    ]
  },
  {
    "num_dep": "55",
    "dep_name": "Meuse",
    "region_name": "Grand Est",
    "coordinates": [
      26.2540493,
      29.2675469
    ]
  },
  {
    "num_dep": "56",
    "dep_name": "Morbihan",
    "region_name": "Bretagne",
    "coordinates": [
      47.8259812,
      -2.7633493
    ]
  },
  {
    "num_dep": "57",
    "dep_name": "Moselle",
    "region_name": "Grand Est",
    "coordinates": [
      49.0207259,
      6.5380352
    ]
  },
  {
    "num_dep": "58",
    "dep_name": "Nièvre",
    "region_name": "Bourgogne-Franche-Comté",
    "coordinates": [
      47.119697,
      3.5448898
    ]
  },
  {
    "num_dep": "59",
    "dep_name": "Nord",
    "region_name": "Hauts-de-France",
    "coordinates": [
      50.5289671,
      3.0883524
    ]
  },
  {
    "num_dep": "60",
    "dep_name": "Oise",
    "region_name": "Hauts-de-France",
    "coordinates": [
      49.4120546,
      2.4064878
    ]
  },
  {
    "num_dep": "61",
    "dep_name": "Orne",
    "region_name": "Normandie",
    "coordinates": [
      48.5760533,
      0.0446617
    ]
  },
  {
    "num_dep": "62",
    "dep_name": "Pas-de-Calais",
    "region_name": "Hauts-de-France",
    "coordinates": [
      50.5144061,
      2.2580078
    ]
  },
  {
    "num_dep": "63",
    "dep_name": "Puy-de-Dôme",
    "region_name": "Auvergne-Rhône-Alpes",
    "coordinates": [
      45.7715343,
      3.0839934
    ]
  },
  {
    "num_dep": "64",
    "dep_name": "Pyrénées-Atlantiques",
    "region_name": "Nouvelle-Aquitaine",
    "coordinates": [
      43.1871865,
      -0.7282474
    ]
  },
  {
    "num_dep": "65",
    "dep_name": "Hautes-Pyrénées",
    "region_name": "Occitanie",
    "coordinates": [
      43.1437925,
      0.1586661
    ]
  },
  {
    "num_dep": "66",
    "dep_name": "Pyrénées-Orientales",
    "region_name": "Occitanie",
    "coordinates": [
      42.625894,
      2.506509
    ]
  },
  {
    "num_dep": "67",
    "dep_name": "Bas-Rhin",
    "region_name": "Grand Est",
    "coordinates": [
      48.5991783,
      7.5336729
    ]
  },
  {
    "num_dep": "68",
    "dep_name": "Haut-Rhin",
    "region_name": "Grand Est",
    "coordinates": [
      47.8654746,
      7.2315433
    ]
  },
  {
    "num_dep": "69",
    "dep_name": "Rhône",
    "region_name": "Auvergne-Rhône-Alpes",
    "coordinates": [
      45.8802348,
      4.5645336
    ]
  },
  {
    "num_dep": "70",
    "dep_name": "Haute-Saône",
    "region_name": "Bourgogne-Franche-Comté",
    "coordinates": [
      47.6384233,
      6.0951141
    ]
  },
  {
    "num_dep": "71",
    "dep_name": "Saône-et-Loire",
    "region_name": "Bourgogne-Franche-Comté",
    "coordinates": [
      46.6557086,
      4.5585548
    ]
  },
  {
    "num_dep": "72",
    "dep_name": "Sarthe",
    "region_name": "Pays de la Loire",
    "coordinates": [
      48.0269287,
      0.2538217
    ]
  },
  {
    "num_dep": "73",
    "dep_name": "Savoie",
    "region_name": "Auvergne-Rhône-Alpes",
    "coordinates": [
      45.4948952,
      6.3846604
    ]
  },
  {
    "num_dep": "74",
    "dep_name": "Haute-Savoie",
    "region_name": "Auvergne-Rhône-Alpes",
    "coordinates": [
      46.0688208,
      6.344537
    ]
  },
  {
    "num_dep": "75",
    "dep_name": "Paris",
    "region_name": "Île-de-France",
    "coordinates": [
      48.8588897,
      2.320041
    ]
  },
  {
    "num_dep": "76",
    "dep_name": "Seine-Maritime",
    "region_name": "Normandie",
    "coordinates": [
      49.6632374,
      0.9401134
    ]
  },
  {
    "num_dep": "77",
    "dep_name": "Seine-et-Marne",
    "region_name": "Île-de-France",
    "coordinates": [
      48.6190207,
      3.0418158
    ]
  },
  {
    "num_dep": "78",
    "dep_name": "Yvelines",
    "region_name": "Île-de-France",
    "coordinates": [
      48.7620373,
      1.8871376
    ]
  },
  {
    "num_dep": "79",
    "dep_name": "Deux-Sèvres",
    "region_name": "Nouvelle-Aquitaine",
    "coordinates": [
      46.53914,
      -0.2994785
    ]
  },
  {
    "num_dep": "80",
    "dep_name": "Somme",
    "region_name": "Hauts-de-France",
    "coordinates": [
      49.9689482,
      2.3738855
    ]
  },
  {
    "num_dep": "81",
    "dep_name": "Tarn",
    "region_name": "Occitanie",
    "coordinates": [
      43.7921741,
      2.1339648
    ]
  },
  {
    "num_dep": "82",
    "dep_name": "Tarn-et-Garonne",
    "region_name": "Occitanie",
    "coordinates": [
      44.080656,
      1.2050633
    ]
  },
  {
    "num_dep": "83",
    "dep_name": "Var",
    "region_name": "Provence-Alpes-Côte d'Azur",
    "coordinates": [
      43.4173592,
      6.266462
    ]
  },
  {
    "num_dep": "84",
    "dep_name": "Vaucluse",
    "region_name": "Provence-Alpes-Côte d'Azur",
    "coordinates": [
      43.9938643,
      5.1818898
    ]
  },
  {
    "num_dep": "85",
    "dep_name": "Vendée",
    "region_name": "Pays de la Loire",
    "coordinates": [
      46.6757732,
      -1.2914463
    ]
  },
  {
    "num_dep": "86",
    "dep_name": "Vienne",
    "region_name": "Nouvelle-Aquitaine",
    "coordinates": [
      48.2083537,
      16.3725042
    ]
  },
  {
    "num_dep": "87",
    "dep_name": "Haute-Vienne",
    "region_name": "Nouvelle-Aquitaine",
    "coordinates": [
      45.9190192,
      1.2031768
    ]
  },
  {
    "num_dep": "88",
    "dep_name": "Vosges",
    "region_name": "Grand Est",
    "coordinates": [
      48.163786,
      6.3820712
    ]
  },
  {
    "num_dep": "89",
    "dep_name": "Yonne",
    "region_name": "Bourgogne-Franche-Comté",
    "coordinates": [
      47.8551257,
      3.6450439
    ]
  },
  {
    "num_dep": "90",
    "dep_name": "Territoire de Belfort",
    "region_name": "Bourgogne-Franche-Comté",
    "coordinates": [
      47.629231,
      6.8993012
    ]
  },
  {
    "num_dep": "91",
    "dep_name": "Essonne",
    "region_name": "Île-de-France",
    "coordinates": [
      48.5303402,
      2.2392918
    ]
  },
  {
    "num_dep": "92",
    "dep_name": "Hauts-de-Seine",
    "region_name": "Île-de-France",
    "coordinates": [
      48.8401859,
      2.19863
    ]
  },
  {
    "num_dep": "93",
    "dep_name": "Seine-Saint-Denis",
    "region_name": "Île-de-France",
    "coordinates": [
      48.9098125,
      2.4528635
    ]
  },
  {
    "num_dep": "94",
    "dep_name": "Val-de-Marne",
    "region_name": "Île-de-France",
    "coordinates": [
      48.7744893,
      2.4543321
    ]
  },
  {
    "num_dep": "95",
    "dep_name": "Val-d'Oise",
    "region_name": "Île-de-France",
    "coordinates": [
      49.0750704,
      2.2098114
    ]
  },
  {
    "num_dep": "971",
    "dep_name": "Guadeloupe",
    "region_name": "Guadeloupe",
    "coordinates": [
      16.2528827,
      -61.5686855
    ]
  },
  {
    "num_dep": "972",
    "dep_name": "Martinique",
    "region_name": "Martinique",
    "coordinates": [
      14.6367927,
      -61.0158269
    ]
  },
  {
    "num_dep": "973",
    "dep_name": "Guyane",
    "region_name": "Guyane",
    "coordinates": [
      4.0039882,
      -52.999998
    ]
  },
  {
    "num_dep": "974",
    "dep_name": "La Réunion",
    "region_name": "La Réunion",
    "coordinates": [
      -21.1307379,
      55.5364801
    ]
  },
  {
    "num_dep": "976",
    "dep_name": "Mayotte",
    "region_name": "Mayotte",
    "coordinates": [
      -12.823048,
      45.1520755
    ]
  }
]
