/**
 * Nest.js imports
 */
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

/**
 * 3rd-party imports
 */
import { HydratedDocument, Schema as MongooseSchema, Types } from 'mongoose';
import {
  Jurisdiction as JurisdictionInterface,
  JurisdictionType
} from '@infra-num/common';

/**
 * TypeScript entities and constants
 */
export type JurisdictionDocument = HydratedDocument<JurisdictionInterface>;
export type JurisdictionWithId = JurisdictionInterface & {
  _id: Types.ObjectId;
};

@Schema()
export class Jurisdiction implements JurisdictionInterface {
  @Prop({type: String, enum: JurisdictionType, required: true})
  type: JurisdictionType;

  @Prop({required: true, unique: true})
  name: string;

  @Prop({required: true, unique: false})
  initialZoom: number;

  @Prop({unique: true})
  code: string;

  @Prop({type: [Number], required: true})
  referenceCoordinates: [number, number];

  @Prop({
    type: [MongooseSchema.Types.ObjectId],
    ref: 'Jurisdiction',
  })
  epciCities?: Jurisdiction[] | Types.ObjectId[];
}

export const JurisdictionSchema = SchemaFactory.createForClass(Jurisdiction);
