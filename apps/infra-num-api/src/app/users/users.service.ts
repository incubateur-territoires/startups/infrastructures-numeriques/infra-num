/**
 * Nest.js imports
 */
import { BadRequestException, forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { JwtService } from '@nestjs/jwt';

/**
 * 3rd-party imports
 */
import { Model, Types } from 'mongoose';
import bcrypt from 'bcryptjs';
import {
  ALLOWED_ACCOUNT_NOT_FOUND,
  EMAIL_ALREADY_EXISTS_ERROR,
  EXPIRED_JWT_TOKEN_ERROR,
  INVALID_JWT_TOKEN_ERROR,
  Jurisdiction,
  JurisdictionType,
  Role,
  TokenObject,
  UserToCreate,
  WEAK_PASSWORD_ERROR
} from '@infra-num/common';

/**
 * Internal imports
 */
import { User, UserDocument, UserWithId } from './user.schema';
import { JurisdictionsService } from './jurisdictions/jurisdictions.service';
import { AllowedAccountsService } from './allowed-accounts/allowed-accounts.service';
import { environment } from '../../environments/environment';
import { EmailsService } from '../emails/emails.service';
import { EmailTemplates, getEmailTemplate } from '../emails/templates/get-email-template';
import { ListmonkApiService } from '../listmonk-api/listmonk-api.service';
import { AllowedAccount } from './allowed-accounts/allowed-account.schema';
import { GristApiService } from '../grist-api/grist-api.service';
import { CityFromEpci, EpcisService } from '../geo-api/epcis/epcis.service';

/**
 * TypeScript entities and constants
 */
export const SALT_OR_ROUNDS: number = 10;

export interface VerifiedUserToken {
  payload?: { userEmail: string; cityHallEmail?: string };
  iat: string;
  exp: string;
}

@Injectable()
export class UsersService {

  constructor(
    @InjectModel(User.name) private userModel: Model<UserDocument>,
    @Inject(forwardRef(() => GristApiService)) private gristApiService: GristApiService,
    private jurisdictionsService: JurisdictionsService,
    private epcisService: EpcisService,
    private allowedAccountsService: AllowedAccountsService,
    private listMonkService: ListmonkApiService,
    private readonly jwtService: JwtService,
    private readonly emailsService: EmailsService
  ) { }

  async create(userDefinition: UserToCreate, cityHallEmail?: string): Promise<void> {
    const existingEmail = await this.findOne(userDefinition.email);

    if (existingEmail) {
      throw new BadRequestException(EMAIL_ALREADY_EXISTS_ERROR);
    }

    // If this user must be created via an allowed account
    if (!userDefinition.roles || userDefinition.roles?.length === 0) {
      const existingAllowedAccount = await this.allowedAccountsService.findOne(userDefinition.email);
      if (!existingAllowedAccount) {
        throw new BadRequestException(ALLOWED_ACCOUNT_NOT_FOUND);
      }

      userDefinition.jurisdictions = existingAllowedAccount.jurisdictions;
      userDefinition.roles = existingAllowedAccount.roles;
    }

    if (userDefinition.hasSubscribedToNewsletter) {
      await this.listMonkService.createSubscriber({
        email: userDefinition.email,
        name: userDefinition.email,
        status: 'enabled',
        lists: [40],
        preconfirm_subscriptions: true
      });
    }

    // Check the strength of the password
    if (!this.isStrongPassword(userDefinition.password)) {
      throw new BadRequestException(WEAK_PASSWORD_ERROR);
    }

    const userToCreate: UserDocument = new this.userModel(userDefinition);
    userToCreate.password = await bcrypt.hash(userDefinition.password, SALT_OR_ROUNDS);

    /*
     * If userDefinition.jurisdictions is an array of actual
     * Jurisdiction objects ( and NOT an array of ObjectIDs ),
     * then we replace userToCreate.jurisdictions ( which, in
     * this case, would be an empty array at this point ) with
     * an array comprised of the IDs of these Jurisdiction objects
     */
    if (
      userDefinition?.jurisdictions?.length > 0 &&
      (userDefinition.jurisdictions as Jurisdiction[]).every((jurisdiction) => jurisdiction?.name !== undefined)) {

      /*
       * When the user is an EPCI, we must create all the
       * jurisdictions of all the municipalities it includes.
       */
      if (userDefinition.roles[0] === Role.EPCI) {
        const allCitiesForCurrentEPCI = await this.epcisService.getCitiesByEpciCode((userDefinition.jurisdictions[0] as Jurisdiction).code);
        const epciCitiesJurisdictions: Jurisdiction[] = allCitiesForCurrentEPCI.map((city: CityFromEpci) => ({
          type: JurisdictionType.City,
          name: city.nom,
          code: city.code,
          initialZoom: 13,
          referenceCoordinates: city.centre.coordinates.reverse() as [number, number]
        }));
        (userDefinition.jurisdictions[0] as Jurisdiction).epciCities = await this.jurisdictionsService.createManyAndOrReturnIds(epciCitiesJurisdictions);
      }

      userToCreate.jurisdictions = await this.jurisdictionsService.createManyAndOrReturnIds(userDefinition.jurisdictions as Jurisdiction[]);
    }

    return userToCreate.save().then(async (userCreated: UserDocument) => {
      await this.sendAccountConfirmationEmail(cityHallEmail ? [cityHallEmail] : [userDefinition.email], userCreated.email);

      /*
       * If the created user roles include "ProjectTeam", we update the column
       * "Compte créé" in Grist for the corresponding user
       */
      if (userCreated.roles.includes(Role.ProjectTeam)) {
        await this.gristApiService.updateGristContactAccountCreatedField(userCreated.email);
      }
    });
  }

  async readAll(): Promise<UserDocument[]> {
    return this.userModel.find();
  }

  async findOne(email: string): Promise<UserWithId> {
    return this.userModel
      .findOne({email})
      .populate({
        path: 'jurisdictions',
        populate: {
          path: 'epciCities'
        }
      }).lean();
  }

  async sendAccountConfirmationEmail(to: string[], sendFor: string): Promise<void> {
    const token = this.generateJwtToken({userEmail: sendFor, cityHallEmail: to[0]});
    const link = `${environment.frontUrl}/validation-compte?token=${token}`;
    const emailDefinition = getEmailTemplate(EmailTemplates.ACCOUNT_CONFIRMATION, to, {sendFor, link});
    await this.emailsService.sendEmail(emailDefinition);
  }

  updateOne({email, roles, jurisdictions, isActive}: Omit<AllowedAccount, 'pointOfContact'>): Promise<UserDocument> {
    return this.userModel.findOneAndUpdate({email}, {
      $set: {
        roles,
        jurisdictions,
        isActive
      }
    });
  }

  async updateOneById(id: Types.ObjectId, user: Partial<User>): Promise<UserDocument> {
    return this.userModel.findByIdAndUpdate(id, user);
  }

  async confirmAccount(tokenObject: TokenObject): Promise<void> {
    const email: string = (await this.verifyToken(tokenObject)).payload.userEmail;
    await this.userModel.findOneAndUpdate({email}, {isEmailConfirmed: true});
  }

  async verifyToken(tokenObject: TokenObject): Promise<VerifiedUserToken> {
    let verifiedToken: VerifiedUserToken;
    try {
      verifiedToken = await this.jwtService.verify(tokenObject.token);
    } catch (error) {
      if (error?.name === 'TokenExpiredError') {
        const emailsDecoded: VerifiedUserToken =
          this.jwtService.decode(tokenObject.token) as VerifiedUserToken;
        throw new BadRequestException({
          message: EXPIRED_JWT_TOKEN_ERROR,
          userEmail: emailsDecoded.payload.userEmail,
          cityHallEmail: emailsDecoded.payload.cityHallEmail
        });
      }

      throw new BadRequestException(INVALID_JWT_TOKEN_ERROR);
    }

    return verifiedToken;
  }

  async resetPassword(password: string, token: string): Promise<void> {
    const verifiedToken: VerifiedUserToken = await this.verifyToken({token});

    // Check the strength of the password
    if (!this.isStrongPassword(password)) {
      throw new BadRequestException(WEAK_PASSWORD_ERROR);
    }

    await this.userModel.findOneAndUpdate(
      {
        email: verifiedToken.payload.userEmail
      },
      {
        password: await bcrypt.hash(password, SALT_OR_ROUNDS)
      }
    );
  }

  generateJwtToken(payload: string | Record<string, unknown>): string {
    return this.jwtService.sign({payload});
  }

  async sendResetPasswordEmail(email: { email: string }): Promise<void> {
    const existingUser: User = await this.userModel.findOne(email);

    if (existingUser?.isEmailConfirmed) {
      const token = this.generateJwtToken({userEmail: existingUser.email});
      const link = `${environment.frontUrl}/nouveau-mdp?token=${token}`;
      const emailDefinition = getEmailTemplate(EmailTemplates.RESET_PASSWORD, [email.email], {link});
      await this.emailsService.sendEmail(emailDefinition);
    }

    return null;
  }

  isStrongPassword(password: string): boolean {

    if (!password) {
      return false;
    }

    // Check the length of the password
    if (password.length < 12) {
      return false;
    }

    // Check for the presence of at least one special character
    const specialChars = /[^A-Za-z0-9]/;
    if (!specialChars.test(password)) {
      return false;
    }

    // Check for the presence of at least one digit
    const numbers = /[0-9]+/;
    if (!numbers.test(password)) {
      return false;
    }

    return true;
  }

}
