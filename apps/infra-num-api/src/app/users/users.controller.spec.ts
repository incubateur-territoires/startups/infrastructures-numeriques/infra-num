/**
 * Nest.js imports
 */
import { Test, TestingModule } from '@nestjs/testing';

/**
 * Internal imports
 */
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { ALLOWED_ACCOUNTS_ALREADY_SAVED } from './tests/already-saved.mock';
import { UsersServiceMock } from './tests/users-service.mock';
import { AllowedAccountsServiceMock } from './tests/allowed-accounts.service.mock';
import { AllowedAccountsService } from './allowed-accounts/allowed-accounts.service';

describe('UsersController', () => {
  let controller: UsersController;
  let allowedAccountsService: AllowedAccountsServiceMock;
  let usersService: UsersServiceMock;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        {
          provide: UsersService,
          useClass: UsersServiceMock
        },
        {
          provide: AllowedAccountsService,
          useClass: AllowedAccountsServiceMock
        }
      ]
    }).compile();

    controller = module.get<UsersController>(UsersController);
    usersService = module.get<UsersServiceMock>(UsersService);
    allowedAccountsService = module.get<AllowedAccountsServiceMock>(AllowedAccountsService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should return an array of contacts when getContacts(department, region) is called', async () => {
    allowedAccountsService.getPointsOfContactInformation = jest.fn().mockResolvedValue(ALLOWED_ACCOUNTS_ALREADY_SAVED);

    const contactsRetrieved = await controller.getContacts('Aube', 'Grand Est');

    expect(allowedAccountsService.getPointsOfContactInformation).toHaveBeenCalled();
    expect(contactsRetrieved).toEqual(ALLOWED_ACCOUNTS_ALREADY_SAVED);
  });
});
