/**
 * Nest.js imports
 */
import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put, Query,
  UploadedFiles, UseGuards,
  UseInterceptors
} from '@nestjs/common';
import { AnyFilesInterceptor } from '@nestjs/platform-express';

/**
 * 3rd-party imports
 */
import { EMAIL_ALREADY_EXISTS_ERROR, TokenObject, UserToCreate } from '@infra-num/common';
import { Multer } from 'multer';
import csvToJson from 'csvtojson';

/**
 * Internal imports
 */
import { UsersService, VerifiedUserToken } from './users.service';
import { User } from './user.schema';
import { AllowedAccountsFromFile, AllowedAccountsService } from './allowed-accounts/allowed-accounts.service';
import { JwtAuthGuard } from '../authentication/guards/jwt-auth.guard';
import { AllowedAccount } from './allowed-accounts/allowed-account.schema';
import { ApiKeyAuthGuard } from '../authentication/guards/api-key-auth.guard';
import { JurisdictionsService } from './jurisdictions/jurisdictions.service';

interface JurisdictionsFromFile {
  'lib_desambig': string,
  'siren_groupement': string,
  'lib_groupement': string,
  'nature_juridique': string,
  'cheflieu': string,
  'insee_geo': string,
  'echelon_geo': string,
  'insee_dep': string,
  'insee_reg': string
}

@Controller('users')
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    private readonly allowedAccountsService: AllowedAccountsService,
    private readonly jurisdictionsService: JurisdictionsService
  ) {}

  @Post()
  async createUser(@Body() {user, cityHallEmail}: { user: UserToCreate, cityHallEmail?: string }): Promise<void> {
    return await this.usersService.create(user, cityHallEmail);
  }

  @Post('account-confirmation-email')
  async sendAccountConfirmationEmail(@Body() {
    sendTo,
    sendFor
  }: { sendTo: string, sendFor: string }): Promise<void> {
    await this.usersService.sendAccountConfirmationEmail([sendTo], sendFor);
  }

  @Get('check-email/:email')
  async checkIfEmailAlreadyExists(@Param('email') email: string): Promise<null> {
    const user: User = await this.usersService.findOne(email);
    if (user) {
      throw new BadRequestException(EMAIL_ALREADY_EXISTS_ERROR);
    }

    return null;
  }

  @Put('confirm-account')
  async confirmUserAccount(@Body() tokenObject: TokenObject): Promise<void> {
    return await this.usersService.confirmAccount(tokenObject);
  }

  @Post('forgot-password')
  sendResetPasswordEmail(@Body() email: { email: string }): Promise<void> {
    return this.usersService.sendResetPasswordEmail(email);
  }

  @Post('verify')
  verifyToken(@Body() tokenObject: TokenObject): Promise<VerifiedUserToken> {
    return this.usersService.verifyToken(tokenObject);
  }

  @Put('reset-password')
  resetPassword(@Body() resetParams: { password: string, token: string }): Promise<void> {
    return this.usersService.resetPassword(resetParams.password, resetParams.token);
  }

  @UseGuards(ApiKeyAuthGuard)
  @Post('init-allowed-accounts')
  @UseInterceptors(AnyFilesInterceptor())
  async initAllowedAccounts(@UploadedFiles() files: Express.Multer.File[]): Promise<void> {
    const allowedAccountsFromFile: AllowedAccountsFromFile[] = await csvToJson().fromString((files[0].buffer).toString());
    const filteredAccounts = await this.allowedAccountsService.filterAccounts(allowedAccountsFromFile);
    for (const {email, roles, jurisdictions, isActive, pointOfContact, fullName} of filteredAccounts) {
      await this.allowedAccountsService.create({
        email,
        roles,
        jurisdictions,
        isActive,
        pointOfContact,
        fullName
      });
      await this.usersService.updateOne({email, roles, jurisdictions, isActive});
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get('contacts')
  getContacts(
    @Query('department') department: string,
    @Query('region') region: string
  ): Promise<AllowedAccount[]> {
    return this.allowedAccountsService.getPointsOfContactInformation(department, region);
  }

  @UseGuards(ApiKeyAuthGuard)
  @Post('add-department-codes')
  @UseInterceptors(AnyFilesInterceptor())
  async addDepartmentCodes(@UploadedFiles() files: Express.Multer.File[]): Promise<void> {
    const jurisdictions: JurisdictionsFromFile[] = await csvToJson().fromString((files[0].buffer).toString());
    const departments: { name: string, code: string }[] = jurisdictions
      .filter((jurisdiction) => (jurisdiction.echelon_geo === 'departement'))
      .map(({lib_groupement, insee_geo}) => ({name: lib_groupement, code: insee_geo}));
    await this.jurisdictionsService.updateManyDepartments(departments);
  }
}
