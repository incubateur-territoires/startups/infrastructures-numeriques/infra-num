/**
 * Nest.js imports
 */
import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { BadRequestException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

/**
 * 3rd-party imports
 */
import {
  EMAIL_ALREADY_EXISTS_ERROR,
  JurisdictionType,
  Role,
  UserToCreate,
  WEAK_PASSWORD_ERROR
} from '@infra-num/common';

/**
 * Internal imports
 */
import { UsersService } from './users.service';
import { User, UserDocument } from './user.schema';
import { USERS_ALREADY_SAVED } from './tests/already-saved.mock';
import { ModelMock } from '../shared/tests/model.mock';
import { JurisdictionsService } from './jurisdictions/jurisdictions.service';
import { Jurisdiction } from './jurisdictions/jurisdiction.schema';
import { JurisdictionsServiceMock } from './tests/jurisdictions-service.mock';
import { EmailsService } from '../emails/emails.service';
import { EmailsServiceMock } from '../emails/tests/emails-service.mock';
import { AllowedAccountsService } from './allowed-accounts/allowed-accounts.service';
import { AllowedAccountsServiceMock } from './tests/allowed-accounts.service.mock';
import { JwtServiceMock } from '../authentication/tests/jwt-service.mock';
import { ListmonkApiService } from '../listmonk-api/listmonk-api.service';
import { ListmonkApiServiceMock } from '../listmonk-api/tests/listmonk-api.service.mock';
import { GristApiService } from '../grist-api/grist-api.service';
import { GristApiServiceMock } from '../grist-api/tests/grist-api.service.mock';

describe('UsersService', () => {
  let usersService: UsersService;
  let jurisdictionsService: JurisdictionsServiceMock;
  let emailsService: EmailsServiceMock;
  let jwtService: JwtServiceMock;
  let userModel: typeof ModelMock;
  let listMonkApiService: ListmonkApiServiceMock;

  beforeEach(async () => {
    const modelUserToken: string = getModelToken(User.name);

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: modelUserToken,
          useValue: ModelMock
        },
        {
          provide: JurisdictionsService,
          useClass: JurisdictionsServiceMock
        },
        {
          provide: AllowedAccountsService,
          useClass: AllowedAccountsServiceMock
        },
        {
          provide: JwtService,
          useClass: JwtServiceMock
        },
        {
          provide: EmailsService,
          useClass: EmailsServiceMock
        },
        {
          provide: ListmonkApiService,
          useClass: ListmonkApiServiceMock
        },
        {
          provide: GristApiService,
          useClass: GristApiServiceMock
        }
      ]
    }).compile();

    usersService = module.get<UsersService>(UsersService);
    jwtService = module.get<JwtServiceMock>(JwtService);
    jurisdictionsService = module.get<JurisdictionsServiceMock>(JurisdictionsService);
    userModel = module.get<typeof ModelMock>(modelUserToken);
    emailsService = module.get<EmailsServiceMock>(EmailsService);
    listMonkApiService = module.get<ListmonkApiServiceMock>(ListmonkApiService);
  });

  it('should be defined', () => {
    expect(usersService).toBeDefined();
  });

  it('should create the new User and a new Jurisdiction in the process', async () => {
    const jurisdictionToCreate: Jurisdiction = {
      type: JurisdictionType.City,
      name: 'Toulouse',
      code: '31',
      initialZoom: 12,
      referenceCoordinates: [42, 42]
    };
    const jurisdictionCreatedId: string = 'my-new-jurisdiction';

    const userToCreate: UserToCreate = {
      email: 'test@test.com',
      roles: [Role.Mayor],
      hasSubscribedToNewsletter: false,
      password: 'StrongPassword1!',
      jurisdictions: [jurisdictionToCreate]
    };

    userModel.findOne.mockReturnValue({populate: jest.fn().mockReturnValue({lean: jest.fn().mockResolvedValue(null)})});
    jurisdictionsService.createManyAndOrReturnIds.mockResolvedValue([jurisdictionCreatedId]);

    await usersService.create(userToCreate);

    expect(jurisdictionsService.createManyAndOrReturnIds).toHaveBeenCalledWith([jurisdictionToCreate]);
  });

  it('should call Listmonk API if hasSubscribedToNewsletter is true', async () => {
    const jurisdictionToCreate: Jurisdiction = {
      type: JurisdictionType.City,
      name: 'Toulouse',
      code: '31',
      initialZoom: 12,
      referenceCoordinates: [42, 42]
    };
    const jurisdictionCreatedId: string = 'my-new-jurisdiction';

    const userToCreate: UserToCreate = {
      email: 'test@test.com',
      roles: [Role.Mayor],
      hasSubscribedToNewsletter: true,
      password: 'StrongPassword1!',
      jurisdictions: [jurisdictionToCreate]
    };

    userModel.findOne.mockReturnValue({populate: jest.fn().mockReturnValue({lean: jest.fn().mockResolvedValue(null)})});
    jurisdictionsService.createManyAndOrReturnIds.mockResolvedValue([jurisdictionCreatedId]);

    await usersService.create(userToCreate);

    expect(jurisdictionsService.createManyAndOrReturnIds).toHaveBeenCalledWith([jurisdictionToCreate]);
    expect(listMonkApiService.createSubscriber).toHaveBeenCalled();
  });

  it('should return the expected list of Users when readAll is called', async () => {
    const usersAlreadySaved: UserToCreate[] = USERS_ALREADY_SAVED;
    userModel.find.mockResolvedValue(usersAlreadySaved);

    const usersRetrieved: UserDocument[] = await usersService.readAll();

    expect(userModel.find).toHaveBeenCalled();
    expect(usersRetrieved).toEqual(usersAlreadySaved);
  });

  it('should return the expected User when findOne (by email) is called', async () => {
    const userAlreadySaved: UserToCreate = USERS_ALREADY_SAVED[0];
    const existingUserEmail = 'test2@test.com';
    userModel.findOne.mockReturnValue({populate: jest.fn().mockReturnValue({lean: jest.fn().mockResolvedValue(userAlreadySaved)})});

    const userRetrieved = await usersService.findOne(existingUserEmail);

    expect(userModel.findOne).toHaveBeenCalled();
    expect(userRetrieved).toEqual(userAlreadySaved);
  });

  it('should throw an error if trying to create a User and the email already exists',
    async () => {
      const userAlreadySaved: UserToCreate = USERS_ALREADY_SAVED[0];

      userModel.findOne.mockReturnValue({populate: jest.fn().mockReturnValue({lean: jest.fn().mockResolvedValue(userAlreadySaved)})});

      try {
        await usersService.create(userAlreadySaved);
        throw 'It should have thrown an error about an already existing email';
      } catch (error) {
        expect(error).toBeInstanceOf(BadRequestException);
        expect((error as BadRequestException).message).toBe(
          EMAIL_ALREADY_EXISTS_ERROR
        );
      }
    }
  );

  it('should call send email when sendMayorAccountConfirmationEmail is called', async () => {
    await usersService.sendAccountConfirmationEmail(['test@gouv.fr'], 'test@test.fr');
    expect(emailsService.sendEmail).toHaveBeenCalled();
  });

  it('should return the generated token for confirmation email link', async () => {
    jwtService.sign = jest.fn().mockReturnValue({auth_token: 'fake token'});
    const token: string = usersService.generateJwtToken('test@test.fr');
    expect(token).toBeDefined();
  });

  describe('Password strength', () => {

    it('should throw an error if trying to create a User and the password is too weak',async () => {
      const userAlreadySaved: UserToCreate = USERS_ALREADY_SAVED[1];
      userModel.findOne.mockReturnValue({populate: jest.fn().mockReturnValue({lean: jest.fn().mockResolvedValue(null)})});

      try {
        await usersService.create(userAlreadySaved);
        throw 'It should have thrown an error about the weakness of the password';
      } catch (error) {
        expect(error).toBeInstanceOf(BadRequestException);
        expect((error as BadRequestException).message).toBe(
          WEAK_PASSWORD_ERROR
        );
      }
    })

    it('should return true for a strong password', () => {
      const strongPassword = "StrongPassword123!";
      expect(usersService.isStrongPassword(strongPassword)).toBe(true);
    });

    it('should return false for a password with less than 12 characters', () => {
      const weakPassword = "Weak123!";
      expect(usersService.isStrongPassword(weakPassword)).toBe(false);
    });

    it('should return false for a password without a special character', () => {
      const passwordWithoutSpecialChar = "Password123";
      expect(usersService.isStrongPassword(passwordWithoutSpecialChar)).toBe(false);
    });

    it('should return false for a password without a digit', () => {
      const passwordWithoutDigit = "Password!";
      expect(usersService.isStrongPassword(passwordWithoutDigit)).toBe(false);
    });
  });

});
