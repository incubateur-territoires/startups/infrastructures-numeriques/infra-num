/**
 * Nest.js imports
 */
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

/**
 * 3rd-party imports
 */
import { HydratedDocument, Schema as MongooseSchema, Types } from 'mongoose';
import {
  Jurisdiction,
  PossibleCoordinates,
  Role,
  UserPreferences
} from '@infra-num/common';

/**
 * TypeScript entities and constants
 */
export type UserDocument = HydratedDocument<User>;
export type UserWithId = User & { _id: Types.ObjectId };
export type UserWithoutPasswords = Omit<
  UserWithId,
  'password' | 'oldPasswords'
>;
export const SAFE_USER_FIELDS = [
  'email',
  'roles',
  'jurisdictions',
  'preferredCoordinates'
];

const lessThan5Elements = [
  function(array: string[]): boolean {
    return array.length <= 5;
  },
  'Only 5 old passwords should be persisted'
];

@Schema()
export class User {
  @Prop({required: true, unique: true})
  email: string;

  @Prop({type: [String], enum: Role, required: true})
  roles: Role[];

  @Prop({
    type: [MongooseSchema.Types.ObjectId],
    ref: 'Jurisdiction',
    required: true,
    default: []
  })
  jurisdictions: Jurisdiction[] | Types.ObjectId[];

  @Prop({
    type: String,
    enum: PossibleCoordinates,
    default(document: UserDocument): PossibleCoordinates {
      return document.roles.includes(Role.Mayor)
        ? PossibleCoordinates.GPS
        : PossibleCoordinates.Lambert93;
    }
  })
  preferredCoordinates: PossibleCoordinates;

  @Prop({default: new Date()})
  creationDate: Date;

  @Prop({required: false})
  lastLoginDate?: Date;

  @Prop({required: false})
  password: string;

  @Prop({type: [String], validate: lessThan5Elements})
  oldPasswords?: string[];

  @Prop({default: false})
  isEmailConfirmed?: boolean;

  @Prop({default: false})
  hasSubscribedToNewsletter?: boolean;

  @Prop({default: true})
  isActive: boolean;

  @Prop({
    type: {
      _id : false,
      defaultReportContact: {
        fullName: String,
        phone: String,
        email: String,
      }
    }
  })
  preferences?: UserPreferences;
}
export const UserSchema = SchemaFactory.createForClass(User);
