/**
 * Nest.js imports
 */
import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';

/**
 * Internal imports
 */
import { AllowedAccountsService } from './allowed-accounts.service';
import { ModelMock } from '../../shared/tests/model.mock';
import { AllowedAccount, AllowedAccountDocument } from './allowed-account.schema';
import { ALLOWED_ACCOUNTS_ALREADY_SAVED } from '../tests/already-saved.mock';

describe('AllowedAccountsService', () => {
  let allowedAccountsService: AllowedAccountsService;
  let allowedAccountModel: typeof ModelMock;

  beforeEach(async () => {
    const allowedAccountModelToken: string = getModelToken(AllowedAccount.name);

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AllowedAccountsService,
        {
          provide: allowedAccountModelToken,
          useValue: ModelMock
        }
      ]
    }).compile();

    allowedAccountsService = module.get<AllowedAccountsService>(AllowedAccountsService);
    allowedAccountModel = module.get<typeof ModelMock>(allowedAccountModelToken);
  });

  it('should be defined', () => {
    expect(allowedAccountsService).toBeDefined();
  });

  // it('should return the AllowedAccount we want it to create', async () => {
  //   const allowedAccountWithoutJurisdiction: Omit<
  //     AllowedAccount,
  //     'jurisdictions'
  //   > = {
  //     email: 'test@test.com',
  //     role: Role.Mayor
  //   };
  //   const allowedAccountToCreate: AllowedAccount = {
  //     ...allowedAccountWithoutJurisdiction,
  //     jurisdictions: [{
  //       type: JurisdictionType.City,
  //       name: 'Toulouse',
  //       initialZoom: 12,
  //       referenceCoordinates: [42, 42]
  //     }]
  //   };
  //
  //   const createdAllowedAccount: AllowedAccountDocument = await allowedAccountsService.create(
  //     allowedAccountToCreate
  //   );
  //
  //   expect(createdAllowedAccount).toMatchObject(allowedAccountWithoutJurisdiction);
  //   expect(createdAllowedAccount.jurisdictions[0]).toBeInstanceOf(Types.ObjectId);
  // });

  it('should return the expected list of AllowedAccounts when readAll is called', async () => {
    const allowedAccountsAlreadySaved: AllowedAccount[] = ALLOWED_ACCOUNTS_ALREADY_SAVED;
    allowedAccountModel.find.mockResolvedValue(allowedAccountsAlreadySaved);

    const allowedAccountsRetrieved: AllowedAccountDocument[] = await allowedAccountsService.readAll();

    expect(allowedAccountModel.find).toHaveBeenCalled();
    expect(allowedAccountsRetrieved).toEqual(allowedAccountsAlreadySaved);
  });

});
