/**
 * Nest.js imports
 */
import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

/**
 * 3rd-party imports
 */
import { Model, Types } from 'mongoose';
import { EMAIL_ALREADY_EXISTS_ERROR, JurisdictionType, Role } from '@infra-num/common';

/**
 * Internal imports
 */
import {
  AllowedAccount,
  AllowedAccountDocument
} from './allowed-account.schema';
import { DEPARTMENTS } from '../jurisdictions/data/departements';
import { JurisdictionsService } from '../jurisdictions/jurisdictions.service';

export interface AllowedAccountsFromFile {
  'Actif': 'true' | 'false',
  'Civilité': string,
  'Prénom': string,
  'Nom': string,
  'Adresse courriel': string,
  'Écrire': string,
  'Fonction': string,
  'Structure': string,
  'Sous-catégorie': string,
  'Programme': string,
  'Rôle': string,
  'Comité': string,
  'Juridiction': string,
  'Référent principal': 'true' | 'false',
  'Téléphone': string,
  'Commentaire': string,
  'Dernière modification': string,
  'Compte créé': 'true' | 'false',
  'Doublon': 'true' | 'false'
}

@Injectable()
export class AllowedAccountsService {

  private readonly logger = new Logger(AllowedAccountsService.name);

  constructor(
    @InjectModel(AllowedAccount.name)
    private allowedAccountModel: Model<AllowedAccountDocument>,
    private jurisdictionsService: JurisdictionsService
  ) {}

  async readAll(): Promise<AllowedAccountDocument[]> {
    return this.allowedAccountModel.find();
  }

  async findOne(email: string, isActive: boolean = true): Promise<AllowedAccount> {
    return this.allowedAccountModel.findOne({email, isActive}).lean();
  }

  async findOneWithoutIsActive(email: string): Promise<AllowedAccount> {
    return this.allowedAccountModel.findOne({email}).lean();
  }

  async create(allowedAccountDefinition: AllowedAccount): Promise<AllowedAccountDocument> {
    const existingAllowedAccount = await this.findOne(allowedAccountDefinition.email);

    if (existingAllowedAccount) {
      this.logger.warn(EMAIL_ALREADY_EXISTS_ERROR, existingAllowedAccount.email);
      return;
    }

    const allowedAccountToCreate = new this.allowedAccountModel(allowedAccountDefinition);

    return allowedAccountToCreate.save();
  }

  async updateOne(
    allowedAccount: AllowedAccount
  ): Promise<AllowedAccountDocument> {
    return this.allowedAccountModel.findOneAndUpdate(
      { email: allowedAccount.email },
      allowedAccount
    );
  }


  async filterAccounts(allowedAccounts: AllowedAccountsFromFile[]): Promise<AllowedAccount[]> {
    const accounts: AllowedAccount[] = [];
    for (const {
      'Adresse courriel': email,
      'Rôle': role,
      'Juridiction': jurisdiction,
      'Actif': isActive,
      'Référent principal': pointOfContact,
      'Prénom': name,
      'Nom': surname
    } of allowedAccounts) {
      // We eliminate accounts without email address
      if (!email) {
        continue;
      }

      // We eliminate accounts with jurisdictions that can't be added and assign France to accounts without jurisdictions
      const jurisdictions: Types.ObjectId[] = [];
      if (!jurisdiction.trim()) {
        const franceJurisdiction = await this.jurisdictionsService.findOne('France');
        jurisdictions.push(franceJurisdiction._id);
      }
      if (jurisdiction.includes(',')) {
        for (const juri of jurisdiction.split(', ')) {
          const jurisdictionId = await this.handleJurisdiction(juri);
          jurisdictions.push(jurisdictionId);
        }
      } else {
        const jurisdictionId = await this.handleJurisdiction(jurisdiction);
        jurisdictions.push(jurisdictionId);
      }
      if (jurisdictions.length === 0) {
        continue;
      }
      // We assign the Guest role to accounts without roles
      const roles: Role[] = [];
      if (role.includes(',')) {
        roles.push(...role.split(', ').map(this.mapRole));
      } else {
        roles.push(this.mapRole(role));
      }

      let fullName: string = ``;
      if (name) {
        fullName += `${name}`;
      }
      if (surname) {
        fullName += ` ${surname.toUpperCase()}`;
      }

      const emailsSplit = email.split(';');
      accounts.push(
        ...emailsSplit.map((emailSplit: string) => (
          {
            email: emailSplit.trim().toLowerCase(),
            roles,
            jurisdictions,
            isActive: isActive === 'true',
            pointOfContact: pointOfContact === 'true',
            fullName: fullName.length ? fullName : undefined
          }
        ))
      );
    }
    return accounts;
  }

  async getPointsOfContactInformation(region: string, department: string): Promise<AllowedAccount[]> {
    const currentRegion = await this.jurisdictionsService.findOne(region);
    const currentDepartment = await this.jurisdictionsService.findOne(department);

    return this.allowedAccountModel.find({
      jurisdictions: { $in: [currentDepartment, currentRegion] },
      pointOfContact: true,
      roles: { $in: [Role.ProjectTeam] }
    }).lean();
  }

  private async handleJurisdiction(jurisdiction: string): Promise<Types.ObjectId> {
    const foundJurisdiction = await this.jurisdictionsService.findOne(jurisdiction);
    if (foundJurisdiction) {
      return foundJurisdiction._id;
    } else {
      const departmentToAdd = DEPARTMENTS.find(department => jurisdiction === department.dep_name);
      if (departmentToAdd) {
        return await this.jurisdictionsService.createAndOrReturnId({
          type: JurisdictionType.Department,
          name: jurisdiction,
          initialZoom: 9,
          referenceCoordinates: departmentToAdd.coordinates
        });
      }
    }
  }

  private mapRole(role: string): Role {
    switch (role) {
      case 'Équipe projet':
        return Role.ProjectTeam;
      case 'ANCT':
        return Role.ANCT;
      case 'Invité':
        return Role.Guest;
      case 'EPCI':
        return Role.EPCI;
      case 'Opérateur':
        return Role.MobileNetworkProvider;
      case 'Porteur de projet (RIP)':
        return Role.ProjectHolder;
      default:
        return Role.Guest;
    }
  }
}
