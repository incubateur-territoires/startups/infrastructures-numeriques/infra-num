/**
 * Nest.js imports
 */
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

/**
 * 3rd-party imports
 */
import { HydratedDocument, Schema as MongooseSchema, Types } from 'mongoose';
import {
  AllowedAccount as AllowedAccountInterface,
  Jurisdiction,
  Role
} from '@infra-num/common';


/**
 * TypeScript entities and constants
 */
export type AllowedAccountDocument = HydratedDocument<AllowedAccount>;

@Schema({
  collection: 'allowed-accounts'
})
export class AllowedAccount implements AllowedAccountInterface {
  @Prop({required: true, unique: true})
  email: string;

  @Prop({type: [String], enum: Role, required: true})
  roles: Role[];

  @Prop({
    type: [MongooseSchema.Types.ObjectId],
    ref: 'Jurisdiction',
    required: true
  })
  jurisdictions: Jurisdiction[] | Types.ObjectId[];

  @Prop({default: false})
  isActive: boolean;

  @Prop({default: false})
  pointOfContact: boolean;

  @Prop()
  fullName?: string;
}

export const AllowedAccountSchema = SchemaFactory.createForClass(AllowedAccount);