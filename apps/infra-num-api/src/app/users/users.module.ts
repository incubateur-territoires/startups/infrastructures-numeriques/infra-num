/**
 * Nest.js imports
 */
import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { JwtModule } from '@nestjs/jwt';

/**
 * Internal imports
 */
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { User, UserSchema } from './user.schema';
import { Jurisdiction, JurisdictionSchema } from './jurisdictions/jurisdiction.schema';
import { JurisdictionsService } from './jurisdictions/jurisdictions.service';
import { AllowedAccountsService } from './allowed-accounts/allowed-accounts.service';
import { AllowedAccount, AllowedAccountSchema } from './allowed-accounts/allowed-account.schema';
import { EmailsModule } from '../emails/emails.module';
import { environment } from '../../environments/environment';
import { ListmonkApiModule } from '../listmonk-api/listmonk-api.module';
import { GristApiModule } from '../grist-api/grist-api.module';
import { GeoApiModule } from '../geo-api/geo-api.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      {name: User.name, schema: UserSchema},
      {name: Jurisdiction.name, schema: JurisdictionSchema},
      {name: AllowedAccount.name, schema: AllowedAccountSchema}
    ]),
    EmailsModule,
    JwtModule.register({
      secret: process.env.INFRANUM_JWT_SECRET,
      signOptions: {expiresIn: environment.jwtExpirationTimes.confirmationEmail}
    }),
    ListmonkApiModule,
    GeoApiModule,
    forwardRef(() => GristApiModule)
  ],
  controllers: [UsersController],
  providers: [
    UsersService,
    JurisdictionsService,
    AllowedAccountsService
  ],
  exports: [UsersService, JurisdictionsService, AllowedAccountsService]
})
export class UsersModule {
}
