/**
 * 3rd-party imports
 */
import { JurisdictionType, Role } from '@infra-num/common';

/**
 * Internal imports
 */
import { Jurisdiction, JurisdictionDocument } from '../jurisdictions/jurisdiction.schema';
import { AllowedAccount, AllowedAccountDocument } from '../allowed-accounts/allowed-account.schema';

/**
 * TypeScript entities and constants
 */
const jurisdiction1: Jurisdiction = {
  type: JurisdictionType.City,
  name: 'Paris',
  initialZoom: 12,
  code: '75',
  referenceCoordinates: [42, 42]
};
const jurisdiction2: Jurisdiction = {
  type: JurisdictionType.Department,
  name: 'Haute-Garonne',
  initialZoom: 9,
  code: '31',
  referenceCoordinates: [42, 42]
};

const allowedAccount1: AllowedAccount = {
  email: 'test2@test.com',
  roles: [Role.Mayor],
  jurisdictions: [jurisdiction1],
  isActive: true,
  pointOfContact: false
};

const allowedAccount2: AllowedAccount = {
  email: 'test3@test.com',
  roles: [Role.ProjectTeam],
  jurisdictions: [jurisdiction2],
  isActive: true,
  pointOfContact: false
};

export const JURISDICTIONS_ALREADY_SAVED: JurisdictionDocument[] = [
  jurisdiction1, jurisdiction2
] as JurisdictionDocument[];

export const ALLOWED_ACCOUNTS_ALREADY_SAVED: AllowedAccountDocument[] = [
  allowedAccount1, allowedAccount2
] as AllowedAccountDocument[];

export const USERS_ALREADY_SAVED = [
  {
    ...allowedAccount1,
    hasSubscribedToNewsletter: false,
    password: 'Password1234!'
  },
  {
    ...allowedAccount2,
    hasSubscribedToNewsletter: false,
    password: 'WeakPassword'
  }
];
