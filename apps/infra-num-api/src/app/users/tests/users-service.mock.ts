export class UsersServiceMock {
  readAll: jest.Mock = jest.fn();
  create: jest.Mock = jest.fn();
  findOne: jest.Mock = jest.fn();
}
