export class AllowedAccountsServiceMock {
  create: jest.Mock = jest.fn();
  findOne: jest.Mock = jest.fn();
  readAll: jest.Mock = jest.fn();
  getPointsOfContactInformation: jest.Mock = jest.fn();
}