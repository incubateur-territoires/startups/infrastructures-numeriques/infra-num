export class JurisdictionsServiceMock {
  findOne: jest.Mock = jest.fn();
  create: jest.Mock = jest.fn();
  createManyAndOrReturnIds: jest.Mock = jest.fn();
  createAndOrReturnId: jest.Mock = jest.fn();
}
