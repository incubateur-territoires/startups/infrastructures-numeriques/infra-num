/**
 * Nest.js imports
 */
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';

/**
 * 3rd-party imports
 */
import request from 'supertest';
import { JurisdictionType, Role, UserToCreate } from '@infra-num/common';

/**
 * Internal imports
 */
import { UsersService } from '../users.service';
import { UsersServiceMock } from './users-service.mock';
import { USERS_ALREADY_SAVED } from './already-saved.mock';
import { UsersController } from '../users.controller';

describe('Users', () => {
  let app: INestApplication;
  let usersService: UsersServiceMock;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        {
          provide: UsersService,
          useClass: UsersServiceMock
        }
      ]
    })
      .compile();

    usersService = moduleRef.get<UsersServiceMock>(UsersService);

    app = moduleRef.createNestApplication();
    await app.init();
  });

  // it(`GET /users`, () => {
  //   usersService.readAll.mockResolvedValue(USERS_ALREADY_SAVED);
  //
  //   return request(app.getHttpServer())
  //     .get('/users')
  //     .expect(200)
  //     .expect(USERS_ALREADY_SAVED);
  // });

  it(`POST /users`, () => {

    const userCreated: UserToCreate = {
      email: 'test5@test.com',
      roles: [Role.ANCT],
      hasSubscribedToNewsletter: false,
      jurisdictions: [{
        type: JurisdictionType.City,
        name: 'Troyes',
        initialZoom: 12,
        referenceCoordinates: [10, 10]
      }],
      password: 'Azerty'
    };

    usersService.create.mockResolvedValue(userCreated);

    return request(app.getHttpServer())
      .post('/users')
      .expect(201)
      .expect(userCreated);
  });

  afterAll(async () => {
    await app.close();
  });
});
