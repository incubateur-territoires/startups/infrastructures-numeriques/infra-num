/**
 * Nest.js imports
 */
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

/**
 * Internal imports
 */
import { ReportsController } from './reports.controller';
import { ReportsService } from './reports.service';
import { Report, ReportSchema } from './report.schema';
import { EmailsModule } from '../emails/emails.module';
import { UsersModule } from '../users/users.module';
import { User, UserSchema } from '../users/user.schema';
import { Jurisdiction, JurisdictionSchema } from '../users/jurisdictions/jurisdiction.schema';
import { StudyPoint, StudyPointSchema } from '../study-points/study-point.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {name: User.name, schema: UserSchema},
      {name: Jurisdiction.name, schema: JurisdictionSchema},
      {name: Report.name, schema: ReportSchema},
      {name: StudyPoint.name, schema: StudyPointSchema},
    ]),
    EmailsModule,
    UsersModule
  ],
  controllers: [ReportsController],
  providers: [
    ReportsService
  ]
})
export class ReportsModule {
}
