/**
 * Nest.js imports
 */
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

/**
 * 3rd-party imports
 */
import { HydratedDocument, Schema as MongooseSchema, Types } from 'mongoose';
import {
  ReportLocation,
  MobileOperator,
  ReportUnavailability,
  Report as ReportInterface,
  ReportStatus,
  SafeUser,
  CityHallContact,
  ReportStatusInfo, ReportModification
} from '@infra-num/common';

/**
 * TypeScript entities and constants
 */
export type ReportDocument = HydratedDocument<Report>;

@Schema()
export class Report implements ReportInterface {
  @Prop({ type: String, required: true })
  latitude: string;

  @Prop({ type: String, required: true })
  longitude: string;

  @Prop({
    type: String,
    required: true,
    validate: {
      validator: (zipCode: string): boolean =>
        zipCode.length === 5,
      message: 'The zip code must have exactly 5 characters'
    }
  })
  zipCode: string;

  @Prop({ type: String, required: true })
  city: string;

  @Prop({ type: String, required: true })
  department: string;

  @Prop({ type: String, required: true })
  region: string;

  @Prop({ type: [{ type: String, enum: ReportLocation }], required: true })
  locations: ReportLocation[];

  @Prop({
    type: [{ type: String, enum: ReportUnavailability }],
    required: true
  })
  unavailabilities: ReportUnavailability[];

  @Prop({ type: [{ type: String, enum: MobileOperator }], required: true })
  operators: MobileOperator[];

  @Prop({ type: String, required: true })
  description: string;

  @Prop({ type: String, enum: ReportStatus, default: ReportStatus.NotTreated })
  status: ReportStatus;

  @Prop({
    type: [{
      _id : false,
      comment: String,
      changeDate: Date,
      status: { type: String, enum: ReportStatus},
      oldStatus: { type: String, enum: ReportStatus},
      changeAuthor: {
        type: MongooseSchema.Types.ObjectId,
        ref: 'User',
        required: true
      }
    }]
  })
  statusInfo?: ReportStatusInfo[];

  @Prop({
    type: MongooseSchema.Types.ObjectId,
    ref: 'User',
    required: true
  })
  author: SafeUser | Types.ObjectId;

  @Prop({ type: Date, default: new Date() })
  creationDate: Date;

  @Prop({
    type: {
      _id : false,
      date: Date,
      author: {
        type: MongooseSchema.Types.ObjectId,
        ref: 'User',
        required: true
      }
    }
  })
  modification?: ReportModification;

  @Prop({ type: String })
  oldPlatformContact?: string;

  @Prop({
    type: {
      _id : false,
      fullName: String,
      phone: String,
      email: String,
    }
  })
  cityHallContact?: CityHallContact;

}
export const ReportSchema = SchemaFactory.createForClass(Report);
