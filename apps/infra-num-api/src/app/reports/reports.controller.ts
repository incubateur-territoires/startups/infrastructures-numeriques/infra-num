/**
 * Nest.js imports
 */
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  Req,
  UseGuards
} from '@nestjs/common';

/**
 * 3rd-party imports
 */
import {
  Report,
  ReportStatus,
  ReportWithId,
  SafeUser
} from '@infra-num/common';
import { Request } from 'express';

/**
 * Internal imports
 */
import { ReportsService } from './reports.service';
import { JwtAuthGuard } from '../authentication/guards/jwt-auth.guard';
import { ApiKeyAuthGuard } from '../authentication/guards/api-key-auth.guard';

@Controller('reports')
export class ReportsController {
  constructor(private readonly reportsService: ReportsService) {}

  @UseGuards(JwtAuthGuard)
  @Get('/count')
  getCount(@Query('conditions') conditions: string): Promise<number> {
    return this.reportsService.getNumberOfReports(conditions);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  getLayer(
    @Query('start', ParseIntPipe) startIndex: number,
    @Query('size', ParseIntPipe) layerSize: number
  ): Promise<Report[]> {
    return this.reportsService.getLayer(startIndex, layerSize);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/:id')
  async getOne(@Param('id') id: string): Promise<any> {
    return this.reportsService.findOne(id);
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  async createReport(
    @Body() report: Report,
    @Req() request: Request & { user: SafeUser }
  ): Promise<Report> {
    return this.reportsService.create(report, request.user);
  }

  @UseGuards(JwtAuthGuard)
  @Put('/status')
  updateMultipleReportsStatus(
    @Body() {ids, status, statusComment}: { ids: string[], status: ReportStatus, statusComment?: string },
    @Req() request: Request & { user: SafeUser }
  ): Promise<Report[]> {
    return this.reportsService.updateMultipleStatuses(ids, status, request.user, statusComment);
  }

  @UseGuards(JwtAuthGuard)
  @Put('/:id')
  async updateReport(
    @Param('id') id: string,
    @Body() report: Partial<Report>,
    @Req() request: Request & { user: SafeUser }
  ): Promise<Report> {
    return this.reportsService.update(id, report, request.user);
  }

  @UseGuards(JwtAuthGuard)
  @Put('/:id/status')
  updateReportStatus(
    @Param('id') id: string,
    @Body() {status, statusComment}: { status: ReportStatus, statusComment?: string },
    @Req() request: Request & { user: SafeUser }
  ): Promise<Report> {
    return this.reportsService.updateStatus(id, status, request.user, statusComment);
  }

  @UseGuards(JwtAuthGuard)
  @Delete('/:id')
  deleteReport(
    @Param('id') id: string,
    @Req() request: Request & { user: SafeUser }
  ): Promise<ReportWithId> {
    return this.reportsService.delete(id, request.user);
  }

  @UseGuards(ApiKeyAuthGuard)
  @Post('/notifications')
  async sendWeeklyNotifications(): Promise<void> {
    const lastWeekReportsByJurisdiction =
      await this.reportsService.getLastWeekReportsByJurisdiction();
    for (const {reports, users} of lastWeekReportsByJurisdiction) {
      await this.reportsService.sendEmail(
        users.map(({email}) => email),
        reports
      );
    }
  }
}
