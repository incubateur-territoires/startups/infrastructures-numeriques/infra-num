/**
 * 3rd-party imports
 */
import {
  Jurisdiction,
  JurisdictionType, MobileOperator,
  PossibleCoordinates,
  Report,
  ReportLocation,
  ReportStatus,
  ReportUnavailability,
  Role, SafeUser
} from '@infra-num/common';
import { Types } from 'mongoose';

/**
 * TypeScript entities and constants
 */
const jurisdictions: Jurisdiction[] = [{
  type: JurisdictionType.City,
  name: 'Rennes',
  initialZoom: 12,
  referenceCoordinates: [48.1159, -1.6884]
}];

const author: SafeUser = {
  _id: 'fake_id' as unknown as Types.ObjectId,
  email: 'test@gouv.fr',
  roles: [Role.Mayor],
  jurisdictions,
  hasSubscribedToNewsletter: false,
  preferredCoordinates: PossibleCoordinates.GPS
};
export const REPORTS_ALREADY_SAVED: Report[] = [
  {
    city: 'Rennes',
    zipCode: '35000',
    latitude: '48.1159',
    longitude: '-1.6884',
    locations: [ReportLocation.Inside],
    unavailabilities: [ReportUnavailability.InternetAccess],
    operators: [MobileOperator.BouyguesTelecom, MobileOperator.SFR, MobileOperator.Free],
    description: 'Internet not available inside ...',
    status: ReportStatus.NotTreated,
    author,
    creationDate: new Date(),
    department: 'Ille-et-Vilaine',
    region: 'Bretagne'
  }
];
