export class ReportsServiceMock {
  getLayer: jest.Mock = jest.fn();
  create: jest.Mock = jest.fn();
}
