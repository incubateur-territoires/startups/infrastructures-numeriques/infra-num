/**
 * Nest.js imports
 */
import { Test } from '@nestjs/testing';
import { ExecutionContext, INestApplication } from '@nestjs/common';

/**
 * 3rd-party imports
 */
import request from 'supertest';
import { MobileOperator, Report, ReportLocation, ReportStatus, ReportUnavailability } from '@infra-num/common';
import { Types } from 'mongoose';

/**
 * Internal imports
 */
import { ReportsService } from '../reports.service';
import { ReportsServiceMock } from './reports-service.mock';
import { REPORTS_ALREADY_SAVED } from './already-saved.mock';
import { ReportsController } from '../reports.controller';
import { JwtStrategy } from '../../authentication/strategies/jwt.strategy';
import { JwtAuthGuard } from '../../authentication/guards/jwt-auth.guard';

describe('Reports', () => {
  let app: INestApplication;
  let reportsService: ReportsServiceMock;
  const guardMock = (context: ExecutionContext) => {
    const req = context.switchToHttp().getRequest();
    req.user = {email: 'test@gouv.fr'};
    return true;
  };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [ReportsController],
      providers: [
        {
          provide: ReportsService,
          useClass: ReportsServiceMock
        },
        JwtStrategy
      ]
    })
      .overrideGuard(JwtAuthGuard)
      .useValue({canActivate: guardMock})
      .compile();

    reportsService = moduleRef.get<ReportsServiceMock>(ReportsService);

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it(`GET /reports`, () => {
    const reports = REPORTS_ALREADY_SAVED;
    reportsService.getLayer.mockResolvedValue(reports);

    return request(app.getHttpServer())
      .get('/reports?start=0&size=5')
      .expect(200)
      .expect(({body}) => {
        const {
          creationDate: expectedCreationDate,
          ...expectedWithoutCreationDate
        } = reports[0];
        expect(body[0]).toEqual({
          creationDate: expectedCreationDate.toISOString(),
          ...expectedWithoutCreationDate
        });
      });
  });

  it(`POST /reports`, () => {
    const reportToCreate: Report = {
      city: 'Rennes',
      zipCode: '35000',
      latitude: '48.1159',
      longitude: '-1.6884',
      locations: [ReportLocation.Inside],
      unavailabilities: [ReportUnavailability.InternetAccess],
      operators: [MobileOperator.BouyguesTelecom, MobileOperator.SFR, MobileOperator.Free],
      description: 'Internet not available inside ...',
      status: ReportStatus.NotTreated,
      author: new Types.ObjectId('638482480beeec3335caa6b6'),
      department: 'Ille-et-Vilaine',
      region: 'Bretagne'
    };

    reportsService.create.mockResolvedValue(reportToCreate);

    return request(app.getHttpServer())
      .post('/reports')
      .expect(201)
      .expect(({body}) => {
        const {
          author: expectedAuthor,
          ...expectedWithoutAuthor
        } = reportToCreate;
        expect(body).toEqual({
          author: expectedAuthor.toString(),
          ...expectedWithoutAuthor
        });
      });
  });

  afterAll(async () => {
    await app.close();
  });
});
