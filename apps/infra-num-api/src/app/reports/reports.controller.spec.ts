/**
 * Nest.js imports
 */
import { Test, TestingModule } from '@nestjs/testing';

/**
 * 3rd-party imports
 */
import { Report } from '@infra-num/common';

/**
 * Internal imports
 */
import { ReportsController } from './reports.controller';
import { REPORTS_ALREADY_SAVED } from './tests/already-saved.mock';
import { ReportsService } from './reports.service';
import { ReportsServiceMock } from './tests/reports-service.mock';

describe('ReportsController', () => {
  let controller: ReportsController;
  let reportsService: ReportsServiceMock;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReportsController],
      providers: [{
        provide: ReportsService,
        useClass: ReportsServiceMock
      }]
    }).compile();

    controller = module.get<ReportsController>(ReportsController);
    reportsService = module.get<ReportsServiceMock>(ReportsService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should return an array of reports when getLayer() is called', async () => {
    const reportsAlreadySaved: Report[] = REPORTS_ALREADY_SAVED;
    reportsService.getLayer.mockResolvedValue(reportsAlreadySaved);

    const reportsRetrieved: Report[] = await controller.getLayer(0, 5);

    expect(reportsService.getLayer).toHaveBeenCalled();
    expect(reportsRetrieved).toEqual(reportsAlreadySaved);
  });
});
