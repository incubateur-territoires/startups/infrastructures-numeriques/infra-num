/**
 * Nest.js imports
 */
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

/**
 * 3rd-party imports
 */
import { Model, PopulateOptions } from 'mongoose';
import {
  CAN_CREATE_REPORT_ROLES,
  CityHallContact,
  Report as ReportInterface,
  ReportStatus,
  ReportStatusInfo,
  ReportWithId,
  Role,
  SafeUser,
  StudyPoint
} from '@infra-num/common';

/**
 * Internal imports
 */
import { Report, ReportDocument } from './report.schema';
import { SAFE_USER_FIELDS, User, UserDocument } from '../users/user.schema';
import {
  EmailDefinition,
  EmailTemplates,
  getEmailTemplate
} from '../emails/templates/get-email-template';
import { EmailsService } from '../emails/emails.service';
import { UsersService } from '../users/users.service';
import {
  Jurisdiction as JurisdictionSchemaClass,
  JurisdictionDocument
} from '../users/jurisdictions/jurisdiction.schema';
import {
  StudyPoint as StudyPointSchemaClass,
  StudyPointDocument
} from '../study-points/study-point.schema';

/**
 * We need async on the methods here, even though we don't use await
 * in the body of the method, so that the Mongoose Query object
 * actually returned by the Model's methods, can be converted to
 * a Promise
 * @link https://mongoosejs.com/docs/queries.html#queries-are-not-promises
 */
@Injectable()
export class ReportsService {
  private reportPopulateOptions: PopulateOptions[] = [
    {
      path: 'author',
      select: SAFE_USER_FIELDS,
      populate: { path: 'jurisdictions' }
    },
    {
      path: 'modification',
      populate: {
        path: 'author',
        select: SAFE_USER_FIELDS,
        populate: { path: 'jurisdictions' }
      }
    },
    {
      path: 'statusInfo',
      populate: {
        path: 'changeAuthor',
        select: SAFE_USER_FIELDS,
        populate: { path: 'jurisdictions' }
      }
    }
  ];

  constructor(
    @InjectModel(Report.name) private reportModel: Model<ReportDocument>,
    @InjectModel(JurisdictionSchemaClass.name)
    private jurisdictionModel: Model<JurisdictionDocument>,
    @InjectModel(User.name) private userModel: Model<UserDocument>,
    @InjectModel(StudyPointSchemaClass.name)
    private studyPointModel: Model<StudyPointDocument>,
    private readonly emailsService: EmailsService,
    private readonly usersService: UsersService
  ) {}

  async getNumberOfReports(conditions: string): Promise<number> {
    return this.reportModel.countDocuments(
      conditions ? JSON.parse(conditions) : {}
    );
  }

  async getLayer(
    startIndex: number,
    layerSize: number
  ): Promise<ReportInterface[]> {
    return this.reportModel
      .find({
        status: { $ne: ReportStatus.Deleted }
      })
      .skip(startIndex)
      .limit(layerSize)
      .populate(this.reportPopulateOptions)
      .lean();
  }

  async findOne(id: string): Promise<ReportInterface> {
    return this.reportModel
      .findOne({ _id: id })
      .populate({
        path: 'author',
        select: SAFE_USER_FIELDS,
        populate: { path: 'jurisdictions' }
      })
      .lean();
  }

  async create(
    reportDefinition: ReportInterface,
    authenticatedUser: SafeUser
  ): Promise<ReportInterface> {
    if (
      !authenticatedUser.roles?.some((role) =>
        CAN_CREATE_REPORT_ROLES.includes(role)
      )
    ) {
      throw new UnauthorizedException();
    }

    const report = await this.reportModel.create(reportDefinition);

    /* Add user city hall contact in user preferences property */
    if (reportDefinition.cityHallContact) {
      await this.updateUserContactPreferences(
        authenticatedUser,
        reportDefinition.cityHallContact
      );
    }

    return report.populate(this.reportPopulateOptions);
  }

  async update(
    id: string,
    reportDefinition: Partial<ReportInterface>,
    authenticatedUser: SafeUser
  ): Promise<ReportInterface> {
    if (
      authenticatedUser._id !== reportDefinition.author._id &&
      !authenticatedUser.roles.includes(Role.ANCT)
    ) {
      throw new UnauthorizedException();
    }

    /* Add user city hall contact in user preferences property */
    if (reportDefinition.cityHallContact) {
      await this.updateUserContactPreferences(
        authenticatedUser,
        reportDefinition.cityHallContact
      );
    }

    // Update "modification" property
    reportDefinition.modification = {
      date: new Date(),
      author: authenticatedUser
    };

    return this.reportModel
      .findByIdAndUpdate(id, reportDefinition, { new: true })
      .populate(this.reportPopulateOptions)
      .lean();
  }

  async updateStatus(
    id: string,
    status: ReportStatus,
    authenticatedUser: SafeUser,
    statusComment?: string
  ): Promise<ReportInterface> {
    const selectedReport: ReportWithId = await this.reportModel.findById(id);
    const listOfJurisdictionsNames: string[] = [];

    for (let jurisdiction of authenticatedUser.jurisdictions) {
      listOfJurisdictionsNames.push(jurisdiction.name);
    }

    if (
      !authenticatedUser.roles.includes(Role.ANCT) &&
      !(
        authenticatedUser.roles.includes(Role.ProjectTeam) &&
        (listOfJurisdictionsNames.includes(selectedReport.department) ||
          listOfJurisdictionsNames.includes(selectedReport.region))
      )
    ) {
      throw new UnauthorizedException();
    }

    const statusInfo: ReportStatusInfo = {
      status,
      oldStatus: selectedReport.status,
      changeDate: new Date(),
      changeAuthor: authenticatedUser._id
    };

    if (statusComment) {
      statusInfo.comment = statusComment;
    }

    const updatedReport = await this.reportModel
      .findByIdAndUpdate(
        id,
        {
          $set: {
            status,
            modification: {
              date: new Date(),
              author: authenticatedUser
            }
          },
          $push: {
            statusInfo: statusInfo
          }
        },
        { new: true }
      )
      .populate(this.reportPopulateOptions)
      .lean();

    // Send an email notification of the change in status of a report
    await this.sendEmailNotificationOfChangedStatus(updatedReport);

    return updatedReport;
  }

  async updateMultipleStatuses(
    ids: string[],
    status: ReportStatus,
    authenticatedUser: SafeUser,
    statusComment?: string
  ): Promise<ReportInterface[]> {
    const listOfJurisdictionsNames: string[] = [];

    for (let jurisdiction of authenticatedUser.jurisdictions) {
      listOfJurisdictionsNames.push(jurisdiction.name);
    }

    const reports = await this.reportModel.find({ _id: { $in: ids } }).lean();
    const reportsRegions = reports.map(({ region }) => region);
    const reportsDepartments = reports.map(({ department }) => department);

    if (
      !authenticatedUser.roles.includes(Role.ANCT) &&
      !(
        authenticatedUser.roles.includes(Role.ProjectTeam) &&
        (reportsRegions.every((region) =>
          listOfJurisdictionsNames.includes(region)
        ) ||
          reportsDepartments.every((department) =>
            listOfJurisdictionsNames.includes(department)
          ))
      )
    ) {
      throw new UnauthorizedException();
    }

    const statusInfo: ReportStatusInfo = {
      status,
      oldStatus: reports[0].status,
      changeDate: new Date(),
      changeAuthor: authenticatedUser._id
    };

    if (statusComment) {
      statusInfo.comment = statusComment;
    }

    await this.reportModel.updateMany(
      {
        _id: {
          $in: ids
        }
      },
      {
        $set: {
          status,
          modification: {
            date: new Date(),
            author: authenticatedUser
          }
        },
        $push: {
          statusInfo
        }
      }
    );

    const updatedReports = await this.reportModel
      .find({ _id: { $in: ids } })
      .populate(this.reportPopulateOptions)
      .lean();

    // Send an email notification of the change in status of a report
    for (const report of updatedReports) {
      await this.sendEmailNotificationOfChangedStatus(report);
    }

    return updatedReports;
  }

  private async sendEmailNotificationOfChangedStatus(
    updatedReport: ReportWithId
  ): Promise<void> {
    if (!updatedReport.author) {
      return;
    }
    const isReportFromOldPlatform: boolean = !!updatedReport.oldPlatformContact;
    const recipient: string =
      updatedReport.oldPlatformContact ??
      (updatedReport.author as SafeUser).email;
    const lastReportStatusInfo: ReportStatusInfo =
      updatedReport.statusInfo.filter(
        (statusInfo) => statusInfo.status === updatedReport.status
      )[0];

    let emailDefinition: EmailDefinition;
    const reportInfo = {
      zipCode: updatedReport.zipCode,
      city: updatedReport.city,
      oldReport: isReportFromOldPlatform
    };

    switch (updatedReport.status) {
      case ReportStatus.Treated:
        emailDefinition = getEmailTemplate(
          EmailTemplates.REPORT_UPDATED_TO_TREATED,
          [recipient],
          {
            report: {
              ...reportInfo,
              reportStatusInfo: lastReportStatusInfo
            }
          }
        );
        break;
      case ReportStatus.Validated:
        const studyPoint: StudyPoint = await this.studyPointModel.findOne({
          report: updatedReport._id
        });
        emailDefinition = getEmailTemplate(
          EmailTemplates.REPORT_UPDATED_TO_VALIDATED,
          [recipient],
          {
            studyPoint,
            report: {
              ...reportInfo,
              reportStatusInfo: lastReportStatusInfo
            }
          }
        );
        break;
      case ReportStatus.Desultory:
        emailDefinition = getEmailTemplate(
          EmailTemplates.REPORT_UPDATED_TO_DESULTORY,
          [recipient],
          {
            report: {
              ...reportInfo,
              reportStatusInfo: lastReportStatusInfo
            }
          }
        );
        break;
      case ReportStatus.Obsolete:
        emailDefinition = getEmailTemplate(
          EmailTemplates.REPORT_UPDATED_TO_OBSOLETE,
          [recipient],
          {
            report: { ...reportInfo }
          }
        );
        break;
    }

    await this.emailsService.sendEmail(emailDefinition);
  }

  async getLastWeekReportsByJurisdiction(): Promise<
    {
      users: { email: string }[];
      jurisdiction: { _id: string; type: string; name: string };
      reports: { zipCode: string; city: string }[];
    }[]
  > {
    // Retrieve reports within the last 7 days
    const reports = await this.reportModel
      .find({
        creationDate: {
          $gte: new Date(Date.now() - 7 * 24 * 60 * 60 * 1000)
        }
      })
      .select('zipCode city department region')
      .lean();

    // Initialize the result array
    const result: {
      users: { email: string }[];
      jurisdiction: { _id: string; type: string; name: string };
      reports: { zipCode: string; city: string }[];
    }[] = [];

    // Iterate over each report
    for (const report of reports) {
      // Find the jurisdictions related to the report
      const jurisdictions = await this.jurisdictionModel
        .find({
          $or: [
            { type: 'France', name: 'France' },
            { type: 'Region', name: report.region },
            { type: 'Department', name: report.department }
          ]
        })
        .select('_id type name')
        .lean();

      // Iterate over each jurisdiction for this report
      for (const jurisdiction of jurisdictions) {
        // Have I already met this jurisdiction in a previous report ?
        let existingResult = result.find(
          (item) =>
            item.jurisdiction._id.toString() === jurisdiction._id.toString()
        );

        if (!existingResult) {
          // If no existing result found for jurisdiction, create a new entry

          // Find users with 'ProjectTeam' role in the jurisdiction
          const users = await this.userModel
            .find({
              jurisdictions: jurisdiction._id,
              roles: { $in: ['ProjectTeam'] }
            })
            .select('email')
            .lean();

          existingResult = {
            jurisdiction: {
              _id: jurisdiction._id.toString(),
              name: jurisdiction.name,
              type: jurisdiction.type
            },
            reports: [],
            users: [...users]
          };
          result.push(existingResult);
        }

        // Add the current report to the existing result for this jurisdiction
        existingResult.reports.push({
          zipCode: report.zipCode,
          city: report.city
        });
      }
    }

    return result;
  }

  async delete(id: string, authenticatedUser: SafeUser): Promise<ReportWithId> {
    const selectedReport = await this.reportModel.findById(id);
    if (
      !selectedReport ||
      (authenticatedUser._id.toString() !==
        selectedReport.author._id.toString() &&
        !authenticatedUser.roles.includes(Role.ANCT))
    ) {
      throw new UnauthorizedException();
    }

    return this.reportModel.findByIdAndDelete(id, { lean: true });
  }

  async sendEmail(
    to: string[],
    reports: { zipCode: string; city: string }[]
  ): Promise<void> {
    const emailDefinition = getEmailTemplate(
      EmailTemplates.REPORTS_CREATION,
      to,
      { reports }
    );
    await this.emailsService.sendEmail(emailDefinition);
  }

  private async updateUserContactPreferences(
    authenticatedUser: SafeUser,
    cityHallContact: CityHallContact
  ): Promise<void> {
    await this.usersService.updateOneById(authenticatedUser._id, {
      preferences: {
        defaultReportContact: cityHallContact
      }
    });
  }
}
