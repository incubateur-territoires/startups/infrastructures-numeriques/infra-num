/**
 * Nest.js imports
 */
import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';

/**
 * Internal imports
 */
import { ReportsService } from './reports.service';
import { ModelMock } from '../shared/tests/model.mock';
import { Report } from './report.schema';
import { EmailsService } from '../emails/emails.service';
import { EmailsServiceMock } from '../emails/tests/emails-service.mock';
import { REPORTS_ALREADY_SAVED } from './tests/already-saved.mock';

describe('ReportsService', () => {
  let reportsService: ReportsService;
  let reportModel: typeof ModelMock;

  beforeEach(async () => {
    const modelReportToken: string = getModelToken(Report.name);

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ReportsService,
        {
          provide: modelReportToken,
          useValue: ModelMock
        }, {
          provide: EmailsService,
          useClass: EmailsServiceMock
        }
      ]
    }).compile();

    reportsService = module.get<ReportsService>(ReportsService);
    reportModel = module.get<typeof ModelMock>(modelReportToken);
  });

  it('should be defined', () => {
    expect(reportsService).toBeDefined();
  });

  it('should return the list of all requested reports when getLayer is called', async () => {
    const reportsAlreadySaved = REPORTS_ALREADY_SAVED;
    reportModel.find
      .mockReturnValue({
        skip: jest.fn()
          .mockReturnValue({
            limit: jest.fn()
              .mockReturnValue({
                populate: jest.fn()
                  .mockReturnValue({
                    lean: jest.fn()
                      .mockResolvedValue(reportsAlreadySaved)
                  })
              })
          })
      });

    const reportsRetrieved = await reportsService.getLayer(0, 5);

    expect(reportModel.find).toHaveBeenCalled();
    expect(reportsRetrieved).toEqual(reportsAlreadySaved);
  });
});
