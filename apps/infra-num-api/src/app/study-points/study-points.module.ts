/**
 * Nest.js imports
 */
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

/**
 * Internal imports
 */
import { StudyPoint, StudyPointSchema } from './study-point.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {name: StudyPoint.name, schema: StudyPointSchema},
    ])
  ]
})
export class StudyPointsModule {
}
