/**
 * Nest.js imports
 */
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

/**
 * 3rd-party imports
 */
import {HydratedDocument, Schema as MongooseSchema, Types} from 'mongoose';
import {
  CoverStatus,
  MobileOperator, Report,
  StudyPoint as StudyPointInterface
} from '@infra-num/common';

/**
 * TypeScript entities and constants
 */
export type StudyPointDocument = HydratedDocument<StudyPoint>;

@Schema({
  collection: 'study-points'
})
export class StudyPoint implements StudyPointInterface {
  @Prop({type: String, required: true})
  batch: string;

  @Prop({type: String, required: true})
  zoneId: string;

  @Prop({type: String, required: true})
  zoneName: string;

  @Prop({type: String, required: true})
  pointId: string;

  @Prop({type: String, required: true})
  pointName: string;

  @Prop({type: String, required: true})
  latitude: string;

  @Prop({type: String, required: true})
  longitude: string;

  @Prop({type: String, required: true})
  department: string;

  @Prop({type: String, required: true})
  region: string;

  @Prop({type: String, enum: CoverStatus, required: true})
  BYTCoverStatus: CoverStatus;

  @Prop({type: String, enum: CoverStatus, required: true})
  FRMCoverStatus: CoverStatus;

  @Prop({type: String, enum: CoverStatus, required: true})
  ORFCoverStatus: CoverStatus;

  @Prop({type: String, enum: CoverStatus, required: true})
  SFRCoverStatus: CoverStatus;

  @Prop({type: [{type: String, enum: MobileOperator}], required: true})
  operatorsConcerned: MobileOperator[];

  @Prop({type: Number, required: true})
  sitesNeededForZone: number;

  @Prop({type: String})
  siteId?: string;

  @Prop({type: String})
  comment?: string;

  @Prop({type: Date, required: true})
  validityLimitDate: Date;

  @Prop({type: Date, default: new Date()})
  creationDate: Date;

  @Prop({type: Date})
  modificationDate?: Date;

  @Prop({
    type: MongooseSchema.Types.ObjectId,
    ref: 'Report'
  })
  report?: Report | Types.ObjectId;
}

export const StudyPointSchema = SchemaFactory.createForClass(StudyPoint);
