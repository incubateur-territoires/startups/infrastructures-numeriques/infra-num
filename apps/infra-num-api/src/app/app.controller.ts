/**
 * Nest.js imports
 */
import { Controller, Get } from '@nestjs/common';

/**
 * Internal imports
 */
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService
  ) {
  }

  @Get()
  getData() {
    return this.appService.getData();
  }
}
