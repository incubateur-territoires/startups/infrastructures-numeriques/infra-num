import { Test, TestingModule } from '@nestjs/testing';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { EmailsService } from './emails/emails.service';

describe('AppController', () => {
  let app: TestingModule;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService, EmailsService],
    }).compile();
  });

  describe('getData', () => {
    it('should return "Welcome to infra-num-api!"', () => {
      const appController = app.get<AppController>(AppController);
      expect(appController.getData()).toEqual({
        message: 'Welcome to infra-num-api! 👋',
      });
    });
  });
});
