/**
 * Nest.js imports
 */
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

/**
 * Internal imports
 */
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ServicePublicApiModule } from './service-public-api/service-public-api.module';
import { UsersModule } from './users/users.module';
import { GeoApiModule } from './geo-api/geo-api.module';
import { AuthenticationModule } from './authentication/authentication.module';
import { ReportsModule } from './reports/reports.module';
import { AdresseApiModule } from './adresse-api/adresse-api.module';
import { GristApiModule } from './grist-api/grist-api.module';
import { StudyPointsModule } from './study-points/study-points.module';

@Module({
  imports: [
    MongooseModule.forRoot(
      `mongodb://${process.env.MONGODB_INFRANUM_ROOT_USER}:${process.env.MONGODB_INFRANUM_ROOT_PASSWORD_URLENCODED}@${process.env.MONGODB_INFRANUM_HOST}`,
      {
        tls: true,
        tlsCAFile: process.env.MONGODB_INFRANUM_TLS_CA_FILE,
        tlsCertificateKeyFile: process.env.MONGODB_INFRANUM_TLS_CERT_FILE
      }
    ),
    UsersModule,
    ServicePublicApiModule,
    GeoApiModule,
    AuthenticationModule,
    ReportsModule,
    AdresseApiModule,
    GristApiModule,
    StudyPointsModule
  ],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {
}
