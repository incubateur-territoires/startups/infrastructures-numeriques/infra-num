/**
 * Nest.js imports
 */
import { Injectable, Logger } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';

/**
 * 3rd-party imports
 */
import { catchError, firstValueFrom, of } from 'rxjs';
import { AxiosError } from 'axios';

interface ListMonkSubscriber {
  email: string;
  name: string;
  status: 'enabled' | 'disabled' | 'blocklisted';
  lists?: number[];
  attribs?: Record<string, unknown>;
  preconfirm_subscriptions?: boolean;
}

@Injectable()
export class ListmonkApiService {
  private readonly logger = new Logger(ListmonkApiService.name);

  private listmonkApiUrl: string =
    'https://listmonk.infrastructures-numeriques.incubateur.anct.gouv.fr';

  constructor(private readonly httpService: HttpService) {}

  async createSubscriber(subscriber: ListMonkSubscriber): Promise<void> {
    await firstValueFrom(
      this.httpService
        .post(`${this.listmonkApiUrl}/api/subscribers`, subscriber, {
          auth: {
            username: process.env.LISTMONK_INFRANUM_USERNAME,
            password: process.env.LISTMONK_INFRANUM_PASSWORD
          }
        })
        .pipe(
          catchError((error: AxiosError) => {
            if (error.response.status === 409) {
              return of('Error 409');
            }
            this.logger.error(error.response.data);
            throw error;
          })
        )
    );
  }
}
