/**
 * Nest.js imports
 */
import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';

/**
 * Internal imports
 */
import { ListmonkApiService } from './listmonk-api.service';

@Module({
  imports: [HttpModule],
  providers: [ListmonkApiService],
  exports: [ListmonkApiService]
})
export class ListmonkApiModule {}
