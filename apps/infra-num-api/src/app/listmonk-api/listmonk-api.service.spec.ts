/**
 * Nest.js imports
 */
import { Test, TestingModule } from '@nestjs/testing';
import { HttpService } from '@nestjs/axios';

/**
 * Internal imports
 */
import { ListmonkApiService } from './listmonk-api.service';

/**
 * Creating mocks
 */
class HttpServiceMock {
  post: jest.Mock = jest.fn();
}

describe('ListmonkService', () => {
  let listMonkApiService: ListmonkApiService;
  let httpService: HttpServiceMock;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ListmonkApiService,
        {
          provide: HttpService,
          useClass: HttpServiceMock
        }
      ]
    }).compile();

    listMonkApiService = module.get<ListmonkApiService>(ListmonkApiService);
    httpService = module.get<HttpServiceMock>(HttpService);
  });

  it('should be defined', () => {
    expect(listMonkApiService).toBeDefined();
  });
});
