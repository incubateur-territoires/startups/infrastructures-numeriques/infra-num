/**
 * Nest.js imports
 */
import { Injectable, Logger } from '@nestjs/common';

/**
 * 3rd-party imports
 */
import {
  SESv2Client,
  SendEmailCommand,
  SendEmailCommandInput,
  SendEmailCommandOutput
} from '@aws-sdk/client-sesv2';

/**
 * Internal imports
 */
import { AWS_REGION } from '../../../serverless';
import { environment } from '../../environments/environment';
import {
  EmailDefinition,
  EmailHtmlParameters
} from './templates/get-email-template';

const fromEmailAddress: string = 'tous-connectes@anct.gouv.fr';

@Injectable()
export class EmailsService {
  private readonly logger = new Logger(EmailsService.name);

  async sendEmail(
    definition: EmailDefinition
  ): Promise<SendEmailCommandOutput> {
    const client = new SESv2Client({ region: AWS_REGION });
    const params: SendEmailCommandInput = {
      FromEmailAddress: fromEmailAddress,
      Destination: {
        ToAddresses: definition.to
      },
      Content: {
        Simple: {
          Subject: {
            Data: definition.subject,
            Charset: 'UTF-8'
          },
          Body: {
            Text: { Data: definition.body.textVersion, Charset: 'UTF-8' },
            Html: {
              Data: this.createEmailHtmlContent(definition.body.htmlVersion),
              Charset: 'UTF-8'
            }
          }
        }
      }
    };
    const command = new SendEmailCommand(params);

    try {
      return await client.send(command);
    } catch (error) {
      this.logger.error(error);
      throw error;
    }
  }

  private createEmailHtmlContent(params: EmailHtmlParameters): string {
    return `
      <!DOCTYPE html>
        <html lang="en">
          <head>
            <title>
              ${params.title}
            </title>
            <meta charset="utf-8">
            <meta content="width=device-width" name="viewport">
            <meta content="IE=edge" http-equiv="X-UA-Compatible">
            <meta name="x-apple-disable-message-reformatting">
            <meta content="telephone=no,address=no,email=no,date=no,url=no" name="format-detection">
            <title>
              Courriel inscription
            </title><!--[if mso]>
                        <style>
                            * {
                                font-family: sans-serif !important;
                            }
                        </style>
                    <![endif]-->
            <!--[if !mso]><!-->
            <!-- <![endif]-->

            <style>
            html {
                margin: 0 !important;
                padding: 0 !important;
            }

            * {
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%;
            }
            td {
                vertical-align: top;
                mso-table-lspace: 0pt !important;
                mso-table-rspace: 0pt !important;
            }
            a {
                text-decoration: none;
            }
            img {
                -ms-interpolation-mode:bicubic;
            }
            @media only screen and (min-device-width: 320px) and (max-device-width: 374px) {
                u ~ div .email-container {
                    min-width: 320px !important;
                }
            }
            @media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
                u ~ div .email-container {
                    min-width: 375px !important;
                }
            }
            @media only screen and (min-device-width: 414px) {
                u ~ div .email-container {
                    min-width: 414px !important;
                }
            }
            </style><!--[if gte mso 9]>
                    <xml>
                        <o:OfficeDocumentSettings>
                            <o:AllowPNG/>
                            <o:PixelsPerInch>96</o:PixelsPerInch>
                        </o:OfficeDocumentSettings>
                    </xml>
                    <![endif]-->

            <style>
            @media only screen and (max-device-width: 599px), only screen and (max-width: 599px) {

                .eh {
                    height:auto !important;
                }
                .desktop {
                    display: none !important;
                    height: 0 !important;
                    margin: 0 !important;
                    max-height: 0 !important;
                    overflow: hidden !important;
                    padding: 0 !important;
                    visibility: hidden !important;
                    width: 0 !important;
                }
                .mobile {
                    display: block !important;
                    width: auto !important;
                    height: auto !important;
                    float: none !important;
                }
                    .email-container {
                        width: 100% !important;
                        margin: auto !important;
                    }
                    .stack-column,
                    .stack-column-center {
                        display: block !important;
                        width: 100% !important;
                        max-width: 100% !important;
                        direction: ltr !important;
                    }
                    .wid-auto {
                        width:auto !important;
                    }
                    .table-w-full-mobile {
                        width: 100%;
                    }
                    .center-on-narrow {
                        text-align: center !important;
                        display: block !important;
                        margin-left: auto !important;
                        margin-right: auto !important;
                        float: none !important;
                    }
                    table.center-on-narrow {
                        display: inline-block !important;
                    }

                }
            </style>
          </head>
          <body style="background-color:#404040;margin:0;padding:0!important;mso-line-height-rule:exactly;">
            <div style="background-color:#404040">
              <!--[if gte mso 9]>
            <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
            <v:fill type="tile" color="#404040"/>
            </v:background>
            <![endif]-->
              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                  <td align="center" valign="top">
                    <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="email-container" id="brick_container" style="margin:0 auto;" width="600">
                      <tr>
                        <td width="600">
                          <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                              <td bgcolor="#FFFFFF" width="600">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tr>
                                    <td width="100%">
                                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                          <td width="100%">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                              <tr>
                                                <td bgcolor="#FFFFFF" width="100%">
                                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                      <td style="vertical-align: middle; padding-left:32px; padding-right:32px;" width="100%">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                          <tr>
                                                            <td style="vertical-align: middle;" width="76">
                                                              <table border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                  <td width="76">
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                      <tr>
                                                                        <td height="12" style="height:12px; min-height:12px; line-height:12px;"></td>
                                                                      </tr>
                                                                      <tr>
                                                                        <td>
                                                                          <table border="0" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                              <td width="33">
                                                                                <img alt='République française - Liberté, Égalité, Fraternité' border="0" id='institution' src='${environment.frontUrl}/assets/illustrations/Logo_marque.png' style="min-width:33px; width:33px; height: auto; display: block;" width="33">
                                                                              </td>
                                                                            </tr>
                                                                          </table>
                                                                        </td>
                                                                      </tr>
                                                                      <tr>
                                                                        <td height="4" style="height:4px; min-height:4px; line-height:4px;"></td>
                                                                      </tr>
                                                                      <tr>
                                                                        <td>
                                                                          <div style="line-height:12px;text-align:left;">
                                                                            <span style="color:#161616;font-weight:700;font-family:Marianne,Arial,sans-serif;font-size:12px;text-transform:uppercase;letter-spacing:0.05999999865889549px;line-height:12px;text-align:left;">république<br>
                                                                            française</span>
                                                                          </div>
                                                                        </td>
                                                                      </tr>
                                                                      <tr>
                                                                        <td height="4" style="height:4px; min-height:4px; line-height:4px;"></td>
                                                                      </tr>
                                                                      <tr>
                                                                        <td>
                                                                          <table border="0" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                              <td width="32">
                                                                                <img border="0" src="${environment.frontUrl}/assets/illustrations/FR_devise.png" style="min-width:32px; width:32px; height: auto; display: block;" width="32">
                                                                              </td>
                                                                            </tr>
                                                                          </table>
                                                                        </td>
                                                                      </tr>
                                                                      <tr>
                                                                        <td height="12" style="height:12px; min-height:12px; line-height:12px;"></td>
                                                                      </tr>
                                                                    </table>
                                                                  </td>
                                                                </tr>
                                                              </table>
                                                            </td>
                                                            <td></td>
                                                            <td style="vertical-align: middle;" align="right">
                                                              <table border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                  <td width="164">
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                      <tr>
                                                                        <td width="164">
                                                                          <img border="0" src="${environment.frontUrl}/assets/illustrations/Logo_ANCT.png" style="min-width:164px; width:164px; height: auto; display: block;" width="164">
                                                                        </td>
                                                                      </tr>
                                                                    </table>
                                                                  </td>
                                                                </tr>
                                                              </table>
                                                            </td>
                                                          </tr>
                                                        </table>
                                                      </td>
                                                    </tr>
                                                    <tr>
                                                      <td align="center" style="vertical-align: middle; padding-left:16px; padding-right:16px;" width="100%">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                          <tr>
                                                            <td align="center" style="vertical-align: middle;" width="568">
                                                              <img border="0" src="${environment.frontUrl}/assets/illustrations/hr_border.png" style="width: 100%; height: auto; display: block;" width="568">
                                                            </td>
                                                          </tr>
                                                        </table>
                                                      </td>
                                                    </tr>
                                                    <tr>
                                                      <td>
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                          <tr>
                                                            <td style=" padding-left:32px; padding-right:32px;">
                                                              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                  <td height="11" style="height:11px; min-height:11px; line-height:11px;"></td>
                                                                </tr>
                                                                <tr>
                                                                  <td>
                                                                    <div style="line-height:28px;text-align:left;">
                                                                      <span style="color:#161616;font-weight:700;font-family:Marianne,Arial,sans-serif;font-size:20px;line-height:28px;text-align:left;">${environment.appName}</span>
                                                                    </div>
                                                                  </td>
                                                                </tr>
                                                                <tr>
                                                                  <td height="12" style="height:12px; min-height:12px; line-height:12px;"></td>
                                                                </tr>
                                                              </table>
                                                            </td>
                                                          </tr>
                                                        </table>
                                                      </td>
                                                    </tr>
                                                  </table>
                                                </td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td width="100%">
                                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                          <td bgcolor="#E8EDFF" style="vertical-align: middle; padding-left:32px; padding-right:32px;" width="100%">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                              <tr>
                                                <td height="24" style="height:24px; min-height:24px; line-height:24px;"></td>
                                              </tr>
                                              <tr>
                                                <td style="vertical-align: middle;">
                                                  <div style="line-height:48px;text-align:left;">
                                                    <span style="color:#000000;font-weight:700;font-family:Marianne,Arial,sans-serif;font-size:40px;line-height:48px;text-align:left;">${params.title}</span>
                                                  </div>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td height="24" style="height:24px; min-height:24px; line-height:24px;"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                  ${params.mainContent}
                                  <tr>
                                    <td width="100%">
                                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                          <td style="vertical-align: middle; padding-left:32px; padding-right:32px;" width="100%">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                              <tr>
                                                <td style="vertical-align: middle;">
                                                  <div style="line-height:48px;text-align:left;">
                                                    <div style="line-height:20px;line-height:20px;">
                                                      <span style="color:#666666;font-weight:700;font-family:Marianne,Arial,sans-serif;font-size:12px;line-height:20px;text-align:left;"></span> <span style="color:#666666;font-family:Marianne,Arial,sans-serif;font-size:12px;line-height:20px;text-align:left;">Contact : ${fromEmailAddress}</span>
                                                    </div>
                                                  </div>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td height="24" style="height:24px; min-height:24px; line-height:24px;"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </div>
          </body>
        </html>
    `;
  }
}
