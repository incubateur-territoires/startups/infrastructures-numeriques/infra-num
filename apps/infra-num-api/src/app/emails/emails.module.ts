/**
 * Nest.js imports
 */
import { Module } from '@nestjs/common';

/**
 * Internal imports
 */
import { EmailsService } from './emails.service';

@Module({
  providers: [EmailsService],
  exports: [EmailsService]
})
export class EmailsModule {}
