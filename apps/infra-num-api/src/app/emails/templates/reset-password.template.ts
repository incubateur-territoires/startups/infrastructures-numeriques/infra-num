import { EmailDefinition } from './get-email-template';

export const generateEmailDefinition = (to: string[], link: string): EmailDefinition => {
  return {
    to,
    subject: 'Réinitialisation du mot de passe',
    body: {
      textVersion:
        'Pour réinitialiser votre mot de passe, copier et coller ce lien dans votre navigateur :' +
        link + '.\n',
      htmlVersion: {
        title: 'Réinitialisation du mot de passe',
        mainContent: `
        <tr>
            <td style=" padding-left:32px; padding-right:32px;" width="100%">
              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                  <td height="32" style="height:32px; min-height:32px; line-height:32px;"></td>
                </tr>
                <tr>
                  <td>
                    <div style="line-height:24px;text-align:left;">
                      <span style="color:#000000;font-family:Marianne,Arial,sans-serif;font-size:16px;line-height:24px;text-align:left;">
                        Pour réinitialiser votre mot de passe, cliquez sur ce bouton :
                      </span>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td height="24" style="height:24px; min-height:24px; line-height:24px;"></td>
                </tr>
                <tr>
                  <td>
                    <table border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td>
                          <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                              <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                  <tr>
                                    <td align="center" bgcolor="#000091" style="vertical-align: middle; height:40px; padding-left:16px; padding-right:16px;">
                                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                          <td height="8" style="height:8px; min-height:8px; line-height:8px;"></td>
                                        </tr>
                                        <tr>
                                          <td align="center" style="vertical-align: middle;">
                                            <div style="line-height:24px;text-align:center;">
                                              <a style="color:#f5f5fe;font-weight:500;font-family:Marianne,Arial,sans-serif;font-size:16px;line-height:24px;text-align:center;"
                                                href='${link}'
                                                target='_blank'
                                                rel='noopener'
                                                title='Réinitialiser votre mot de passe - ouvre une nouvelle fenêtre dans votre navigateur'>
                                                Réinitialiser
                                              </a>
                                            </div>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td height="8" style="height:8px; min-height:8px; line-height:8px;"></td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td height="24" style="height:24px; min-height:24px; line-height:24px;"></td>
                </tr>
                <tr>
                  <td>
                    <div style="line-height:20px;text-align:left;">
                      <span style="color:#666666;font-family:Marianne,Arial,sans-serif;font-size:12px;line-height:20px;text-align:left;">
                      Vous pouvez également copier et coller ce lien dans votre navigateur pour réinitialiser votre mot de passe :
                      </span>
                      <span style="color:#666666;font-weight:700;font-family:Marianne,Arial,sans-serif;font-size:12px;line-height:20px;text-align:left;word-break:break-word;">
                        ${link}
                      </span>
                    </div>
                    <div style="height:24px;line-height:24px;font-size:24px;">
                      &nbsp;
                    </div>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        `
      }
    }
  };
};
