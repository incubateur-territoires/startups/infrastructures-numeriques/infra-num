/**
 *  Internal imports
 */
import { environment } from '../../../environments/environment';

/**
 *  3rd-party imports
 */
import { ReportStatusInfo, SafeUser, setInstitutionByAuthor } from '@infra-num/common';

/**
 * TypeScript entities and constants
 */
export const formatDate = (date: Date) => {
  const yyyy = date.getFullYear();
  let mm: number | string = date.getMonth() + 1;
  let dd: number | string = date.getDate();

  if (dd < 10) dd = '0' + dd;
  if (mm < 10) mm = '0' + mm;

  return dd + '/' + mm + '/' + yyyy;
};

export const REPORT_UPDATED_INTRODUCTION_TEXT = `
  Votre signalement concernant une mauvaise couverture de réseau de téléphonie mobile a été mis à jour :
  <br><br>
`;

export const OLD_REPORT_UPDATED_INTRODUCTION_TEXT = `
  Vous aviez réalisé un signalement concernant une mauvaise couverture de réseau de téléphonie mobile sur
  l’ancienne plateforme France Mobile, qui avait depuis été importé sur la plateforme
  <a href="${environment.frontUrl}"><strong><u>Toutes et tous connecté·e·s</u></strong></a>.
  <br><br>
  Nous vous informons que ce sigalement a fait l’objet d’une mise à jour sur la plateforme :
  <br><br>
`;

export const REPORT_UPDATED_BOTTOM_TEXT = `Cliquez ici pour retrouver votre signalement :`;

export const OLD_REPORT_UPDATED_BOTTOM_TEXT = `
  Vous pouvez retrouver votre signalement sur la plateforme, en cliquant sur le lien ci-dessous.
  Si vous n’avez pas encore créé de compte, il vous sera nécessaire d’en créer un pour accéder à
  la plateforme <a href="${environment.frontUrl}/connexion"><u>(créer un compte)</u></a>.
`;

export const formatStatusComment = (reportStatusInfo: ReportStatusInfo) => {

  return `
    <div style="background-color: #EEEEEE; border-radius: 4px; border-left: 4px solid #6A6AF4;padding: 16px 16px 24px 20px">
      <span style="color: #3A3A3A; font-size: 14px;">
        <strong>Commentaire</strong>
        <br><br>
        ${reportStatusInfo.comment}
        <br><br>
      </span>
      <span style="color: #666666; font-size: 12px;">
        Le ${formatDate(reportStatusInfo.changeDate)}, 
        par ${setInstitutionByAuthor(reportStatusInfo.changeAuthor as SafeUser)}
      </span>
    </div>
  `;
}
