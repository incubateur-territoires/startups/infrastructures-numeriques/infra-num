/**
 * 3rd-party imports
 */
import { EMAIL_TEMPLATE_NOT_FOUND } from '@infra-num/common';

/**
 * Internal imports
 */
import { generateEmailDefinition as generateAccountConfirmationTemplate } from './account-confirmation.template';
import { generateEmailDefinition as generateResetPasswordTemplate } from './reset-password.template';
import { generateEmailDefinition as generateReportsCreationTemplate } from './reports-creation.template';
import { generateEmailDefinition as generateReportUpdatedToTreatedTemplate } from './report-updated/report-updated-to-treated.template';
import { generateEmailDefinition as generateReportUpdatedToObsoleteTemplate } from './report-updated/report-updated-to-obsolete.template';
import { generateEmailDefinition as generateReportUpdatedToValidatedTemplate } from './report-updated/report-updated-to-validated.template';
import { generateEmailDefinition as generateReportUpdatedToDesultoryTemplate } from './report-updated/report-updated-to-desultory.template';

/**
 * TypeScript entities and constants
 */
export interface EmailDefinition {
  to: string[];
  subject: string;
  body: EmailBody;
}

interface EmailBody {
  textVersion: string;
  htmlVersion: EmailHtmlParameters;
}

export interface EmailHtmlParameters {
  title: string;
  mainContent: string;
}

export enum EmailTemplates {
  ACCOUNT_CONFIRMATION = 'ACCOUNT_CONFIRMATION',
  RESET_PASSWORD = 'RESET_PASSWORD',
  REPORTS_CREATION = 'REPORTS_CREATION',
  REPORT_UPDATED_TO_TREATED = 'REPORT_UPDATED_TO_TREATED',
  REPORT_UPDATED_TO_VALIDATED = 'REPORT_UPDATED_TO_VALIDATED',
  REPORT_UPDATED_TO_DESULTORY = 'UPDATE_REPORT_TO_DESULTORY',
  REPORT_UPDATED_TO_OBSOLETE = 'REPORT_UPDATED_TO_OBSOLETE'
}

export const getEmailTemplate = (template: EmailTemplates, to: string[], data: any): EmailDefinition => {
  switch (template) {
    case EmailTemplates.ACCOUNT_CONFIRMATION:
      return generateAccountConfirmationTemplate(to, data.sendFor, data.link);
    case EmailTemplates.RESET_PASSWORD:
      return generateResetPasswordTemplate(to, data.link);
    case EmailTemplates.REPORTS_CREATION:
      return generateReportsCreationTemplate(to, data.reports);
    case EmailTemplates.REPORT_UPDATED_TO_TREATED:
      return generateReportUpdatedToTreatedTemplate(to, data.report);
    case EmailTemplates.REPORT_UPDATED_TO_VALIDATED:
      return generateReportUpdatedToValidatedTemplate(to, data);
    case EmailTemplates.REPORT_UPDATED_TO_DESULTORY:
      return generateReportUpdatedToDesultoryTemplate(to, data.report);
    case EmailTemplates.REPORT_UPDATED_TO_OBSOLETE:
      return generateReportUpdatedToObsoleteTemplate(to, data.report);
    default:
      throw EMAIL_TEMPLATE_NOT_FOUND;
  }
};
