/**
 * 3rd-party imports
 */
import { ReportStatusInfo } from '@infra-num/common';

/**
 * Internal imports
 */
import { EmailDefinition } from '../get-email-template';
import { environment } from '../../../../environments/environment';
import {
  formatStatusComment,
  OLD_REPORT_UPDATED_BOTTOM_TEXT,
  OLD_REPORT_UPDATED_INTRODUCTION_TEXT,
  REPORT_UPDATED_BOTTOM_TEXT,
  REPORT_UPDATED_INTRODUCTION_TEXT
} from '../common';

export const generateEmailDefinition = (
  to: string[],
  report: {
    zipCode: string,
    city: string,
    oldReport: boolean,
    reportStatusInfo: ReportStatusInfo
  }
  ): EmailDefinition => {
  return {
    to,
    subject: `Signalement mis à jour`,
    body: {
      textVersion:
        'Votre signalement concernant une mauvaise couverture de réseau de téléphonie mobile a été mis à jour :' +
        'Le signalement est abandonné.',
      htmlVersion: {
        title: 'Signalement mis à jour',
        mainContent: `
          <table
            width="100%"
            cellpadding="0"
            cellspacing="0"
            border="0"
          >
            <tr>
              <td valign="top" align="center">
                <table
                  bgcolor="#ffffff"
                  style="margin: 0 auto"
                  align="center"
                  id="brick_container"
                  cellspacing="0"
                  cellpadding="0"
                  border="0"
                  width="600"
                  class="email-container"
                >
                  <tr>
                    <td width="600">
                      <table
                        cellspacing="0"
                        cellpadding="0"
                        border="0"
                      >
                        <tr>
                          <td
                            width="600"
                            align="center"
                            style="background-color: #ffffff"
                            bgcolor="#ffffff"
                          >
                            <table
                              width="100%"
                              border="0"
                              cellpadding="0"
                              cellspacing="0"
                            >
                              <tr>
                                <td width="100%">
                                  <table
                                    width="100%"
                                    cellspacing="0"
                                    cellpadding="0"
                                    border="0"
                                  >
                                    <tr>
                                      <td
                                        width="100%"
                                        style="
                                          background-color: #ffffff;
                                        "
                                        bgcolor="#ffffff"
                                      >
                                        <table
                                          width="100%"
                                          border="0"
                                          cellpadding="0"
                                          cellspacing="0"
                                        >
                                          <tr>
                                            <td
                                              width="100%"
                                              style="
                                                padding-left: 32px;
                                                padding-right: 32px;
                                              "
                                            >
                                              <table
                                                width="100%"
                                                border="0"
                                                cellpadding="0"
                                                cellspacing="0"
                                              >
                                                <tr>
                                                  <td
                                                    height="32"
                                                    style="
                                                      height: 32px;
                                                      min-height: 32px;
                                                      line-height: 32px;
                                                    "
                                                  ></td>
                                                </tr>
                                                <tr>
                                                  <td>
                                                    <div style="
                                                        line-height: 24px;
                                                        text-align: left;
                                                      ">
                                                      <span
                                                        style="
                                                          color: #000000;
                                                          font-family: Marianne,
                                                            Arial,
                                                            sans-serif;
                                                          font-size: 16px;
                                                          line-height: 24px;
                                                          text-align: left;
                                                        ">
                                                        Bonjour, 
                                                        <br><br>
                                                        
                                                        ${report.oldReport ? OLD_REPORT_UPDATED_INTRODUCTION_TEXT : REPORT_UPDATED_INTRODUCTION_TEXT}
                                                        
                                                        <strong>Le signalement est abandonné.</strong>
                                                        <br><br>
                                                      </span>
                                                    </div>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td>
                                                    ${formatStatusComment(report.reportStatusInfo)}
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td
                                                    height="24"
                                                    style="
                                                      height: 24px;
                                                      min-height: 24px;
                                                      line-height: 24px;
                                                    "
                                                  ></td>
                                                </tr>
                                                <tr>
                                                  <td>
                                                    <div
                                                      style="
                                                        line-height: 24px;
                                                        text-align: left;
                                                      "
                                                    >
                                                      <span
                                                        style="
                                                          color: #000000;
                                                          font-family: Marianne,
                                                            Arial,
                                                            sans-serif;
                                                          font-size: 16px;
                                                          line-height: 24px;
                                                          text-align: left;
                                                        "
                                                      >
                                                        ${report.oldReport ? OLD_REPORT_UPDATED_BOTTOM_TEXT : REPORT_UPDATED_BOTTOM_TEXT}
                                                        
                                                      </span>
                                                    </div>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td
                                                    height="24"
                                                    style="
                                                      height: 24px;
                                                      min-height: 24px;
                                                      line-height: 24px;
                                                    "
                                                  ></td>
                                                </tr>
                                                <tr>
                                                  <td>
                                                    <table
                                                      width="100%"
                                                      border="0"
                                                      cellpadding="0"
                                                      cellspacing="0"
                                                    >
                                                      <tr>
                                                        <td
                                                          width="536"
                                                        >
                                                          <table
                                                            width="100%"
                                                            border="0"
                                                            cellpadding="0"
                                                            cellspacing="0"
                                                          >
                                                            <tr>
                                                              <td
                                                                width="49%"
                                                              >
                                                                <table
                                                                  width="100%"
                                                                  cellspacing="0"
                                                                  cellpadding="0"
                                                                  border="0"
                                                                >
                                                                  <tr>
                                                                    <td
                                                                      width="49%"
                                                                      style="
                                                                        vertical-align: middle;
                                                                        background-color: #ffffff;
                                                                        border-radius: 4px;
                                                                        border: 1px
                                                                          solid
                                                                          #dddddd;
                                                                        padding-left: 16px;
                                                                        padding-right: 16px;
                                                                      "
                                                                      bgcolor="#ffffff"
                                                                    >
                                                                      <table
                                                                        width="100%"
                                                                        border="0"
                                                                        cellpadding="0"
                                                                        cellspacing="0"
                                                                      >
                                                                        <tr>
                                                                          <td
                                                                            height="8"
                                                                            style="
                                                                              height: 8px;
                                                                              min-height: 8px;
                                                                              line-height: 8px;
                                                                            "
                                                                          ></td>
                                                                        </tr>
                                                                        <tr>
                                                                          <td
                                                                            style="
                                                                              vertical-align: middle;
                                                                            "
                                                                            width="100%"
                                                                          >
                                                                            <table
                                                                              cellspacing="0"
                                                                              cellpadding="0"
                                                                              border="0"
                                                                            >
                                                                              <tr>
                                                                                <td
                                                                                  width="100%"
                                                                                >
                                                                                  <table
                                                                                    width="100%"
                                                                                    border="0"
                                                                                    cellpadding="0"
                                                                                    cellspacing="0"
                                                                                  >
                                                                                    <tr>
                                                                                      <td>
                                                                                        <a href="${environment.frontUrl}">
                                                                                        <div
                                                                                          style="
                                                                                            line-height: 24px;
                                                                                            text-align: left;
                                                                                          "
                                                                                        >
                                                                                          <span
                                                                                            style="
                                                                                              color: #161616;
                                                                                              font-family: Marianne,
                                                                                                Arial,
                                                                                                sans-serif;
                                                                                              font-size: 16px;
                                                                                              line-height: 24px;
                                                                                              text-align: left;
                                                                                            "
                                                                                            >${report.zipCode} </span
                                                                                          ><span
                                                                                            style="
                                                                                              color: #161616;
                                                                                              font-weight: 700;
                                                                                              font-family: Marianne,
                                                                                                Arial,
                                                                                                sans-serif;
                                                                                              font-size: 16px;
                                                                                              line-height: 24px;
                                                                                              text-align: left;
                                                                                            "
                                                                                            >${report.city}</span
                                                                                          >
                                                                                        </div>
                                                                                        </a>
                                                                                      </td>
                                                                                    </tr>
                                                                                  </table>
                                                                                </td>
                                                                              </tr>
                                                                            </table>
                                                                          </td>
                                                                          <td></td>
                                                                          <td
                                                                            style="
                                                                              vertical-align: middle;
                                                                            "
                                                                            width="40"
                                                                          >
                                                                            <table
                                                                              cellspacing="0"
                                                                              cellpadding="0"
                                                                              border="0"
                                                                            >
                                                                              <tr>
                                                                                <td
                                                                                  width="40"
                                                                                >
                                                                                  <table
                                                                                    width="100%"
                                                                                    border="0"
                                                                                    cellpadding="0"
                                                                                    cellspacing="0"
                                                                                  >
                                                                                    <tr>
                                                                                      <td
                                                                                        style="
                                                                                          padding-left: 8px;
                                                                                          padding-right: 8px;
                                                                                        "
                                                                                      >
                                                                                        <table
                                                                                          width="100%"
                                                                                          border="0"
                                                                                          cellpadding="0"
                                                                                          cellspacing="0"
                                                                                        >
                                                                                          <tr>
                                                                                            <td
                                                                                              width="24"
                                                                                            >
                                                                                              <a href="${environment.frontUrl}">
                                                                                                <svg
                                                                                                  xmlns="http://www.w3.org/2000/svg"
                                                                                                  viewBox="0 0 24 24"
                                                                                                  width="24"
                                                                                                  height="24"
                                                                                                  fill="#000091"
                                                                                                >
                                                                                                  <path
                                                                                                    d="M18.364 3.636a9 9 0 0 1 0 12.728L12 22.728l-6.364-6.364A9 9 0 0 1 18.364 3.636ZM7.05 5.051a7 7 0 0 0 0 9.899L12 19.9l4.95-4.95a7 7 0 0 0-9.9-9.9ZM12 8a2 2 0 1 1 0 4 2 2 0 0 1 0-4Z"
                                                                                                  />
                                                                                                </svg>
                                                                                              </a>
                                                                                            </td>
                                                                                          </tr>
                                                                                        </table>
                                                                                      </td>
                                                                                    </tr>
                                                                                  </table>
                                                                                </td>
                                                                              </tr>
                                                                            </table>
                                                                          </td>
                                                                        </tr>
                                                                        <tr>
                                                                          <td
                                                                            height="8"
                                                                            style="
                                                                              height: 8px;
                                                                              min-height: 8px;
                                                                              line-height: 8px;
                                                                            "
                                                                          ></td>
                                                                        </tr>
                                                                      </table>
                                                                    </td>
                                                                  </tr>
                                                                </table>
                                                              </td>
                                                              <td
                                                                style="
                                                                  width: 8px;
                                                                  min-width: 8px;
                                                                "
                                                                width="8"
                                                              ></td>
                                                            </tr>
                                                          </table>
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td
                                                          height="8"
                                                          style="
                                                            height: 8px;
                                                            min-height: 8px;
                                                            line-height: 8px;
                                                          "
                                                        ></td>
                                                      </tr>
                                                    </table>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td
                                                    height="24"
                                                    style="
                                                      height: 24px;
                                                      min-height: 24px;
                                                      line-height: 24px;
                                                    "
                                                  ></td>
                                                </tr>
                                                <tr>
                                                  <td>
                                                    <div
                                                      style="
                                                        line-height: 24px;
                                                        text-align: left;
                                                      "
                                                    >
                                                      <span
                                                        style="
                                                          color: #000000;
                                                          font-family: Marianne,
                                                            Arial,
                                                            sans-serif;
                                                          font-size: 16px;
                                                          line-height: 24px;
                                                          text-align: left;
                                                        "
                                                      >
                                                      L’équipe de la plateforme <a href="${environment.frontUrl}">Toutes et tous connecté·e·s</a> vous remercie.
                                                      </span>
                                                    </div>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td
                                                    height="24"
                                                    style="
                                                      height: 24px;
                                                      min-height: 24px;
                                                      line-height: 24px;
                                                    "
                                                  ></td>
                                                </tr>
                                              </table>
                                            </td>
                                          </tr>
                                        </table>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        `
      }
    }
  };
};
