/**
 * Internal imports
 */
import { EmailDefinition } from '../get-email-template';
import { environment } from '../../../../environments/environment';
import {
  formatStatusComment,
  OLD_REPORT_UPDATED_BOTTOM_TEXT,
  OLD_REPORT_UPDATED_INTRODUCTION_TEXT, REPORT_UPDATED_BOTTOM_TEXT,
  REPORT_UPDATED_INTRODUCTION_TEXT
} from '../common';

/**
 * 3rd-party imports
 */
import { CoverStatus, ReportStatusInfo, StudyPoint } from '@infra-num/common';

const getTemplateByCoverStatus = (coverStatus: CoverStatus) => {
  let textColor: string, backgroundColor: string, label: string;
  switch (coverStatus) {
    case CoverStatus.CL:
      textColor = '#66673d';
      backgroundColor = '#fceeac';
      label = 'COUVERTURE LIMITÉE';
      break;
    case CoverStatus.BC:
      textColor = '#447049';
      backgroundColor = '#c9fcac';
      label = 'BONNE COUVERTURE';
      break;
    case CoverStatus.TBC:
      textColor = '#297254';
      backgroundColor = '#c3fad5';
      label = 'TRÈS BONNE COUVERTURE';
      break;
    case CoverStatus.NC:
      textColor = '#8d533e';
      backgroundColor = '#fee9e6';
      label = 'NON COUVERT';
      break;
  }

  return `
  <span style="
    font-size: 12px;
    font-weight: bold;
    display: inline-block;
    padding: 5px;
    color: ${textColor};
    background-color: ${backgroundColor};
    border-radius: 4px;">
    ${label}
  </span>
`;
};


const formatRadioStudyResult = (studyPoint: StudyPoint) => {
  return {
    BouyguesTelecom : getTemplateByCoverStatus(studyPoint.BYTCoverStatus),
    Free : getTemplateByCoverStatus(studyPoint.FRMCoverStatus),
    Orange : getTemplateByCoverStatus(studyPoint.ORFCoverStatus),
    SFR :getTemplateByCoverStatus(studyPoint.SFRCoverStatus),
  };
};

export const generateEmailDefinition = (
  to: string[],
  data: {
    report: {
      zipCode: string,
      city: string
      oldReport: boolean,
      reportStatusInfo?: ReportStatusInfo
    },
    studyPoint?: StudyPoint
  }
): EmailDefinition => {
  return {
    to,
    subject: `Signalement mis à jour`,
    body: {
      textVersion:
        'Votre signalement concernant une mauvaise couverture de réseau de téléphonie mobile a été mis à jour :' +
        'L’étude radio pour analyser cette zone a été réalisée.',
      htmlVersion: {
        title: 'Signalement mis à jour',
        mainContent: `
          <table
            width="100%"
            cellpadding="0"
            cellspacing="0"
            border="0"
          >
            <tr>
              <td valign="top" align="center">
                <table
                  bgcolor="#ffffff"
                  style="margin: 0 auto"
                  align="center"
                  id="brick_container"
                  cellspacing="0"
                  cellpadding="0"
                  border="0"
                  width="600"
                  class="email-container"
                >
                  <tr>
                    <td width="600">
                      <table
                        cellspacing="0"
                        cellpadding="0"
                        border="0"
                      >
                        <tr>
                          <td
                            width="600"
                            align="center"
                            style="background-color: #ffffff"
                            bgcolor="#ffffff"
                          >
                            <table
                              width="100%"
                              border="0"
                              cellpadding="0"
                              cellspacing="0"
                            >
                              <tr>
                                <td width="100%">
                                  <table
                                    width="100%"
                                    cellspacing="0"
                                    cellpadding="0"
                                    border="0"
                                  >
                                    <tr>
                                      <td
                                        width="100%"
                                        style="
                                          background-color: #ffffff;
                                        "
                                        bgcolor="#ffffff"
                                      >
                                        <table
                                          width="100%"
                                          border="0"
                                          cellpadding="0"
                                          cellspacing="0"
                                        >
                                          <tr>
                                            <td
                                              width="100%"
                                              style="
                                                padding-left: 32px;
                                                padding-right: 32px;
                                              "
                                            >
                                              <table
                                                width="100%"
                                                border="0"
                                                cellpadding="0"
                                                cellspacing="0"
                                              >
                                                <tr>
                                                  <td
                                                    height="32"
                                                    style="
                                                      height: 32px;
                                                      min-height: 32px;
                                                      line-height: 32px;
                                                    "
                                                  ></td>
                                                </tr>
                                                <tr>
                                                  <td>
                                                    <div style="
                                                        line-height: 24px;
                                                        text-align: left;
                                                      ">
                                                      <span
                                                        style="
                                                          color: #000000;
                                                          font-family: Marianne,
                                                            Arial,
                                                            sans-serif;
                                                          font-size: 16px;
                                                          line-height: 24px;
                                                          text-align: left;
                                                        ">
                                                        Bonjour,
                                                        <br><br>

                                                        ${data.report.oldReport ? OLD_REPORT_UPDATED_INTRODUCTION_TEXT : REPORT_UPDATED_INTRODUCTION_TEXT}

                                                        <strong>L’étude radio pour analyser cette zone a été réalisée.</strong>
                                                        <br><br>
                                                      </span>
                                                    </div>
                                                  </td>
                                                </tr>
                                                ${data.studyPoint ? `
                                                <tr>
                                                  <td>
                                                    <table
                                                      width="100%"
                                                      border="0"
                                                      cellpadding="0"
                                                      cellspacing="0">
                                                      <tr>
                                                        <th align="left">
                                                          <span style="color: #161616; font-size: 16px; font-weight: bold;">Résultats</span>
                                                        </th>
                                                        <th align="right">
                                                          <a href="${environment.frontUrl}" style="text-decoration: underline; display: inline-block;">
                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                              <tr>
                                                                <td style="vertical-align: middle;">
                                                                  <div style="line-height:24px;text-align:right;">
                                                                    <span style="color:#000091;font-family:Marianne,Arial,sans-serif;font-size:14px;line-height:24px;text-align:left;">
                                                                      En savoir plus
                                                                    </span>
                                                                  </div>
                                                                </td>
                                                              <td style="width:8px; min-width:8px;" width="8"></td>
                                                              <td style="vertical-align: middle;" width="16">
                                                                <img
                                                                  src="${environment.frontUrl}/assets/illustrations/icon-external-link.png"
                                                                  width="16"
                                                                  border="0"
                                                                  style="min-width:16px; width:16px;height: auto; display: block;"/></td>
                                                              </tr>
                                                            </table>
                                                          </a>
                                                        </th>
                                                      </tr>
                                                      <tr>
                                                        <td colspan="2" height="5px"></td>
                                                      </tr>
                                                      <tr>
                                                        <td colspan="2" style="border-bottom: 1px solid #DDDDDD;"></td>
                                                      </tr>
                                                      <tr>
                                                        <td colspan="2" height="5px"></td>
                                                      </tr>
                                                      <tr>
                                                        <td align="left">
                                                          <span style="font-size: 12px; color:#161616; line-height: 28px;">Bouygues Telecom</span>
                                                        </td>
                                                        <td align="right">
                                                          ${formatRadioStudyResult(data.studyPoint).BouyguesTelecom}
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td colspan="2" height="5px"></td>
                                                      </tr>
                                                      <tr>
                                                        <td colspan="2" style="border-bottom: 1px solid #DDDDDD;"></td>
                                                      </tr>
                                                      <tr>
                                                        <td colspan="2" height="5px"></td>
                                                      </tr>
                                                      <tr>
                                                        <td align="left">
                                                          <span style="font-size: 12px; color:#161616; line-height: 28px;">Free Mobile</span>
                                                        </td>
                                                        <td align="right">
                                                          ${formatRadioStudyResult(data.studyPoint).Free}
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td colspan="2" height="5px"></td>
                                                      </tr>
                                                      <tr>
                                                        <td colspan="2" style="border-bottom: 1px solid #DDDDDD;"></td>
                                                      </tr>
                                                      <tr>
                                                        <td colspan="2" height="5px"></td>
                                                      </tr>
                                                      <tr>
                                                        <td align="left">
                                                          <span style="font-size: 12px; color:#161616; line-height: 28px;">Orange</span>
                                                        </td>
                                                        <td align="right">
                                                          ${formatRadioStudyResult(data.studyPoint).Orange}
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td colspan="2" height="5px"></td>
                                                      </tr>
                                                      <tr>
                                                        <td colspan="2" style="border-bottom: 1px solid #DDDDDD;"></td>
                                                      </tr>
                                                      <tr>
                                                        <td colspan="2" height="5px"></td>
                                                      </tr>
                                                      <tr>
                                                        <td align="left">
                                                          <span style="font-size: 12px; color:#161616; line-height: 28px;">SFR</span>
                                                        </td>
                                                        <td align="right">
                                                          ${formatRadioStudyResult(data.studyPoint).SFR}
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td colspan="2" height="5px"></td>
                                                      </tr>
                                                      <tr>
                                                        <td colspan="2" style="border-bottom: 1px solid #DDDDDD;"></td>
                                                      </tr>
                                                      <tr>
                                                        <td colspan="2" height="5px"></td>
                                                      </tr>
                                                    </table>
                                                    <span  style="font-size: 12px; color:#3A3A3A; line-height: 20px;">
                                                      Si les résultats diffèrent de votre réalité d'usage, contactez votre équipe projet locale.
                                                    </span>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td
                                                    height="24"
                                                    style="
                                                      height: 24px;
                                                      min-height: 24px;
                                                      line-height: 24px;
                                                    "
                                                  ></td>
                                                </tr>`
                                                : ''}

                                                ${data.report.reportStatusInfo?.comment ? `
                                                  <tr>
                                                    <td>
                                                      ${formatStatusComment(data.report.reportStatusInfo)}
                                                    </td>
                                                  </tr>
                                                  <tr>
                                                  <td
                                                    height="24"
                                                    style="
                                                      height: 24px;
                                                      min-height: 24px;
                                                      line-height: 24px;
                                                    "
                                                  ></td>
                                                </tr>
                                                ` : ''}
                                                <tr>
                                                  <td>
                                                    <div
                                                      style="
                                                        line-height: 24px;
                                                        text-align: left;
                                                      ">
                                                      <span
                                                        style="
                                                          color: #000000;
                                                          font-family: Marianne,
                                                            Arial,
                                                            sans-serif;
                                                          font-size: 16px;
                                                          line-height: 24px;
                                                          text-align: left;
                                                        "
                                                      >
                                                        ${data.report.oldReport ? OLD_REPORT_UPDATED_BOTTOM_TEXT : REPORT_UPDATED_BOTTOM_TEXT}
                                                      </span>
                                                    </div>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td
                                                    height="24"
                                                    style="
                                                      height: 24px;
                                                      min-height: 24px;
                                                      line-height: 24px;
                                                    "
                                                  ></td>
                                                </tr>
                                                <tr>
                                                  <td>
                                                    <table
                                                      width="100%"
                                                      border="0"
                                                      cellpadding="0"
                                                      cellspacing="0"
                                                    >
                                                      <tr>
                                                        <td
                                                          width="536"
                                                        >
                                                          <table
                                                            width="100%"
                                                            border="0"
                                                            cellpadding="0"
                                                            cellspacing="0"
                                                          >
                                                            <tr>
                                                              <td
                                                                width="49%"
                                                              >
                                                                <table
                                                                  width="100%"
                                                                  cellspacing="0"
                                                                  cellpadding="0"
                                                                  border="0"
                                                                >
                                                                  <tr>
                                                                    <td
                                                                      width="49%"
                                                                      style="
                                                                        vertical-align: middle;
                                                                        background-color: #ffffff;
                                                                        border-radius: 4px;
                                                                        border: 1px
                                                                          solid
                                                                          #dddddd;
                                                                        padding-left: 16px;
                                                                        padding-right: 16px;
                                                                      "
                                                                      bgcolor="#ffffff"
                                                                    >
                                                                      <table
                                                                        width="100%"
                                                                        border="0"
                                                                        cellpadding="0"
                                                                        cellspacing="0"
                                                                      >
                                                                        <tr>
                                                                          <td
                                                                            height="8"
                                                                            style="
                                                                              height: 8px;
                                                                              min-height: 8px;
                                                                              line-height: 8px;
                                                                            "
                                                                          ></td>
                                                                        </tr>
                                                                        <tr>
                                                                          <td
                                                                            style="
                                                                              vertical-align: middle;
                                                                            "
                                                                            width="100%"
                                                                          >
                                                                            <table
                                                                              cellspacing="0"
                                                                              cellpadding="0"
                                                                              border="0"
                                                                            >
                                                                              <tr>
                                                                                <td
                                                                                  width="100%"
                                                                                >
                                                                                  <table
                                                                                    width="100%"
                                                                                    border="0"
                                                                                    cellpadding="0"
                                                                                    cellspacing="0"
                                                                                  >
                                                                                    <tr>
                                                                                      <td>
                                                                                        <a href="${environment.frontUrl}">
                                                                                        <div
                                                                                          style="
                                                                                            line-height: 24px;
                                                                                            text-align: left;
                                                                                          "
                                                                                        >
                                                                                          <span
                                                                                            style="
                                                                                              color: #161616;
                                                                                              font-family: Marianne,
                                                                                                Arial,
                                                                                                sans-serif;
                                                                                              font-size: 16px;
                                                                                              line-height: 24px;
                                                                                              text-align: left;
                                                                                            "
                                                                                            >${data.report.zipCode} </span
                                                                                          ><span
                                                                                            style="
                                                                                              color: #161616;
                                                                                              font-weight: 700;
                                                                                              font-family: Marianne,
                                                                                                Arial,
                                                                                                sans-serif;
                                                                                              font-size: 16px;
                                                                                              line-height: 24px;
                                                                                              text-align: left;
                                                                                            "
                                                                                            >${data.report.city}</span
                                                                                          >
                                                                                        </div>
                                                                                        </a>
                                                                                      </td>
                                                                                    </tr>
                                                                                  </table>
                                                                                </td>
                                                                              </tr>
                                                                            </table>
                                                                          </td>
                                                                          <td></td>
                                                                          <td
                                                                            style="
                                                                              vertical-align: middle;
                                                                            "
                                                                            width="40"
                                                                          >
                                                                            <table
                                                                              cellspacing="0"
                                                                              cellpadding="0"
                                                                              border="0"
                                                                            >
                                                                              <tr>
                                                                                <td
                                                                                  width="40"
                                                                                >
                                                                                  <table
                                                                                    width="100%"
                                                                                    border="0"
                                                                                    cellpadding="0"
                                                                                    cellspacing="0"
                                                                                  >
                                                                                    <tr>
                                                                                      <td
                                                                                        style="
                                                                                          padding-left: 8px;
                                                                                          padding-right: 8px;
                                                                                        "
                                                                                      >
                                                                                        <table
                                                                                          width="100%"
                                                                                          border="0"
                                                                                          cellpadding="0"
                                                                                          cellspacing="0"
                                                                                        >
                                                                                          <tr>
                                                                                            <td
                                                                                              width="24"
                                                                                            >
                                                                                              <a href="${environment.frontUrl}">
                                                                                                <svg
                                                                                                  xmlns="http://www.w3.org/2000/svg"
                                                                                                  viewBox="0 0 24 24"
                                                                                                  width="24"
                                                                                                  height="24"
                                                                                                  fill="#000091"
                                                                                                >
                                                                                                  <path
                                                                                                    d="M18.364 3.636a9 9 0 0 1 0 12.728L12 22.728l-6.364-6.364A9 9 0 0 1 18.364 3.636ZM7.05 5.051a7 7 0 0 0 0 9.899L12 19.9l4.95-4.95a7 7 0 0 0-9.9-9.9ZM12 8a2 2 0 1 1 0 4 2 2 0 0 1 0-4Z"
                                                                                                  />
                                                                                                </svg>
                                                                                              </a>
                                                                                            </td>
                                                                                          </tr>
                                                                                        </table>
                                                                                      </td>
                                                                                    </tr>
                                                                                  </table>
                                                                                </td>
                                                                              </tr>
                                                                            </table>
                                                                          </td>
                                                                        </tr>
                                                                        <tr>
                                                                          <td
                                                                            height="8"
                                                                            style="
                                                                              height: 8px;
                                                                              min-height: 8px;
                                                                              line-height: 8px;
                                                                            "
                                                                          ></td>
                                                                        </tr>
                                                                      </table>
                                                                    </td>
                                                                  </tr>
                                                                </table>
                                                              </td>
                                                              <td
                                                                style="
                                                                  width: 8px;
                                                                  min-width: 8px;
                                                                "
                                                                width="8"
                                                              ></td>
                                                            </tr>
                                                          </table>
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td
                                                          height="8"
                                                          style="
                                                            height: 8px;
                                                            min-height: 8px;
                                                            line-height: 8px;
                                                          "
                                                        ></td>
                                                      </tr>
                                                    </table>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td
                                                    height="24"
                                                    style="
                                                      height: 24px;
                                                      min-height: 24px;
                                                      line-height: 24px;
                                                    "
                                                  ></td>
                                                </tr>
                                                <tr>
                                                  <td>
                                                    <div
                                                      style="
                                                        line-height: 24px;
                                                        text-align: left;
                                                      "
                                                    >
                                                      <span
                                                        style="
                                                          color: #000000;
                                                          font-family: Marianne,
                                                            Arial,
                                                            sans-serif;
                                                          font-size: 16px;
                                                          line-height: 24px;
                                                          text-align: left;
                                                        "
                                                      >
                                                      L’équipe de la plateforme <a href="${environment.frontUrl}">Toutes et tous connecté·e·s</a> vous remercie.
                                                      </span>
                                                    </div>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td
                                                    height="24"
                                                    style="
                                                      height: 24px;
                                                      min-height: 24px;
                                                      line-height: 24px;
                                                    "
                                                  ></td>
                                                </tr>
                                              </table>
                                            </td>
                                          </tr>
                                        </table>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        `
      }
    }
  };
};
