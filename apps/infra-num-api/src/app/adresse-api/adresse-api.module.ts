/**
 * Nest.js imports
 */
import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';


/**
 * Internal imports
 */
import { AdresseApiService } from './adresse-api.service';
import { AdresseApiController } from './adresse-api.controller';

@Module({
  imports: [HttpModule],
  controllers: [AdresseApiController],
  providers: [AdresseApiService]
})
export class AdresseApiModule {}
