/**
 * Nest.js imports
 */
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';

/**
 * 3rd-party imports
 */
import request from 'supertest';

/**
 * Internal imports
 */
import { AdresseApiService } from '../adresse-api.service';
import { AdresseApiModule } from '../adresse-api.module';
import { AdresseApiServiceMock } from './adresse-api.service.mock';
import { ADDRESSES_FORMATTED } from './adresse-api-already-saved.mock';

describe('AdresseApi', () => {
  let app: INestApplication;
  let adresseApiService: AdresseApiServiceMock;

  beforeAll(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [AdresseApiModule]
    })
      .overrideProvider(AdresseApiService)
      .useValue(AdresseApiServiceMock)
      .compile();

    adresseApiService = moduleRef.get<AdresseApiServiceMock>(AdresseApiService);
    app = moduleRef.createNestApplication();
    await app.init();
  });

  it('/GET /addresses?search=8+bd+du+pont', () => {
    adresseApiService.searchAddresses = jest.fn().mockResolvedValue(ADDRESSES_FORMATTED);

    return request(app.getHttpServer())
      .get('/addresses?search=8+bd+du+pont')
      .expect(200)
      .expect(ADDRESSES_FORMATTED);
  });

  afterAll(async () => {
    await app.close();
  });
});
