/**
 * 3rd-party imports
 */
import { Address, AdresseApiResult } from '@infra-num/common';

/**
 * TypeScript entities and constants
 */
export const ADDRESSES_EMPTY: AdresseApiResult = {
  type: 'FeatureCollection',
  features: []
};

export const ADDRESSES_RECEIVED: AdresseApiResult = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [6.193187, 49.299104]
      },
      properties: {
        label: '8 Boulevard du Pont 57310 Guénange',
        score: 0.4806901913875598,
        housenumber: '8',
        id: '57269_0540_00008',
        name: '8 Boulevard du Pont',
        postcode: '57310',
        citycode: '57269',
        x: 932242.57,
        y: 6915751.35,
        city: 'Guénange',
        context: '57, Moselle, Grand Est',
        type: 'housenumber',
        importance: 0.55075,
        street: 'Boulevard du Pont'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [6.637955, 43.315297]
      },
      properties: {
        label: '8 Boulevard du Ponant 83120 Sainte-Maxime',
        score: 0.4422994805194805,
        housenumber: '8',
        id: '83115_0725_00008',
        name: '8 Boulevard du Ponant',
        postcode: '83120',
        citycode: '83115',
        x: 995187.94,
        y: 6253038.76,
        city: 'Sainte-Maxime',
        context: "83, Var, Provence-Alpes-Côte d'Azur",
        type: 'housenumber',
        importance: 0.57958,
        street: 'Boulevard du Ponant'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [-4.616249, 48.441308]
      },
      properties: {
        label: '8 Boulevard du Ponant 29290 Saint-Renan',
        score: 0.4382258441558441,
        housenumber: '8',
        id: '29260_0765_00008',
        name: '8 Boulevard du Ponant',
        postcode: '29290',
        citycode: '29260',
        x: 137587.01,
        y: 6842814.65,
        city: 'Saint-Renan',
        context: '29, Finistère, Bretagne',
        type: 'housenumber',
        importance: 0.53477,
        street: 'Boulevard du Ponant'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [3.271411, 48.203903]
      },
      properties: {
        label: '8 Boulevard du Pont Neuf 89100 Sens',
        score: 0.3951490909090909,
        housenumber: '8',
        id: '89387_4350_00008',
        name: '8 Boulevard du Pont Neuf',
        postcode: '89100',
        citycode: '89387',
        x: 720163.8,
        y: 6789317.99,
        city: 'Sens',
        context: '89, Yonne, Bourgogne-Franche-Comté',
        type: 'housenumber',
        importance: 0.59664,
        street: 'Boulevard du Pont Neuf'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [2.448834, 44.929283]
      },
      properties: {
        label: '8 Boulevard du Pont Rouge 15000 Aurillac',
        score: 0.3856363636363636,
        housenumber: '8',
        id: '15014_1950_00008',
        name: '8 Boulevard du Pont Rouge',
        postcode: '15000',
        citycode: '15014',
        x: 656513.99,
        y: 6425716.5,
        city: 'Aurillac',
        context: '15, Cantal, Auvergne-Rhône-Alpes',
        type: 'housenumber',
        importance: 0.642,
        street: 'Boulevard du Pont Rouge'
      }
    }
  ]
};

export const ADDRESSES_FORMATTED: Address[] = [
  {
    id: '57269_0540_00008',
    label: '8 Boulevard du Pont 57310 Guénange',
    street: 'Boulevard du Pont',
    codePostal: '57310',
    city: 'Guénange',
    coordinates: [6.193187, 49.299104],
    housenumber: "8",
    type: 'address',
    zoom: 17
  },
  {
    id: '83115_0725_00008',
    label: '8 Boulevard du Ponant 83120 Sainte-Maxime',
    street: 'Boulevard du Ponant',
    codePostal: '83120',
    city: 'Sainte-Maxime',
    coordinates: [6.637955, 43.315297],
    housenumber: '8',
    type: 'address',
    zoom: 17
  },
  {
    id: '29260_0765_00008',
    label: '8 Boulevard du Ponant 29290 Saint-Renan',
    street: 'Boulevard du Ponant',
    codePostal: '29290',
    city: 'Saint-Renan',
    coordinates: [-4.616249, 48.441308],
    housenumber: '8',
    type: 'address',
    zoom: 17
  },
  {
    id: '89387_4350_00008',
    label: '8 Boulevard du Pont Neuf 89100 Sens',
    street: 'Boulevard du Pont Neuf',
    codePostal: '89100',
    city: 'Sens',
    coordinates: [3.271411, 48.203903],
    housenumber: '8',
    type: 'address',
    zoom: 17
  },
  {
    id: '15014_1950_00008',
    label: '8 Boulevard du Pont Rouge 15000 Aurillac',
    street: 'Boulevard du Pont Rouge',
    codePostal: '15000',
    city: 'Aurillac',
    coordinates: [2.448834, 44.929283],
    housenumber: '8',
    type: 'address',
    zoom: 17
  }
];
