/**
 * Nest.js imports
 */
import { Injectable, Logger } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';

/**
 * 3rd-party imports
 */
import { AxiosError } from 'axios';
import { Address, AdresseApiResult } from '@infra-num/common';
import { catchError, firstValueFrom } from 'rxjs';

@Injectable()
export class AdresseApiService {

  private readonly logger = new Logger(AdresseApiService.name);
  private adresseApiUrl: string = 'https://api-adresse.data.gouv.fr';

  constructor(private readonly httpService: HttpService) {}

  async searchAddresses(search: string): Promise<Address[]> {
    const { data } = await firstValueFrom(
      this.httpService
        .get<AdresseApiResult>(`${this.adresseApiUrl}/search/?q=${search}`)
        .pipe(
          catchError((error: AxiosError) => {
            this.logger.error(error.response.data);
            throw error;
          })
        )
    );
    return this.formatAddresses(data);
  }

  private formatAddresses(adresseApiResult: AdresseApiResult): Address[] {
    const addresses: Address[] = [];
    adresseApiResult.features.map((feature) => {
      addresses.push({
        id: feature.properties.id,
        label: feature.properties.label,
        street: feature.properties.street,
        codePostal: feature.properties.postcode,
        city: feature.properties.city,
        coordinates: feature.geometry.coordinates,
        housenumber: feature.properties.housenumber,
        zoom: 17,
        type: 'address',
      });
    });

    return addresses;
  }

}
