/**
 * Nest.js imports
 */
import { Controller, Get, Query } from '@nestjs/common';

/**
 * Internal imports
 */
import { AdresseApiService } from './adresse-api.service';

/**
 * 3rd-party imports
 */
import { Address } from '@infra-num/common';

@Controller()
export class AdresseApiController {
  constructor(private readonly adresseApiService: AdresseApiService) {}

  @Get('addresses')
  async searchAddresses(
    @Query('search') search: string,
  ): Promise<Address[]> {
    return this.adresseApiService.searchAddresses(search);
  }
}
