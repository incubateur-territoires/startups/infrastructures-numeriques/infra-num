/**
 * Nest.js imports
 */
import { Test, TestingModule } from '@nestjs/testing';
import { HttpService } from '@nestjs/axios';

/**
 * 3rd-party imports
 */
import { of } from 'rxjs';

/**
 * Internal imports
 */
import { AdresseApiService } from './adresse-api.service';
import {
  ADDRESSES_RECEIVED,
  ADDRESSES_FORMATTED
} from './tests/adresse-api-already-saved.mock';

/**
 * Creating mocks
 */
class HttpServiceMock {
  get: jest.Mock = jest.fn();
}

describe('AdresseApiService', () => {
  let addressesService: AdresseApiService;
  let httpService: HttpServiceMock;

  let testAddress = '8 bd du pont';

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AdresseApiService,
        {
          provide: HttpService,
          useClass: HttpServiceMock
        }
      ]
    }).compile();

    addressesService = module.get<AdresseApiService>(AdresseApiService);
    httpService = module.get<HttpServiceMock>(HttpService);
  });

  it('should be defined', () => {
    expect(addressesService).toBeDefined();
  });

  it('should transform an AdresseApiResult object into an array of Address ', async () => {
    const response = {
      data: ADDRESSES_RECEIVED,
      headers: {},
      config: { url: 'http://localhost:3333/api' },
      status: 200,
      statusText: 'OK'
    };

    httpService.get.mockReturnValue(of(response));

    const addresses = await addressesService.searchAddresses(testAddress);

    expect(httpService.get).toHaveBeenCalled();
    expect(addresses).toEqual(ADDRESSES_FORMATTED);
  });

});
