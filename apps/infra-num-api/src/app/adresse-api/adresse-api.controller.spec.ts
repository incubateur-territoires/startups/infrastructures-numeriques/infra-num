/**
 * Nest.js imports
 */
import { Test, TestingModule } from '@nestjs/testing';

/**
 * Internal imports
 */
import { AdresseApiController } from './adresse-api.controller';
import { AdresseApiServiceMock } from './tests/adresse-api.service.mock';
import { AdresseApiService } from './adresse-api.service';
import { ADDRESSES_FORMATTED } from './tests/adresse-api-already-saved.mock';

describe('AdresseApiController', () => {
  let controller: AdresseApiController;
  let adresseApiService: AdresseApiServiceMock;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AdresseApiController],
      providers: [{
        provide: AdresseApiService,
        useClass: AdresseApiServiceMock
      }]
    }).compile();

    controller = module.get<AdresseApiController>(AdresseApiController);
    adresseApiService = module.get<AdresseApiServiceMock>(AdresseApiService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should return an array of addresses', async () => {
    adresseApiService.searchAddresses = jest.fn().mockResolvedValue(ADDRESSES_FORMATTED);
    const addresses = await controller.searchAddresses('8 bd du pont');
    expect(adresseApiService.searchAddresses).toHaveBeenCalled();
    expect(addresses).toEqual(ADDRESSES_FORMATTED);
  });

});
