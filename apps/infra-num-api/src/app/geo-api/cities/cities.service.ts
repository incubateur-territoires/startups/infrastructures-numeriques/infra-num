/**
 * Nest.js imports
 */
import { Injectable, Logger } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';

/**
 * 3rd-party imports
 */
import { catchError, firstValueFrom } from 'rxjs';
import { AxiosError } from 'axios';

export interface GeoApiCity {
  nom: string;
  centre?: {
    type: 'Point';
    coordinates: [number, number];
  };
  codesPostaux: string[];
  code: string;
  departement: {
    code: string;
    nom: string;
  };
  region: {
    code: string;
    nom: string;
  };
}

@Injectable()
export class CitiesService {
  private readonly logger = new Logger(CitiesService.name);

  private geoApiUrl: string = 'https://geo.api.gouv.fr/communes';

  constructor(private readonly httpService: HttpService) {}

  private async getCities(
    query: string,
    fields: string = 'nom,centre,departement,region,codesPostaux'
  ): Promise<GeoApiCity[]> {
    const { data } = await firstValueFrom(
      this.httpService
        .get<GeoApiCity[]>(`${this.geoApiUrl}?${query}&fields=${fields}`)
        .pipe(
          catchError((error: AxiosError) => {
            this.logger.error(error.response.data);
            throw error;
          })
        )
    );
    return data;
  }

  async getCitiesByZipCode(zipCode: string): Promise<GeoApiCity[]> {
    return this.getCities(`codePostal=${zipCode}`);
  }

  async getCitiesByName(name: string): Promise<GeoApiCity[]> {
    return this.getCities(`nom=${name}`);
  }

  async getCityByGPSCoordinates(
    longitude: string,
    latitude: string
  ): Promise<GeoApiCity[]> {
    return this.getCities(
      `lat=${latitude}&lon=${longitude}`,
      'nom,codesPostaux,departement,region'
    );
  }
}
