export class CitiesServiceMock {
  getCitiesByZipCode: jest.Mock = jest.fn();
  getCitiesByName: jest.Mock = jest.fn();
  getCityByGPSCoordinates: jest.Mock = jest.fn();
}
