/**
 * Nest.js imports
 */
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';

/**
 * 3rd-party imports
 */
import request from 'supertest';

/**
 * Internal imports
 */
import { CITIES_ALREADY_SAVED, GEO_API_CITIES_ALREADY_SAVED } from './cities-already-saved.mock';
import { CitiesServiceMock } from './cities.service.mock';
import { CitiesService } from '../cities.service';
import { GeoApiModule } from '../../geo-api.module';

describe( 'Cities', () => {
  let app: INestApplication;
  let citiesService: CitiesServiceMock;

  beforeAll( async () => {
    const moduleRef: TestingModule = await Test.createTestingModule( {
      imports: [ GeoApiModule ]
    } )
      .overrideProvider( CitiesService )
      .useValue( CitiesServiceMock )
      .compile();

    citiesService = moduleRef.get<CitiesServiceMock>( CitiesService );
    app = moduleRef.createNestApplication();
    await app.init();
  } );

  it( `/GET cities?search=paris`, () => {
    citiesService.getCitiesByName = jest.fn().mockResolvedValue( GEO_API_CITIES_ALREADY_SAVED );

    return request( app.getHttpServer() )
      .get( '/cities?search=paris' )
      .expect( 200 )
      .expect( CITIES_ALREADY_SAVED );
  } );

  it( `/GET cities?search=1234`, () => {
    citiesService.getCitiesByZipCode = jest.fn().mockResolvedValue( GEO_API_CITIES_ALREADY_SAVED );

    return request( app.getHttpServer() )
      .get( '/cities?search=1234' )
      .expect( 200 )
      .expect( CITIES_ALREADY_SAVED );
  } );

  afterAll( async () => {
    await app.close();
  } );
} );
