/**
 * 3rd-party imports
 */
import { City } from '@infra-num/common';

/**
 * Internal imports
 */
import { GeoApiCity } from '../cities.service';

/**
 * TypeScript entities and constants
 */
const geoApiCity1: GeoApiCity = {
  code: '78646',
  nom: 'Versailles',
  centre: {
    type: 'Point',
    coordinates: [
      2.1191,
      48.8039
    ]
  },
  codesPostaux: [
    '78000'
  ],
  departement: {
    code: '78',
    nom: 'Yvelines'
  },
  region: {
    code: '11',
    nom: 'Île-de-France'
  }
};

const geoApiCity2: GeoApiCity = {
  code: '75056',
  nom: 'Paris',
  centre: {
    type: 'Point',
    coordinates: [
      2.347,
      48.8589
    ]
  },
  codesPostaux: [
    '75001',
    '75002'
  ],
  departement: {
    code: '75',
    nom: 'Paris'
  },
  region: {
    code: '11',
    nom: 'Île-de-France'
  }
};

const city1: City = {
  code: '78646',
  nom: 'Versailles',
  centre: {
    type: 'Point',
    coordinates: [
      2.1191,
      48.8039
    ]
  },
  codePostal: '78000',
  codeDepartement: '78',
  nomDepartement: 'Yvelines',
  codeRegion: '11',
  nomRegion: 'Île-de-France',
  zoom: 13,
  type: 'city'
};

const city2: City = {
  code: '75056',
  nom: 'Paris',
  centre: {
    type: 'Point',
    coordinates: [
      2.347,
      48.8589
    ]
  },
  codePostal: '75001',
  codeDepartement: '75',
  nomDepartement: 'Paris',
  codeRegion: '11',
  nomRegion: 'Île-de-France',
  zoom: 13,
  type: 'city'
};

const city3: City = {
  code: '75056',
  nom: 'Paris',
  centre: {
    type: 'Point',
    coordinates: [
      2.347,
      48.8589
    ]
  },
  codePostal: '75002',
  codeDepartement: '75',
  nomDepartement: 'Paris',
  codeRegion: '11',
  nomRegion: 'Île-de-France',
  zoom: 13,
  type: 'city'
};

export const GEO_API_CITIES_ALREADY_SAVED = [ geoApiCity1, geoApiCity2 ];
export const CITIES_ALREADY_SAVED = [ city1, city2, city3 ];
