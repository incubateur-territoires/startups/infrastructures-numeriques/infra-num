export class DepartmentsServiceMock {
  getDepartmentsByName: jest.Mock = jest.fn();
  getDepartmentsByNames: jest.Mock = jest.fn();
}
