/**
 * Nest.js imports
 */
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';

/**
 * 3rd-party imports
 */
import request from 'supertest';

/**
 * Internal imports
 */
import { GEO_API_DEPARTMENTS_ALREADY_SAVED, DEPARTMENTS_ALREADY_SAVED } from './departments-already-saved.mock';
import { DepartmentsServiceMock } from './departments.service.mock';
import { DepartmentsService } from '../departments.service';
import { GeoApiModule } from '../../geo-api.module';
import { of } from 'rxjs';

describe('Departments', () => {
  let app: INestApplication;
  let departmentsService: DepartmentsServiceMock;

  beforeAll(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [GeoApiModule]
    })
      .overrideProvider(DepartmentsService)
      .useClass(DepartmentsServiceMock)
      .compile();

    departmentsService = moduleRef.get<DepartmentsServiceMock>(DepartmentsService);
    app = moduleRef.createNestApplication();
    await app.init();
  });

  it(`/GET departments?search=aube`, () => {
    departmentsService.getDepartmentsByNames.mockReturnValue(of([
      GEO_API_DEPARTMENTS_ALREADY_SAVED[0]
    ]));
    const query = JSON.stringify(['aube']);

    return request(app.getHttpServer())
      .get(`/departments?search=${query}`)
      .expect(200)
      .expect(DEPARTMENTS_ALREADY_SAVED);
  });

  afterAll(async () => {
    await app.close();
  });
});