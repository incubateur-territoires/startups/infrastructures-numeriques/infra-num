/**
 * Internal imports
 */
import { Department } from '@infra-num/common';
import { GeoApiDepartment } from '../departments.service';

/**
 * TypeScript entities and constants
 */
const geoApiDepartment1: GeoApiDepartment = {
  code: '10',
  nom: 'Aube',
  codeRegion: '11',
  region: {
    code: '11',
    nom: 'Grand-Est'
  }
};

const geoApiDepartment2: GeoApiDepartment = {
  code: '75',
  nom: 'Paris',
  codeRegion: '30',
  region: {
    code: '30',
    nom: 'Ile-de-France'
  }
};

const department1: Department = {
  code: '10',
  nom: 'Aube',
  codeRegion: '11',
  region: { code: '11', nom: 'Grand-Est' },
  type: 'department',
  zoom: 8,
  centre: { type: 'Point', coordinates: [4.1905397, 48.3201921] }
};

export const GEO_API_DEPARTMENTS_ALREADY_SAVED = [geoApiDepartment1, geoApiDepartment2];
export const DEPARTMENTS_ALREADY_SAVED = [department1];
