/**
 * Nest.js imports
 */
import { Injectable, Logger } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';

/**
 * 3rd-party imports
 */
import { catchError, combineLatest, firstValueFrom, map, Observable } from 'rxjs';
import { AxiosError, AxiosResponse } from 'axios';

/**
 * TypeScript entities and constants
 */
export interface GeoApiDepartment {
  nom: string;
  code: string;
  codeRegion: string;
  region: {
    code: string;
    nom: string;
  };
}

@Injectable()
export class DepartmentsService {
  private readonly logger = new Logger(DepartmentsService.name);

  private geoApiUrl: string = 'https://geo.api.gouv.fr/departements';

  constructor(private readonly httpService: HttpService) {}

  getDepartmentsByNames(
    names: string[]
  ): Observable<GeoApiDepartment[]> {
    return combineLatest(
      names.map(name => this.httpService
        .get<GeoApiDepartment[]>(`${this.geoApiUrl}?nom=${name}&fields=region,codeRegion`))
    ).pipe(
      map((departments: AxiosResponse<GeoApiDepartment[]>[]) =>
        departments.reduce(
          (previous: GeoApiDepartment[], current: AxiosResponse<GeoApiDepartment[]>) => [
            ...previous,
            ...current.data.filter(({ nom }) => names.includes(nom))
          ],
          [] as GeoApiDepartment[]
        )
      ),
      catchError((error: AxiosError) => {
        this.logger.error(error.response.data);
        throw error;
      })
    );
  }

  async getDepartmentsByName(
    query: string
  ): Promise<GeoApiDepartment[]> {
    const {data} = await firstValueFrom(
      this.httpService
        .get<GeoApiDepartment[]>(`${this.geoApiUrl}?nom=${query}&fields=region,codeRegion`)
        .pipe(
          catchError((error: AxiosError) => {
            this.logger.error(error.response.data);
            throw error;
          })
        )
    );
    return data;
  }
}
