/**
 * Nest.js imports
 */
import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';

/**
 * Internal imports
 */
import { GeoApiController } from './geo-api.controller';
import { GeoApiService } from './geo-api.service';
import { CitiesService } from './cities/cities.service';
import { RegionsService } from './regions/regions.service';
import { DepartmentsService } from './departments/departments.service';
import { EpcisService } from './epcis/epcis.service';

@Module({
  imports: [HttpModule],
  controllers: [GeoApiController],
  providers: [
    GeoApiService,
    CitiesService,
    DepartmentsService,
    RegionsService,
    EpcisService
  ],
  exports: [EpcisService]
})
export class GeoApiModule {}
