/**
 * Nest.js imports
 */
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';

/**
 * 3rd-party imports
 */
import request from 'supertest';

/**
 * Internal imports
 */
import { REGIONS_ALREADY_SAVED, GEO_API_REGIONS_ALREADY_SAVED } from './regions-already-saved.mock';
import { RegionsServiceMock } from './regions.service.mock';
import { RegionsService } from '../regions.service';
import { GeoApiModule } from '../../geo-api.module';

describe( 'Regions', () => {
  let app: INestApplication;
  let regionsService: RegionsServiceMock;

  beforeAll( async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [ GeoApiModule ]
    })
      .overrideProvider( RegionsService )
      .useClass( RegionsServiceMock )
      .compile();

    regionsService = moduleRef.get<RegionsServiceMock>( RegionsService );
    app = moduleRef.createNestApplication();
    await app.init();
  } );

  it( `/GET regions?search=grand`, () => {
    regionsService.getRegionsByName.mockResolvedValue([
      GEO_API_REGIONS_ALREADY_SAVED[0]
    ]);

    return request(app.getHttpServer())
      .get( '/regions?search=grand' )
      .expect(200)
      .expect(REGIONS_ALREADY_SAVED);
  } );

  afterAll( async () => {
    await app.close();
  } );
} );
