/**
 * Internal imports
 */
import { Region } from '@infra-num/common';
import { GeoApiRegion } from '../regions.service';

/**
 * TypeScript entities and constants
 */
const geoApiRegion1: GeoApiRegion = {
  code: '44',
  nom: 'Grand-Est',
};

const geoApiRegion2: GeoApiRegion = {
  code: '10',
  nom: 'Ile-de-France',
};

const region1: Region = {
  code: '44',
  nom: 'Grand-Est',
  type: 'region',
  zoom: 6.8,
  centre: { type: 'Point', coordinates: [6.113035,48.4845157] }
}

export const GEO_API_REGIONS_ALREADY_SAVED = [geoApiRegion1, geoApiRegion2];
export const REGIONS_ALREADY_SAVED = [region1];
