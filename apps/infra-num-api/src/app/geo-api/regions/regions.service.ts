/**
 * Nest.js imports
 */
import { Injectable, Logger } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';

/**
 * 3rd-party imports
 */
import { catchError, firstValueFrom } from 'rxjs';
import { AxiosError } from 'axios';

/**
 * TypeScript entities and constants
 */
export interface GeoApiRegion {
  nom: string;
  code: string;
}

@Injectable()
export class RegionsService {
  private readonly logger = new Logger(RegionsService.name);

  private geoApiUrl: string = 'https://geo.api.gouv.fr/regions';

  constructor(private readonly httpService: HttpService) {}

  async getRegionsByName(
    query: string
  ): Promise<GeoApiRegion[]> {
    const { data } = await firstValueFrom(
      this.httpService
        .get<GeoApiRegion[]>(`${this.geoApiUrl}?nom=${query}`)
        .pipe(
          catchError((error: AxiosError) => {
            this.logger.error(error.response.data);
            throw error;
          })
        )
    );
    return data;
  }

}
