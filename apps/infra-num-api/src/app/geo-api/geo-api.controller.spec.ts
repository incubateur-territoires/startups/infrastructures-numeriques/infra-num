/**
 * Nest.js imports
 */
import { Test, TestingModule } from '@nestjs/testing';

/**
 * Internal imports
 */
import { GeoApiController } from './geo-api.controller';
import { GeoApiService } from './geo-api.service';
import { GeoApiServiceMock } from './tests/geo-api.service.mock';
import { CITIES_ALREADY_SAVED } from './cities/tests/cities-already-saved.mock';

describe('GeoApiController', () => {
  let controller: GeoApiController;
  let geoApiService: GeoApiServiceMock;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GeoApiController],
      providers: [{
        provide: GeoApiService,
        useClass: GeoApiServiceMock
      }]
    }).compile();

    controller = module.get<GeoApiController>(GeoApiController);
    geoApiService = module.get<GeoApiServiceMock>(GeoApiService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should return an array of City objects when search() is called', async () => {
    geoApiService.searchCities = jest.fn().mockResolvedValue(CITIES_ALREADY_SAVED);

    const cities = await controller.searchCities('paris', null, null);

    expect(geoApiService.searchCities).toHaveBeenCalled();
    expect(cities).toEqual(CITIES_ALREADY_SAVED);
  });
});
