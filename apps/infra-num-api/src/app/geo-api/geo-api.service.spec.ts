/**
 * Nest.js imports
 */
import { Test, TestingModule } from '@nestjs/testing';

/**
 * Internal imports
 */
import { GeoApiService } from './geo-api.service';
import { CitiesService } from './cities/cities.service';
import { CitiesServiceMock } from './cities/tests/cities.service.mock';
import { CITIES_ALREADY_SAVED, GEO_API_CITIES_ALREADY_SAVED } from './cities/tests/cities-already-saved.mock';
import { RegionsService } from './regions/regions.service';
import { RegionsServiceMock } from './regions/tests/regions.service.mock';
import { DepartmentsService } from './departments/departments.service';
import { DepartmentsServiceMock } from './departments/tests/departments.service.mock';

describe('GeoApiService', () => {
  let geoApiService: GeoApiService;
  let citiesService: CitiesServiceMock;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GeoApiService, {
        provide: CitiesService,
        useClass: CitiesServiceMock
      }, {
        provide: RegionsService,
        useClass: RegionsServiceMock
      }, {
        provide: DepartmentsService,
        useClass: DepartmentsServiceMock
      }]
    }).compile();

    geoApiService = module.get<GeoApiService>(GeoApiService);
    citiesService = module.get<CitiesServiceMock>(CitiesService);
  });

  it('should be defined', () => {
    expect(geoApiService).toBeDefined();
  });

  it('should get cities by name when we call searchCities with letters', async () => {
    const search = 'paris';
    citiesService.getCitiesByName = jest.fn().mockResolvedValue(GEO_API_CITIES_ALREADY_SAVED);

    const geoApiCities = await geoApiService.searchCities(search, '', '');

    expect(citiesService.getCitiesByName).toHaveBeenCalled();
    expect(citiesService.getCitiesByZipCode).not.toHaveBeenCalled();
    expect(geoApiCities).toEqual(CITIES_ALREADY_SAVED);
  });

  it('should get cities by zip code when we call searchCities with numbers', async () => {
    const search = '75000';
    citiesService.getCitiesByZipCode = jest.fn().mockResolvedValue(GEO_API_CITIES_ALREADY_SAVED);

    const geoApiCities = await geoApiService.searchCities(search, '', '');

    expect(citiesService.getCitiesByZipCode).toHaveBeenCalled();
    expect(citiesService.getCitiesByName).not.toHaveBeenCalled();
    expect(geoApiCities).toEqual(CITIES_ALREADY_SAVED);
  });

  it('should get city by GPS coordinates', async () => {
    const longitude: string = '40.4543';
    const latitude: string = '2.00';
    const result = {
      nom: 'Versailles',
      code: '78646',
      codePostal: '78000',
      'codeDepartement': '78',
      'nomDepartement': 'Yvelines',
      'codeRegion': '11',
      'nomRegion': 'Île-de-France',
      'type': 'city',
      'zoom': 13
    };
    citiesService.getCityByGPSCoordinates = jest.fn().mockResolvedValue(GEO_API_CITIES_ALREADY_SAVED);

    const geoApiCities = await geoApiService.searchCities('', longitude, latitude);

    expect(citiesService.getCityByGPSCoordinates).toHaveBeenCalled();
    expect(geoApiCities).toEqual(result);
  });
});
