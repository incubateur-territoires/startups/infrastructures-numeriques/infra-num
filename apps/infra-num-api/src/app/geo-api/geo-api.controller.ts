/**
 * Nest.js imports
 */
import { Controller, Get, Query } from '@nestjs/common';

/**
 * Internal imports
 */
import { GeoApiService } from './geo-api.service';

/**
 * 3rd-party imports
 */
import { City, Department, Epci, GeographicSearchResult, Region } from '@infra-num/common';
import { Observable } from 'rxjs';

@Controller()
export class GeoApiController {

  constructor(private readonly geoApiService: GeoApiService) {}

  @Get('epcis')
  async searchEpcis(
    @Query('search') search: string
  ): Promise<Epci[]> {
    return this.geoApiService.searchEpcis(search);
  }

  @Get('cities')
  async searchCities(
    @Query('search') search: string,
    @Query('lon') longitude: string,
    @Query('lat') latitude: string
  ): Promise<City[] | City> {
    return this.geoApiService.searchCities(search, longitude, latitude);
  }

  @Get('departments')
  searchDepartments(
    @Query('search') search: string,
  ): Observable<Department[]> {
    return this.geoApiService.searchDepartmentsByNames(search);
  }

  @Get('regions')
  searchRegions(
    @Query('search') search: string,
  ): Promise<Region[]> {
    return this.geoApiService.searchRegions(search);
  }

  @Get('localities')
  searchLocalities(
    @Query('search') search: string,
  ): Promise<GeographicSearchResult> {
    return this.geoApiService.searchLocalitiesByName(search);
  }
}
