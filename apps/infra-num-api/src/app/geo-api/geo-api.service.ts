/**
 * Nest.js imports
 */
import { BadRequestException, Injectable } from '@nestjs/common';

/**
 * Internal imports
 */
import { CitiesService, GeoApiCity } from './cities/cities.service';
import { GeoApiRegion, RegionsService } from './regions/regions.service';
import { DepartmentsService, GeoApiDepartment } from './departments/departments.service';
import { DEPARTMENTS } from '../users/jurisdictions/data/departements';
import { REGIONS } from '../users/jurisdictions/data/regions';

/**
 * 3rd-party imports
 */
import { City, Department, GeographicSearchResult, Region, isNumeric, Epci } from '@infra-num/common';
import { map, Observable } from 'rxjs';
import { EpcisService, GeoApiEpci } from './epcis/epcis.service';

/**
 * TypeScript entities and constants
 */
const GEO_API_FORMAT_ERROR: string = 'Please enter valid search query for Geo API';

@Injectable()
export class GeoApiService {
  constructor(
    private readonly citiesService: CitiesService,
    private readonly regionsService: RegionsService,
    private readonly departmentsService: DepartmentsService,
    private readonly epcisService: EpcisService
  ) {
  }

  async searchEpcis(search: string) : Promise<Epci[]> {
    return await this.epcisService
      .getEpcisByName(search)
      .then((epcis: GeoApiEpci[]) => {
        return epcis.map(
          (epci: GeoApiEpci) =>
            this.turnToSearchResult(epci, 'epci') as Epci
        );
      });
  }

  async searchCities(
    search: string,
    longitude: string,
    latitude: string
  ): Promise<City[] | City> {
    switch (true) {
      case !!search:
        return this.searchCityByNameOrZipCode(search);
      case !!longitude && !!latitude:
        return this.searchCityByGPSCoordinates(longitude, latitude);
      default:
        throw new BadRequestException(GEO_API_FORMAT_ERROR);
    }
  }

  private async searchCityByNameOrZipCode(search: string): Promise<City[]> {
    const geoApiCities = isNumeric(search)
      ? await this.citiesService.getCitiesByZipCode(search)
      : await this.citiesService.getCitiesByName(search);

    /*
     * Some cities can have several zip codes and are usually presented
     * by zip code in search results ( like Paris ).
     * Therefore, we're separating cities per their zip codes
     */
    return geoApiCities.reduce(
      (
        accumulator,
        {nom, code, centre, codesPostaux, region, departement}
      ) => {
        return [
          ...accumulator,
          ...codesPostaux.map((codePostal) => ({
            codePostal,
            code,
            nom,
            centre,
            codeDepartement: departement.code,
            nomDepartement: departement.nom,
            codeRegion: region.code,
            nomRegion: region.nom,
            type: 'city',
            zoom: 13
          }))
        ];
      },
      []
    );
  }

  private async searchCityByGPSCoordinates(
    longitude: string,
    latitude: string
  ): Promise<City> {
    const cities = await this.citiesService.getCityByGPSCoordinates(
      longitude,
      latitude
    );

    if (!cities.length) {
      return null;
    }

    return {
      nom: cities[0].nom,
      code: cities[0].code,
      codePostal: cities[0].codesPostaux[0],
      codeDepartement: cities[0].departement.code,
      nomDepartement: cities[0].departement.nom,
      nomRegion: cities[0].region.nom,
      codeRegion: cities[0].region.code,
      type: 'city',
      zoom: 13
    };
  }

  private async searchDepartments(search: string): Promise<Department[]> {
    return await this.departmentsService
      .getDepartmentsByName(search)
      .then((departments: GeoApiDepartment[]) => {
        return departments.map(
          (department: GeoApiDepartment) =>
            this.turnToSearchResult(department, 'department') as Department
        );
      });
  }

  searchDepartmentsByNames(search: string): Observable<Department[]> {
    const names: string[] = JSON.parse(search);
    return this.departmentsService
      .getDepartmentsByNames(names)
      .pipe(map((departments: GeoApiDepartment[]) =>
        departments.map(
          (department: GeoApiDepartment) =>
            this.turnToSearchResult(department, 'department') as Department
        )
      ));
  }

  async searchRegions(search: string): Promise<Region[]> {
    return await this.regionsService
      .getRegionsByName(search)
      .then((regions: GeoApiRegion[]) => {
        return regions.map(
          (region: GeoApiRegion) =>
            this.turnToSearchResult(region, 'region') as Region
        );
      });
  }

  async searchLocalitiesByName(
    search: string
  ): Promise<GeographicSearchResult> {
    if (!search) {
      throw new BadRequestException(GEO_API_FORMAT_ERROR);
    }

    const regions = await this.searchRegions(search);
    const departments = await this.searchDepartments(search);
    const cities = await this.searchCityByNameOrZipCode(search);

    return [...regions, ...departments, ...cities];
  }

  private getDepartmentCoordinates(departmentName: string): [number, number] {
    const coordinates = DEPARTMENTS.filter(department => departmentName === department.dep_name)[0].coordinates;
    return [coordinates[1], coordinates[0]];
  }

  private getRegionCoordinates(codeRegion: string): [number, number] {
    const coordinates = REGIONS.filter(region => codeRegion === region.code)[0].coordinates;
    return [coordinates[1], coordinates[0]];
  }

  private turnToSearchResult(
    apiObject: GeoApiRegion | GeoApiDepartment | GeoApiCity | GeoApiEpci,
    type: 'region' | 'department' | 'city' | 'epci'
  ): Region | Department | City | Epci {
    let searchResult: Region | Department | City | Epci;
    switch (type) {
      case 'epci':
        searchResult = {
          ...(apiObject as Epci),
          type: 'epci',
          zoom: 10
        };
        break;
      case 'region':
        searchResult = {
          ...apiObject,
          type: 'region',
          zoom: 6.8,
          centre: {
            type: 'Point',
            coordinates: this.getRegionCoordinates(apiObject.code)
          }
        };
        break;
      case 'department':
        searchResult = {
          ...(apiObject as GeoApiDepartment),
          type: 'department',
          zoom: 8,
          centre: {
            type: 'Point',
            coordinates: this.getDepartmentCoordinates(apiObject.nom)
          }
        };
        break;
      case 'city':
        searchResult = {
          ...apiObject,
          codePostal: (apiObject as GeoApiCity).codesPostaux[0],
          type: 'city',
          zoom: 13
        };
        break;
    }

    return searchResult;
  }
}
