/**
 * Nest.js imports
 */
import { Injectable, Logger } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';

/**
 * 3rd-party imports
 */
import { catchError, firstValueFrom } from 'rxjs';
import { AxiosError } from 'axios';

/**
 * TypeScript entities and constants
 */
interface LocalityShape {
  centre?: {
    type: 'Point';
    coordinates: [number, number];
  };
  contour?: {
    type: 'Polygon';
    coordinates: [number, number][][];
  };
  bbox?: {
    type: 'Polygon';
    coordinates: [number, number][][];
  };
}

export interface GeoApiEpci extends LocalityShape {
  code: string;
  nom: string;
  codesDepartements?: string[];
  codesRegions?: string[];
  population?: number;
  financement?: 'FA' | 'FPU';
  type?: 'CA' | 'CC' | 'CU' | 'MET69' | 'METRO';
  surface?: number;
}

export interface CityFromEpci extends LocalityShape {
  nom: string;
  code: string;
  codeDepartement?: string;
  siren?: string;
  codeEpci?: string;
  codeRegion?: string;
  codesPostaux?: string[];
  population?: number;
}

@Injectable()
export class EpcisService {

  private readonly logger = new Logger(EpcisService.name);
  private geoApiUrl: string = 'https://geo.api.gouv.fr/epcis';

  constructor(private readonly httpService: HttpService) {}

  async getEpcisByName(name: string): Promise<GeoApiEpci[]> {
    const { data } = await firstValueFrom(
      this.httpService
        .get<GeoApiEpci[]>(`${this.geoApiUrl}?nom=${name}&fields=code,nom,centre`)
        .pipe(
          catchError((error: AxiosError) => {
            this.logger.error(error.response.data);
            throw error;
          })
        )
    );
    return data;
  }

  async getCitiesByEpciCode(epciCode: string): Promise<CityFromEpci[]> {
    const { data } = await firstValueFrom(
      this.httpService
        .get<CityFromEpci[]>(`${this.geoApiUrl}/${epciCode}/communes?fields=code,nom,centre`)
        .pipe(
          catchError((error: AxiosError) => {
            this.logger.error(error.response.data);
            throw error;
          })
        )
    );
    return data;
  }

}
