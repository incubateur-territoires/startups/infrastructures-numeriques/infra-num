/**
 * 3rd-party imports
 */
import { Epci } from '@infra-num/common';

/**
 * Internal imports
 */
import { GeoApiEpci } from '../epcis.service';


/**
 * TypeScript entities and constants
 */
const geoApiEpci1: GeoApiEpci = {
  nom: 'CC Seine et Aube',
  code: '200070126',
  centre: {
    type: 'Point',
    coordinates: [3.9466, 48.5216]
  }
};

const geoApiEpci2: GeoApiEpci = {
  nom: 'CC de la Région de Bar sur Aube',
  code: '241000405',
  centre: {
    type: 'Point',
    coordinates: [4.7019, 48.1924]
  }
};

const epci1: Epci = {
  code: '200070126',
  nom: 'CC Seine et Aube',
  type: 'epci',
  zoom: 10,
  centre: {
    type: 'Point',
    coordinates: [3.9466, 48.5216]
  }
};

const epci2: Epci = {
  nom: 'CC de la Région de Bar sur Aube',
  code: '241000405',
  type: 'epci',
  zoom: 10,
  centre: {
    type: 'Point',
    coordinates: [4.7019, 48.1924]
  }
};

export const GEO_API_EPCIS_ALREADY_SAVED = [geoApiEpci1, geoApiEpci2];
export const EPCIS_ALREADY_SAVED = [epci1, epci2];

