/**
 * Nest.js imports
 */
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';

/**
 * 3rd-party imports
 */
import request from 'supertest';

/**
 * Internal imports
 */
import { GeoApiModule } from '../../geo-api.module';
import { EpcisServiceMock } from './epcis.service.mock';
import { EpcisService } from '../epcis.service';
import { EPCIS_ALREADY_SAVED, GEO_API_EPCIS_ALREADY_SAVED } from './epcis-already-saved.mock';

describe('Epcis', () => {
  let app: INestApplication;
  let epcisService: EpcisServiceMock;

  beforeAll(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [GeoApiModule]
    })
      .overrideProvider(EpcisService)
      .useClass(EpcisServiceMock)
      .compile();

    epcisService = moduleRef.get<EpcisServiceMock>(EpcisService);
    app = moduleRef.createNestApplication();
    await app.init();
  });

  it(`/GET epcis?search=aube`, () => {
    epcisService.getEpcisByName = jest.fn().mockResolvedValue( GEO_API_EPCIS_ALREADY_SAVED );

    return request(app.getHttpServer())
      .get(`/epcis?search=aube`)
      .expect(200)
      .expect(EPCIS_ALREADY_SAVED);
  });

  afterAll(async () => {
    await app.close();
  });
});
