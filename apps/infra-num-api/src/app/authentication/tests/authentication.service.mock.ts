export class AuthenticationServiceMock {
  login: jest.Mock = jest.fn();
  validateUser: jest.Mock = jest.fn();
}
