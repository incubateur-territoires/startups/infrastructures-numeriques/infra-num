/**
 * Nest.js imports
 */
import { Test } from '@nestjs/testing';
import { ExecutionContext, INestApplication } from '@nestjs/common';

/**
 * 3rd-party imports
 */
import request from 'supertest';
import cookie from 'expect-cookies';

/**
 * Internal imports
 */
import { AuthenticationController } from '../authentication.controller';
import { AuthenticationService } from '../authentication.service';
import { AuthenticationServiceMock } from './authentication.service.mock';
import { LocalAuthGuard } from '../guards/local-auth.guard';
import { LocalStrategy } from '../strategies/local.strategy';
import { JwtStrategy } from '../strategies/jwt.strategy';
import { JwtAuthGuard } from '../guards/jwt-auth.guard';

describe('Authentication', () => {
  let app: INestApplication;
  let authService: AuthenticationServiceMock;
  const guardMock = (context: ExecutionContext) => {
    const req = context.switchToHttp().getRequest();
    req.user = {email: 'test@gouv.fr'};
    return true;
  };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [AuthenticationController],
      providers: [
        {
          provide: AuthenticationService,
          useClass: AuthenticationServiceMock
        },
        LocalStrategy,
        JwtStrategy
      ]
    })
      .overrideGuard(LocalAuthGuard)
      .useValue({canActivate: guardMock})
      .overrideGuard(JwtAuthGuard)
      .useValue({canActivate: guardMock})
      .compile();

    authService = moduleRef.get<AuthenticationServiceMock>(AuthenticationService);

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it(`POST /login`, () => {
    authService.login.mockResolvedValue({access_token: 'fake token'});

    return request(app.getHttpServer())
      .post('/login')
      .send({email: 'test@gouv.fr', password: 'fake pass'})
      .expect(200)
      .expect(cookie.set({'name': 'auth'}))
      .expect({email: 'test@gouv.fr'});
  });

  it(`GET /logout`, () => {
    return request(app.getHttpServer())
      .get('/logout')
      .expect(200)
      .expect('');
  });

  it(`GET /me`, () => {
    return request(app.getHttpServer())
      .get('/me')
      .expect(200)
      .expect({email: 'test@gouv.fr'});
  });

  afterAll(async () => {
    await app.close();
  });
});
