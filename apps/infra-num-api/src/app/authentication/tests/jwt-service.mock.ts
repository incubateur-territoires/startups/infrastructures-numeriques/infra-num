export class JwtServiceMock {
  verify: jest.Mock = jest.fn();
  sign: jest.Mock = jest.fn();
}
