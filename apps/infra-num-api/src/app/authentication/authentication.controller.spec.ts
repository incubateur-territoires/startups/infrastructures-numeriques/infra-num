/**
 * Nest.js imports
 */
import { Test, TestingModule } from '@nestjs/testing';

/**
 * 3rd-party imports
 */
import { Request, Response } from 'express';
import { SafeUser } from '@infra-num/common';

/**
 * Internal imports
 */
import { AuthenticationController } from './authentication.controller';
import { AuthenticationService } from './authentication.service';
import { AuthenticationServiceMock } from './tests/authentication.service.mock';
import { UserWithoutPasswords } from '../users/user.schema';

describe('AuthenticationController', () => {
  let controller: AuthenticationController;
  let authService: AuthenticationServiceMock;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthenticationController],
      providers: [
        {
          provide: AuthenticationService,
          useClass: AuthenticationServiceMock
        }
      ]
    }).compile();

    controller = module.get<AuthenticationController>(AuthenticationController);
    authService = module.get<AuthenticationServiceMock>(AuthenticationService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should login a user and send the cookie to be stored in the front', async () => {
    authService.login.mockResolvedValue({ access_token: 'fake token' });
    const res = {
      status: jest.fn(),
      cookie: jest.fn(),
      json: jest.fn()
    };

    res.status.mockReturnValue(res);
    res.cookie.mockReturnValue(res);

    await controller.login(
      { user: { email: 'test@gouv.fr' } } as Request & {
        user: UserWithoutPasswords;
      },
      res as unknown as Response
    );

    expect(authService.login).toHaveBeenCalled();
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.cookie).toHaveBeenCalledWith('auth', 'fake token', {
      httpOnly: true,
      maxAge: 2 * 3600 * 1000
    });
    expect(res.json).toHaveBeenCalledWith({ email: 'test@gouv.fr' });
  });

  it('should logout a user and remove the cookie', async () => {
    const res = {
      status: jest.fn(),
      clearCookie: jest.fn(),
      send: jest.fn()
    };

    res.status.mockReturnValue(res);
    res.clearCookie.mockReturnValue(res);

    await controller.logout(res as unknown as Response);

    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.clearCookie).toHaveBeenCalledWith('auth', { httpOnly: true });
    expect(res.send).toHaveBeenCalled();
  });
});
