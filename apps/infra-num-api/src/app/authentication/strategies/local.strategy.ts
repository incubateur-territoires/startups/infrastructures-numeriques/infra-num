/**
 * Nest.js imports
 */
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';

/**
 * 3rd-party imports
 */
import { Strategy } from 'passport-local';

/**
 * Internal imports
 */
import { AuthenticationService } from '../authentication.service';
import { UserWithoutPasswords } from '../../users/user.schema';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {

  constructor(private authService: AuthenticationService) {
    super({usernameField: 'email'});
  }

  async validate(email: string, password: string): Promise<UserWithoutPasswords> {
    const user: UserWithoutPasswords | string = await this.authService.validateUser(email, password);
    if (typeof user === 'string') {
      throw new UnauthorizedException(user);
    }
    return user;
  }
}
