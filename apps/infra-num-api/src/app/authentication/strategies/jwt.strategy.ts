/**
 * Nest.js imports
 */
import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';

/**
 * 3rd-party imports
 */
import { ExtractJwt, Strategy } from 'passport-jwt';
import { Request } from 'express';
import { Jurisdiction, SafeUser } from '@infra-num/common';

/**
 * Internal imports
 */
import { UsersService } from '../../users/users.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private usersService: UsersService) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([JwtStrategy.extractJWT]),
      ignoreExpiration: false,
      secretOrKey: process.env.INFRANUM_JWT_SECRET
    });
  }

  private static extractJWT(req: Request): string | null {
    if (req.cookies && 'auth' in req.cookies) {
      return req.cookies.auth;
    }
    return null;
  }

  async validate(payload: SafeUser): Promise<SafeUser> {
    const currentUser = await this.usersService.findOne(payload.email);

    return {
      _id: currentUser._id,
      email: currentUser.email,
      roles: currentUser.roles,
      jurisdictions: currentUser.jurisdictions as Jurisdiction[],
      preferredCoordinates: currentUser.preferredCoordinates,
      preferences: currentUser.preferences,
      hasSubscribedToNewsletter: currentUser.hasSubscribedToNewsletter
    };
  }
}
