/**
 * Nest.js imports
 */
import {
  Controller,
  Post,
  Response,
  UseGuards,
  Request,
  Get
} from '@nestjs/common';

/**
 * 3rd-party imports
 */
import { AccessToken, SafeUser } from '@infra-num/common';
import { Request as RequestObject, Response as ResponseObject } from 'express';

/**
 * Internal imports
 */
import { AuthenticationService } from './authentication.service';
import { environment } from '../../environments/environment';
import { UserWithoutPasswords } from '../users/user.schema';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { UsersService } from '../users/users.service';

@Controller()
export class AuthenticationController {
  constructor(
    private readonly authService: AuthenticationService,
    private readonly usersService: UsersService
  ) {}

  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(
    @Request() req: RequestObject & { user: UserWithoutPasswords },
    @Response() res: ResponseObject
  ) {
    const jwt: AccessToken = await this.authService.login(req.user);

    res
      .status(200)
      .cookie('auth', jwt.access_token, {
        httpOnly: true,
        maxAge: 2 * 3600 * 1000,
        ...(environment.production && { sameSite: 'strict', secure: true })
      })
      .json({
        _id: req.user._id,
        email: req.user.email,
        roles: req.user.roles,
        jurisdictions: req.user.jurisdictions,
        preferredCoordinates: req.user.preferredCoordinates,
        preferences: req.user.preferences
      });
  }

  @Get('logout')
  async logout(@Response() res: ResponseObject) {
    res
      .status(200)
      .clearCookie('auth', {
        httpOnly: true,
        ...(environment.production && { sameSite: 'strict', secure: true })
      })
      .send();
  }

  @UseGuards(JwtAuthGuard)
  @Get('user-information')
  async userInformation(@Request() req: RequestObject & { user: SafeUser }) {
    const currentUser = await this.usersService.findOne(req.user.email);
    return {
      _id: currentUser._id,
      email: currentUser.email,
      roles: currentUser.roles,
      jurisdictions: currentUser.jurisdictions,
      preferredCoordinates: currentUser.preferredCoordinates,
      preferences: currentUser.preferences
    };
  }
}
