/**
 * Nest.js imports
 */
import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';

/**
 * Internal imports
 */
import { AuthenticationService } from './authentication.service';
import { UsersModule } from '../users/users.module';
import { LocalStrategy } from './strategies/local.strategy';
import { JwtStrategy } from './strategies/jwt.strategy';
import { AuthenticationController } from './authentication.controller';
import { environment } from '../../environments/environment';

@Module({
  imports: [
    UsersModule,
    PassportModule,
    JwtModule.register({
      secret: process.env.INFRANUM_JWT_SECRET,
      signOptions: {expiresIn: environment.jwtExpirationTimes.authentication}
    })
  ],
  controllers: [
    AuthenticationController
  ],
  providers: [
    AuthenticationService,
    LocalStrategy,
    JwtStrategy
  ]
})
export class AuthenticationModule {
}
