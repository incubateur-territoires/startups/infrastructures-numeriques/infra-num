/**
 * Nest.js imports
 */
import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

/**
 * 3rd-party imports
 */
import {
  AccessToken,
  ACCOUNT_DEACTIVATED,
  ACCOUNT_NOT_CONFIRMED,
  INVALID_LOGIN_CREDENTIALS,
  SafeUser
} from '@infra-num/common';
import bcrypt from 'bcryptjs';

/**
 * Internal imports
 */
import { UsersService } from '../users/users.service';
import { UserWithoutPasswords } from '../users/user.schema';

@Injectable()
export class AuthenticationService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService
  ) {}

  async validateUser(
    email: string,
    password: string
  ): Promise<UserWithoutPasswords | string> {
    const user = await this.usersService.findOne(email);
    if (!user) {
      return INVALID_LOGIN_CREDENTIALS;
    }
    const { password: userPassword, oldPasswords, ...result } = user;
    const isPasswordCorrect = await bcrypt.compare(password, userPassword);
    if (!isPasswordCorrect) {
      return INVALID_LOGIN_CREDENTIALS;
    }
    if (!result.isEmailConfirmed) {
      return ACCOUNT_NOT_CONFIRMED;
    }
    if (!result.isActive) {
      return ACCOUNT_DEACTIVATED;
    }
    return result as UserWithoutPasswords;
  }

  async login(user: UserWithoutPasswords): Promise<AccessToken> {
    const payload: Omit<SafeUser, 'jurisdictions'> = {
      _id: user._id,
      email: user.email,
      roles: user.roles,
      hasSubscribedToNewsletter: user.hasSubscribedToNewsletter,
      preferredCoordinates: user.preferredCoordinates,
      preferences: user.preferences
    };

    // Tracking the date of the last login
    await this.usersService.updateOneById(user._id, {
      lastLoginDate: new Date()
    });

    return {
      access_token: this.jwtService.sign(payload)
    };
  }
}
