/**
 * Nest.js imports
 */
import { Test, TestingModule } from '@nestjs/testing';
import { JwtService } from '@nestjs/jwt';

/**
 * 3rd-party imports
 */
jest.mock('bcryptjs');
import bcrypt from 'bcryptjs';

/**
 * Internal imports
 */
import { UsersService } from '../users/users.service';
import { UsersServiceMock } from '../users/tests/users-service.mock';
import { JwtServiceMock } from './tests/jwt-service.mock';
import { AuthenticationService } from './authentication.service';
import { Role } from '@infra-num/common';
import { UserDocument } from '../users/user.schema';

describe('AuthenticationService', () => {
  let authService: AuthenticationService;
  let usersService: UsersServiceMock;
  let jwtService: JwtServiceMock;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthenticationService,
        {
          provide: UsersService,
          useClass: UsersServiceMock
        },
        {
          provide: JwtService,
          useClass: JwtServiceMock
        }
      ]
    }).compile();
    authService = module.get<AuthenticationService>(AuthenticationService);
    usersService = module.get<UsersServiceMock>(UsersService);
    jwtService = module.get<JwtServiceMock>(JwtService);
  });

  it('should be defined', () => {
    expect(authService).toBeDefined();
  });

  it('should validate user if credentials are right', async () => {
    const email: string = 'test@test.com';
    const password: string = 'Resistance is futile';
    usersService.findOne.mockResolvedValue({
      email,
      password,
      isEmailConfirmed: true
    });
    (bcrypt.compare as jest.Mock).mockResolvedValue(true);

    const userValidated = await authService.validateUser(email, password);

    expect(usersService.findOne).toHaveBeenCalled();
    expect(bcrypt.compare).toHaveBeenCalled();
    expect(userValidated).toEqual({email, isEmailConfirmed: true});
  });


  it('should not validate if no user corresponds to the provided email', async () => {
    usersService.findOne.mockResolvedValue(null);
    const isValid = await authService.validateUser('test@gouv.fr', 'pass');
    expect(usersService.findOne).toHaveBeenCalled();
    expect(bcrypt.compare).not.toHaveBeenCalled();
    expect(isValid).toEqual(null);
  });

  it('should not validate if the provided password is wrong', async () => {
    usersService.findOne.mockResolvedValue({
      email: 'test@test.com',
      isEmailConfirmed: true,
      role: Role.Mayor,
      password: 'Resistance is futile'
    });
    bcrypt.compare = jest.fn().mockResolvedValue(false);
    const isValid = await authService.validateUser('test@gouv.fr', 'pass');
    expect(usersService.findOne).toHaveBeenCalled();
    expect(bcrypt.compare).toHaveBeenCalled();
    expect(isValid).toEqual(null);
  });

  it('should login user if credentials are right', async () => {
    jwtService.sign.mockReturnValue('fake token');
    const token = await authService.login({
      email: 'test@gouv.fr',
      _id: 'fakeId',
      role: Role.Mayor
    } as unknown as Omit<UserDocument, 'password'>);
    expect(jwtService.sign).toHaveBeenCalled();
    expect(token.access_token).toEqual('fake token');
  });
});
