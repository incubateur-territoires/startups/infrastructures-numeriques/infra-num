export const environment = {
  production: true,
  appName: 'Toutes et tous connecté·e·s',
  frontUrl: 'https://tous-connectes.anct.gouv.fr',
  corsOrigin: 'to be defined',
  jwtExpirationTimes: {
    confirmationEmail: '2d',
    authentication: '2h'
  }
};
