export const environment = {
  production: false,
  appName: 'Toutes et tous connecté·e·s',
  frontUrl: 'https://dev.tous-connectes.anct.gouv.fr',
  corsOrigin: 'http://localhost:4200',
  jwtExpirationTimes: {
    confirmationEmail: '2d',
    authentication: '2h'
  }
};
