# InfraNum API

This project is part of the **InfraNum** MonoRepo, managed via [Nx](https://nx.dev). It holds the Back-End part
of the "Infrastructures Numériques" Web app, as a **REST API**, built with [Nest.js](https://nestjs.com/) and served with [AWS Lambda](https://aws.amazon.com/fr/lambda/?nc2=h_ql_prod_fs_lbd),
through the [serverless](https://www.serverless.com/) framework.

## How to use this project ? 🤓

### Develop locally
Tap this in your Terminal, _while being at the MonoRepo's root_:
```shell
nx serve infra-num-api
```
Now, you can update the code and the generated server will be automatically
updated as well.

### Make a first deployment on AWS Lambda
Tap this in your Terminal, _while being at the MonoRepo's root_:
 ```shell
 nx deploy infra-num-api
 ```

<blockquote>
  🚨 Once it's done, the output in your Terminal will give you
the proper URL to use to query your API 🚨
</blockquote>

<blockquote>
  ⚠️ Use this <em>only</em> to make your <em>first</em> deployment as it takes quite some time.
Once you've made this, you can simply update the deployment, which is
much faster 😉
</blockquote>

### Update an already-made deployment on AWS Lambda
If you need to debug your API on AWS Lambda ( _for a bug that would only occur
online and not locally_ ) or if you just want to tweak your code and see the result
of your changes directly on AWS Lambda, tap this in your Terminal, _while being at the MonoRepo's root_:
 ```shell
 nx update infra-num-api
 ```

<blockquote>
  ⚠️ To see the difference between <code>deploy</code> and <code>update</code>, and why it's faster, feel
free to check <a href="https://www.serverless.com/framework/docs/providers/aws/guide/deploying#deploy-function">this</a> 😉
</blockquote>

<hr>

Even better ! If you plan on doing this for a while, use this to automatically update your deployment
on each change in your codebase 😊

 ```shell
 nx watch infra-num-api
 ```

### Remove your current deployment on AWS Lambda
Tap this in your Terminal, _while being at the MonoRepo's root_:
 ```shell
 nx remove infra-num-api
 ```

<blockquote>
  ⚠️ DO NOT FORGET to remove a deployment on AWS Lambda that was meant only to test your
implementation. In the end, it will take up resources and might cost money for nothing 😉
</blockquote>
