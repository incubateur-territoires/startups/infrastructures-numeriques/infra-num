/**
 * Angular imports
 */
import { Component } from '@angular/core';

@Component({
  templateUrl: './site-map.component.html',
  styleUrls: ['./site-map.component.scss']
})
export class SiteMapComponent {
}
