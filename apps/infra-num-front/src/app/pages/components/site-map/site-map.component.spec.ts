/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

/**
 * Internal imports
 */
import { SiteMapComponent } from './site-map.component';

/**
 * 3rd-Party imports
 */
import { DsfrLinkModule } from '@betagouv/ngx-dsfr/link';

describe('SiteMapComponent', () => {
  let component: SiteMapComponent;
  let fixture: ComponentFixture<SiteMapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, DsfrLinkModule],
      declarations: [SiteMapComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(SiteMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
