/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';

/**
 * Internal imports
 */
import { CookiesManagementComponent } from './cookies-management.component';

describe('CookiesManagementComponent', () => {
  let component: CookiesManagementComponent;
  let fixture: ComponentFixture<CookiesManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CookiesManagementComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(CookiesManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
