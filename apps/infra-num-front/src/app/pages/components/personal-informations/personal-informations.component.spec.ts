/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';

/**
 * Internal imports
 */
import { PersonalInformationsComponent } from './personal-informations.component';

describe('PersonalInformationsComponent', () => {
  let component: PersonalInformationsComponent;
  let fixture: ComponentFixture<PersonalInformationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PersonalInformationsComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PersonalInformationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
