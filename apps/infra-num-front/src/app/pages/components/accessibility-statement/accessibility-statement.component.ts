/**
 * Angular imports
 */
import { Component } from '@angular/core';

@Component({
  templateUrl: './accessibility-statement.component.html',
  styleUrl: './accessibility-statement.component.scss'
})
export class AccessibilityStatementComponent {
}
