/**
 * Angular imports
 */
import { Component } from '@angular/core';

@Component({
  templateUrl: './statistics-impact.component.html',
  styleUrls: ['./statistics-impact.component.scss']
})
export class StatisticsImpactComponent {
}
