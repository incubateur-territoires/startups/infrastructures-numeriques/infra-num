/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';

/**
 * Internal imports
 */
import { StatisticsImpactComponent } from './statistics-impact.component';

describe('StatisticsImpactComponent', () => {
  let component: StatisticsImpactComponent;
  let fixture: ComponentFixture<StatisticsImpactComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StatisticsImpactComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(StatisticsImpactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
