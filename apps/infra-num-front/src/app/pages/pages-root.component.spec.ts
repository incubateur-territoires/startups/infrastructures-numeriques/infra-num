/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

/**
 * 3rd-party imports
 */
import { DsfrNavigationModule } from '@betagouv/ngx-dsfr/navigation';
import { DsfrHeaderModule } from '@betagouv/ngx-dsfr/header';
import { DsfrFooterModule } from '@betagouv/ngx-dsfr/footer';
import { DsfrLinkModule } from '@betagouv/ngx-dsfr/link';

/**
 * Internal imports
 */
import { PagesRootComponent } from './pages-root.component';
import { AuthenticationService } from '../user/services/authentication.service';
import { AuthenticationServiceMock } from '../user/mocks/authentication.service.mock';
import { StandardLayoutComponent } from '../shared/components/standard-layout/standard-layout.component';
import { HeaderComponent } from '../shared/components/header/header.component';
import { FooterComponent } from '../shared/components/footer/footer.component';

describe('PagesRootComponent', () => {
  let component: PagesRootComponent;
  let fixture: ComponentFixture<PagesRootComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        DsfrNavigationModule,
        DsfrHeaderModule,
        DsfrFooterModule,
        DsfrLinkModule
      ],
      declarations: [
        PagesRootComponent,
        StandardLayoutComponent,
        HeaderComponent,
        FooterComponent
      ],
      providers: [
        { provide: AuthenticationService, useClass: AuthenticationServiceMock }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(PagesRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
