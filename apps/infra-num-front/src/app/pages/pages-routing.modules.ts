/**
 * Angular imports
 */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

/**
 * Internal imports
 */
import { PagesRootComponent } from './pages-root.component';
import { SiteMapComponent } from './components/site-map/site-map.component';
import { CookiesManagementComponent } from './components/cookies-management/cookies-management.component';
import { TermsAndConditionsComponent } from './components/terms-and-conditions/terms-and-conditions.component';
import { PersonalInformationsComponent } from './components/personal-informations/personal-informations.component';
import {
  AccessibilityStatementComponent
} from './components/accessibility-statement/accessibility-statement.component';
import { StatisticsImpactComponent } from './components/statistics-impact/statistics-impact.component';

/**
 * TypeScript entities and constants
 */
const routes: Routes = [
  {
    path: '',
    component: PagesRootComponent,
    children: [
      {
        path: 'plan-du-site', component: SiteMapComponent
      },
      {
        path: 'declaration-accessibilite', component: AccessibilityStatementComponent
      },
      {
        path: 'mentions-legales', component: TermsAndConditionsComponent
      },
      {
        path: 'gestion-des-cookies', component: CookiesManagementComponent
      },
      {
        path: 'donnees-personnelles', component: PersonalInformationsComponent
      },
      {
        path: 'statistiques-et-impact', component: StatisticsImpactComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule {
}
