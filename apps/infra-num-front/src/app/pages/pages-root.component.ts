/**
 * Angular imports
 */
import { Component } from '@angular/core';

@Component({
  selector: 'infra-num-pages-root',
  templateUrl: './pages-root.component.html',
  styleUrls: ['./pages-root.component.scss']
})
export class PagesRootComponent {
}
