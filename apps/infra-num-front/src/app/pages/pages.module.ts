/**
 * Angular imports
 */
import { NgModule } from '@angular/core';

/**
 * Internal imports
 */
import { SharedModule } from '../shared/shared.module';
import { PagesRoutingModule } from './pages-routing.modules';
import { CookiesManagementComponent } from './components/cookies-management/cookies-management.component';
import { TermsAndConditionsComponent } from './components/terms-and-conditions/terms-and-conditions.component';
import { PersonalInformationsComponent } from './components/personal-informations/personal-informations.component';
import { SiteMapComponent } from './components/site-map/site-map.component';
import { PagesRootComponent } from './pages-root.component';
import { AccessibilityStatementComponent } from './components/accessibility-statement/accessibility-statement.component';
import { StatisticsImpactComponent } from './components/statistics-impact/statistics-impact.component';

/**
 * 3rd-Party imports
 */
import { DsfrLinkModule } from '@betagouv/ngx-dsfr/link';

@NgModule( {
  imports: [
    SharedModule,
    DsfrLinkModule,
    PagesRoutingModule,
  ],
  declarations: [
    PagesRootComponent,
    CookiesManagementComponent,
    TermsAndConditionsComponent,
    PersonalInformationsComponent,
    SiteMapComponent,
    AccessibilityStatementComponent,
    StatisticsImpactComponent
  ]
})
export class PagesModule {
}
