/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';

/**
 * 3rd-party imports
 */
import { DsfrButtonModule } from '@betagouv/ngx-dsfr/button';

/**
 * Internal imports
 */
import { ActionMenuComponent } from './action-menu.component';
import { AuthenticationService } from '../../../user/services/authentication.service';
import { AuthenticationServiceMock } from '../../../user/mocks/authentication.service.mock';

jest.mock('maplibre-gl', () => ({
  Map: jest
    .fn()
    .mockReturnValue({
      addControl: jest.fn(),
      on: jest.fn(),
      remove: jest.fn()
    }),
  NavigationControl: jest.fn(),
  ScaleControl: jest.fn(),
  GeoJSONSource: jest.fn(),
  MapGeoJSONFeature: jest.fn()
}));

describe('ActionMenuComponent', () => {
  let component: ActionMenuComponent;
  let fixture: ComponentFixture<ActionMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DsfrButtonModule],
      declarations: [ActionMenuComponent],
      providers: [
        {provide: AuthenticationService, useClass: AuthenticationServiceMock}
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(ActionMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
