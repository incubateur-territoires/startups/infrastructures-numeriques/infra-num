/**
 * Angular imports
 */
import { Component, effect, Injector, OnInit, Signal } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

/**
 * 3rd-party imports
 */
import { SafeUser, CAN_CREATE_REPORT_ROLES } from '@infra-num/common';

/**
 * Internal imports
 */
import {
  DynamicBoxMobileDisplay,
  DynamicBoxOptions,
  DynamicBoxService
} from '../../dynamic-box.service';
import { ReportFormComponent } from '../../../mobile/report/components/report-form/report-form.component';
import { AuthenticationService } from '../../../user/services/authentication.service';

@Component({
  selector: 'infra-num-action-menu',
  templateUrl: './action-menu.component.html',
  styleUrls: ['./action-menu.component.scss']
})
export class ActionMenuComponent implements OnInit {
  boxOptions: Signal<DynamicBoxOptions> =
    this.dynamicBoxService.dynamicBoxOptions;

  displayReportBadNetworkButton: boolean = false;

  private user: SafeUser;
  private takeUntilDestroyed = takeUntilDestroyed();

  constructor(
    private readonly dynamicBoxService: DynamicBoxService,
    private readonly authenticationService: AuthenticationService,
    private readonly injector: Injector
  ) {}

  ngOnInit(): void {
    this.getUser();
    this.setDisplayFlags();
  }

  private getUser(): void {
    this.authenticationService.authenticatedUser
      .pipe(this.takeUntilDestroyed)
      .subscribe((authenticatedUser: SafeUser) => {
        this.user = authenticatedUser;
      });
  }

  private setDisplayFlags(): void {
    effect(
      () => {
        this.displayReportBadNetworkButton =
          !this.boxOptions().isForm &&
          this.user?.roles?.some((role) =>
            CAN_CREATE_REPORT_ROLES.includes(role)
          );
      },
      { injector: this.injector }
    );
  }

  onReportBadNetwork(): void {
    this.dynamicBoxService.showBoxWithContent(ReportFormComponent, null, {
      animated: true,
      display: true,
      mainBox: true,
      isForm: true,
      displayOnMobile: DynamicBoxMobileDisplay.Intermediate
    });
  }
}
