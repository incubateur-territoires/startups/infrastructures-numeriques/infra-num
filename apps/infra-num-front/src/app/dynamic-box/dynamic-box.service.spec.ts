/**
 * Angular imports
 */
import { TestBed } from '@angular/core/testing';

/**
 * Internal imports
 */
import { DynamicBoxService } from './dynamic-box.service';

describe('DynamicBoxService', () => {
  let service: DynamicBoxService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DynamicBoxService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
