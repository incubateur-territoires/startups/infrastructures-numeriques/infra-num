/**
 * Angular imports
 */
import { Injectable, Injector, WritableSignal, signal } from '@angular/core';
import { ComponentPortal, ComponentType } from '@angular/cdk/portal';

/**
 * 3rd-party imports
 */
import { Subject } from 'rxjs';

/**
 * Internal imports
 */
import { COMPONENT_PORTAL_DATA } from '../common';
import { PossibleView, ViewService } from '../shared/view.service';

/**
 * TypeScript entities and constants
 */
export enum DynamicBoxMobileDisplay {
  Hidden = 'Hidden',
  Intermediate = 'Intermediate',
  FullHeight = 'FullHeight'
}

export interface DynamicBoxOptions {
  animated: boolean;
  display: boolean;
  isForm: boolean;
  hasStrip: boolean;
  displayOnMobile: DynamicBoxMobileDisplay;
  showCloseOnMobile: boolean;
  mainBox: boolean;
}

export interface DynamicBoxContent {
  content: ComponentPortal<any>;
  mainBox: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class DynamicBoxService {
  private readonly defaultBoxOptions: DynamicBoxOptions = {
    animated: false,
    display: false,
    isForm: false,
    hasStrip: false,
    displayOnMobile: DynamicBoxMobileDisplay.Hidden,
    mainBox: true,
    showCloseOnMobile: false
  };

  dynamicBoxOptions: WritableSignal<DynamicBoxOptions> = signal({
    ...this.defaultBoxOptions
  });

  dynamicBoxContent: WritableSignal<DynamicBoxContent> = signal(null);
  boxClosedOnMobile: Subject<boolean> = new Subject<boolean>();

  constructor(private viewService: ViewService) {}

  /**
   * The proper way to do this is to set the content first,
   * then to give a delay so that the content has a chance
   * to finish loading before actually displaying the Box
   *
   * @param newContent: the Component we want to display within
   * the Dynamic Box
   * @param data: any data we need to provide to the Component
   * @param requestedOptions: the display options for the Box
   *
   * @returns void
   */
  showBoxWithContent<T>(
    newContent: ComponentType<T>,
    data: Record<string, any> = null,
    requestedOptions: Partial<DynamicBoxOptions>
  ): void {
    const mainBox = requestedOptions.mainBox ?? true;
    this.setContentBox(
      {
        content: newContent,
        mainBox
      },
      data
    );

    setTimeout(() => {
      this.dynamicBoxOptions.set({
        animated: requestedOptions.animated ?? false,
        display: requestedOptions.display ?? false,
        isForm: requestedOptions.isForm ?? false,
        hasStrip: requestedOptions.hasStrip ?? false,
        mainBox,
        displayOnMobile:
          requestedOptions.displayOnMobile ?? DynamicBoxMobileDisplay.Hidden,
        showCloseOnMobile: requestedOptions.showCloseOnMobile ?? false
      });
    }, 200);
  }

  private setContentBox<T>(
    newContent: {content: ComponentType<T>, mainBox: boolean},
    data: Record<string, any> = null
  ): void {
    const portalInjector = Injector.create({
      providers: [{ provide: COMPONENT_PORTAL_DATA, useValue: data }]
    });
    this.dynamicBoxContent.set(
      {
        content: new ComponentPortal<T>(newContent.content, null, portalInjector),
        mainBox: newContent.mainBox
      }
    );
  }

  /**
   * Used when we need to hide the Box and we keep
   * displaying the parent Component a.k.a. we're INTENTIONALLY
   * closing the Box
   *
   * The 500ms delay is used to make sure the content of the Box
   * is being emptied once the Box is not being displayed anymore
   */
  hideBox(mainBox:boolean = true): void {
    this.dynamicBoxOptions.set(
      {
        ...this.defaultBoxOptions,
        mainBox: mainBox
      }
    );

    setTimeout(() => {
      this.dynamicBoxContent.set({
        content: null,
        mainBox
      });

      if (!mainBox) {
        this.dynamicBoxOptions.set(
          {
            animated: true,
            display: true,
            isForm: false,
            hasStrip: this.viewService.currentView() === PossibleView.MAP,
            mainBox: true,
            displayOnMobile: DynamicBoxMobileDisplay.FullHeight,
            showCloseOnMobile: true
          }
        );
      }

    }, mainBox ? 500 : 0);
  }

  /**
   * Used only when destroying the DynamicBoxComponent
   */
  resetBox(): void {
    this.dynamicBoxContent.set(null);
    this.dynamicBoxOptions.set({ ...this.defaultBoxOptions });
  }
}
