/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';

/**
 * 3rd-party imports
 */
import { PortalModule } from '@angular/cdk/portal';

/**
 * Internal imports
 */
import { DynamicBoxComponent } from './dynamic-box.component';

describe('DynamicBoxComponent', () => {
  let component: DynamicBoxComponent;
  let fixture: ComponentFixture<DynamicBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PortalModule],
      declarations: [DynamicBoxComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(DynamicBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
