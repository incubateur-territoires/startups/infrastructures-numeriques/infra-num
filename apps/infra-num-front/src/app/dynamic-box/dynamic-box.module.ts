/**
 * Angular imports
 */
import { NgModule } from '@angular/core';
import { PortalModule } from '@angular/cdk/portal';
import {ScrollingModule} from '@angular/cdk/scrolling';

/**
 * 3rd-party imports
 */
import { DsfrButtonModule } from '@betagouv/ngx-dsfr/button';

/**
 * Internal imports
 */
import { DynamicBoxComponent } from './dynamic-box.component';
import { ActionMenuComponent } from './components/action-menu/action-menu.component';
import { ReportModule } from '../mobile/report/report.module';
import { SharedModule } from '../shared/shared.module';

/**
 * TypeScript entities and constants
 */
const exportableDeclarables = [DynamicBoxComponent, ActionMenuComponent];

@NgModule({
  imports: [SharedModule, DsfrButtonModule, PortalModule, ReportModule, ScrollingModule],
  declarations: exportableDeclarables,
  exports: exportableDeclarables
})
export class DynamicBoxModule {}
