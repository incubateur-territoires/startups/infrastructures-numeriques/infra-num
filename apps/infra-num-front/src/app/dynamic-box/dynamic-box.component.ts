/**
 * Angular imports
 */
import {
  Component,
  computed,
  Input,
  effect,
  HostBinding,
  OnDestroy,
  Signal
} from '@angular/core';
import { ComponentPortal } from '@angular/cdk/portal';

/**
 * 3rd-party imports
 */
import { ButtonType } from '@betagouv/ngx-dsfr/button';

/**
 * Internal imports
 */
import {
  DynamicBoxMobileDisplay,
  DynamicBoxOptions,
  DynamicBoxService
} from './dynamic-box.service';

@Component({
  selector: 'infra-num-dynamic-box',
  templateUrl: './dynamic-box.component.html',
  styleUrls: ['./dynamic-box.component.scss']
})
export class DynamicBoxComponent implements OnDestroy{

  buttonTypes = ButtonType;

  @Input() mainBox: boolean = true;

  boxOptions: Signal<DynamicBoxOptions> = computed(() => {
    this.currentOptions = null;
    const dynamicBoxOptions = this.dynamicBoxService.dynamicBoxOptions();

    if (
      (this.mainBox && dynamicBoxOptions?.mainBox) ||
      (!this.mainBox && !dynamicBoxOptions?.mainBox)
    ) {
      this.currentOptions = dynamicBoxOptions;
    }

    return this.currentOptions;
  });

  boxContent: Signal<ComponentPortal<any>> = computed(() => {
    const dynamicBoxContent = this.dynamicBoxService.dynamicBoxContent();
    if (
      (this.mainBox && dynamicBoxContent?.mainBox) ||
      (!this.mainBox && !dynamicBoxContent?.mainBox)
    ) {
      this.currentContent = dynamicBoxContent?.content;
    }
    return this.currentContent;
  });

  private currentOptions: DynamicBoxOptions;
  private currentContent: ComponentPortal<any>;

  @HostBinding('class.animated')
  animated: boolean = false;

  @HostBinding('class.active')
  displayBox: boolean = false;

  @HostBinding('class.mobile-intermediate')
  intermediateStateMobile: boolean = false;

  @HostBinding('class.mobile-full-height')
  fullHeightStateMobile: boolean = false;

  @HostBinding('class.secondary-box')
  secondaryBox: boolean = true;

  private setHostClasses = effect(() => {
    const boxOptions = this.boxOptions();

    if (boxOptions) {
      this.animated = boxOptions.animated;
      this.displayBox = boxOptions.display;
      this.secondaryBox = !boxOptions.mainBox;
      this.intermediateStateMobile =
        boxOptions.displayOnMobile === DynamicBoxMobileDisplay.Intermediate;
      this.fullHeightStateMobile =
        boxOptions.displayOnMobile === DynamicBoxMobileDisplay.FullHeight;
    }
  });

  constructor(private readonly dynamicBoxService: DynamicBoxService) {}

  ngOnDestroy(): void {
    // Cleaning up for next time it will be used
    this.dynamicBoxService.resetBox();
  }

  onStripClicked(): void {
    this.dynamicBoxService.dynamicBoxOptions.update(
      (options: DynamicBoxOptions) => {
        return {
          ...options,
          display: !this.displayBox
        };
      }
    );
  }

  onCloseDynamicBox(): void {
    this.dynamicBoxService.hideBox(this.mainBox);
    if (this.mainBox) {
      this.dynamicBoxService.boxClosedOnMobile.next(true);
    }
  }

}
