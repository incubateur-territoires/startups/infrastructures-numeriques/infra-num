/**
 * Angular imports
 */
import { APP_INITIALIZER, LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

/**
 * 3rd-party imports
 */
import { Observable } from 'rxjs';
import { SafeUser } from '@infra-num/common';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';

/**
 * Internal imports
 */
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { UserModule } from './user/user.module';
import { AuthenticationService } from './user/services/authentication.service';
import { PagesModule } from './pages/pages.module';
import { HomeModule } from './home/home.module';

function initializeApp(
  authenticationService: AuthenticationService
): () => Observable<SafeUser> {
  return () => {
    return authenticationService.updateAuthenticatedUser();
  };
}

registerLocaleData(localeFr);

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HomeModule,
    UserModule,
    PagesModule,
    AppRoutingModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializeApp,
      deps: [AuthenticationService],
      multi: true
    },
    { provide: LOCALE_ID, useValue: 'fr-FR' }
  ],
  declarations: [AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule {}
