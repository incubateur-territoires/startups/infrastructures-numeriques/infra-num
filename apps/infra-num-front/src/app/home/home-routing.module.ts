/**
 * Angular imports
 */
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

/**
 * Internal imports
 */
import { HomeComponent } from './components/home/home.component';

/**
 * TypeScript entities and constants
 */
const routes = [
  {
    path: '',
    component: HomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {}
