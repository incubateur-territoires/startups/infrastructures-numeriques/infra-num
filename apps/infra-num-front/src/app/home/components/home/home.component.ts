/**
 * Angular imports
 */
import { Component } from '@angular/core';

/**
 * Internal imports
 */
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'infra-num-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  appName: string = environment.appName;
}
