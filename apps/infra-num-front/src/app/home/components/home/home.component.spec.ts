/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

/**
 * 3rd-party imports
 */
import { DsfrLinkModule } from '@betagouv/ngx-dsfr/link';
import { DsfrFooterModule } from '@betagouv/ngx-dsfr/footer';
import { DsfrHeaderModule } from '@betagouv/ngx-dsfr/header';
import { DsfrNavigationModule } from '@betagouv/ngx-dsfr/navigation';

/**
 * Internal imports
 */
import { HomeComponent } from './home.component';
import { AuthenticationService } from '../../../user/services/authentication.service';
import { AuthenticationServiceMock } from '../../../user/mocks/authentication.service.mock';
import { StandardLayoutComponent } from '../../../shared/components/standard-layout/standard-layout.component';
import { HeaderComponent } from '../../../shared/components/header/header.component';
import { FooterComponent } from '../../../shared/components/footer/footer.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        DsfrLinkModule,
        DsfrFooterModule,
        DsfrHeaderModule,
        DsfrNavigationModule,
        RouterTestingModule
      ],
      declarations: [
        HeaderComponent,
        FooterComponent,
        StandardLayoutComponent,
        HomeComponent
      ],
      providers: [
        { provide: AuthenticationService, useClass: AuthenticationServiceMock }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});