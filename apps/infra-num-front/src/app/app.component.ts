/**
 * Angular imports
 */
import { Component } from '@angular/core';

@Component({
  selector: 'infra-num-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {}
