/**
 * Angular imports
 */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { isUserAuthenticated } from './user/guards/auth.guard';

/**
 * TypeScript entities and constants
 */
const routes: Routes = [
  {
    path: 'telephonie-mobile',
    canActivate: [isUserAuthenticated],
    loadChildren: () =>
      import('./mobile/mobile.module').then((m) => m.MobileModule)
  },
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled',
      enableTracing: false
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
