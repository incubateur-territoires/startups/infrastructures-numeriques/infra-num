/**
 * 3rd-party imports
 */
import { MobileOperator, ReportLocation, ReportUnavailability } from '@infra-num/common';

/**
 * TypeScript entities and constants
 */
export const mobileOperators: Map<MobileOperator, string> = new Map<
  MobileOperator,
  string
>([
  [MobileOperator.SFR, 'SFR'],
  [MobileOperator.Orange, 'Orange'],
  [MobileOperator.Free, 'Free'],
  [MobileOperator.BouyguesTelecom, 'Bouygues Télécom']
]);

export const locationsLabels: Map<ReportLocation, string> = new Map<
  ReportLocation,
  string
>([
  [ReportLocation.Outside, 'Extérieur'],
  [ReportLocation.Inside, 'Intérieur']
]);

export const unavailabilitiesLabels: Map<ReportUnavailability, string> = new Map<
  ReportUnavailability,
  string
>([
  [ReportUnavailability.CallOrSendSms, 'Pas d’appels et sms'],
  [ReportUnavailability.InternetAccess, 'Pas d’internet']
]);
