/**
 * Angular imports
 */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

/**
 * Internal imports
 */
import { MobileRootComponent } from './mobile-root.component';
import { isUserAuthenticated } from '../user/guards/auth.guard';

/**
 * TypeScript entities and constants
 */
const routes: Routes = [
  {
    path: '',
    component: MobileRootComponent,
    children: [
      {
        path: 'carte',
        canActivate: [isUserAuthenticated],
        loadChildren: () => import('../map/map.module').then((m) => m.MapModule)
      },
      {
        path: 'liste',
        canActivate: [isUserAuthenticated],
        loadChildren: () =>
          import('../list/list.module').then((m) => m.ListModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MobileRoutingModule {}
