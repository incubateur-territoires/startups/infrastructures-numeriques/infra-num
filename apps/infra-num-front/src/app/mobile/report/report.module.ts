/**
 * Angular imports
 */
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

/**
 * 3rd-party imports
 */
import { DsfrCheckboxModule } from '@betagouv/ngx-dsfr/checkbox';
import { DsfrButtonModule } from '@betagouv/ngx-dsfr/button';
import { DsfrInputModule } from '@betagouv/ngx-dsfr/input';
import { DsfrTagModule } from '@betagouv/ngx-dsfr/tag';
import { DsfrBadgeModule } from '@betagouv/ngx-dsfr/badge';
import { DsfrLinkModule } from '@betagouv/ngx-dsfr/link';
import { DsfrRadioModule } from '@betagouv/ngx-dsfr/radio';

/**
 * Internal imports
 */
import { ReportFormComponent } from './components/report-form/report-form.component';
import { SharedModule } from '../../shared/shared.module';
import { MAP_SOURCES } from '../../map/map-source';
import { ReportMapSourceService } from './services/report-map-source.service';
import { ReportDetailsComponent } from './components/report-details/report-details.component';
import { ReportsListComponent } from './components/reports-list/reports-list.component';
import { ReportsFiltersComponent } from './components/reports-filters/reports-filters.component';
import { ReportExportButtonComponent } from './components/report-export-button/report-export-button.component';
import { EditStatusMenuComponent } from './components/edit-status-menu/edit-status-menu.component';
import {
  ReportsMultipleStatusUpdateComponent
} from './components/reports-multiple-status-update/reports-multiple-status-update.component';
import { ReportHistoryComponent } from './components/report-history/report-history.component';

@NgModule({
  imports: [
    SharedModule,
    FormsModule,
    DsfrCheckboxModule,
    DsfrInputModule,
    DsfrButtonModule,
    DsfrTagModule,
    DsfrBadgeModule,
    DsfrLinkModule,
    DsfrRadioModule
  ],
  declarations: [
    ReportExportButtonComponent,
    ReportsFiltersComponent,
    ReportsListComponent,
    ReportDetailsComponent,
    ReportFormComponent,
    EditStatusMenuComponent,
    ReportsMultipleStatusUpdateComponent,
    ReportHistoryComponent
  ],
  providers: [
    ReportMapSourceService,
    {
      provide: MAP_SOURCES,
      multi: true,
      useExisting: ReportMapSourceService
    }
  ]
})
export class ReportModule {}
