/**
 * Angular imports
 */
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { NonNullableFormBuilder, Validators } from '@angular/forms';

/**
 * 3rd-party imports
 */
import { ReportStatus } from '@infra-num/common';
import { ButtonType } from '@betagouv/ngx-dsfr/button';

/**
 * Internal imports
 */
import { reportStatuses } from '../../common';
import { EDIT_STATUS_MENU_VALIDATOR } from './edit-status-menu.validators';

@Component({
  selector: 'infra-num-edit-status-menu',
  templateUrl: './edit-status-menu.component.html',
  styleUrls: ['./edit-status-menu.component.scss']
})
export class EditStatusMenuComponent implements OnInit {
  @Input() initialStatus!: ReportStatus;
  @Output() updateCanceled = new EventEmitter<void>();
  @Output() updateSubmitted = new EventEmitter<{
    status: ReportStatus;
    statusComment?: string;
  }>();

  editStatusForm = this.formBuilder.group(
    {
      status: ['', Validators.required],
      statusComment: ['']
    },
    {
      validators: EDIT_STATUS_MENU_VALIDATOR
    }
  );

  buttonType: typeof ButtonType = ButtonType;
  statusCommentLabel: string = 'Ajouter un commentaire';
  statuses: typeof ReportStatus = ReportStatus;
  statusUpdateSelected: ReportStatus;
  statusUpdateItems;

  private takeUntilDestroyed = takeUntilDestroyed();

  constructor(private formBuilder: NonNullableFormBuilder) {}

  ngOnInit(): void {
    this.statusUpdateSelected = this.initialStatus;
    this.setStatusCommentState();

    this.statusUpdateItems = [
      {
        label: reportStatuses.get(ReportStatus.NotTreated).label,
        id: ReportStatus.NotTreated,
        value: ReportStatus.NotTreated,
        disabled: this.initialStatus === ReportStatus.Treated
      },
      {
        label: reportStatuses.get(ReportStatus.Desultory).label,
        id: ReportStatus.Desultory,
        value: ReportStatus.Desultory
      },
      {
        label: reportStatuses.get(ReportStatus.Treated).label,
        id: ReportStatus.Treated,
        value: ReportStatus.Treated
      },
      {
        label: reportStatuses.get(ReportStatus.Validated).label,
        id: ReportStatus.Validated,
        value: ReportStatus.Validated
      }
    ];

    this.editStatusForm.patchValue({
      status: this.initialStatus
    });

    this.editStatusForm
      .controls['status'].valueChanges
      .pipe(this.takeUntilDestroyed)
      .subscribe({
        next: (selectedStatus: ReportStatus) => {
          this.statusUpdateSelected = selectedStatus;
          this.setStatusCommentState();
        }
      });
  }

  private setStatusCommentState() {

    // Update the label according to the selected status
    switch (this.statusUpdateSelected) {
      case this.statuses.Treated:
      case this.statuses.Validated:
        this.statusCommentLabel = 'Ajouter un commentaire (optionnel)';
        break;
      case this.statuses.Desultory:
        this.statusCommentLabel = 'Ajouter un commentaire';
        break;
    }

    // Activate or deactivate the “statusComment” field depending on the selected status
    if (this.initialStatus === this.statusUpdateSelected) {
      this.editStatusForm.get('statusComment').disable();
    } else {
      this.editStatusForm.get('statusComment').enable();
    }
  }

  onUpdateStatus(): void {
    if (this.editStatusForm.valid) {
      this.updateSubmitted.emit({
        status: this.editStatusForm.value.status as ReportStatus,
        statusComment: this.editStatusForm.value.statusComment ?? undefined
      });
    }
  }
}
