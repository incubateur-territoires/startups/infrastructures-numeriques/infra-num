/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

/**
 * 3rd-party imports
 */
import { DsfrButtonModule } from '@betagouv/ngx-dsfr/button';
import { DsfrRadioModule } from '@betagouv/ngx-dsfr/radio';
import { DsfrInputModule } from '@betagouv/ngx-dsfr/input';
import { ReportStatus } from '@infra-num/common';

/**
 * Internal imports
 */
import { EditStatusMenuComponent } from './edit-status-menu.component';

describe('ReportDetailsComponent', () => {
  let component: EditStatusMenuComponent;
  let fixture: ComponentFixture<EditStatusMenuComponent>;
  const selectValidatedStatus: () => void = () => {
    expect(component.editStatusForm.controls.status.value).toBeUndefined();
    const validatedLabel = fixture.debugElement.query(
      By.css('label[for="Validated"]')
    );
    validatedLabel.nativeElement.click();
    expect(component.editStatusForm.controls.status.value).toEqual(
      ReportStatus.Validated
    );
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DsfrRadioModule, DsfrButtonModule, DsfrInputModule, ReactiveFormsModule],
      declarations: [EditStatusMenuComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(EditStatusMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should set the proper statusCommentLabel when the status is validated', () => {
    selectValidatedStatus();
    expect(component.statusCommentLabel).toEqual(
      'Ajouter un commentaire (optionnel)'
    );
  });

  it('should add a comment field for the right selected status', () => {
    selectValidatedStatus();
    const commentField = fixture.debugElement.query(By.css('#statusComment'));
    expect(commentField).not.toBeUndefined();
  });
});
