/**
 * Angular imports
 */
import {
  FormGroup,
  ValidatorFn,
  ValidationErrors
} from '@angular/forms';

/**
 * 3rd-party imports
 */
import { ReportStatus } from '@infra-num/common';

export const EDIT_STATUS_MENU_VALIDATOR: ValidatorFn = (
  control: FormGroup
): ValidationErrors | null => {
  const statuses: typeof ReportStatus = ReportStatus;
  const currentStatus: ReportStatus = control.get('status').value;
  const currentStatusComment: string = control.get('statusComment').value;

  if ((currentStatus === statuses.Desultory) && !currentStatusComment?.length) {
    return { editStatusMenuFormValid: false };
  }

  return null;
};
