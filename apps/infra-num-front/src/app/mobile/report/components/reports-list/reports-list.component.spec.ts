/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { OverlayModule } from '@angular/cdk/overlay';

/**
 * 3rd-party imports
 */
import { DsfrButtonModule } from '@betagouv/ngx-dsfr/button';
import { of } from 'rxjs';
import { SafeUser } from '@infra-num/common';

/**
 * Internal imports
 */
import { ReportsListComponent } from './reports-list.component';
import { ReportsService } from '../../services/reports.service';
import { ReportsServiceMock } from '../../services/reports.service.mock';
import { AuthenticationServiceMock } from '../../../../user/mocks/authentication.service.mock';
import { AuthenticationService } from '../../../../user/services/authentication.service';
import { ReportExportButtonComponent } from '../report-export-button/report-export-button.component';

jest.mock('maplibre-gl', () => ({
  Marker: jest.fn().mockReturnValue({
    setLngLat: jest.fn()
  })
}));

describe('ReportsListComponent', () => {
  let component: ReportsListComponent;
  let fixture: ComponentFixture<ReportsListComponent>;
  let authenticationService: AuthenticationServiceMock;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DsfrButtonModule, OverlayModule],
      declarations: [ReportsListComponent, ReportExportButtonComponent],
      providers: [
        {
          provide: AuthenticationService,
          useClass: AuthenticationServiceMock
        },
        {
          provide: ReportsService,
          useClass: ReportsServiceMock
        }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(ReportsListComponent);
    component = fixture.componentInstance;
    authenticationService = TestBed.inject(
      AuthenticationService
    ) as unknown as AuthenticationServiceMock;
    authenticationService.authenticatedUser = of({} as SafeUser);
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
