/**
 * Angular imports
 */
import {
  Component,
  computed,
  effect,
  Injector,
  OnDestroy,
  OnInit,
  signal,
  Signal,
  WritableSignal
} from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';

/**
 * 3rd-party imports
 */
import { ButtonType } from '@betagouv/ngx-dsfr/button';
import { ElementSize } from '@betagouv/ngx-dsfr';
import { ReportStatus, ReportWithId } from '@infra-num/common';

/**
 * Internal imports
 */
import {
  DynamicBoxMobileDisplay,
  DynamicBoxService
} from '../../../../dynamic-box/dynamic-box.service';
import {
  ReportsFiltersConfiguration,
  ReportsService
} from '../../services/reports.service';
import { reportStatuses } from '../../common';
import { ReportDetailsComponent } from '../report-details/report-details.component';
import { ReportsFiltersComponent } from '../reports-filters/reports-filters.component';
import { ReportExportButtonComponent } from '../report-export-button/report-export-button.component';
import { mobileOperators } from '../../../common';
import { environment } from '../../../../../environments/environment';
import {
  ReportsMultipleStatusUpdateComponent
} from '../reports-multiple-status-update/reports-multiple-status-update.component';

@Component({
  templateUrl: './reports-list.component.html',
  styleUrls: ['./reports-list.component.scss']
})
export class ReportsListComponent implements OnInit, OnDestroy {
  filteredReports: Signal<ReportWithId[]> = this.reportsService.filteredReports;
  pagesToDisplay: WritableSignal<number> = signal(1);
  selectedReports: Signal<ReportWithId[]> = this.reportsService.selectedReports;
  selectedStatus: Signal<ReportStatus> = this.reportsService.selectedStatus;
  isMultipleSelectMode = this.reportsService.isMultipleSelectMode;
  paginatedReports: Signal<ReportWithId[]> = computed(() => {
    const reportsAvailable: ReportWithId[] = this.filteredReports();

    return reportsAvailable.slice(0, this.pagesToDisplay() * 25);
  });

  activatedFilters: Signal<number> = computed(() => {
    const filters: ReportsFiltersConfiguration =
      this.reportsService.reportsFilters();

    return (
      filters.status.length +
      filters.operators.length +
      filters.geographic.regions.length +
      filters.geographic.departments.length +
      filters.geographic.cities.length
    );
  });

  reportStatuses = reportStatuses;
  mobileOperators = mobileOperators;

  buttonTypes: typeof ButtonType = ButtonType;
  elementSizes: typeof ElementSize = ElementSize;
  selectedReport: ReportWithId;

  constructor(
    private readonly dynamicBoxService: DynamicBoxService,
    private readonly reportsService: ReportsService,
    private readonly breakpointObserver: BreakpointObserver,
    private readonly injector: Injector
  ) {}

  ngOnInit(): void {
    effect(
      () => {
        const report = this.reportsService.selectedReport();
        if (report) {
          this.selectedReport = report;
          this.dynamicBoxService.showBoxWithContent(
            ReportDetailsComponent,
            {
              report
            },
            {
              display: true,
              displayOnMobile: DynamicBoxMobileDisplay.FullHeight,
              showCloseOnMobile: true
            }
          );
        } else {
          this.selectedReport = null;
          const isDesktop = this.breakpointObserver.isMatched(
            `(min-width: ${environment.breakpointDesktop})`
          );
          if (isDesktop && !this.isMultipleSelectMode()) {
            this.displayFilters();
          }
        }
      },
      {injector: this.injector, allowSignalWrites: true}
    );

    this.reportsService.retrieveEntities();
  }

  ngOnDestroy() {
    this.reportsService.selectedReports.set([])
    this.reportsService.selectedReport.set(null);
  }

  onFiltersButtonClicked(): void {
    this.reportsService.selectedReport.set(null);
    this.displayFilters();
  }

  onMoreReportsClicked(): void {
    this.pagesToDisplay.update((currentValue: number) => {
      return ++currentValue;
    });
  }

  onListItemClicked(report: ReportWithId): void {
    if (this.isMultipleSelectMode()) {
      if (!this.selectedStatus() || report.status === this.selectedStatus()) {
        this.reportsService.selectedReports.update((selectedReports) => (
          selectedReports.includes(report) ?
            [...selectedReports].filter((item) => (item._id !== report._id)) :
            [...selectedReports, report])
        );
      }
    } else {
      this.reportsService.selectedReport.set(report);
    }
  }

  onOpenMobileContextualMenu(): void {
    this.dynamicBoxService.showBoxWithContent(
      ReportExportButtonComponent,
      undefined,
      {
        animated: true,
        display: true,
        mainBox: true,
        displayOnMobile: DynamicBoxMobileDisplay.Intermediate,
        showCloseOnMobile: true
      }
    );
  }

  onUpdateMultipleStatus() {
    this.reportsService.selectedReport.set(null);
    this.reportsService.isMultipleSelectMode.set(true);
    this.dynamicBoxService.showBoxWithContent(
      ReportsMultipleStatusUpdateComponent,
      undefined,
      {
        animated: true,
        display: true,
        mainBox: true,
        displayOnMobile: DynamicBoxMobileDisplay.Hidden,
        showCloseOnMobile: false
      }
    );
  }

  onCancelUpdateMultipleStatus() {
    this.reportsService.isMultipleSelectMode.set(false);
    this.reportsService.selectedReports.set([]);
    this.dynamicBoxService.showBoxWithContent(
      ReportsFiltersComponent,
      undefined,
      {
        animated: true,
        display: true,
        mainBox: true,
        displayOnMobile: DynamicBoxMobileDisplay.Hidden,
        showCloseOnMobile: false
      }
    );
  }

  private displayFilters(): void {
    this.dynamicBoxService.showBoxWithContent(ReportsFiltersComponent, null, {
      display: true,
      displayOnMobile: DynamicBoxMobileDisplay.FullHeight,
      showCloseOnMobile: true
    });
  }
}
