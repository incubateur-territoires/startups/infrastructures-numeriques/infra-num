/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { OverlayModule } from '@angular/cdk/overlay';

/**
 * 3rd-party imports
 */
import { DsfrButtonModule } from '@betagouv/ngx-dsfr/button';

/**
 * Internal imports
 */
import { ReportExportButtonComponent } from './report-export-button.component';
import { ReportsService } from '../../services/reports.service';
import { ReportsServiceMock } from '../../services/reports.service.mock';

describe('ReportExportButtonComponent', () => {
  let component: ReportExportButtonComponent;
  let fixture: ComponentFixture<ReportExportButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [OverlayModule, DsfrButtonModule],
      declarations: [ReportExportButtonComponent],
      providers: [
        {
          provide: ReportsService,
          useClass: ReportsServiceMock
        }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(ReportExportButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
