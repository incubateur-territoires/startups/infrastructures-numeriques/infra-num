/**
 * Angular imports
 */
import { Component, OnInit, Signal } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';

/**
 * 3rd-party imports
 */
import { GPSToLambert93, ReportDownload, SafeUser } from '@infra-num/common';
import * as xlsx from 'xlsx';
import { ButtonType } from '@betagouv/ngx-dsfr/button';
import { ElementSize } from '@betagouv/ngx-dsfr';

/**
 * Internal imports
 */
import { reportStatuses } from '../../common';
import { ReportsService } from '../../services/reports.service';
import {
  locationsLabels,
  mobileOperators,
  unavailabilitiesLabels
} from '../../../common';
import { environment } from '../../../../../environments/environment';

/**
 * TypeScript entities and constants
 */
enum DownloadStatus {
  NOT_INITIATED,
  INITIATED,
  READY
}

enum PossibleExportFormat {
  XLSX = 'xlsx',
  CSV = 'csv'
}

@Component({
  selector: 'infra-num-report-export-button',
  templateUrl: './report-export-button.component.html',
  styleUrls: ['./report-export-button.component.scss']
})
export class ReportExportButtonComponent implements OnInit {
  downloadStatus: DownloadStatus = DownloadStatus.NOT_INITIATED;
  isAFilterSelected: Signal<boolean> = this.reportsService.isAFilterSelected;
  buttonTypes: typeof ButtonType = ButtonType;
  elementSizes: typeof ElementSize = ElementSize;
  downloadStatuses: typeof DownloadStatus = DownloadStatus;
  exportFormats: typeof PossibleExportFormat = PossibleExportFormat;
  isDesktopFormatsMenuOpen: boolean = false;
  isMobileFormatsMenuOpen: boolean = false;

  private xlsx: typeof xlsx;
  private reportsWorkbook: xlsx.WorkBook;
  private format: PossibleExportFormat;

  constructor(
    private readonly reportsService: ReportsService,
    private readonly breakpointObserver: BreakpointObserver
  ) {}

  async ngOnInit(): Promise<void> {
    this.xlsx = await import(
      /* webpackChunkName: "xlsx" */
      'xlsx/dist/xlsx.mini.min.js'
      );
  }

  onExportButtonClicked(): void {
    const isDesktop = this.breakpointObserver.isMatched(
      `(min-width: ${environment.breakpointDesktop})`
    );
    if (isDesktop) {
      this.isDesktopFormatsMenuOpen = !this.isDesktopFormatsMenuOpen;
    } else {
      this.isMobileFormatsMenuOpen = true;
    }
  }

  initiateDownload(chosenFormat: PossibleExportFormat) {
    this.format = chosenFormat;
    const reports: ReportDownload[] = this.reportsService
      .filteredReports()
      .map((report) => ({
        region: report.region,
        department: report.department,
        city: report.city,
        creationDate: new Date(report.creationDate).toJSON().split('T')[0],
        author: (report.author as SafeUser)?.email || '',
        status: reportStatuses.get(report.status).label,
        operators: report.operators
          .map((operator) => mobileOperators.get(operator))
          .join(', '),
        issues: [
          ...report.locations.map((location) => locationsLabels.get(location)),
          ...report.unavailabilities.map((unavailability) =>
            unavailabilitiesLabels.get(unavailability)
          )
        ].join(', '),
        description: report.description,
        xLambert: GPSToLambert93(
          +report.longitude,
          +report.latitude
        ).lambertE.toString(),
        yLambert: GPSToLambert93(
          +report.longitude,
          +report.latitude
        ).lambertN.toString(),
        longitude: report.longitude,
        latitude: report.latitude
      }));

    const worksheet = this.xlsx.utils.json_to_sheet(reports);
    const workbook = this.xlsx.utils.book_new();
    this.xlsx.utils.book_append_sheet(workbook, worksheet, 'Signalements');
    this.xlsx.utils.sheet_add_aoa(
      worksheet,
      [
        [
          'Région',
          'Département',
          'Ville',
          'Crée le',
          'Crée par',
          'Statut',
          'Opérateurs concernés',
          'Situation du problème',
          'Description',
          'X Lambert 93',
          'Y Lambert 93',
          'X WGS84 (GPS)',
          'Y WGS84 (GPS)'
        ]
      ],
      { origin: 'A1' }
    );
    worksheet['!cols'] = [
      { wch: 15 },
      { wch: 15 },
      { wch: 20 },
      { wch: 10 },
      { wch: 35 },
      { wch: 10 },
      { wch: 35 },
      { wch: 40 },
      { wch: 60 },
      { wch: 20 },
      { wch: 20 },
      { wch: 20 },
      { wch: 20 }
    ];
    this.reportsWorkbook = workbook;
    this.downloadStatus = DownloadStatus.READY;
  }

  onDownloadReportsClicked(): void {
    this.xlsx.writeFile(this.reportsWorkbook, `Signalements.${this.format}`, {
      compression: true
    });
    this.downloadStatus = DownloadStatus.NOT_INITIATED;
    this.isMobileFormatsMenuOpen = this.isDesktopFormatsMenuOpen = false;
  }
}
