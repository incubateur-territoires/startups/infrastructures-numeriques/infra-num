/**
 * Angular imports
 */
import { Component, Inject, OnInit } from '@angular/core';

/**
 * 3rd-party imports
 */
import { ReportStatusInfo, ReportWithId, SafeUser, setInstitutionByAuthor } from '@infra-num/common';

/**
 * Internal imports
 */
import { COMPONENT_PORTAL_DATA } from '../../../../common';
import { reportStatuses } from '../../common';

@Component({
  selector: 'infra-num-report-history',
  templateUrl: './report-history.component.html',
  styleUrls: ['./report-history.component.scss']
})
export class ReportHistoryComponent implements OnInit {

  report: ReportWithId;
  reportStatuses = reportStatuses;
  statusInfos: (ReportStatusInfo & { changeInstitution: string })[];

  constructor(
    @Inject(COMPONENT_PORTAL_DATA)
    private data: { report: ReportWithId & { author: SafeUser } }
  ) {}

  ngOnInit(): void {
    this.report = this.data.report;
    this.sortStatusInfoArray();
  }

  private sortStatusInfoArray(): void {
    const orderedStatusInfos = this.report.statusInfo.sort(
      (statusInfo1, statusInfo2) => {
        return (
          +new Date(statusInfo2.changeDate) - +new Date(statusInfo1.changeDate)
        );
      }
    );

    this.statusInfos = orderedStatusInfos.map(
      (statusInfo: ReportStatusInfo) => {
        return {
          ...statusInfo,
          changeInstitution: setInstitutionByAuthor(
            statusInfo.changeAuthor as SafeUser
          )
        };
      }
    );
  }

}