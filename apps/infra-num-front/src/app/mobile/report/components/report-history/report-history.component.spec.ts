/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';

/**
 * Internal imports
 */
import { COMPONENT_PORTAL_DATA } from '../../../../common';
import { ReportHistoryComponent } from './report-history.component';

describe('ReportHistoryComponent', () => {
  let component: ReportHistoryComponent;
  let fixture: ComponentFixture<ReportHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ReportHistoryComponent],
      providers: [
        {
          provide: COMPONENT_PORTAL_DATA,
          useValue: {
            report: {
              _id: '644adf10fbc6db8702df41ed',
              latitude: '48.99167930252372',
              longitude: '2.253634929659313',
              zipCode: '95120',
              city: 'Ermont',
              locations: ['Outside', 'Inside'],
              unavailabilities: ['CallOrSendSms', 'InternetAccess'],
              operators: ['BouyguesTelecom', 'Free', 'Orange', 'SFR'],
              description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis bibendum orci non eu ac',
              status: 'NotTreated',
              statusInfo: [
                {
                  changeAuthor: {
                    _id: '64301065e314e60dedededdf6ee92f8',
                    email: 'ff@gmail.com',
                    role: 'Mayor',
                    jurisdiction: {
                      _id: 'edededededededede',
                      type: 'City',
                      name: 'Ermont',
                      referenceCoordinates: [2.258, 48.9891]
                    }
                  },
                  oldStatus: 'NotTreated',
                  status: 'Treated',
                  changeDate: new Date()
                }
              ],
              author: {
                _id: '64301065e314e60df6ee92f8',
                email: 'ff@gmail.com',
                role: 'Mayor',
                jurisdiction: {
                  _id: '63fdff2f3018488a328e1179',
                  type: 'City',
                  name: 'Ermont',
                  referenceCoordinates: [2.258, 48.9891]
                },
                preferredCoordinates: 'Lambert93'
              },
              creationDate: new Date()
            }
          }
        }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(ReportHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
