/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';

/**
 * 3rd-party imports
 */
import { DsfrTagModule } from '@betagouv/ngx-dsfr/tag';

/**
 * Internal imports
 */
import { ReportsFiltersComponent } from './reports-filters.component';
import { ReportsService } from '../../services/reports.service';
import { ReportsServiceMock } from '../../services/reports.service.mock';
import { COMPONENT_PORTAL_DATA } from '../../../../common';

describe('ReportsListFiltersComponent', () => {
  let component: ReportsFiltersComponent;
  let fixture: ComponentFixture<ReportsFiltersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DsfrTagModule],
      declarations: [ReportsFiltersComponent],
      providers: [
        {
          provide: ReportsService,
          useClass: ReportsServiceMock
        },
        {
          provide: COMPONENT_PORTAL_DATA,
          useValue: { view: 'List' }
        }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(ReportsFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
