/**
 * Angular imports
 */
import {
  Component,
  HostBinding,
  Inject,
  OnInit,
  Signal,
  computed
} from '@angular/core';

/**
 * 3rd-party imports
 */
import { TagType } from '@betagouv/ngx-dsfr/tag';
import {
  City,
  Department,
  MobileOperator,
  Region,
  ReportStatus
} from '@infra-num/common';

/**
 * Internal imports
 */
import {
  ReportsFiltersConfiguration,
  ReportsService
} from '../../services/reports.service';
import { COMPONENT_PORTAL_DATA } from '../../../../common';
import { reportStatuses } from '../../common';

/**
 * TypeScript entities and constants
 */
interface GeographicFilter {
  label: string;
  locality: Region | Department;
  tagType: TagType;
}

@Component({
  templateUrl: './reports-filters.component.html',
  styleUrls: ['./reports-filters.component.scss']
})
export class ReportsFiltersComponent implements OnInit {
  reportStatusLabels = reportStatuses;
  reportsFilters: Signal<ReportsFiltersConfiguration> =
    this.reportsService.reportsFilters;

  regionsFilters: Signal<GeographicFilter[]> = computed(() => {
    const regions: Region[] = this.reportsFilters().geographic?.regions;

    return regions.map((region: Region) => {
      const existingRegionByCity =
        this.reportsFilters().geographic.cities.filter(
          (city) => city.codeRegion === region.code
        ).length;
      const existingRegionByDepartement =
        this.reportsFilters().geographic.departments.filter(
          (department) => department.region.code === region.code
        ).length;

      return {
        label: region.nom,
        locality: region,
        tagType:
          existingRegionByCity || existingRegionByDepartement
            ? TagType.NON_CLICKABLE
            : TagType.DELETABLE
      };
    });
  });

  departmentsFilters: Signal<GeographicFilter[]> = computed(() => {
    const departments: Department[] =
      this.reportsFilters().geographic?.departments;

    return departments.map((department: Department) => {
      const existingDepartmentByCity =
        this.reportsFilters().geographic.cities.filter(
          (city) => city.codeDepartement === department.code
        ).length;

      return {
        label: department?.nom,
        locality: department,
        tagType: existingDepartmentByCity
          ? TagType.NON_CLICKABLE
          : TagType.DELETABLE
      };
    });
  });

  tagTypes: typeof TagType = TagType;
  reportStatuses: typeof ReportStatus = ReportStatus;
  reportOperators: typeof MobileOperator = MobileOperator;

  view: 'Map' | 'List';

  @HostBinding('class.map-display')
  mapDisplay: boolean = false;

  constructor(
    private readonly reportsService: ReportsService,
    @Inject(COMPONENT_PORTAL_DATA)
    private data: { view: 'Map' | 'List' }
  ) {}

  ngOnInit(): void {
    this.view = this.data?.view || 'List';
    this.mapDisplay = this.view === 'Map';
  }

  onGeographicTagDeleted(geographicFilter: Region | Department | City) {
    this.reportsService.reportsFilters.update(
      (filtersConfig: ReportsFiltersConfiguration) => {
        let newFiltersConfig: ReportsFiltersConfiguration = {
          ...filtersConfig
        };
        switch (geographicFilter.type) {
          case 'region':
            newFiltersConfig.geographic.regions =
              filtersConfig.geographic.regions.filter(
                (region) => region.nom !== geographicFilter.nom
              );
            break;
          case 'department':
            newFiltersConfig.geographic.departments =
              filtersConfig.geographic.departments.filter(
                (department) => department.nom !== geographicFilter.nom
              );
            break;
          case 'city':
            newFiltersConfig.geographic.cities =
              filtersConfig.geographic.cities.filter(
                (city) => city.nom !== geographicFilter.nom
              );
            break;
        }

        return newFiltersConfig;
      }
    );
  }

  onStatusSelected(isSelected: boolean, status: ReportStatus) {
    this.reportsService.reportsFilters.update(
      (filtersConfig: ReportsFiltersConfiguration) => {
        if (isSelected) {
          return {
            ...filtersConfig,
            status: [...filtersConfig.status, status]
          };
        }

        return {
          ...filtersConfig,
          status: filtersConfig.status.filter(
            (element: ReportStatus) => element !== status
          )
        };
      }
    );
  }

  onOperatorSelected(isSelected: boolean, operator: MobileOperator) {
    this.reportsService.reportsFilters.update(
      (filtersConfig: ReportsFiltersConfiguration) => {
        if (isSelected) {
          return {
            ...filtersConfig,
            operators: [...filtersConfig.operators, operator]
          };
        }

        return {
          ...filtersConfig,
          operators: filtersConfig.operators.filter(
            (element: MobileOperator) => element !== operator
          )
        };
      }
    );
  }
}
