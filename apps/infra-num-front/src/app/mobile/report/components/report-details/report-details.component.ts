/**
 * Angular imports
 */
import {
  Component,
  Inject,
  OnInit,
  Signal
} from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

/**
 * 3rd-party imports
 */
import {
  City,
  PossibleCoordinates,
  ReportLocation,
  ReportStatus, ReportStatusInfo,
  ReportWithId,
  Role,
  SafeUser,
  setInstitutionByAuthor
} from '@infra-num/common';
import { Marker } from 'maplibre-gl';
import { ButtonType } from '@betagouv/ngx-dsfr/button';
import { ElementSize } from '@betagouv/ngx-dsfr';

/**
 * Internal imports
 */
import {
  DynamicBoxMobileDisplay, DynamicBoxOptions,
  DynamicBoxService
} from '../../../../dynamic-box/dynamic-box.service';
import { AuthenticationService } from '../../../../user/services/authentication.service';
import { LocationDetailsFeature } from '../../../../shared/components/location-details/location-details.component';
import { MarkerWithCity } from '../../../../map/map.service';
import { CitySearchService } from '../../../../api-gouv/services/city-search.service';
import { ReportFormComponent } from '../report-form/report-form.component';
import { reportStatuses } from '../../common';
import { COMPONENT_PORTAL_DATA } from '../../../../common';
import {
  ContactInformationComponent
} from '../../../../shared/components/contact-information/contact-information.component';
import { ReportsService } from '../../services/reports.service';
import {
  locationsLabels,
  mobileOperators,
  unavailabilitiesLabels
} from '../../../common';
import { ReportHistoryComponent } from '../report-history/report-history.component';

@Component({
  selector: 'infra-num-report-details',
  templateUrl: './report-details.component.html',
  styleUrls: ['./report-details.component.scss']
})
export class ReportDetailsComponent implements OnInit {
  user: SafeUser;
  report: ReportWithId & { author: SafeUser };
  reportModificationInstitution: string;
  reportCreationInstitution: string;
  statusUpdateInstitution: string;
  cityMarker: MarkerWithCity;
  preferredCoordinates: PossibleCoordinates;
  reportStatuses = reportStatuses;
  mobileOperators = mobileOperators;
  issues: { key: string; value: string }[] = [];
  roles: typeof Role = Role;
  possibleFeatures: typeof LocationDetailsFeature = LocationDetailsFeature;
  locations: typeof ReportLocation = ReportLocation;
  buttonType: typeof ButtonType = ButtonType;
  elementSize: typeof ElementSize = ElementSize;
  statuses: typeof ReportStatus = ReportStatus;
  canDisplayEditStatusButton: boolean = false;
  isDisplayingEditStatusMenu: boolean = false;
  lastReportStatusInfo: ReportStatusInfo;

  boxOptions: Signal<DynamicBoxOptions> = this.dynamicBoxService.dynamicBoxOptions;

  activeReportMenu: 'contactInformation' | 'history' | undefined;

  private takeUntilDestroyed = takeUntilDestroyed();

  constructor(
    private readonly dynamicBoxService: DynamicBoxService,
    private readonly authenticationService: AuthenticationService,
    private readonly citySearchService: CitySearchService,
    private readonly reportsService: ReportsService,
    @Inject(COMPONENT_PORTAL_DATA)
    private data: { report: ReportWithId & { author: SafeUser } }
  ) {}

  ngOnInit(): void {
    this.report = this.data.report;
    this.getLastReportStatusInfo();
    this.setInstitutions();

    this.getUser();
    this.getCity();
    this.getIssues();

    /*
     * When closing a Dynamic Box displaying this Component
     * on Mobile, we're intentionally closing it, not just
     * hiding it.
     *
     * So, along with destroying this Component, we MUST
     * make the app aware there's no selected Report anymore
     */
    this.dynamicBoxService.boxClosedOnMobile
      .pipe(this.takeUntilDestroyed)
      .subscribe({
        next: () => this.reportsService.selectedReport.set(null)
      });
  }

  private getUser(): void {
    this.authenticationService.authenticatedUser
      .pipe(this.takeUntilDestroyed)
      .subscribe((authenticatedUser: SafeUser) => {
        this.user = authenticatedUser;
        this.preferredCoordinates = authenticatedUser.preferredCoordinates;
        this.canWeDisplayEditStatusButton();
      });
  }

  private getCity(): void {
    this.citySearchService
      .getCityByGPSCoordinates(+this.report.longitude, +this.report.latitude)
      .pipe(this.takeUntilDestroyed)
      .subscribe((city: City) => {
        const marker = new Marker();
        marker.setLngLat({
          lng: +this.report.longitude,
          lat: +this.report.latitude
        });
        this.cityMarker = {marker, city};
      });
  }

  private getIssues(): void {
    this.issues = [
      ...this.report.locations.map((location) => ({
        key: location,
        value: locationsLabels.get(location)
      })),
      ...this.report.unavailabilities.map((unavailability) => ({
        key: unavailability,
        value: unavailabilitiesLabels.get(unavailability)
      }))
    ];
  }

  private canWeDisplayEditStatusButton(): void {
    const isUserAllowedToUpdateStatus: boolean =
      this.user?.roles?.includes(Role.ANCT) ||
      (this.user?.roles?.includes(Role.ProjectTeam) &&
        this.user?.jurisdictions?.some(({name}) => {
          return name === this.report.department || name === this.report.region;
        }));
    const nonFinalReportStatuses: ReportStatus[] = [
      ReportStatus.NotTreated,
      ReportStatus.Treated
    ];

    this.canDisplayEditStatusButton =
      isUserAllowedToUpdateStatus &&
      nonFinalReportStatuses.includes(this.report.status);
  }

  private setInstitutions(): void {
    this.reportCreationInstitution = setInstitutionByAuthor(this.report.author);

    if (this.report.modification) {
      this.reportModificationInstitution = setInstitutionByAuthor(
        this.report.modification.author as SafeUser
      );
    }

    if (this.lastReportStatusInfo) {
      this.statusUpdateInstitution = setInstitutionByAuthor(
        this.lastReportStatusInfo?.changeAuthor as SafeUser
      );
    }
  }

  editReport() {
    this.dynamicBoxService.showBoxWithContent(
      ReportFormComponent,
      {
        report: this.report
      },
      {
        display: true,
        hasStrip: false,
        isForm: true,
        mainBox: true,
        displayOnMobile: DynamicBoxMobileDisplay.FullHeight
      }
    );
  }

  displayReportHistory() {
    this.activeReportMenu = 'history';
    this.dynamicBoxService.showBoxWithContent(
      ReportHistoryComponent,
      {
        report: this.report
      },
      {
        display: true,
        mainBox: false,
        isForm: false,
        displayOnMobile: DynamicBoxMobileDisplay.FullHeight
      }
    );
  }

  displayContactInformationBoxContent() {
    this.activeReportMenu = 'contactInformation';
    this.dynamicBoxService.showBoxWithContent(
      ContactInformationComponent,
      {
        report: this.report,
        codeInsee: this.cityMarker.city.code
      },
      {
        display: true,
        mainBox: false,
        isForm: false,
        displayOnMobile: DynamicBoxMobileDisplay.FullHeight
      }
    );
  }

  private getLastReportStatusInfo(): void {
    this.lastReportStatusInfo = this.report.statusInfo?.filter((statusInfo) => statusInfo.status === this.report.status)[0];
  }

  editReportStatus(reportStatus: { status: ReportStatus, statusComment?: string }) {
    if (this.report.status === reportStatus.status) {
      this.isDisplayingEditStatusMenu = false;
    } else {
      this.reportsService
        .updateReportStatus(
          this.report._id,
          reportStatus.status,
          reportStatus.statusComment
        )
        .pipe(this.takeUntilDestroyed)
        .subscribe((updatedReport: ReportWithId) => {
          this.report = {...updatedReport, author: this.report.author};
          this.getLastReportStatusInfo();
          this.setInstitutions();
          this.canWeDisplayEditStatusButton();
          this.isDisplayingEditStatusMenu = false;
        });
    }
  }
}
