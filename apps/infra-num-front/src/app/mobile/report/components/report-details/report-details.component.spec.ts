/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';

/**
 * 3rd-party imports
 */
import { of } from 'rxjs';
import { SafeUser } from '@infra-num/common';

/**
 * Internal imports
 */
import { ReportDetailsComponent } from './report-details.component';
import { MapModule } from '../../../../map/map.module';
import { AuthenticationService } from '../../../../user/services/authentication.service';
import { AuthenticationServiceMock } from '../../../../user/mocks/authentication.service.mock';
import { COMPONENT_PORTAL_DATA } from '../../../../common';

jest.mock('maplibre-gl', () => ({
  Map: jest.fn().mockReturnValue({
    addControl: jest.fn(),
    on: jest.fn(),
    remove: jest.fn()
  }),
  NavigationControl: jest.fn(),
  ScaleControl: jest.fn(),
  GeoJSONSource: jest.fn(),
  MapGeoJSONFeature: jest.fn()
}));

describe('ReportDetailsComponent', () => {
  let component: ReportDetailsComponent;
  let fixture: ComponentFixture<ReportDetailsComponent>;
  let authenticationService: AuthenticationServiceMock;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MapModule, HttpClientModule],
      declarations: [ReportDetailsComponent],
      providers: [
        {provide: AuthenticationService, useClass: AuthenticationServiceMock},
        {
          provide: COMPONENT_PORTAL_DATA,
          useValue: {
            report: {
              _id: '644adf10fbc6db8702df41ed',
              latitude: '48.99167930252372',
              longitude: '2.253634929659313',
              zipCode: '95120',
              city: 'Ermont',
              locations: ['Outside', 'Inside'],
              unavailabilities: ['CallOrSendSms', 'InternetAccess'],
              operators: ['BouyguesTelecom', 'Free', 'Orange', 'SFR'],
              description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis bibendum orci non eu ac',
              status: 'NotTreated',
              author: {
                _id: '64301065e314e60df6ee92f8',
                email: 'anasouahidi@gmail.com',
                role: 'Mayor',
                jurisdiction: {
                  _id: '63fdff2f3018488a328e1179',
                  type: 'City',
                  name: 'Ermont',
                  referenceCoordinates: [2.258, 48.9891]
                },
                preferredCoordinates: 'Lambert93'
              },
              creationDate: new Date()
            }
          }
        }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(ReportDetailsComponent);
    component = fixture.componentInstance;
    authenticationService = TestBed.inject(
      AuthenticationService
    ) as unknown as AuthenticationServiceMock;
    authenticationService.authenticatedUser = of({
      _id: '64301065e314e60df6ee92f8',
      email: 'dede@gmail.com',
      roles: ['ANCT'],
      jurisdictions: [{
        type: 'City',
        name: 'Ermont',
        referenceCoordinates: [2.258, 48.9891]
      }],
      preferredCoordinates: 'Lambert93',
      hasSubscribedToNewsletter: false
    } as unknown as SafeUser);
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
