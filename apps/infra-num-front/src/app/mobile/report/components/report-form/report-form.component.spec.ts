/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';

/**
 * Internal imports
 */
import { ReportFormComponent } from './report-form.component';
import { CitySearchService } from '../../../../api-gouv/services/city-search.service';
import { MapModule } from '../../../../map/map.module';
import { AuthenticationService } from '../../../../user/services/authentication.service';
import { AuthenticationServiceMock } from '../../../../user/mocks/authentication.service.mock';
import { COMPONENT_PORTAL_DATA } from '../../../../common';

jest.mock('maplibre-gl', () => ({
  Map: jest.fn().mockReturnValue({
    addControl: jest.fn(),
    on: jest.fn(),
    remove: jest.fn()
  }),
  NavigationControl: jest.fn(),
  ScaleControl: jest.fn(),
  GeoJSONSource: jest.fn(),
  MapGeoJSONFeature: jest.fn()
}));

describe('ReportFormComponent', () => {
  let component: ReportFormComponent;
  let fixture: ComponentFixture<ReportFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MapModule, HttpClientModule],
      declarations: [ReportFormComponent],
      providers: [
        CitySearchService,
        { provide: AuthenticationService, useClass: AuthenticationServiceMock },
        {
          provide: COMPONENT_PORTAL_DATA,
          useValue: {
            report: {
              _id: '644adf10fbc6db8702df41ed',
              latitude: '48.99167930252372',
              longitude: '2.253634929659313',
              zipCode: '95120',
              city: 'Ermont',
              locations: ['Outside', 'Inside'],
              unavailabilities: ['CallOrSendSms', 'InternetAccess'],
              operators: ['BouyguesTelecom', 'Free', 'Orange', 'SFR'],
              description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis bibendum orci non eu ac',
              status: 'NotTreated',
              author: {
                _id: '64301065e314e60df6ee92f8',
                email: 'anasouahidi@gmail.com',
                role: 'Mayor',
                jurisdiction: {
                  _id: '63fdff2f3018488a328e1179',
                  type: 'City',
                  name: 'Ermont',
                  referenceCoordinates: [2.258, 48.9891]
                },
                preferredCoordinates: 'Lambert93'
              },
              creationDate: new Date()
            }
          }
        }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(ReportFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
