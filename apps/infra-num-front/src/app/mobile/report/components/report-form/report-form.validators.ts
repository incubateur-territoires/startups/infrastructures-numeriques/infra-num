import {
  FormGroup,
  ValidatorFn,
  ValidationErrors
} from '@angular/forms';

export const CITY_HALL_CONTACT_VALIDATOR: ValidatorFn = (
  control: FormGroup
): ValidationErrors | null => {
  if (
    (
      control.get('fullName').value &&
      control.get('email').value
    ) ||
    (
      control.get('fullName').value &&
      control.get('phone').value
    ) ||
    (
      !control.get('fullName').value &&
      !control.get('phone').value &&
      !control.get('email').value
    )
  ) {
    return null;
  }

  return { cityHallContactFormValid : false };
};
