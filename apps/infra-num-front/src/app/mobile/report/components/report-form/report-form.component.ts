/**
 * Angular imports
 */
import { Component, effect, Inject, Injector, OnInit } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { NonNullableFormBuilder, Validators } from '@angular/forms';

/**
 * 3rd-party imports
 */
import { ElementSize } from '@betagouv/ngx-dsfr';
import { ButtonHtmlType, ButtonType } from '@betagouv/ngx-dsfr/button';
import { CheckboxItem } from '@betagouv/ngx-dsfr/checkbox';
import {
  City,
  CityHallContact,
  MobileOperator,
  PossibleCoordinates,
  ReportLocation,
  ReportStatus,
  ReportUnavailability,
  ReportWithId,
  SafeUser,
  Role
} from '@infra-num/common';
import { Marker } from 'maplibre-gl';
import { firstValueFrom } from 'rxjs';

/**
 * Internal imports
 */
import {
  DynamicBoxMobileDisplay,
  DynamicBoxOptions,
  DynamicBoxService
} from '../../../../dynamic-box/dynamic-box.service';
import { MapService, MarkerWithCity } from '../../../../map/map.service';
import { ReportsService } from '../../services/reports.service';
import { AuthenticationService } from '../../../../user/services/authentication.service';
import { LocationDetailsFeature } from '../../../../shared/components/location-details/location-details.component';
import { ReportDetailsComponent } from '../report-details/report-details.component';
import { CitySearchService } from '../../../../api-gouv/services/city-search.service';
import { COMPONENT_PORTAL_DATA } from '../../../../common';
import { environment } from '../../../../../environments/environment';
import { CITY_HALL_CONTACT_VALIDATOR } from './report-form.validators';

@Component({
  selector: 'infra-num-report-form',
  templateUrl: './report-form.component.html',
  styleUrls: ['./report-form.component.scss']
})
export class ReportFormComponent implements OnInit {
  reportForm = this.formBuilder.group({
    locatedProblem: [
      this.data?.report ? this.data.report.locations : [],
      Validators.required
    ],
    whatYouCantDo: [
      this.data?.report ? this.data.report.unavailabilities : [],
      Validators.required
    ],
    whichOperators: [
      this.data?.report ? this.data.report.operators : [],
      Validators.required
    ],
    description: [
      this.data?.report ? this.data.report.description : '',
      Validators.required
    ],
    cityHallContact: this.formBuilder.group(
      {
        fullName: [
          this.data?.report?.cityHallContact
            ? this.data?.report?.cityHallContact.fullName
            : ''
        ],
        phone: [
          this.data?.report?.cityHallContact
            ? this.data?.report?.cityHallContact.phone
            : ''
        ],
        email: [
          this.data?.report?.cityHallContact
            ? this.data?.report?.cityHallContact.email
            : '',
          Validators.email
        ]
      },
      {
        validators: CITY_HALL_CONTACT_VALIDATOR
      }
    )
  });

  displayDeletingConfirmationMessage: boolean = false;
  selectedCity: MarkerWithCity;
  preferredCoordinates: PossibleCoordinates;
  reportLocatedProblemItems: CheckboxItem[] = [
    {
      label: 'En extérieur',
      value: ReportLocation.Outside,
      id: 'outside'
    },
    {
      label: 'En intérieur',
      value: ReportLocation.Inside,
      id: 'inside'
    }
  ];
  reportWhatYouCantDoItems: CheckboxItem[] = [
    {
      label: 'Appeler ou envoyer des SMS',
      value: ReportUnavailability.CallOrSendSms,
      id: 'callOrSendSms'
    },
    {
      label: 'Accéder à internet',
      value: ReportUnavailability.InternetAccess,
      id: 'internet'
    }
  ];
  reportWhichOperatorsItems: CheckboxItem[] = [
    {
      label: 'Bouygues Télécom',
      value: MobileOperator.BouyguesTelecom,
      id: 'bouyguesTelecom'
    },
    {
      label: 'Free',
      value: MobileOperator.Free,
      id: 'free'
    },
    {
      label: 'Orange',
      value: MobileOperator.Orange,
      id: 'orange'
    },
    {
      label: 'SFR',
      value: MobileOperator.SFR,
      id: 'sfr'
    }
  ];

  elementSize: typeof ElementSize = ElementSize;
  htmlType: typeof ButtonHtmlType = ButtonHtmlType;
  buttonType: typeof ButtonType = ButtonType;
  possibleFeatures: typeof LocationDetailsFeature = LocationDetailsFeature;
  user: SafeUser;
  displayFullFormOnMobile: boolean = false;
  environment = environment;
  roles: typeof Role = Role;

  private takeUntilDestroyed = takeUntilDestroyed();

  constructor(
    private readonly formBuilder: NonNullableFormBuilder,
    private readonly dynamicBoxService: DynamicBoxService,
    private readonly mapService: MapService,
    private readonly reportsService: ReportsService,
    private readonly citySearchService: CitySearchService,
    private readonly authenticationService: AuthenticationService,
    private readonly injector: Injector,
    @Inject(COMPONENT_PORTAL_DATA)
    public data: { report: ReportWithId & { author: SafeUser } }
  ) {}

  ngOnInit(): void {
    this.getUser();
    this.observeMap();

    if (this.data?.report) {
      this.displayFullFormOnMobile = true;
      this.getCity();
    } else {
      // We want to create a new Report
      this.mapService.selectedCity.set(null);
      this.reportsService.selectedReport.set(null);
    }
  }

  private getUser(): void {
    this.authenticationService.authenticatedUser
      .pipe(this.takeUntilDestroyed)
      .subscribe((authenticatedUser: SafeUser) => {
        this.user = authenticatedUser;
        this.preferredCoordinates = authenticatedUser.preferredCoordinates;
        this.initCityHallContactFormWithUserPreferences();
      });
  }

  private initCityHallContactFormWithUserPreferences(): void {
    if (this.isCityHallContactFormEmpty() && this.user.preferences?.defaultReportContact?.fullName) {
      this.reportForm.controls['cityHallContact'].setValue(this.user.preferences?.defaultReportContact);
    }
  }

  private isCityHallContactFormEmpty(): boolean {
    return !this.data?.report?.cityHallContact && this.reportForm.get('cityHallContact').pristine;
  }

  private observeMap(): void {
    effect(
      () => {
        const newSelectedCity: MarkerWithCity | null =
          this.mapService.selectedCity();
        if (newSelectedCity) {
          this.selectedCity = newSelectedCity;
        }
      },
      { injector: this.injector }
    );
  }

  private getCity(): void {
    this.citySearchService
      .getCityByGPSCoordinates(
        +this.data.report.longitude,
        +this.data.report.latitude
      )
      .pipe(this.takeUntilDestroyed)
      .subscribe((city: City) => {
        const marker = new Marker();
        marker.setLngLat({
          lng: +this.data.report.longitude,
          lat: +this.data.report.latitude
        });
        this.selectedCity = { marker, city };
      });
  }

  onSubmit() {
    if (this.reportForm.valid) {
      const reportToCreate = this.reportForm.value;
      this.reportsService
        .createOrUpdateReport(
          {
            latitude: this.selectedCity.marker.getLngLat().lat.toString(),
            longitude: this.selectedCity.marker.getLngLat().lng.toString(),
            zipCode: this.selectedCity.city.codePostal,
            city: this.selectedCity.city.nom,
            department: this.selectedCity.city.nomDepartement,
            region: this.selectedCity.city.nomRegion,
            locations: reportToCreate.locatedProblem,
            unavailabilities: reportToCreate.whatYouCantDo,
            operators: reportToCreate.whichOperators,
            description: reportToCreate.description,
            author: this.data?.report?.author || this.user,
            cityHallContact: this.isCityHallContactFormEmpty() ?
              undefined :
              reportToCreate.cityHallContact as CityHallContact
          },
          this.data?.report?._id.toString()
        )
        .pipe(this.takeUntilDestroyed)
        .subscribe({
          next: async (report: ReportWithId) => {
            if (!this.reportForm.get('cityHallContact').pristine) {
              await firstValueFrom(this.authenticationService.updateAuthenticatedUser());
            }
            this.showReportDetails(report);
          }
        });
    }
  }

  cancelReportCreationOrEdition(): void {
    if (this.data?.report) {
      this.showReportDetails(this.data.report);
    } else {
      this.dynamicBoxService.hideBox();
      this.reportsService.selectedReport.set(null);
    }
  }

  deleteReport() {
    if (this.data?.report) {
      this.reportsService
        .updateReportStatus(
          this.data?.report._id,
          ReportStatus.Deleted
        )
        .pipe(this.takeUntilDestroyed)
        .subscribe({
          next: () => {
            this.dynamicBoxService.hideBox();
            this.reportsService.selectedReport.set(null);
          }
        });
    }
  }

  private showReportDetails(report: ReportWithId): void {
    this.dynamicBoxService.showBoxWithContent(
      ReportDetailsComponent,
      {
        report
      },
      {
        display: true,
        hasStrip: true,
        mainBox: true,
        displayOnMobile: DynamicBoxMobileDisplay.FullHeight,
        showCloseOnMobile: true
      }
    );
    this.reportsService.selectedReport.set(report);
  }

  onLocationValidated(): void {
    this.displayFullFormOnMobile = true;
    this.dynamicBoxService.dynamicBoxOptions.update(
      (options: DynamicBoxOptions) => {
        return {
          ...options,
          displayOnMobile: DynamicBoxMobileDisplay.FullHeight
        };
      }
    );
  }

  onLocalizeClicked(): void {
    this.displayFullFormOnMobile = false;
    this.dynamicBoxService.dynamicBoxOptions.update(
      (options: DynamicBoxOptions) => {
        return {
          ...options,
          displayOnMobile: DynamicBoxMobileDisplay.Intermediate
        };
      }
    );
  }
}
