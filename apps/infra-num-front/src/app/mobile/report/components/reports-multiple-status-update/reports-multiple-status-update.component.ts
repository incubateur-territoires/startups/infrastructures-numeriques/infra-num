/**
 * Angular imports
 */
import { Component, Signal } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

/**
 * 3rd-party imports
 */
import { ReportStatus, ReportWithId } from '@infra-num/common';

/**
 * Internal imports
 */
import { ReportsService } from '../../services/reports.service';
import { ReportsFiltersComponent } from '../reports-filters/reports-filters.component';
import { DynamicBoxMobileDisplay, DynamicBoxService } from '../../../../dynamic-box/dynamic-box.service';

@Component({
  templateUrl: './reports-multiple-status-update.component.html',
  styleUrls: ['./reports-multiple-status-update.component.scss']
})
export class ReportsMultipleStatusUpdateComponent {
  selectedReports: Signal<ReportWithId[]> = this.reportsService.selectedReports;
  selectedStatus: Signal<ReportStatus> = this.reportsService.selectedStatus;

  private takeUntilDestroyed = takeUntilDestroyed();

  constructor(
    private readonly reportsService: ReportsService,
    private readonly dynamicBoxService: DynamicBoxService
  ) {}

  onCancelUpdate() {
    this.reportsService.selectedReports.set([]);
  }

  onSubmitUpdate({status, statusComment}: { status: ReportStatus, statusComment?: string }) {
    this.reportsService
      .updateMultipleReportsStatus(
        this.reportsService.selectedReports().map(({_id}) => (_id)),
        status,
        statusComment
      )
      .pipe(this.takeUntilDestroyed)
      .subscribe({
        next: () => {
          this.reportsService.selectedReports.set([]);
          this.reportsService.isMultipleSelectMode.set(false);
          this.dynamicBoxService.showBoxWithContent(
            ReportsFiltersComponent,
            undefined,
            {
              animated: true,
              display: true,
              mainBox: true,
              displayOnMobile: DynamicBoxMobileDisplay.Hidden,
              showCloseOnMobile: false
            }
          );
        }
      });
  }
}
