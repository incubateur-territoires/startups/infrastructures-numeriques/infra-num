/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';

/**
 * Internal imports
 */
import { ReportsMultipleStatusUpdateComponent } from './reports-multiple-status-update.component';
import { ReportsService } from '../../services/reports.service';
import { ReportsServiceMock } from '../../services/reports.service.mock';

describe('ReportsMultipleStatusUpdateComponent', () => {
  let component: ReportsMultipleStatusUpdateComponent;
  let fixture: ComponentFixture<ReportsMultipleStatusUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ReportsMultipleStatusUpdateComponent],
      providers: [
        {
          provide: ReportsService,
          useClass: ReportsServiceMock
        }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(ReportsMultipleStatusUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
