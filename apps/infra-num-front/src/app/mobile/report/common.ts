/**
 * 3rd-party imports
 */
import { ReportStatus } from '@infra-num/common';
import { ThemeColor } from '@betagouv/ngx-dsfr';

/**
 * TypeScript entities and constants
 */
export const reportStatuses: Map<
  ReportStatus,
  { label: string; theme: ThemeColor }
> = new Map([
  [
    ReportStatus.NotTreated,
    {
      label: 'En attente',
      theme: ThemeColor.PURPLE_GLYCINE
    }
  ],
  [
    ReportStatus.Treated,
    {
      label: 'Étude radio demandée',
      theme: ThemeColor.NEW
    }
  ],
  [
    ReportStatus.Validated,
    {
      label: 'Étude radio réalisée',
      theme: ThemeColor.SUCCESS
    }
  ],
  [
    ReportStatus.Desultory,
    {
      label: 'Sans suite',
      theme: ThemeColor.ERROR
    }
  ],
  [
    ReportStatus.Obsolete,
    {
      label: 'Obsolète',
      theme: ThemeColor.GREY
    }
  ]
]);
