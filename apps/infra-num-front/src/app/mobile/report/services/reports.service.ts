/**
 * Angular imports
 */
import { computed, Injectable, Signal, signal, WritableSignal } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

/**
 * 3rd-party imports
 */
import { GeoJSON } from 'geojson';
import { Observable, tap } from 'rxjs';
import { Types } from 'mongoose';
import { City, Department, MobileOperator, Region, Report, ReportStatus, ReportWithId } from '@infra-num/common';
import { ItemResult } from '@betagouv/ngx-dsfr/search-bar';

/**
 * Internal imports
 */
import { environment } from '../../../../environments/environment';
import { CachedLayeredRequestEnabler } from '../../../shared/cached-layered-request-enabler';

/**
 * TypeScript entities and constants
 */
export interface ReportsFiltersConfiguration {
  displayReports: boolean;
  status: ReportStatus[];
  operators: MobileOperator[];
  geographic: {
    regions: Region[];
    departments: Department[];
    cities: City[];
  };
}

@Injectable({
  providedIn: 'root'
})
export class ReportsService extends CachedLayeredRequestEnabler<ReportWithId> {
  selectedReport: WritableSignal<ReportWithId> = signal(null);
  selectedReports: WritableSignal<ReportWithId[]> = signal([]);
  selectedStatus: Signal<ReportStatus> = computed(() => {
    const selectedReports: ReportWithId[] = this.selectedReports();

    return selectedReports.length > 0 ? selectedReports.at(0).status : null;
  });
  isMultipleSelectMode: WritableSignal<boolean> = signal(false);
  reportsFilters: WritableSignal<ReportsFiltersConfiguration> = signal({
    displayReports: true,
    status: [],
    operators: [],
    geographic: {
      regions: [],
      departments: [],
      cities: []
    }
  });

  isAFilterSelected: Signal<boolean> = computed<boolean>(
    () =>
      this.reportsFilters().geographic.regions.length > 0 ||
      this.reportsFilters().geographic.departments.length > 0 ||
      this.reportsFilters().geographic.cities.length > 0 ||
      this.reportsFilters().status.length > 0 ||
      this.reportsFilters().operators.length > 0
  );

  filteredReports: Signal<ReportWithId[]> = computed(() => {
    const filters: ReportsFiltersConfiguration = this.reportsFilters();

    if (!filters.displayReports) {
      return [];
    }

    return this.entities().filter((report: Report) => {
      let keepItForGeoFilters: boolean;
      let keepIt: boolean;

      /*
       * If at least one geographic filter is activated,
       * we keep this Report if and only if AT LEAST ONE
       * activated geographic filter is satisfied
       */

      /*
       * If at least one Region is selected, we keep this Report
       * if and only if its region
       * is in the array filters.geographic.regions
       */
      if (filters.geographic.regions.length > 0) {
        keepItForGeoFilters = filters.geographic.regions.some(
          (region) => region.nom === report.region
        );
      }

      /*
       * If at least one Department is selected, we keep this
       * Report if and only if its department
       * is in the array filters.geographic.departments
       */
      if (filters.geographic.departments.length > 0) {
        keepItForGeoFilters =
          keepItForGeoFilters ||
          filters.geographic.departments.some(
            (department) => department?.nom === report.department
          );
      }

      /*
       * If at least one City is selected, we keep this Report
       * if and only if its city
       * is in the array filters.geographic.cities
       */
      if (filters.geographic.cities.length > 0) {
        keepItForGeoFilters =
          keepItForGeoFilters ||
          filters.geographic.cities.some(
            (city) =>
              city.nom === report.city && city.codePostal === report.zipCode
          );
      }

      /*
       * If no filter is activated, we keep the report by default.
       * Otherwise, among those that have been activated,
       * ever since a category of filter is not satisfied, the Report is out
       */
      keepIt = keepItForGeoFilters === undefined ? true : keepItForGeoFilters;

      /*
       * If at least one Status is selected, we keep this Report
       * if and only if its status
       * is in the array filters.status
       */
      if (filters.status.length > 0) {
        keepIt = keepIt && filters.status.includes(report.status);
      }

      /*
       * If at least one Operator is selected, we keep this Report
       * if and only if at least
       * one of its operators is in the array filters.operators
       */
      if (filters.operators.length > 0) {
        keepIt =
          keepIt &&
          report.operators.some((operator: MobileOperator) => {
            return filters.operators.includes(operator);
          });
      }

      return keepIt;
    });
  });

  filteredReportsForMap: Signal<GeoJSON> = computed(() => {
    return {
      type: 'FeatureCollection',
      features: this.filteredReports().map((report, index) => ({
        type: 'Feature',
        geometry: {
          type: 'Point',
          coordinates: [+report.longitude, +report.latitude]
        },
        properties: {
          name: `Point ${index}`,
          id: report._id
        }
      }))
    };
  });

  constructor(private readonly httpClient: HttpClient) {
    super();
  }

  protected getNumberOfEligibleEntities(): Observable<number> {
    return this.httpClient.get<number>(`${environment.apiUrl}/reports/count`, {
      withCredentials: true
    });
  }

  protected getEntities(startIndex: number): Observable<ReportWithId[]> {
    return this.httpClient.get<ReportWithId[]>(
      `${environment.apiUrl}/reports`,
      {
        params: new HttpParams({
          fromString: `start=${startIndex}&size=${this.layerSize}`
        }),
        withCredentials: true
      }
    );
  }

  protected getEntity(id: Types.ObjectId): Observable<ReportWithId> {
    return this.httpClient.get<ReportWithId>(
      `${environment.apiUrl}/reports/${id}`,
      {
        withCredentials: true
      }
    );
  }

  onSearchLocality(value: ItemResult): void {
    this.reportsFilters.update((filtersConfig: ReportsFiltersConfiguration) => {
      let newFiltersConfig: ReportsFiltersConfiguration = {...filtersConfig};
      switch (value.originalObject.type) {
        case 'region':
          const existingRegion = filtersConfig.geographic.regions.some(
            (region) => region.nom === value.originalObject.nom
          );
          if (!existingRegion) {
            newFiltersConfig.geographic.regions.push(value.originalObject);
          }
          break;
        case 'department':
          const existingDepartment = filtersConfig.geographic.departments.some(
            (department) => department.nom === value.originalObject.nom
          );
          if (!existingDepartment) {
            newFiltersConfig.geographic.departments.push(value.originalObject);
          }
          break;
        case 'city':
          const existingCity = filtersConfig.geographic.cities.some(
            (city) =>
              city.nom === value.originalObject.nom &&
              city.codePostal === value.originalObject.codePostal
          );
          if (!existingCity) {
            newFiltersConfig.geographic.cities.push(value.originalObject);
          }
          break;
      }

      return newFiltersConfig;
    });
  }

  updateReportStatus(
    id: Types.ObjectId,
    status: ReportStatus,
    statusComment?: string
  ): Observable<ReportWithId> {
    return this.httpClient
      .put<ReportWithId>(
        `${environment.apiUrl}/reports/${id}/status`,
        {status, statusComment},
        {withCredentials: true}
      )
      .pipe(
        tap({
          next: (reportUpdated: ReportWithId) => {
            this.updateCache(reportUpdated, status === ReportStatus.Deleted);
          }
        })
      );
  }

  createOrUpdateReport(
    report: Partial<Report>,
    reportId: string = null
  ): Observable<ReportWithId> {
    if (reportId) {
      return this.updateReport(reportId, report);
    }

    return this.createReport(report);
  }

  private createReport(
    reportToCreate: Partial<Report>
  ): Observable<ReportWithId> {
    return this.httpClient
      .post<ReportWithId>(`${environment.apiUrl}/reports`, reportToCreate, {
        withCredentials: true
      })
      .pipe(
        tap({
          next: (reportCreated: ReportWithId) => {
            this.updateCache(reportCreated);
          }
        })
      );
  }

  private updateReport(
    reportId: string,
    reportToUpdate: Partial<Report>
  ): Observable<ReportWithId> {
    return this.httpClient
      .put<ReportWithId>(
        `${environment.apiUrl}/reports/${reportId}`,
        reportToUpdate,
        {withCredentials: true}
      )
      .pipe(
        tap({
          next: (reportUpdated: ReportWithId) => {
            this.updateCache(reportUpdated);
          }
        })
      );
  }

  deleteReport(reportToDelete: ReportWithId): Observable<ReportWithId> {
    return this.httpClient
      .delete<ReportWithId>(
        `${environment.apiUrl}/reports/${reportToDelete._id}`,
        {
          withCredentials: true
        }
      )
      .pipe(
        tap({
          next: () => {
            this.updateCache(reportToDelete, true);
          }
        })
      );
  }

  updateMultipleReportsStatus(
    ids: Types.ObjectId[],
    status: ReportStatus,
    statusComment?: string
  ): Observable<ReportWithId[]> {
    return this.httpClient
      .put<ReportWithId[]>(
        `${environment.apiUrl}/reports/status`,
        {ids, status, statusComment},
        {withCredentials: true}
      )
      .pipe(
        tap({
          next: (reportsUpdated: ReportWithId[]) => {
            for (const report of reportsUpdated) {
              this.updateCache(report, status === ReportStatus.Deleted);
            }
          }
        })
      );
  }
}
