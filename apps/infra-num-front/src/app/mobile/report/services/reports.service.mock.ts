/**
 * Angular imports
 */
import { Signal, signal, WritableSignal } from '@angular/core';

/**
 * 3rd-party imports
 */
import { ReportStatus, ReportWithId } from '@infra-num/common';
import { GeoJSON } from 'geojson';

/**
 * Internal imports
 */
import { ReportsFiltersConfiguration } from './reports.service';

export class ReportsServiceMock {
  selectedReport: WritableSignal<ReportWithId> = signal(null);
  reportsFilters: WritableSignal<ReportsFiltersConfiguration> = signal({
    displayReports: true,
    status: [],
    operators: [],
    geographic: {
      regions: [],
      departments: [],
      cities: []
    }
  });
  isAFilterSelected: Signal<boolean> = signal(false);
  filteredReports: Signal<ReportWithId[]> = signal([]);
  filteredReportsForMap: Signal<GeoJSON> = signal({
    type: 'FeatureCollection',
    features: []
  });
  selectedReports: WritableSignal<ReportWithId[]> = signal([]);
  selectedStatus: WritableSignal<ReportStatus> = signal(ReportStatus.Treated);
  createOrUpdateReport: jest.Mock = jest.fn();
  retrieveEntities: jest.Mock = jest.fn();
}
