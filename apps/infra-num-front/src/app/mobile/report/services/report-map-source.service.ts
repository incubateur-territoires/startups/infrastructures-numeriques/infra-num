/**
 * Angular imports
 */
import { effect, Injectable, Injector, Signal } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

/**
 * 3rd-party imports
 */
import { GeoJSONSource, MapGeoJSONFeature } from 'maplibre-gl';
import { GeoJSON, Point } from 'geojson';

/**
 * Internal imports
 */
import { ReportsService } from './reports.service';
import { MapSource } from '../../../map/map-source';
import { ReportDetailsComponent } from '../components/report-details/report-details.component';
import {
  DynamicBoxMobileDisplay,
  DynamicBoxService
} from '../../../dynamic-box/dynamic-box.service';

@Injectable()
export class ReportMapSourceService extends MapSource {
  protected sourceData: Signal<GeoJSON> =
    this.reportsService.filteredReportsForMap;

  constructor(
    private readonly reportsService: ReportsService,
    private readonly dynamicBoxService: DynamicBoxService,
    protected readonly injector: Injector
  ) {
    super(injector);
  }

  /**
   * Adding a new source of data to the map.
   * This one will represent reports.
   * We initialize it with empty data at first and
   * then plug it to a stream of GeoJSON representing
   * reports
   */
  protected initMap(): void {
    this.map.addSource('reports', {
      type: 'geojson',
      data: {
        type: 'FeatureCollection',
        features: []
      },
      cluster: true,
      clusterMaxZoom: 14, // Max zoom to cluster points on
      clusterRadius: 150 // Radius of each cluster when clustering points (defaults to 50)
    });

    this.initDisplayForIndividualItems();

    this.initClickHandlerForIndividualItems();

    this.initDisplayForClusters();

    this.initClusterInspection();

    this.initStyleForLayers();

    this.initDisplayForSingleItemSelected();

    // Plugging the reports map source onto a signal of reports
    this.effectsReferences.push(
      effect(
        () => {
          (this.map.getSource('reports') as GeoJSONSource).setData(
            this.sourceData()
          );
        },
        { injector: this.injector }
      )
    );

    /*
     * Making sure everytime we have a new selected Report
     * it will be displayed in the Dynamic Box,
     * gets a "selectedMarker" image and the Map is centered
     * on it with the proper zoom
     */
    this.effectsReferences.push(
      effect(
        () => {
          const report = this.reportsService.selectedReport();
          const imageLoaded: boolean = this.selectedMarkerImageLoaded();

          if (report && imageLoaded) {
            this.dynamicBoxService.showBoxWithContent(
              ReportDetailsComponent,
              {
                report
              },
              {
                animated: true,
                display: true,
                hasStrip: true,
                mainBox: true,
                displayOnMobile: DynamicBoxMobileDisplay.FullHeight,
                showCloseOnMobile: true
              }
            );

            this.addSelectedMarkerLayer(report._id as unknown as string);
            this.selectedMarkerId = report._id as unknown as string;

            this.map.flyTo({
              center: [+report.longitude, +report.latitude]
            });
            this.dynamicBoxService.hideBox(false);
          } else if (report === null) {
            if (this.selectedMarkerId) {
              this.map.removeLayer(this.selectedMarkerId);
              this.selectedMarkerId = null;
            }
          }
        },
        { injector: this.injector, allowSignalWrites: true }
      )
    );

    /*
     * Initiating retrieval of data which, in the end,
     * will inject the first iteration of reports into the stream
     */
    this.displayInitialData();
  }

  protected initDisplayForIndividualItems(): void {
    // Loading an image to use as a marker for individual reports
    this.map.loadImage(
      'assets/illustrations/map-marker-report_4x.png',
      (err, image) => {
        if (err) throw err;

        // Adding the image to the map for further reference
        this.map.addImage('report', image);

        /*
         * Creating a layer, based on the previously added source,
         * to display individual reports
         */
        this.map.addLayer({
          id: 'single-report-layer',
          type: 'symbol',
          source: 'reports',
          filter: ['!', ['has', 'point_count']],
          layout: {
            'icon-image': 'report',
            'icon-size': 0.3
          }
        });
      }
    );
  }

  private initClickHandlerForIndividualItems(): void {
    this.map.on('click', 'single-report-layer', (event) => {
      if (!this.editMode) {
        const reportId = event.features[0].properties.id;
        // Reinit the icon of previously selected marker
        if (this.selectedMarkerId) {
          this.map.removeLayer(this.selectedMarkerId);
        }
        this.reportsService
          .retrieveEntity(reportId)
          .pipe(takeUntilDestroyed(this.destroyRef))
          .subscribe({
            next: (report) => {
              if (report) {
                this.reportsService.selectedReport.set(report);
              }
            }
          });
      }
    });
  }

  private addSelectedMarkerLayer(markerId: string): void {
    if (!this.map.getLayer(markerId)) {
      this.map.addLayer({
        id: markerId,
        type: 'symbol',
        source: 'reports',
        filter: ['==', 'id', markerId],
        layout: {
          'icon-image': 'selected-report',
          'icon-offset': [0, -8],
          'icon-size': 1
        }
      });
    }
  }

  protected initDisplayForSingleItemSelected(): void {
    // Loading an image to use as a marker for selected report
    this.map.loadImage(
      'assets/illustrations/marker-selected.png',
      (err, image) => {
        if (err) throw err;

        this.selectedMarkerImageLoaded.set(true);
        this.map.addImage('selected-report', image);
      }
    );
  }

  protected initDisplayForClusters(): void {
    // Loading an image to use as a marker for clusters of reports
    this.map.loadImage(
      'assets/illustrations/cluster-reports_4x.png',
      (err, image) => {
        if (err) throw err;

        /*
         * Adding the image to the map for further reference.
         * Here, we're using the stretchable image technique
         * ( https://maplibre.org/maplibre-gl-js-docs/example/add-image-stretchable/ )
         * to be able to make the image fit the number of clustered points
         */
        const pixelRatio = 4;
        this.map.addImage('cluster-reports', image, {
          // The range of pixels that can be stretched horizontally
          stretchX: [[30 * pixelRatio, 39 * pixelRatio]],
          // The part of the image that can contain text ([x1, y1, x2, y2])
          content: [
            30 * pixelRatio,
            4 * pixelRatio,
            39 * pixelRatio,
            28 * pixelRatio
          ],
          // This is a high-dpi image
          pixelRatio: pixelRatio
        });

        /*
         * Creating a layer, based on the previously added source,
         * to display clusters of reports and the number of
         * reports each one aggregates
         */
        this.map.addLayer({
          id: 'clusters-reports-layer',
          type: 'symbol',
          source: 'reports',
          filter: ['has', 'point_count'],
          layout: {
            'text-field': ['get', 'point_count_abbreviated'],
            'icon-text-fit': 'both',
            'icon-text-fit-padding': [3, 3, 3, 1],
            'icon-image': 'cluster-reports',
            'icon-overlap': 'always',
            'text-overlap': 'always',
            'text-size': 16
          },
          paint: {
            'text-color': '#ffffff'
          }
        });
      }
    );
  }

  protected initClusterInspection(): void {
    this.map.on('click', 'clusters-reports-layer', (e) => {
      const features: MapGeoJSONFeature[] = this.map.queryRenderedFeatures(
        e.point,
        {
          layers: ['clusters-reports-layer']
        }
      );
      const clusterId = features[0].properties.cluster_id;
      (this.map.getSource('reports') as GeoJSONSource).getClusterExpansionZoom(
        clusterId,
        (err, zoom) => {
          if (err) return;

          this.map.easeTo({
            center: (features[0].geometry as Point).coordinates as [
              number,
              number
            ],
            zoom: zoom
          });
        }
      );
    });
  }

  protected initStyleForLayers(): void {
    this.map.on('mouseenter', 'clusters-reports-layer', () => {
      this.map.getCanvas().style.cursor = 'pointer';
    });
    this.map.on('mouseleave', 'clusters-reports-layer', () => {
      this.map.getCanvas().style.cursor = '';
    });
    this.map.on('mouseenter', 'single-report-layer', () => {
      this.map.getCanvas().style.cursor = 'pointer';
    });
    this.map.on('mouseleave', 'single-report-layer', () => {
      this.map.getCanvas().style.cursor = '';
    });
  }

  protected displayInitialData(): void {
    this.reportsService.retrieveEntities();
  }
}
