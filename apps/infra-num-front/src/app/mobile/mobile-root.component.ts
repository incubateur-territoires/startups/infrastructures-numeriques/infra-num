/**
 * Angular imports
 */
import { Component, OnInit } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

/**
 * 3rd-party imports
 */
import { Department, Role, SafeUser } from '@infra-num/common';
import { mergeMap, of } from 'rxjs';

/**
 * Internal imports
 */
import { AuthenticationService } from '../user/services/authentication.service';
import {
  ReportsFiltersConfiguration,
  ReportsService
} from './report/services/reports.service';
import { DepartmentSearchService } from '../api-gouv/services/department-search.service';

@Component({
  selector: 'infra-num-mobile-root',
  templateUrl: './mobile-root.component.html',
  styleUrls: ['./mobile-root.component.scss']
})
export class MobileRootComponent implements OnInit {
  private takeUntilDestroyed = takeUntilDestroyed();

  constructor(
    private readonly authenticationService: AuthenticationService,
    private readonly reportsService: ReportsService,
    private readonly departmentsService: DepartmentSearchService
  ) {}

  ngOnInit(): void {
    this.getUser();
  }

  private getUser(): void {
    this.authenticationService.authenticatedUser
      .pipe(
        mergeMap((authenticatedUser: SafeUser) => {
          if (authenticatedUser?.roles.includes(Role.ProjectTeam)) {
            const query = JSON.stringify(authenticatedUser?.jurisdictions?.map(({name}) => name));
            return this.departmentsService.search(query);
          }
          return of([]);
        }),
        this.takeUntilDestroyed
      )
      .subscribe({
        next: (departments: Department[]) => {
          this.reportsService.reportsFilters.update(
            (filtersConfig: ReportsFiltersConfiguration) => {
              const departmentsNames: string[] =
                filtersConfig.geographic.departments.map(({ nom }) => nom);
              const departmentsToAddToFilters = departments.filter(
                ({ nom }) => !departmentsNames.includes(nom)
              );

              return {
                ...filtersConfig,
                geographic: {
                  ...filtersConfig.geographic,
                  departments: [
                    ...filtersConfig.geographic.departments,
                    ...departmentsToAddToFilters
                  ]
                }
              };
            }
          );
        }
      });
  }
}
