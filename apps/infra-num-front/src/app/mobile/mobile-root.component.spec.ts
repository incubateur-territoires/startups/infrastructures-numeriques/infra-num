/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

/**
 * Internal imports
 */
import { MobileRootComponent } from './mobile-root.component';
import { SharedModule } from '../shared/shared.module';
import { AuthenticationService } from '../user/services/authentication.service';
import { AuthenticationServiceMock } from '../user/mocks/authentication.service.mock';
import { MapModule } from '../map/map.module';
import { MAP_SOURCES } from '../map/map-source';
import { MapSourceMock } from '../map/mocks/map-source.mock';

jest.mock('maplibre-gl', () => ({
  Map: jest.fn().mockReturnValue({
    addControl: jest.fn(),
    on: jest.fn(),
    remove: jest.fn()
  }),
  NavigationControl: jest.fn(),
  ScaleControl: jest.fn(),
  GeoJSONSource: jest.fn(),
  MapGeoJSONFeature: jest.fn()
}));

describe('MobileRootComponent', () => {
  let component: MobileRootComponent;
  let fixture: ComponentFixture<MobileRootComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        MapModule,
        SharedModule
      ],
      declarations: [MobileRootComponent],
      providers: [
        { provide: AuthenticationService, useClass: AuthenticationServiceMock }
      ]
    })
      .overrideProvider(MAP_SOURCES, {
        useFactory: () => {
          return [new MapSourceMock()];
        },
        multi: true
      })
      .compileComponents();

    fixture = TestBed.createComponent(MobileRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
