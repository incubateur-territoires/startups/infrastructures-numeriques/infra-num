/**
 * Angular imports
 */
import { NgModule } from '@angular/core';

/**
 * Internal imports
 */
import { MobileRoutingModule } from './mobile-routing.module';
import { MobileRootComponent } from './mobile-root.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [SharedModule, MobileRoutingModule],
  declarations: [MobileRootComponent]
})
export class MobileModule {}
