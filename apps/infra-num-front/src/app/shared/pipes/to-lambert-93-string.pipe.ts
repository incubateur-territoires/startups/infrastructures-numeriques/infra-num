/**
 * Angular imports
 */
import { Pipe, PipeTransform } from '@angular/core';

/**
 * 3rd-party imports
 */
import { GPSToLambert93 } from '@infra-num/common';
import { formatNumber } from '@angular/common';

@Pipe({ name: 'toLambert93String' })
export class ToLambert93StringPipe implements PipeTransform {
  transform(
    value: { latitude: string; longitude: string },
    separator: string = ' | '
  ): string {
    const GPSCoordinates: [number, number] = [
      parseFloat(value.longitude),
      parseFloat(value.latitude)
    ];
    const lambert93Coordinates = GPSToLambert93(...GPSCoordinates);

    return `${formatNumber(
      lambert93Coordinates.lambertE,
      'fr',
      '1.0-0'
    )}${separator}${formatNumber(
      lambert93Coordinates.lambertN,
      'fr',
      '1.0-0'
    )}`;
  }
}
