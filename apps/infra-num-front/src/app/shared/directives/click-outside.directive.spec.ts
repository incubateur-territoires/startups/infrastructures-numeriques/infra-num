/**
 * Angular imports
 */

import { TestBed, ComponentFixture } from '@angular/core/testing';
import { Component } from '@angular/core';
import { By } from '@angular/platform-browser';
/**
 * 3rd-party imports
 */
import { ClickOutsideDirective } from './click-outside.directive';

@Component({
  template: `
    <div class="inside"
         (clickOutside)="clicked()"></div>
    <div class="outside"></div>`
})
class TestClickOutsideComponent {
  clicked() {
  }
}

describe('Directive: ClickOutside', () => {
  let component: TestClickOutsideComponent;
  let fixture: ComponentFixture<TestClickOutsideComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TestClickOutsideComponent, ClickOutsideDirective]
    });
    fixture = TestBed.createComponent(TestClickOutsideComponent);
    component = fixture.componentInstance;
  });

  it('should create directive', () => {
    fixture.detectChanges();

    expect(component).toBeTruthy();
  });

  it('should trigger click inside', () => {
    jest.spyOn(component, 'clicked');
    fixture.detectChanges();

    const inside = fixture.debugElement.query(By.css('.inside')).nativeElement;
    inside.click();

    expect(component.clicked).not.toHaveBeenCalled();
  });

  it('should trigger click outside', () => {
    jest.spyOn(component, 'clicked');
    fixture.detectChanges();

    const outside = fixture.debugElement.query(By.css('.outside')).nativeElement;
    outside.click();

    expect(component.clicked).toHaveBeenCalled();
  });
});