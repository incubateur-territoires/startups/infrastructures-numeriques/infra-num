/**
 * Angular imports
 */
import { Directive, EventEmitter, HostListener, Output } from '@angular/core';

/**
 * TypeScript entities and constants
 */
export enum SwipeDirection {
  UP,
  DOWN
}

@Directive({
  selector: '[infraNumSwipable]'
})
export class SwipableDirective {
  private touchstart = { x: 0, y: 0, time: 0 };

  @HostListener('touchstart', ['$event'])
  @HostListener('touchend', ['$event'])
  handleTouch(event: TouchEvent) {
    // Necessary to prevent scroll chaining on the rest of the layout
    event.preventDefault();

    const touch = event.changedTouches[0];

    if (event.type === 'touchstart') {
      this.touchstart.x = touch.pageX;
      this.touchstart.y = touch.pageY;
      this.touchstart.time = event.timeStamp;
    } else if (event.type === 'touchend') {
      let deltaY = touch.pageY - this.touchstart.y;
      let deltaTime = event.timeStamp - this.touchstart.time;

      /*
       * The touchstart/touchend sequence is admissible
       * as a Swipe only if it lasted less than 500 ms and
       * spanned more than 30 px
       */
      if (deltaTime < 500) {
        if (Math.abs(deltaY) > 30) {
          if (deltaY > 0) {
            this.swiped.emit(SwipeDirection.DOWN);
          } else {
            this.swiped.emit(SwipeDirection.UP);
          }
        }
      }
    }
  }

  @Output()
  swiped = new EventEmitter<SwipeDirection>();
}
