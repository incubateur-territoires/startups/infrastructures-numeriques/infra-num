/**
 * Angular imports
 */
import { DestroyRef, inject, signal, WritableSignal } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

/**
 * 3rd-party imports
 */
import { concatMap, map, merge, Observable, of, scan, tap } from 'rxjs';
import { Types } from 'mongoose';

/**
 * TypeScript entities and constants
 */
interface Cache<T> {
  entities: Map<Types.ObjectId, T>;
  stable: boolean;
}

interface LayerResult<T> {
  layer: number;
  entities: T[];
}

/**
 * This abstract class is supposed to be extended and implemented by
 * all Services which purpose is to interact with the Back-End on
 * behalf of a specific resource ( like the ReportsService for
 * instance ).
 *
 * The Cached Layered Request ( CLR ) design pattern enables us to minimize
 * the number of requests to the Back-End, as well as the delay to
 * display something relevant in the interface.
 *
 * Here, the generic type T is the type of the resource the child Service
 * will handle
 */
export abstract class CachedLayeredRequestEnabler<
  T extends { _id: Types.ObjectId }
> {
  private cache: Cache<T> = {
    entities: new Map<Types.ObjectId, T>(),
    stable: true
  };

  private _layerSize: number = 250;

  protected get layerSize(): number {
    return this._layerSize;
  }

  private set layerSize(value: number) {
    this._layerSize = value;
  }

  protected entities: WritableSignal<T[]> = signal([]);
  protected destroyRef = inject(DestroyRef);

  /**
   * Called directly from the resource-specific data Service that
   * extends this class, this method does nothing if the cache
   * is not stable, as it means we need to wait for a previously
   * initiated CLR to complete
   */
  retrieveEntities(): void {
    if (this.isCacheStable()) {
      if (this.isCacheEmpty()) {
        this.initiateCachedLayeredRequest();
      } else {
        /*
         * If the cache is stable and already populated,
         * then no need to make requests, simply inject the
         * content of the cache into the app via a Signal
         */
        this.entities.set(Array.from(this.cache.entities.values()));
      }
    }
  }

  retrieveEntity(id: Types.ObjectId): Observable<T> {
    if (this.isCacheStable()) {
      if (!this.cache.entities.has(id)) {
        this.cache.stable = false;

        return this.getEntity(id).pipe(
          tap({
            next: (entity: T) => {
              this.updateCache(entity);
              this.cache.stable = true;
            }
          })
        );
      }

      return of(this.cache.entities.get(id));
    }

    return of(null);
  }

  isCacheEmpty(): boolean {
    return this.cache.entities.size === 0;
  }

  isCacheStable(): boolean {
    return this.cache.stable;
  }

  protected updateCache(entity: T, deleting: boolean = false): void {
    if (deleting) {
      this.cache.entities.delete(entity._id);
    } else {
      this.cache.entities.set(entity._id, entity);
    }
    this.entities.set(Array.from(this.cache.entities.values()));
  }

  private initiateCachedLayeredRequest(): void {
    this.cache.stable = false;

    const maxConcurrentRequestsAllowed = 10;
    let hasRetrievedFirstLayer: boolean = false;

    // Step 1 : retrieve the number of eligible entities
    this.getNumberOfEligibleEntities()
      .pipe(
        concatMap((nb: number) => {
          this.layerSize = Math.ceil(nb / maxConcurrentRequestsAllowed);

          let layeredRequest: Observable<LayerResult<T>>[] = [];

          /*
           * Step 2 : decompose the request in several paginated
           * sub-requests, in charge with retrieving only one layer
           * of eligible entities
           */
          for (let i = 0; i < maxConcurrentRequestsAllowed; i++) {
            layeredRequest.push(
              this.getEntities(this.layerSize * i).pipe(
                map((entities: T[]) => {
                  /*
                   * Step 3 : mark each sub-request with its layer number,
                   * so we can identify later which layer we're getting
                   * results for
                   */
                  return {
                    layer: i + 1,
                    entities
                  };
                })
              )
            );
          }

          // Step 4 : all sub-requests are made in parallel to save time
          return merge(...layeredRequest);
        }),
        scan((acc: T[][], currentValue: LayerResult<T>) => {
          /*
           * Step 5 : each time a paginated sub-request returns with results,
           * populate an array, at the right slot for the layer
           * for which we've received results.
           * The scan operator ( https://rxjs.dev/api/operators/scan ) will then
           * emit the newest version of this possibly sparse array
           * ( https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Indexed_collections#sparse_arrays ),
           * emitting a more and more populated array each time
           */
          acc[currentValue.layer - 1] = currentValue.entities;

          return acc;
        }, []),
        takeUntilDestroyed(this.destroyRef)
      )
      .subscribe({
        next: (layeredRequestResult: T[][]) => {
          for (const entity of layeredRequestResult.flat()) {
            this.cache.entities.set(entity._id, entity);
          }

          let hasEmptySlot: boolean = false;
          for (let i = 0; i < layeredRequestResult.length; i++) {
            if (i === 0 && layeredRequestResult[i] !== undefined) {
              hasRetrievedFirstLayer = true;
            }

            if (layeredRequestResult[i] === undefined) {
              hasEmptySlot = true;
            }
          }

          /*
           * Step 6 : we're waiting to retrieve the first layer before injecting
           * the entities into the app. We need to do that because we decided
           * to launch the sub-requests in parallel so there's no guarantee on
           * the order of reception AND we don't want the interface to look like
           * it's "jumping" to a new set of data if we had displayed layer 3
           * before receiving finally layer 1, for instance
           */
          if (hasRetrievedFirstLayer) {
            this.entities.set(Array.from(this.cache.entities.values()));
          }

          /*
           * Step 7 : finally, when the array emitted by scan is fully
           * populated, it means the CLR is over and the cache can now
           * be marked as stable
           */
          if (
            layeredRequestResult.length === maxConcurrentRequestsAllowed &&
            !hasEmptySlot
          ) {
            this.cache.stable = true;
          }
        }
      });
  }

  /**
   * This method will be used during the CLR process to retrieve the total
   * number of entities of a given resource that we'll need to retrieve for
   * our context : it can be all of them or just the subset of them that the
   * currently logged-in user is authorized to see, for instance.
   *
   * It's up to the resource-specific data service that will extend this class
   * to implement this method as it's the only one that should need to know
   * how to talk to the Back-End to retrieve that resource
   * @protected
   */
  protected abstract getNumberOfEligibleEntities(): Observable<number>;

  /**
   * This method will be used during the CLR process to retrieve a paginated
   * subset of the overall eligible entities, aka a layer.
   *
   * It's up to the resource-specific data service that will extend this class
   * to implement this method as it's the only one that should need to know
   * how to talk to the Back-End to retrieve that resource
   * @protected
   */
  protected abstract getEntities(startIndex: number): Observable<T[]>;

  /**
   * This method will be used to retrieve a specific entity.
   *
   * It's up to the resource-specific data service that will extend this class
   * to implement this method as it's the only one that should need to know
   * how to talk to the Back-End to retrieve that resource
   * @protected
   */
  protected abstract getEntity(id: Types.ObjectId): Observable<T>;
}
