/**
 * Angular imports
 */
import { Injectable, signal, WritableSignal } from '@angular/core';

/**
 * TypeScript entities and constants
 */
export enum PossibleView {
  MAP,
  LIST
}

@Injectable({
  providedIn: 'root'
})
export class ViewService {
  currentView: WritableSignal<PossibleView> = signal(PossibleView.MAP);
}
