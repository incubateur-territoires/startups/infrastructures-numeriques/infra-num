/**
 * Angular imports
 */
import { Injectable } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { HttpClient, HttpParams, HttpRequest } from '@angular/common/http';
import {
  HttpClientTestingModule,
  HttpTestingController,
  TestRequest
} from '@angular/common/http/testing';

/**
 * 3rd-party imports
 */
import { Observable } from 'rxjs';

/**
 * Internal imports
 */
import { CachedLayeredRequestEnabler } from './cached-layered-request-enabler';

/**
 * TypeScript entities and constants
 */
interface Starship {
  _id: any;
  name: string;
  universe: string;
  faction: string;
}

const starshipsApiUrl = '/api/starships';

/**
 * Creating mocks
 */
const borgCube: Starship = {
  _id: 2,
  name: 'Borg Cube',
  faction: 'Borg Collective',
  universe: 'Star trek'
};

/*
 * We're using any instead of Types.ObjectId because importing Types
 * from mongoose here makes an error since we're using jsdom as
 * test environment with Jest
 * https://stackoverflow.com/questions/69015811/referenceerror-textencoder-is-not-defined-node-js-with-mongoose
 */
const mockedStarships: Map<any, Starship> = new Map([
  [
    1,
    {
      _id: 1,
      name: 'Millenium Falcon',
      faction: 'Han Solo & Chewie',
      universe: 'Star Wars'
    }
  ],
  [borgCube._id, borgCube],
  [
    3,
    {
      _id: 3,
      name: 'Prometheus',
      faction: "Air Force Space Command ( Tau'ri )",
      universe: 'Stargate'
    }
  ]
]);

@Injectable()
class StarshipsServiceMock extends CachedLayeredRequestEnabler<Starship> {
  constructor(protected readonly httpClient: HttpClient) {
    super();
  }

  getNumberOfEligibleEntities(): Observable<number> {
    return this.httpClient.get<number>(`${starshipsApiUrl}/count`);
  }

  getEntities(startIndex: number): Observable<Starship[]> {
    return this.httpClient.get<Starship[]>(starshipsApiUrl, {
      params: new HttpParams({
        fromString: `start=${startIndex}&size=${this.layerSize}`
      })
    });
  }

  getEntity(id: any): Observable<Starship> {
    return this.httpClient.get<Starship>(`${starshipsApiUrl}/${id}`);
  }
}

describe('A CachedLayeredRequestEnabler', () => {
  let clrEnabler: StarshipsServiceMock;
  let httpController: HttpTestingController;
  let initiateCLRSpy: jest.SpyInstance;
  let entitiesSignalSpy: jest.SpyInstance;
  let getNumberSpy: jest.SpyInstance;
  let getEntitiesSpy: jest.SpyInstance;
  let getEntitySpy: jest.SpyInstance;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [StarshipsServiceMock]
    });
    clrEnabler = TestBed.inject(StarshipsServiceMock);
    httpController = TestBed.inject(HttpTestingController);

    /*
     * WARNING ⚠️: jest.spyOn actually calls its mock function
     * AS WELL AS the real method, when the method is called
     * in our code
     * ( https://jestjs.io/docs/jest-object#jestspyonobject-methodname )
     *
     * If we want to prevent that, we must provide a specific
     * implementation for the mock function.
     *
     * Here, we cast as any
     * to be able to spy on a private method
     */
    initiateCLRSpy = jest.spyOn(
      CachedLayeredRequestEnabler.prototype as any,
      'initiateCachedLayeredRequest'
    );

    /*
     * Here we cast as any
     * to be able to spy on a protected property
     */
    entitiesSignalSpy = jest.spyOn(clrEnabler as any, 'entities');
    entitiesSignalSpy['set'] = jest.fn();

    getNumberSpy = jest.spyOn(clrEnabler, 'getNumberOfEligibleEntities');
    getEntitiesSpy = jest.spyOn(clrEnabler, 'getEntities');
    getEntitySpy = jest.spyOn(clrEnabler, 'getEntity');
  });

  afterEach(() => {
    jest.restoreAllMocks();
    httpController.verify();
  });

  describe(', when being asked to retrieve several entities of the same resource, ', () => {
    it('should initiate a Cached Layered Request if the cache is empty', () => {
      initiateCLRSpy.mockImplementation(() => null);

      expect(clrEnabler.isCacheEmpty()).toBe(true);
      expect(clrEnabler.isCacheStable()).toBe(true);

      clrEnabler.retrieveEntities();

      expect(initiateCLRSpy).toHaveBeenCalled();
    });

    it('should not do anything and wait for a previous Cached Layered Request to finish if the cache is not stable', () => {
      /*
       * Manipulating private properties through bracket notation
       * for Unit Tests
       */
      clrEnabler['cache']['stable'] = false;
      expect(clrEnabler.isCacheEmpty()).toBe(true);
      expect(clrEnabler.isCacheStable()).toBe(false);

      clrEnabler.retrieveEntities();

      expect(initiateCLRSpy).not.toHaveBeenCalled();
      expect(entitiesSignalSpy['set']).not.toHaveBeenCalled();
    });

    it('should return the cache if it is stable and not empty', () => {
      /*
       * Manipulating private properties through bracket notation
       * for Unit Tests
       */
      clrEnabler['cache']['entities'] = mockedStarships;
      expect(clrEnabler.isCacheEmpty()).toBe(false);
      expect(clrEnabler.isCacheStable()).toBe(true);

      clrEnabler.retrieveEntities();

      expect(initiateCLRSpy).not.toHaveBeenCalled();
      expect(entitiesSignalSpy['set']).toHaveBeenCalledWith(
        Array.from(mockedStarships.values())
      );
    });
  });

  describe(', when being asked to initiate a Cached Layered Request, ', () => {
    it('should make a first request to determine the total number of eligible entities to retrieve', () => {
      clrEnabler.retrieveEntities();

      const countRequest: TestRequest = httpController.expectOne(
        `${starshipsApiUrl}/count`
      );

      expect(getNumberSpy).toHaveBeenCalled();
      expect(countRequest.request.method).toEqual('GET');
    });

    it('should launch enough requests in parallel to retrieve all eligible entities, layer by layer', () => {
      /*
       * Getting protected properties through bracket notation
       * for Unit Tests
       */
      const layerSize = clrEnabler['layerSize'];

      clrEnabler.retrieveEntities();

      const countRequest: TestRequest = httpController.expectOne(
        `${starshipsApiUrl}/count`
      );
      countRequest.flush(314);

      const paginatedRequests: TestRequest[] = httpController.match(
        (req: HttpRequest<any>) => {
          return req.url.startsWith(starshipsApiUrl);
        }
      );

      expect(getEntitiesSpy).toHaveBeenCalledTimes(2);
      expect(paginatedRequests.length).toBe(2);

      for (let i = 0; i < paginatedRequests.length; i++) {
        const paginatedRequest: TestRequest = paginatedRequests[i];

        /*
         * Checking the ith call to getEntities has been made with
         * the proper argument
         */
        expect(getEntitiesSpy.mock.calls[i]).toEqual([layerSize * i]);
        expect(paginatedRequest.request.urlWithParams).toBe(
          `${starshipsApiUrl}?start=${layerSize * i}&size=${layerSize}`
        );
      }
    });

    it('should cache and make accessible to consumers the first layer of data as soon as it is received', () => {
      clrEnabler.retrieveEntities();

      const countRequest: TestRequest = httpController.expectOne(
        `${starshipsApiUrl}/count`
      );
      countRequest.flush(314);

      const paginatedRequests: TestRequest[] = httpController.match(
        (req: HttpRequest<any>) => {
          return req.url.startsWith(starshipsApiUrl);
        }
      );

      // At first, cache is empty
      expect(clrEnabler.isCacheEmpty()).toBe(true);

      // Receiving response for layer number 2
      paginatedRequests[1].flush([]);

      // Cache should still be empty and Signal.set has not been called yet
      expect(clrEnabler.isCacheEmpty()).toBe(true);
      expect(entitiesSignalSpy['set']).not.toHaveBeenCalled();

      // Receiving response for layer number 1
      paginatedRequests[0].flush(Array.from(mockedStarships.values()));

      /*
       * Cache should not be empty and Signal.set should have been
       * called for the first time
       */
      expect(clrEnabler.isCacheEmpty()).toBe(false);
      expect(entitiesSignalSpy['set']).toHaveBeenCalledTimes(1);
      expect(entitiesSignalSpy['set']).toHaveBeenCalledWith(
        Array.from(mockedStarships.values())
      );
    });

    it('should mark the cache as stable when all layers of data are received', () => {
      // At first, cache should be stable
      expect(clrEnabler.isCacheStable()).toBe(true);

      clrEnabler.retrieveEntities();

      // After initiating CLR, cache should not be stable anymore
      expect(clrEnabler.isCacheStable()).toBe(false);

      const countRequest: TestRequest = httpController.expectOne(
        `${starshipsApiUrl}/count`
      );
      countRequest.flush(314);

      const paginatedRequests: TestRequest[] = httpController.match(
        (req: HttpRequest<any>) => {
          return req.url.startsWith(starshipsApiUrl);
        }
      );

      // Simulating reception of responses for the paginated sub-requests
      paginatedRequests[0].flush(Array.from(mockedStarships.values()));

      // We haven't received every layer yet, cache should still be not stable
      expect(clrEnabler.isCacheStable()).toBe(false);

      paginatedRequests[1].flush([]);

      // Every layer has had a response, cache should be marked as stable
      expect(clrEnabler.isCacheStable()).toBe(true);
    });
  });

  describe(', when being asked to retrieve a specific entity, ', () => {
    it("should make a call to the Back-End if it's not already in the cache", () => {
      clrEnabler.retrieveEntity(2 as any).subscribe({
        next: (entity: Starship | null) => {
          expect(entity).toEqual(borgCube);
        }
      });

      /*
       * If a request is about to be fired, the cache
       * should be marked as unstable
       */
      expect(clrEnabler.isCacheStable()).toBe(false);

      expect(getEntitySpy).toHaveBeenCalledWith(2);

      const entityRequest: TestRequest = httpController.expectOne(
        `${starshipsApiUrl}/2`
      );
      expect(entityRequest.request.method).toBe('GET');

      // Simulating a response
      entityRequest.flush(borgCube);

      /*
       * After reception of a response, it should be cached
       * and the cache should be marked as stable
       */
      expect(clrEnabler.isCacheEmpty()).toBe(false);
      expect(clrEnabler.isCacheStable()).toBe(true);
    });

    it('should return null if the cache is not stable', () => {
      /*
       * Manipulating private properties through bracket notation
       * for Unit Tests
       */
      clrEnabler['cache']['stable'] = false;

      clrEnabler.retrieveEntity(2 as any).subscribe({
        next: (entity: Starship | null) => {
          expect(entity).toBe(null);
        }
      });
    });

    it("should return it from the cache if it's already in there", () => {
      /*
       * Manipulating private properties through bracket notation
       * for Unit Tests
       */
      clrEnabler['cache']['entities'] = new Map<any, Starship>([
        [borgCube._id, borgCube]
      ]);

      clrEnabler.retrieveEntity(2 as any).subscribe({
        next: (entity: Starship | null) => {
          expect(entity).toBe(borgCube);
        }
      });

      expect(getEntitySpy).not.toHaveBeenCalled();
      httpController.expectNone(`${starshipsApiUrl}/2`);
    });
  });
});
