/**
 * Angular imports
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { OverlayModule } from '@angular/cdk/overlay';

/**
 * 3rd-party imports
 */
import { DsfrHeaderModule } from '@betagouv/ngx-dsfr/header';
import { DsfrLinkModule } from '@betagouv/ngx-dsfr/link';
import { DsfrNavigationModule } from '@betagouv/ngx-dsfr/navigation';
import { DsfrBadgeModule } from '@betagouv/ngx-dsfr/badge';
import { DsfrFooterModule } from '@betagouv/ngx-dsfr/footer';
import { DsfrToggleModule } from '@betagouv/ngx-dsfr/toggle';
import { DsfrButtonModule } from '@betagouv/ngx-dsfr/button';

/**
 * Internal imports
 */
import { StandardLayoutComponent } from './components/standard-layout/standard-layout.component';
import { ExtraViewLayoutComponent } from './components/extra-view-layout/extra-view-layout.component';
import { HeaderComponent } from './components/header/header.component';
import { AccountComponent } from './components/account/account.component';
import { ClickOutsideDirective } from './directives/click-outside.directive';
import { FooterComponent } from './components/footer/footer.component';
import { LocationDetailsComponent } from './components/location-details/location-details.component';
import { SwipableDirective } from './directives/swipable.directive';
import { ToLambert93StringPipe } from './pipes/to-lambert-93-string.pipe';
import { ContactInformationComponent } from './components/contact-information/contact-information.component';

/**
 * TypeScript entities and constants
 */
const exportableDeclarables = [
  StandardLayoutComponent,
  ExtraViewLayoutComponent,
  ClickOutsideDirective,
  LocationDetailsComponent,
  ContactInformationComponent,
  SwipableDirective,
  ToLambert93StringPipe
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    DsfrHeaderModule,
    DsfrLinkModule,
    DsfrNavigationModule,
    DsfrFooterModule,
    DsfrBadgeModule,
    DsfrToggleModule,
    DsfrButtonModule,
    OverlayModule
  ],
  declarations: [
    HeaderComponent,
    AccountComponent,
    FooterComponent,
    ...exportableDeclarables
  ],
  exports: [
    CommonModule,
    ReactiveFormsModule,
    OverlayModule,
    ...exportableDeclarables
  ]
})
export class SharedModule {}
