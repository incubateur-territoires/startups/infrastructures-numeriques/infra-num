/**
 * Angular imports
 */
import { Component } from '@angular/core';

/**
 * 3rd-party imports
 */
import { FooterLink } from '@betagouv/ngx-dsfr/footer';

/**
 * Internal imports
 */
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'infra-num-footer',
  templateUrl: './footer.component.html'
})
export class FooterComponent {

  appName = environment.appName;
  bottomLinks: FooterLink[] = [
    {
      label: 'Plan du site',
      href: '/plan-du-site',
      title:'Plan du site'
    },
    {
      label: 'Accessibilité non conforme',
      href: '/declaration-accessibilite',
      title:'Accessibilité non conforme'
    },
    {
      label: 'Mentions légales',
      href: '/mentions-legales',
      title:'Mentions légales'
    },
    {
      label: 'Données personnelles',
      href: '/donnees-personnelles',
      title:'Données personnelles'
    },
    {
      label: 'Gestion des cookies',
      href: '/gestion-des-cookies',
      title:'Gestion des cookies'
    },
    {
      label: 'Statistiques et impact',
      href: '/statistiques-et-impact',
      title:'Statistiques et impact'
    },
  ];
}
