/**
 * Angular imports
 */
import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  Output,
  ViewChild
} from '@angular/core';

/**
 * 3rd-party imports
 */
import { GPSToLambert93, PossibleCoordinates } from '@infra-num/common';
import { ElementSize, ThemeColor } from '@betagouv/ngx-dsfr';

/**
 * Internal imports
 */
import { MapService, MarkerWithCity } from '../../../map/map.service';
import { PossibleView, ViewService } from '../../view.service';

/**
 * TypeScript entities and constants
 */
export enum LocationDetailsFeature {
  CanLocalize,
  CanDelete,
  CanEdit
}

type UniversalCoordinate = Record<PossibleCoordinates, number>;

@Component({
  selector: 'infra-num-location-details',
  templateUrl: './location-details.component.html',
  styleUrls: ['./location-details.component.scss']
})
export class LocationDetailsComponent implements OnChanges {
  @Input() selectedCity!: MarkerWithCity;
  @Input() preferredCoordinates!: PossibleCoordinates;
  @Input() features: LocationDetailsFeature[] = [];

  @Output()
  featureClicked: EventEmitter<LocationDetailsFeature> =
    new EventEmitter<LocationDetailsFeature>();

  @ViewChild('coordinatesMenuButton')
  coordinatesMenuButton: ElementRef;

  @ViewChild('coordinatesMenu')
  coordinatesMenu: ElementRef;

  @HostListener('document:mousedown', ['$event'])
  onClickOutsideCoordinatesMenu(event): void {
    if (
      this.coordinatesMenu &&
      !this.coordinatesMenu.nativeElement.contains(event.target) &&
      !this.coordinatesMenuButton.nativeElement.contains(event.target)
    ) {
      this.isDisplayingCoordinatesMenu = false;
    }
  }

  isDisplayingCoordinatesMenu: boolean = false;
  latitude: UniversalCoordinate;
  longitude: UniversalCoordinate;
  displayedCoordinatesType: PossibleCoordinates;

  themeColor: typeof ThemeColor = ThemeColor;
  badgeSize: typeof ElementSize = ElementSize;
  possibleCoordinates: PossibleCoordinates[] = [
    PossibleCoordinates.Lambert93,
    PossibleCoordinates.GPS
  ];
  possibleFeatures: typeof LocationDetailsFeature = LocationDetailsFeature;

  constructor(
    private readonly mapService: MapService,
    private readonly viewService: ViewService
  ) {}

  ngOnChanges(): void {
    if (this.selectedCity && this.preferredCoordinates) {
      const GPSCoordinates: [number, number] = [
        this.selectedCity.marker.getLngLat().lng,
        this.selectedCity.marker.getLngLat().lat
      ];
      const lambert93Coordinates = GPSToLambert93(...GPSCoordinates);

      this.longitude = {
        [PossibleCoordinates.GPS]: GPSCoordinates[0],
        [PossibleCoordinates.Lambert93]: lambert93Coordinates.lambertN
      };
      this.latitude = {
        [PossibleCoordinates.GPS]: GPSCoordinates[1],
        [PossibleCoordinates.Lambert93]: lambert93Coordinates.lambertE
      };
      this.displayedCoordinatesType = this.preferredCoordinates;
    }
  }

  onLocalize(): void {
    this.featureClicked.emit(LocationDetailsFeature.CanLocalize);

    if (this.viewService.currentView() === PossibleView.LIST) {
      this.viewService.currentView.set(PossibleView.MAP);
    }

    this.mapService.location.set({
      longitude: this.longitude[PossibleCoordinates.GPS],
      latitude: this.latitude[PossibleCoordinates.GPS],
      zoom: 12
    });
  }

  onDelete(): void {
    // TODO: to be implemented
  }

  onEdit(): void {
    // TODO: to be implemented
  }
}
