/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';

/**
 * 3rd-party imports
 */
import { DsfrToggleModule } from '@betagouv/ngx-dsfr/toggle';
import { DsfrNavigationModule } from '@betagouv/ngx-dsfr/navigation';
import { DsfrHeaderModule } from '@betagouv/ngx-dsfr/header';
import { DsfrLinkModule } from '@betagouv/ngx-dsfr/link';

/**
 * Internal imports
 */
import { ExtraViewLayoutComponent } from './extra-view-layout.component';
import { AuthenticationService } from '../../../user/services/authentication.service';
import { AuthenticationServiceMock } from '../../../user/mocks/authentication.service.mock';
import { HeaderComponent } from '../header/header.component';

describe('ExtraViewLayoutComponent', () => {
  let component: ExtraViewLayoutComponent;
  let fixture: ComponentFixture<ExtraViewLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        ReactiveFormsModule,
        DsfrToggleModule,
        DsfrNavigationModule,
        DsfrHeaderModule,
        DsfrLinkModule
      ],
      declarations: [ExtraViewLayoutComponent, HeaderComponent],
      providers: [
        { provide: AuthenticationService, useClass: AuthenticationServiceMock }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(ExtraViewLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
