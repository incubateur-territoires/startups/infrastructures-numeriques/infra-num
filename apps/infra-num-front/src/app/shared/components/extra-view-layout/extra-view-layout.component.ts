/**
 * Angular imports
 */
import { Component, effect, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl } from '@angular/forms';

/**
 * Internal imports
 */
import { PossibleView, ViewService } from '../../view.service';

@Component({
  selector: 'infra-num-extra-view-layout',
  templateUrl: './extra-view-layout.component.html',
  styleUrls: ['./extra-view-layout.component.scss']
})
export class ExtraViewLayoutComponent implements OnInit {
  view: PossibleView;

  set currentView(view: PossibleView) {
    this.view = view;
    this.viewService.currentView.set(view);
  }

  possibleViews: typeof PossibleView = PossibleView;

  toggleControl: FormControl<boolean>;

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly viewService: ViewService,
    private readonly injector: Injector
  ) {}

  ngOnInit(): void {
    const isListView: boolean = this.router.url.includes('liste');
    this.currentView = isListView ? PossibleView.LIST : PossibleView.MAP;
    this.toggleControl = new FormControl<boolean>(isListView);

    effect(
      () => {
        this.view = this.viewService.currentView();
        this.toggleControl?.setValue(this.view === PossibleView.LIST);
        const newRoute: 'liste' | 'carte' =
          this.view === PossibleView.LIST ? 'liste' : 'carte';
        this.switchRoute(newRoute);
      },
      { injector: this.injector, allowSignalWrites: true }
    );
  }

  onViewSwitched(toggleChecked: boolean): void {
    this.currentView = toggleChecked ? PossibleView.LIST : PossibleView.MAP;
  }

  private switchRoute(newRoute: 'liste' | 'carte'): void {
    // If we're not already on it, navigate to the required View
    if (!this.router.url.includes(newRoute)) {
      this.router.navigate([newRoute], {
        relativeTo: this.route
      });
    }
  }
}
