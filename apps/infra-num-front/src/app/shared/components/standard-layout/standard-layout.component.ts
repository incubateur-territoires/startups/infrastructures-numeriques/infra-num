/**
 * Angular imports
 */
import { Component } from '@angular/core';

@Component({
  selector: 'infra-num-standard-layout',
  templateUrl: './standard-layout.component.html',
  styleUrls: ['./standard-layout.component.scss']
})
export class StandardLayoutComponent {
}
