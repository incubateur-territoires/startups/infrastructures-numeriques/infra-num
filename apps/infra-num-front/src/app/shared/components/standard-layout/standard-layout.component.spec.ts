/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

/**
 * 3rd-party imports
 */
import { DsfrFooterModule } from '@betagouv/ngx-dsfr/footer';
import { DsfrNavigationModule } from '@betagouv/ngx-dsfr/navigation';
import { DsfrHeaderModule } from '@betagouv/ngx-dsfr/header';
import { DsfrLinkModule } from '@betagouv/ngx-dsfr/link';

/**
 * Internal imports
 */
import { StandardLayoutComponent } from './standard-layout.component';
import { AuthenticationService } from '../../../user/services/authentication.service';
import { AuthenticationServiceMock } from '../../../user/mocks/authentication.service.mock';
import { FooterComponent } from '../footer/footer.component';
import { HeaderComponent } from '../header/header.component';

describe('StandardLayoutComponent', () => {
  let component: StandardLayoutComponent;
  let fixture: ComponentFixture<StandardLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        DsfrFooterModule,
        DsfrNavigationModule,
        DsfrHeaderModule,
        DsfrLinkModule
      ],
      declarations: [StandardLayoutComponent, FooterComponent, HeaderComponent],
      providers: [
        { provide: AuthenticationService, useClass: AuthenticationServiceMock }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(StandardLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
