/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';

/**
 * 3rd-party imports
 */
import { DsfrButtonModule } from '@betagouv/ngx-dsfr/button';

/**
 * Internal imports
 */
import { ContactInformationComponent } from './contact-information.component';
import { ServicePublicApiService } from '../../../api-gouv/services/service-public-api.service';
import { ServicePublicApiServiceMock } from '../../../user/mocks/service-public-api.service.mock';
import { UsersService } from '../../../user/services/users.service';
import { UsersServiceMock } from '../../../user/mocks/users.service.mock';
import { COMPONENT_PORTAL_DATA } from '../../../common';

describe('ContactInformationComponent', () => {
  let component: ContactInformationComponent;
  let fixture: ComponentFixture<ContactInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DsfrButtonModule],
      declarations: [ContactInformationComponent],
      providers: [
        {
          provide: UsersService,
          useClass: UsersServiceMock
        },
        {
          provide: ServicePublicApiService,
          useClass: ServicePublicApiServiceMock
        },
        {
          provide: COMPONENT_PORTAL_DATA,
          useValue: {
            report: {
              _id: '644adf10fbc6db8702df41ed',
              latitude: '48.99167930252372',
              longitude: '2.253634929659313',
              zipCode: '95120',
              department: 'Yvelines',
              region: 'Île-de-France',
              city: 'Ermont',
              locations: ['Outside', 'Inside'],
              unavailabilities: ['CallOrSendSms', 'InternetAccess'],
              operators: ['BouyguesTelecom', 'Free', 'Orange', 'SFR'],
              description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis bibendum orci non eu ac',
              status: 'NotTreated',
              author: {
                _id: '64301065e314e60df6ee92f8',
                email: 'anasouahidi@gmail.com',
                role: 'Mayor',
                pointOfContact: true,
                jurisdiction: {
                  _id: '63fdff2f3018488a328e1179',
                  type: 'City',
                  name: 'Ermont',
                  referenceCoordinates: [2.258, 48.9891]
                },
                preferredCoordinates: 'Lambert93'
              },
              creationDate: new Date()
            }
          }
        }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(ContactInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
