/**
 * Angular imports
 */
import { Component, Inject, OnInit } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

/**
 * 3rd-party imports
 */
import {
  AllowedAccount,
  CityHallContact,
  ReportWithId,
  SafeUser,
  ServicePublicApiProperties
} from '@infra-num/common';
import { ButtonType } from '@betagouv/ngx-dsfr/button';

/**
 * Internal imports
 */
import { DynamicBoxService } from '../../../dynamic-box/dynamic-box.service';
import { COMPONENT_PORTAL_DATA } from '../../../common';
import { ServicePublicApiService } from '../../../api-gouv/services/service-public-api.service';
import { UsersService } from '../../../user/services/users.service';

@Component({
  selector: 'infra-num-contact-information',
  templateUrl: './contact-information.component.html',
  styleUrls: ['./contact-information.component.scss']
})
export class ContactInformationComponent implements OnInit {

  protected readonly JSON = JSON;

  buttonTypes: typeof ButtonType =  ButtonType;
  report: ReportWithId & { author: SafeUser };
  projectTeam: AllowedAccount[] = [];
  cityHallInformation: CityHallContact;

  private takeUntilDestroyed = takeUntilDestroyed();

  constructor(
    private readonly dynamicBoxService: DynamicBoxService,
    private readonly servicePublicApiService: ServicePublicApiService,
    private readonly usersService: UsersService,
    @Inject(COMPONENT_PORTAL_DATA)
    private data: {
      report: ReportWithId & { author: SafeUser };
      codeInsee: string;
    }
  ) {}

  ngOnInit(): void {
    this.report = this.data.report;

    this.getPointsOfContactInformationByJurisdictions(
      this.report.department,
      this.report.region
    );

    if (this.report.cityHallContact) {
      this.cityHallInformation = this.report.cityHallContact;
    } else {
      this.getCityHallInformationByCodeInsee(this.data.codeInsee);
    }
  }

  private getPointsOfContactInformationByJurisdictions(
    department: string,
    region: string
  ): void {
    this.usersService
      .getPointsOfContactInformation(department, region)
      .pipe(this.takeUntilDestroyed)
      .subscribe({
        next: (pointOfContacts: AllowedAccount[]) => {
          this.projectTeam = pointOfContacts.length ? pointOfContacts : [];
        }
      });
  }

  private getCityHallInformationByCodeInsee(codeInsee: string): void {
    this.servicePublicApiService
      .getLocalityInformation('city', codeInsee)
      .pipe(this.takeUntilDestroyed)
      .subscribe({
        next: (cityHallInformation: ServicePublicApiProperties) => {
          /*
           * No fullName provided because the name given by
           * the ServicePublicApi is the name of the city hall
           */
          this.cityHallInformation = {
            fullName: '',
            email: cityHallInformation.adresse_courriel,
            phone: cityHallInformation.telephone
          };
        }
      });
  }

  closeSecondaryDynamicBox() {
    this.dynamicBoxService.hideBox(false);
  }
}
