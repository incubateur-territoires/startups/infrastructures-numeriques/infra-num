/**
 * Angular imports
 */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

/**
 * 3rd-party imports
 */
import { Subject, takeUntil } from 'rxjs';

/**
 * Internal imports
 */
import { AuthenticationService } from '../../../user/services/authentication.service';

@Component({
  selector: 'infra-num-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss'],
})
export class AccountComponent implements OnInit, OnDestroy {
  userEmail: string = null;
  isMenuOpen = false;
  canManageAllowedAccounts = false;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private readonly authenticationService: AuthenticationService,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.authenticationService.authenticatedUser
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((authenticatedUser) => {
        this.userEmail = authenticatedUser?.email || null;
      });
  }

  onLogout(): void {
    this.authenticationService
      .logout()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: () => {
          this.router.navigate(['/']);
        }
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
