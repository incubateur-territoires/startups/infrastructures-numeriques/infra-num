/**
 * Angular imports
 */
import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';

/**
 * 3rd-party imports
 */
import { ElementAlignment } from '@betagouv/ngx-dsfr';
import { Navigation } from '@betagouv/ngx-dsfr/navigation';

/**
 * Internal imports
 */
import { environment } from '../../../../environments/environment';
import { AuthenticationService } from '../../../user/services/authentication.service';

@Component({
  selector: 'infra-num-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit, OnDestroy {
  isAuthenticated = false;
  appName = environment.appName;
  iconAlignment: typeof ElementAlignment = ElementAlignment;
  navigation: Navigation = [
    {
      id: 'nav-item-accueil',
      label: 'Accueil',
      href: '/',
      routerLinkActiveExact: true
    },
    {
      id: 'nav-item-mobile',
      label: 'Téléphonie mobile (4G)',
      href: '/telephonie-mobile'
    }
    /*{ // Uncomment it only when we're ready to develop this part of the app
      id: 'nav-item-ftth',
      label: 'Internet très haut débit (fibre)',
      href: '/internet-fibre'
    }*/
  ];

  private unsubscribe$ = new Subject<void>();

  constructor(private authenticationService: AuthenticationService) {}

  ngOnInit() {
    this.authenticationService.authenticatedUser
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((authenticatedUser) => {
        this.isAuthenticated = !!authenticatedUser;
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
