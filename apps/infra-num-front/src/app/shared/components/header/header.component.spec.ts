/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

/**
 * 3rd-party imports
 */
import { DsfrHeaderModule } from '@betagouv/ngx-dsfr/header';
import { DsfrNavigationModule } from '@betagouv/ngx-dsfr/navigation';
import { DsfrLinkModule } from '@betagouv/ngx-dsfr/link';

/**
 * Internal imports
 */
import { HeaderComponent } from './header.component';
import { AuthenticationService } from '../../../user/services/authentication.service';
import { AuthenticationServiceMock } from '../../../user/mocks/authentication.service.mock';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        DsfrHeaderModule,
        DsfrNavigationModule,
        DsfrLinkModule
      ],
      declarations: [HeaderComponent],
      providers: [
        { provide: AuthenticationService, useClass: AuthenticationServiceMock }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
