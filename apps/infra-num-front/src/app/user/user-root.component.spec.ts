/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

/**
 * 3rd-party imports
 */
import { DsfrFooterModule } from '@betagouv/ngx-dsfr/footer';
import { DsfrNavigationModule } from '@betagouv/ngx-dsfr/navigation';
import { DsfrHeaderModule } from '@betagouv/ngx-dsfr/header';
import { DsfrLinkModule } from '@betagouv/ngx-dsfr/link';

/**
 * Internal imports
 */
import { UserRootComponent } from './user-root.component';
import { AuthenticationService } from './services/authentication.service';
import { AuthenticationServiceMock } from './mocks/authentication.service.mock';
import { StandardLayoutComponent } from '../shared/components/standard-layout/standard-layout.component';
import { FooterComponent } from '../shared/components/footer/footer.component';
import { HeaderComponent } from '../shared/components/header/header.component';

describe('UserRootComponent', () => {
  let component: UserRootComponent;
  let fixture: ComponentFixture<UserRootComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        DsfrFooterModule,
        DsfrNavigationModule,
        DsfrHeaderModule,
        DsfrLinkModule
      ],
      declarations: [
        UserRootComponent,
        StandardLayoutComponent,
        FooterComponent,
        HeaderComponent
      ],
      providers: [
        { provide: AuthenticationService, useClass: AuthenticationServiceMock }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(UserRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
