/**
 * 3rd-party imports
 */
import { of } from 'rxjs';

export class ServicePublicApiServiceMock {
  getCityHallInformation: jest.Mock = jest.fn(() => of([]));
}
