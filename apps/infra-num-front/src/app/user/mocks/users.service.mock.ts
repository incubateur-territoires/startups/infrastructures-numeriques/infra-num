/**
 * 3rd-party imports
 */
import { of } from 'rxjs';

export class UsersServiceMock {
  createUser: jest.Mock = jest.fn();
  sendAccountConfirmationEmail: jest.Mock = jest.fn();
  checkIfEmailAlreadyExists: jest.Mock = jest.fn();
  confirmUserAccount: jest.Mock = jest.fn();
  getPointsOfContactInformation: jest.Mock = jest.fn(() => of([]));
}
