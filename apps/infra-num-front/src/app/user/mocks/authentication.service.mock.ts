/**
 * 3rd-party imports
 */
import { Observable, of } from 'rxjs';
import { SafeUser } from '@infra-num/common';

export class AuthenticationServiceMock {
  authenticatedUser: Observable<SafeUser> = of(null);
  logout: jest.Mock = jest.fn();
  login: jest.Mock = jest.fn();
  profile: jest.Mock = jest.fn();
}
