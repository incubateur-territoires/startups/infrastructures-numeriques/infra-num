/**
 * Angular imports
 */
import { Component } from '@angular/core';

@Component({
  selector: 'infra-num-user-root',
  templateUrl: './user-root.component.html',
  styleUrls: ['./user-root.component.scss']
})
export class UserRootComponent {
}
