/**
 * Angular imports
 */
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

/**
 * 3rd-party imports
 */
import { DsfrTileModule } from '@betagouv/ngx-dsfr/tile';
import { DsfrStepperModule } from '@betagouv/ngx-dsfr/stepper';
import { DsfrInputModule } from '@betagouv/ngx-dsfr/input';
import { DsfrPasswordModule } from '@betagouv/ngx-dsfr/password';
import { DsfrButtonModule } from '@betagouv/ngx-dsfr/button';
import { DsfrAlertModule } from '@betagouv/ngx-dsfr/alert';
import { DsfrCheckboxModule } from '@betagouv/ngx-dsfr/checkbox';
import {
  DSFR_SEARCH_BAR_SERVICE_TOKEN,
  DsfrSearchBarModule
} from '@betagouv/ngx-dsfr/search-bar';

/**
 * Internal imports
 */
import { UserRoutingModule } from './user-routing.module';
import { SharedModule } from '../shared/shared.module';
import { UserRootComponent } from './user-root.component';
import { SubscriptionUserTypeComponent } from './components/subscription-user-type/subscription-user-type.component';
import { SubscriptionMayorEpciComponent } from './components/subscription-mayor-epci/subscription-mayor-epci.component';
import { SubscriptionMayorEpciInformationComponent } from './components/subscription-mayor-epci-information/subscription-mayor-epci-information.component';
import { SubscriptionMayorEpciCredentialsComponent } from './components/subscription-mayor-epci-credentials/subscription-mayor-epci-credentials.component';
import { SubscriptionMayorEpciCheckEmailComponent } from './components/subscription-mayor-epci-check-email/subscription-mayor-epci-check-email.component';
import { SubscriptionAccountValidationComponent } from './components/subscription-account-validation/subscription-account-validation.component';
import { SubscriptionAccountConfirmationComponent } from './components/subscription-account-confirmation/subscription-account-confirmation.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { UsersService } from './services/users.service';
import { SubscriptionOthersComponent } from './components/subscription-others/subscription-others.component';
import { SubscriptionOthersCredentialsComponent } from './components/subscription-others-credentials/subscription-others-credentials.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { AuthenticationService } from './services/authentication.service';
import { EpciCitySearchService } from '../api-gouv/services/epci-city-search.service';

@NgModule({
  imports: [
    SharedModule,
    HttpClientModule,
    ReactiveFormsModule,
    DsfrTileModule,
    DsfrStepperModule,
    DsfrInputModule,
    DsfrPasswordModule,
    DsfrButtonModule,
    DsfrSearchBarModule,
    DsfrAlertModule,
    DsfrCheckboxModule,
    UserRoutingModule,
  ],
  declarations: [
    SubscriptionUserTypeComponent,
    SubscriptionMayorEpciComponent,
    SubscriptionMayorEpciCredentialsComponent,
    SubscriptionMayorEpciInformationComponent,
    SubscriptionAccountConfirmationComponent,
    SubscriptionMayorEpciCheckEmailComponent,
    LoginFormComponent,
    SubscriptionOthersCredentialsComponent,
    SubscriptionOthersComponent,
    SubscriptionAccountValidationComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    UserRootComponent
  ],
  providers: [
    UsersService,
    AuthenticationService,
    {
      provide: DSFR_SEARCH_BAR_SERVICE_TOKEN,
      useClass: EpciCitySearchService
    }
  ]
})
export class UserModule {}
