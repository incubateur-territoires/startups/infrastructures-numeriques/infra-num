/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';

/**
 * Internal imports
 */
import { SubscriptionAccountValidationComponent } from './subscription-account-validation.component';
import { UsersService } from '../../services/users.service';
import { UsersServiceMock } from '../../mocks/users.service.mock';

/**
 * 3rd-party imports
 */
import { DsfrButtonModule } from '@betagouv/ngx-dsfr/button';
import { DsfrAlertModule } from '@betagouv/ngx-dsfr/alert';
import { of } from 'rxjs';

describe('SubscriptionAccountValidationComponent', () => {
  let component: SubscriptionAccountValidationComponent;
  let fixture: ComponentFixture<SubscriptionAccountValidationComponent>;
  let usersService: UsersServiceMock;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DsfrButtonModule, DsfrAlertModule],
      providers: [{ provide: UsersService, useClass: UsersServiceMock }],
      declarations: [SubscriptionAccountValidationComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(SubscriptionAccountValidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    usersService = TestBed.inject(UsersService) as unknown as UsersServiceMock;
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it("'should assign true to isEmailResent when the user clicks on the resend button", () => {
    jest.spyOn(component, 'resendConfirmationEmail');
    usersService.sendAccountConfirmationEmail.mockReturnValue(of('success'));
    let button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    expect(component.resendConfirmationEmail).toHaveBeenCalled();
    expect(usersService.sendAccountConfirmationEmail).toHaveBeenCalled();
    expect(component.isEmailResent).toBeTruthy();
  });
});
