/**
 * Angular imports
 */
import { Component, Input, OnDestroy } from '@angular/core';

/**
 * 3rd-party imports
 */
import { ElementAlignment } from '@betagouv/ngx-dsfr';
import { AlertType } from '@betagouv/ngx-dsfr/alert';
import { ButtonType } from '@betagouv/ngx-dsfr/button';

/**
 * Internal imports
 */
import { environment } from '../../../../environments/environment';
import { UsersService } from '../../services/users.service';
import { Subject, takeUntil } from 'rxjs';

@Component( {
  selector: 'infra-num-subscription-account-validation',
  templateUrl: './subscription-account-validation.component.html',
  styleUrls: [ './subscription-account-validation.component.scss' ]
} )
export class SubscriptionAccountValidationComponent implements OnDestroy {
  @Input() userEmail!: string;
  @Input() cityHallEmail: string | undefined;

  alertType: typeof AlertType = AlertType;
  buttonType: typeof ButtonType = ButtonType;
  iconAlignment: typeof ElementAlignment = ElementAlignment;
  isEmailResent: boolean = false;
  environment = environment;
  private unsubscribe$ = new Subject<void>();

  constructor( private usersService: UsersService ) {
  }

  resendConfirmationEmail() {
    this.usersService.sendAccountConfirmationEmail( this.cityHallEmail || this.userEmail, this.userEmail )
      .pipe( takeUntil( this.unsubscribe$ ) )
      .subscribe( {
        next: () => this.isEmailResent = true
      } );
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
