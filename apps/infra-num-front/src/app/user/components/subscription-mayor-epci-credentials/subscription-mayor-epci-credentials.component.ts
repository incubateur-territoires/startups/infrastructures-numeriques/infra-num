/**
 * Angular imports
 */
import { Component, EventEmitter, Input, OnInit, Output, OnDestroy } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormControlStatus,
  FormGroup,
  Validators
} from '@angular/forms';

/**
 * Internal imports
 */
import { SubscriptionData, SubscriptionStepComponent } from '../../subscription-common';
import { UsersService } from '../../services/users.service';

/**
 * 3rd-party imports
 */
import { ElementSize } from '@betagouv/ngx-dsfr';
import { PasswordParams, PasswordStatus } from '@betagouv/ngx-dsfr/password';
import { ButtonHtmlType } from '@betagouv/ngx-dsfr/button';
import { HttpErrorResponse } from '@angular/common/http';
import { Subject, takeUntil } from 'rxjs';
import { INVALID_FORMAT_EMAIL_ERROR } from '@infra-num/common';
import { CheckboxItem } from '@betagouv/ngx-dsfr/checkbox';

export const EMAIL_ALREADY_EXISTS_ERROR: string = 'Un compte existant utilise déjà cette adresse électronique';

@Component({
  selector: 'infra-num-subscription-mayor-epci-credentials',
  templateUrl: './subscription-mayor-epci-credentials.component.html',
  styleUrls: ['./subscription-mayor-epci-credentials.component.scss']
})
export class SubscriptionMayorEpciCredentialsComponent implements OnInit, OnDestroy, SubscriptionStepComponent {

  @Input() subscriptionData: SubscriptionData | undefined;
  @Input() passwordStatusInitialValue!: PasswordStatus;

  @Output() stepSubmitted = new EventEmitter<SubscriptionData>();

  subscriptionForm: FormGroup | undefined;
  passwordParams: PasswordParams = {
    minSize: 12,
    minDigitalCharacters: 1,
    minSpecialCharacters: 1
  };
  buttonSize = ElementSize.MEDIUM;
  buttonType = ButtonHtmlType.SUBMIT;
  passwordStatus;
  alreadyExistingEmail: string | undefined;
  failureMessage: string = EMAIL_ALREADY_EXISTS_ERROR;
  checkboxItem: CheckboxItem[] = [{
    id: 'newsletter',
    label: 'Être informé·e par courriel des fonctionnalités à venir sur la plateforme',
    value: 'subscribed'
  }];

  private unsubscribe$ = new Subject<void>();

  constructor(
    private formBuilder: FormBuilder,
    private usersService: UsersService
  ) {}

  ngOnInit(): void {
    this.passwordStatus = this.passwordStatusInitialValue;
    this.initForms();
  }

  private initForms(): void {
    this.subscriptionForm = this.formBuilder.group({
      email: new FormControl(this.subscriptionData?.email, [Validators.required, Validators.email]),
      password: new FormControl(this.subscriptionData?.password, [Validators.required]),
      newsletter: new FormControl(this.subscriptionData?.newsletter || [], []),
      honey: new FormControl('', [Validators.maxLength(0)])
    });

    this.subscriptionForm.get('email')
      .statusChanges.pipe(takeUntil(this.unsubscribe$)).subscribe({
      next: (status: FormControlStatus) => {
        if (status === 'INVALID') {
          this.failureMessage = INVALID_FORMAT_EMAIL_ERROR;
        } else if (status === 'VALID') {
          this.failureMessage = EMAIL_ALREADY_EXISTS_ERROR;
        }
      }
    });
  }

  onPasswordStatusChange(status: PasswordStatus) {
    this.passwordStatus = status;
  }

  onSubmit(): void {
    if ((this.subscriptionForm.valid) && this.passwordStatus === PasswordStatus.VALID) {
      this.usersService.checkIfEmailAlreadyExists(this.subscriptionForm.value.email)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe({
          next: () => this.stepSubmitted.emit(this.subscriptionForm.value),
          error: (httpErrorResponse: HttpErrorResponse) => {
            if (httpErrorResponse.error.statusCode === 400) {
              this.alreadyExistingEmail = this.subscriptionForm.value.email;
              this.failureMessage = EMAIL_ALREADY_EXISTS_ERROR;
            }
          }
        });
    }
  }

  onEmailInput(event: Event): void {
    const currentValue: string = (event.target as HTMLInputElement).value;
    if (currentValue !== this.alreadyExistingEmail) {
      this.alreadyExistingEmail = undefined;
    }
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
