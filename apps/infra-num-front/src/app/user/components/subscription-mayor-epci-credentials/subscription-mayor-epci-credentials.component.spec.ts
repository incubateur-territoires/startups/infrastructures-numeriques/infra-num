/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';

/**
 * Internal imports
 */
import { SubscriptionMayorEpciCredentialsComponent } from './subscription-mayor-epci-credentials.component';
import { UsersService } from '../../services/users.service';
import { UsersServiceMock } from '../../mocks/users.service.mock';

/**
 * 3rd-party imports
 */
import { DsfrInputModule } from '@betagouv/ngx-dsfr/input';
import { DsfrPasswordModule } from '@betagouv/ngx-dsfr/password';
import { DsfrStepperModule } from '@betagouv/ngx-dsfr/stepper';
import { DsfrButtonModule } from '@betagouv/ngx-dsfr/button';
import { DsfrCheckboxModule } from '@betagouv/ngx-dsfr/checkbox';

describe('SubscriptionMayorEpciCredentialsComponent', () => {
  let component: SubscriptionMayorEpciCredentialsComponent;
  let fixture: ComponentFixture<SubscriptionMayorEpciCredentialsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        DsfrInputModule,
        DsfrPasswordModule,
        DsfrCheckboxModule,
        DsfrStepperModule,
        DsfrButtonModule
      ],
      providers: [{ provide: UsersService, useClass: UsersServiceMock }],
      declarations: [SubscriptionMayorEpciCredentialsComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(SubscriptionMayorEpciCredentialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
