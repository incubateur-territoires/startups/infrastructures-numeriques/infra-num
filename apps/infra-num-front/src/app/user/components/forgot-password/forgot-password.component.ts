/**
 * Angular imports
 */
import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormControlStatus,
  NonNullableFormBuilder,
  Validators
} from '@angular/forms';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

/**
 * 3rd-party imports
 */
import { AlertType } from '@betagouv/ngx-dsfr/alert';
import { ButtonHtmlType } from '@betagouv/ngx-dsfr/button';
import { INVALID_FORMAT_EMAIL_ERROR } from '@infra-num/common';

/**
 * Internal imports
 */
import { UsersService } from '../../services/users.service';

@Component({
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  private takeUntilDestroyed = takeUntilDestroyed();

  forgottenPasswordForm = this.formBuilder.group({
    email: new FormControl('', [Validators.required, Validators.email]),
    honey: new FormControl('', [Validators.maxLength(0)])
  });
  failureMessage: string = INVALID_FORMAT_EMAIL_ERROR;
  buttonTypes: typeof ButtonHtmlType = ButtonHtmlType;
  isEmailSubmitted: boolean = false
  alertTypes: typeof AlertType = AlertType;

  constructor(
    private formBuilder: NonNullableFormBuilder,
    private usersService: UsersService
  ) {}

  ngOnInit(): void {
    const emailControl = this.forgottenPasswordForm.get('email');

    emailControl.statusChanges.pipe(this.takeUntilDestroyed).subscribe({
      next: (status: FormControlStatus) => {
        if (status === 'INVALID') {
          this.failureMessage = emailControl.hasError('required')
            ? 'Veuillez renseigner l’adresse électronique associée à votre compte'
            : INVALID_FORMAT_EMAIL_ERROR;
        }
      }
    });
  }

  submitForm(): void {
    if (this.forgottenPasswordForm.valid) {
      this.usersService.forgotPassword(this.forgottenPasswordForm.value.email)
      .pipe(this.takeUntilDestroyed)
      .subscribe({
        next: () => this.isEmailSubmitted = true
      });
    }
  }

}
