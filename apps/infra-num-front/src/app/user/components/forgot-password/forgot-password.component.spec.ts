/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';

/**
 * 3rd-party imports
 */
import { DsfrInputModule } from '@betagouv/ngx-dsfr/input';
import { DsfrButtonModule } from '@betagouv/ngx-dsfr/button';


/**
 * Internal imports
 */
import { ForgotPasswordComponent } from './forgot-password.component';
import { UsersService } from '../../services/users.service';
import { UsersServiceMock } from '../../mocks/users.service.mock';

describe('ForgotPasswordComponent', () => {
  let component: ForgotPasswordComponent;
  let fixture: ComponentFixture<ForgotPasswordComponent>;

  beforeEach( async () => {
    await TestBed.configureTestingModule( {
      imports: [ReactiveFormsModule, DsfrInputModule, DsfrButtonModule],
      declarations: [ForgotPasswordComponent],
      providers: [{ provide: UsersService, useClass: UsersServiceMock }]
    }).compileComponents();

    fixture = TestBed.createComponent(ForgotPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  } );

  it( 'should be created', () => {
    expect( component ).toBeTruthy();
  } );
} );
