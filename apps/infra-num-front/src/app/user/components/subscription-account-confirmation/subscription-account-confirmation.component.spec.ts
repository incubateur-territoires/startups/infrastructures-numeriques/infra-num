/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

/**
 * Internal imports
 */
import { SubscriptionAccountConfirmationComponent } from './subscription-account-confirmation.component';
import { UsersService } from '../../services/users.service';
import { UsersServiceMock } from '../../mocks/users.service.mock';

describe('SubscriptionAccountConfirmationComponent', () => {
  let component: SubscriptionAccountConfirmationComponent;
  let fixture: ComponentFixture<SubscriptionAccountConfirmationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [SubscriptionAccountConfirmationComponent],
      providers: [{ provide: UsersService, useClass: UsersServiceMock }]
    }).compileComponents();

    fixture = TestBed.createComponent(SubscriptionAccountConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
