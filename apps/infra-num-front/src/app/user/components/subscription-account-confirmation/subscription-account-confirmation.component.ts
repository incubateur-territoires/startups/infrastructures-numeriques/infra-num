/**
 * Angular imports
 */
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

/**
 * 3rd-party imports
 */
import { filter, Subject, takeUntil } from 'rxjs';
import { AlertType } from '@betagouv/ngx-dsfr/alert';
import { EXPIRED_JWT_TOKEN_ERROR, INVALID_JWT_TOKEN_ERROR, TokenObject } from '@infra-num/common';
import { ElementAlignment } from '@betagouv/ngx-dsfr';

/**
 * Internal imports
 */
import { UsersService } from '../../services/users.service';

@Component( {
  templateUrl: './subscription-account-confirmation.component.html',
  styleUrls: [ './subscription-account-confirmation.component.scss' ]
} )
export class SubscriptionAccountConfirmationComponent implements OnInit, OnDestroy {

  private unsubscribe$ = new Subject<void>();

  isEmailConfirmed: boolean;
  isErrorToken: boolean;
  alertType: typeof AlertType = AlertType;
  isExpiredToken: boolean;
  userEmail: string | undefined
  cityHallEmail: string | undefined
  isEmailResent: boolean = false;
  iconAlignment: typeof ElementAlignment = ElementAlignment;

  constructor(
    private usersService: UsersService,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.route.queryParams.pipe(
      takeUntil( this.unsubscribe$ ),
      filter( params => params.token )
    ).subscribe( {
      next: ( params ) => this.verifyEmailConfirmationToken( { token: params.token } )
    } );
  }

  private verifyEmailConfirmationToken( tokenObject: TokenObject ) {

    this.usersService.confirmUserAccount( tokenObject ).pipe(
      takeUntil( this.unsubscribe$ )
    ).subscribe({
      next: () => this.isEmailConfirmed = true,
      error: ( httpErrorResponse: HttpErrorResponse ) => {
        switch ( httpErrorResponse.error.message ) {
          case EXPIRED_JWT_TOKEN_ERROR: {
            this.isExpiredToken = true;
            this.userEmail = httpErrorResponse.error.userEmail;
            this.cityHallEmail = httpErrorResponse.error?.cityHallEmail;
            break;
          }
          case INVALID_JWT_TOKEN_ERROR: {
            this.isErrorToken = true;
            break;
          }
        }
      }
    });
  }

  goToLoginPage() {
    this.router.navigate( [ '/connexion' ] );
  }

  resendConfirmationEmail() {
    this.usersService.sendAccountConfirmationEmail( this.cityHallEmail || this.userEmail, this.userEmail )
      .pipe(takeUntil( this.unsubscribe$))
      .subscribe({
        next: () => this.isEmailResent = true
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
