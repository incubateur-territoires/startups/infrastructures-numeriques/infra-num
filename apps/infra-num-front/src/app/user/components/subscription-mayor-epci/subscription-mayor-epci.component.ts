/**
 * Angular imports
 */
import { Component, OnDestroy } from '@angular/core';

/**
 * 3rd-party imports
 */
import { PasswordStatus } from '@betagouv/ngx-dsfr/password';
import { ServicePublicApiProperties } from '@infra-num/common';
import { Subject, takeUntil } from 'rxjs';

/**
 * Internal imports
 */
import { SubscriptionData, SubscriptionMayorStep } from '../../subscription-common';
import { UsersService } from '../../services/users.service';
import { ServicePublicApiService } from '../../../api-gouv/services/service-public-api.service';

@Component({
  templateUrl: './subscription-mayor-epci.component.html',
  styleUrls: ['./subscription-mayor-epci.component.scss']
})
export class SubscriptionMayorEpciComponent implements OnDestroy {

  step: SubscriptionMayorStep = 1;
  subscriptionData: SubscriptionData | undefined;
  passwordStatus = PasswordStatus.INVALID;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private servicePublicApiService: ServicePublicApiService,
    private usersService: UsersService
  ) {
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  onGoBack() {
    this.passwordStatus = PasswordStatus.VALID;
    this.step--;
  }

  onStepSubmitted(data: SubscriptionData) {
    this.subscriptionData = {...this.subscriptionData, ...data};
    switch (this.step) {
      case 1: {
        this.step++;
        break;
      }
      case 2: {
        if (this.subscriptionData.locality) {
          this.servicePublicApiService
            .getLocalityInformation(
              this.subscriptionData.locality.originalObject.type,
              this.subscriptionData.locality.originalObject.code
            )
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
              next: (localityInformation: ServicePublicApiProperties) => {
                this.subscriptionData.locality.localityInformation = localityInformation;
                this.step++;
              }
            });

        }
        break;
      }
      case 3: {
        if (this.subscriptionData.user && this.subscriptionData.localityEmail) {
          this.usersService.createUser(this.subscriptionData.user, this.subscriptionData.localityEmail)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe({
              next: () => this.step++
            });
        }
      }
    }
  }
}
