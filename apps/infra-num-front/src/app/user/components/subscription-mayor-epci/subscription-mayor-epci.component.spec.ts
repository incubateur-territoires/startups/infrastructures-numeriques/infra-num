/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';

/**
 * 3rd-party imports
 */
import { DsfrButtonModule } from '@betagouv/ngx-dsfr/button';
import { DsfrCheckboxModule } from '@betagouv/ngx-dsfr/checkbox';
import { DsfrPasswordModule } from '@betagouv/ngx-dsfr/password';
import { DsfrInputModule } from '@betagouv/ngx-dsfr/input';
import { DsfrStepperModule } from '@betagouv/ngx-dsfr/stepper';

/**
 * Internal imports
 */
import { SubscriptionMayorEpciComponent } from './subscription-mayor-epci.component';
import { UsersService } from '../../services/users.service';
import { UsersServiceMock } from '../../mocks/users.service.mock';
import { ServicePublicApiServiceMock } from '../../mocks/service-public-api.service.mock';
import { ServicePublicApiService } from '../../../api-gouv/services/service-public-api.service';
import { SubscriptionMayorEpciCredentialsComponent } from '../subscription-mayor-epci-credentials/subscription-mayor-epci-credentials.component';

describe('SubscriptionMayorEpciComponent', () => {
  let component: SubscriptionMayorEpciComponent;
  let fixture: ComponentFixture<SubscriptionMayorEpciComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        DsfrButtonModule,
        DsfrCheckboxModule,
        DsfrPasswordModule,
        DsfrInputModule,
        DsfrStepperModule
      ],
      declarations: [
        SubscriptionMayorEpciComponent,
        SubscriptionMayorEpciCredentialsComponent
      ],
      providers: [
        {
          provide: ServicePublicApiService,
          useClass: ServicePublicApiServiceMock
        },
        { provide: UsersService, useClass: UsersServiceMock }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(SubscriptionMayorEpciComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
