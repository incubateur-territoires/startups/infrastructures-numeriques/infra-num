/**
 * Angular imports
 */
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';

/**
 * Internal imports
 */
import { SubscriptionData, SubscriptionStepComponent } from '../../subscription-common';

/**
 * 3rd-party imports
 */
import { ElementSize } from '@betagouv/ngx-dsfr';
import { ButtonHtmlType, ButtonType } from '@betagouv/ngx-dsfr/button';

@Component({
  selector: 'infra-num-subscription-mayor-epci-information',
  templateUrl: './subscription-mayor-epci-information.component.html',
  styleUrls: ['./subscription-mayor-epci-information.component.scss']
})
export class SubscriptionMayorEpciInformationComponent implements OnInit, SubscriptionStepComponent {

  @Input() subscriptionData: SubscriptionData | undefined;

  @Output() stepSubmitted = new EventEmitter<SubscriptionData>();
  @Output() goingBack = new EventEmitter<void>();

  subscriptionForm: FormGroup | undefined;
  buttonSize = ElementSize.MEDIUM;
  backButtonType = ButtonType.SECONDARY;
  submitButtonHtmlType = ButtonHtmlType.SUBMIT;
  backButtonHtmlType = ButtonHtmlType.BUTTON;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.initForms();
  }

  private initForms(): void {
    this.subscriptionForm = this.formBuilder.group({
      locality: [this.subscriptionData?.locality, Validators.required]
    });
  }

  onSubmit(): void {
    this.stepSubmitted.emit({ locality: this.subscriptionForm.value.locality });
  }

  onBackButtonClicked(): void {
    this.goingBack.emit();
  }
}
