/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';

/**
 * Internal imports
 */
import { SubscriptionMayorEpciInformationComponent } from './subscription-mayor-epci-information.component';

/**
 * 3rd-party imports
 */
import { DsfrStepperModule } from '@betagouv/ngx-dsfr/stepper';
import { DsfrButtonModule } from '@betagouv/ngx-dsfr/button';
import { DsfrSearchBarModule } from '@betagouv/ngx-dsfr/search-bar';

describe('SubscriptionMayorEpciInformationComponent', () => {
  let component: SubscriptionMayorEpciInformationComponent;
  let fixture: ComponentFixture<SubscriptionMayorEpciInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        DsfrStepperModule,
        DsfrButtonModule,
        DsfrSearchBarModule
      ],
      declarations: [SubscriptionMayorEpciInformationComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(SubscriptionMayorEpciInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
