/**
 * Angular imports
 */
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output
} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormControlStatus,
  FormGroup,
  Validators
} from '@angular/forms';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

/**
 * Internal imports
 */
import { SubscriptionData, SubscriptionStepComponent } from '../../subscription-common';
import { environment } from '../../../../environments/environment';

/**
 * 3rd-party imports
 */
import { ElementSize } from '@betagouv/ngx-dsfr';
import { PasswordParams, PasswordStatus } from '@betagouv/ngx-dsfr/password';
import { ButtonHtmlType } from '@betagouv/ngx-dsfr/button';
import { AlertType } from '@betagouv/ngx-dsfr/alert';
import {
  EMAIL_ALLOWED_ACCOUNT_NOT_FOUND_ERROR,
  INVALID_FORMAT_EMAIL_ERROR
} from '@infra-num/common';
import { CheckboxItem } from '@betagouv/ngx-dsfr/checkbox';


@Component({
  selector: 'infra-num-subscription-others-credentials',
  templateUrl: './subscription-others-credentials.component.html',
  styleUrls: ['./subscription-others-credentials.component.scss']
})
export class SubscriptionOthersCredentialsComponent implements OnInit, SubscriptionStepComponent, OnChanges {

  @Input() disallowedEmail: string | undefined;
  @Input() serverErrorMessage: string | undefined;

  @Output() stepSubmitted = new EventEmitter<SubscriptionData>();

  subscriptionForm: FormGroup | undefined;
  passwordParams: PasswordParams = {
    minSize: 12,
    minDigitalCharacters: 1,
    minSpecialCharacters: 1
  };
  buttonSize = ElementSize.MEDIUM;
  buttonType = ButtonHtmlType.SUBMIT;
  passwordStatus: PasswordStatus;
  environment = environment;
  emailNotFound: boolean = false;
  alertType: typeof AlertType = AlertType;
  failureMessage: string = EMAIL_ALLOWED_ACCOUNT_NOT_FOUND_ERROR;
  checkboxItem: CheckboxItem[] = [{
    id: 'newsletter',
    label: 'Être informé·e par courriel des fonctionnalités à venir sur la plateforme',
    value: 'subscribed'
  }];

  private takeUntilDestroyed = takeUntilDestroyed();

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.initForms();
  }

  ngOnChanges() {
    this.failureMessage = this.serverErrorMessage;
  }

  private initForms(): void {
    this.subscriptionForm = this.formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
      newsletter: new FormControl([], []),
      honey: new FormControl('', [Validators.maxLength(0)])
    });

    this.subscriptionForm.get('email').statusChanges
      .pipe(this.takeUntilDestroyed)
      .subscribe({
        next: (status: FormControlStatus) => {
          if (status === 'INVALID') {
            this.failureMessage = INVALID_FORMAT_EMAIL_ERROR;
          } else if (status === 'VALID') {
            this.failureMessage = this.serverErrorMessage = undefined;
          }
        }
      });
  }

  onEmailInput(event: Event): void {
    const currentValue: string = (event.target as HTMLInputElement).value;
    if (currentValue !== this.disallowedEmail) {
      this.disallowedEmail = undefined;
    }
  }

  onPasswordStatusChange(status: PasswordStatus) {
    this.passwordStatus = status;
  }

  onSubmit(): void {
    if ((this.subscriptionForm.valid) && this.passwordStatus === PasswordStatus.VALID) {
      this.stepSubmitted.emit(this.subscriptionForm.value);
    }
  }
}
