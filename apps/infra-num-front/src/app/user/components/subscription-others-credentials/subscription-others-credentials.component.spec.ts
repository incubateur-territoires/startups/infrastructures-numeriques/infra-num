/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

/**
 * Internal imports
 */
import { SubscriptionOthersCredentialsComponent } from './subscription-others-credentials.component';
import { UsersService } from '../../services/users.service';

/**
 * 3rd-party imports
 */
import { DsfrInputModule } from '@betagouv/ngx-dsfr/input';
import { DsfrPasswordModule } from '@betagouv/ngx-dsfr/password';
import { DsfrButtonModule } from '@betagouv/ngx-dsfr/button';
import { DsfrAlertModule } from '@betagouv/ngx-dsfr/alert';
import { DsfrCheckboxModule } from '@betagouv/ngx-dsfr/checkbox';

describe('SubscriptionOthersCredentialsComponent', () => {
  let component: SubscriptionOthersCredentialsComponent;
  let fixture: ComponentFixture<SubscriptionOthersCredentialsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        DsfrInputModule,
        DsfrPasswordModule,
        DsfrCheckboxModule,
        DsfrButtonModule,
        DsfrAlertModule,
        HttpClientModule
      ],
      providers: [UsersService],
      declarations: [SubscriptionOthersCredentialsComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(SubscriptionOthersCredentialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
