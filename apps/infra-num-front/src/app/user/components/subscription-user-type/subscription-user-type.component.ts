/**
 * Angular imports
 */
import { Component } from '@angular/core';

@Component({
  selector: 'infra-num-subscription-user-type',
  templateUrl: './subscription-user-type.component.html',
  styleUrls: ['./subscription-user-type.component.scss']
})
export class SubscriptionUserTypeComponent {
}
