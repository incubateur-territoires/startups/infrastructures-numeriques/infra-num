/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

/**
 * 3rd-party imports
 */
import { DsfrTileModule } from '@betagouv/ngx-dsfr/tile';

/**
 * Internal imports
 */
import { SubscriptionUserTypeComponent } from './subscription-user-type.component';

describe('SubscriptionUserTypeComponent', () => {
  let component: SubscriptionUserTypeComponent;
  let fixture: ComponentFixture<SubscriptionUserTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        DsfrTileModule
      ],
      declarations: [SubscriptionUserTypeComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(SubscriptionUserTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
