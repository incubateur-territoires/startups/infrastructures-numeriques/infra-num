/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

/**
 * Internal imports
 */
import { ResetPasswordComponent } from './reset-password.component';
import { UsersService } from '../../services/users.service';
import { UsersServiceMock } from '../../mocks/users.service.mock';

describe('ResetPasswordComponent', () => {
  let component: ResetPasswordComponent;
  let fixture: ComponentFixture<ResetPasswordComponent>;

  beforeEach( async () => {
    await TestBed.configureTestingModule( {
      imports: [
        RouterTestingModule
      ],
      declarations: [ResetPasswordComponent],
      providers: [{ provide: UsersService, useClass: UsersServiceMock }]
    }).compileComponents();

    fixture = TestBed.createComponent(ResetPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it( 'should be created', () => {
    expect( component ).toBeTruthy();
  });
});
