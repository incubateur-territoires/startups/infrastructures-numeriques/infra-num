/**
 * Angular imports
 */
import { Component, OnInit } from '@angular/core';
import { FormControl, NonNullableFormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

/**
 * 3rd-party imports
 */
import { PasswordParams, PasswordStatus } from '@betagouv/ngx-dsfr/password';
import { mergeMap } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { AlertType } from '@betagouv/ngx-dsfr/alert';
import { ButtonHtmlType } from '@betagouv/ngx-dsfr/button';

/**
 * Internal imports
 */
import { UsersService } from '../../services/users.service';
import { HttpErrorResponse } from '@angular/common/http';
import { EXPIRED_JWT_TOKEN_ERROR, INVALID_JWT_TOKEN_ERROR } from '@infra-num/common';

@Component({
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  passwordStatus = PasswordStatus.INVALID;
  isPasswordReset: boolean = false;
  resetPasswordForm = this.formBuilder.group({
    password: new FormControl('', [Validators.required]),
    honey: new FormControl('', [Validators.maxLength(0)])
  });
  buttonTypes: typeof ButtonHtmlType = ButtonHtmlType;
  alertTypes: typeof AlertType = AlertType;
  resetPasswordToken: string = '';
  isErrorToken: boolean = false;
  errorMessage: string = 'Une erreur est survenue, veuillez réessayer plus tard.';
  passwordSignUpParams: PasswordParams = {
    minSize: 12,
    minDigitalCharacters: 1,
    minSpecialCharacters: 1
  };

  private takeUntilDestroyed = takeUntilDestroyed();

  constructor(
    private usersService: UsersService,
    private formBuilder: NonNullableFormBuilder,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.route.queryParams.pipe(
      mergeMap((params) => {
        this.resetPasswordToken = params.token;
        if (!this.resetPasswordToken) {
          this.router.navigate(['/']);
          return;
        }
        return this.usersService.verifyResetPasswordToken({token: this.resetPasswordToken});
      }),
      this.takeUntilDestroyed
    ).subscribe({
      error: (httpErrorResponse: HttpErrorResponse) => {
        this.isErrorToken = true;
        switch (httpErrorResponse.error.message) {
          case EXPIRED_JWT_TOKEN_ERROR: {
            this.errorMessage = 'Le lien de validation a expiré.'
            break;
          }
          case INVALID_JWT_TOKEN_ERROR: {
            this.errorMessage = 'Le code de validation est incorrect.'
            break;
          }
        }
      }
    });
  }

  submitForm(): void {
    if ((this.resetPasswordForm.valid) && this.passwordStatus === PasswordStatus.VALID) {
      this.usersService.resetPassword(this.resetPasswordForm.value.password, this.resetPasswordToken)
        .pipe(this.takeUntilDestroyed)
        .subscribe({
          next: () => this.isPasswordReset = true,
        });
    }
  }
}
