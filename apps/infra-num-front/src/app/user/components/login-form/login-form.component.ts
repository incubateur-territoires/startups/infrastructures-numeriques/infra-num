/**
 * Angular imports
 */
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  FormControl,
  FormControlStatus,
  FormGroup,
  NonNullableFormBuilder,
  Validators
} from '@angular/forms';
import { Router } from '@angular/router';

/**
 * 3rd-party imports
 */
import { ElementSize } from '@betagouv/ngx-dsfr';
import { ButtonHtmlType, ButtonType } from '@betagouv/ngx-dsfr/button';
import { Subject, takeUntil } from 'rxjs';
import { INVALID_FORMAT_EMAIL_ERROR, INVALID_LOGIN_CREDENTIALS } from '@infra-num/common';

/**
 * Internal imports
 */
import {
  AuthenticationService,
  Credentials
} from '../../services/authentication.service';
import { environment } from '../../../../environments/environment';

/**
 * TypeScript entities and constants
 */
type LoginFormGroup = {
  honey: FormControl<string>;
  email: FormControl<string>;
  password: FormControl<string>;
};

@Component({
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();
  appName = environment.appName;
  loginForm: FormGroup<LoginFormGroup> | undefined;
  buttonSize: typeof ElementSize = ElementSize;
  buttonHtmlType: typeof ButtonHtmlType = ButtonHtmlType;
  buttonType: typeof ButtonType = ButtonType;
  loginFormError: boolean = false;
  failureMessage: string = INVALID_FORMAT_EMAIL_ERROR;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private formBuilder: NonNullableFormBuilder
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  private initForm(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      honey: ['', Validators.maxLength(0)]
    });

    this.loginForm.get('email').statusChanges.pipe(
      takeUntil(this.unsubscribe$)).subscribe({
        next: (status: FormControlStatus) => {
          this.loginFormError = false;
          if (status === 'INVALID') {
            this.failureMessage = INVALID_FORMAT_EMAIL_ERROR;
          } else if (status === 'VALID') {
            this.failureMessage = INVALID_LOGIN_CREDENTIALS;
          }
        }
      });
  }

  onSubmit(): void {
    if (this.loginForm.valid) {
      const credentials: Credentials = {
        email: this.loginForm.value.email.toLowerCase(),
        password: this.loginForm.value.password
      };

      this.authenticationService
        .login(credentials)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe({
          next: () => {
            this.router.navigate(['/telephonie-mobile']);
          },
          error: (httpErrorResponse: HttpErrorResponse) => {
            this.loginFormError = true;
            this.failureMessage = httpErrorResponse.error.message;
          }
        });
    }
  }

  goToSignUpPage(): void {
    this.router.navigate(['/inscription']);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
