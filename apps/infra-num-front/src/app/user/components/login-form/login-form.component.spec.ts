/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';

/**
 * 3rd-party imports
 */
import { DsfrInputModule } from '@betagouv/ngx-dsfr/input';
import { DsfrPasswordModule } from '@betagouv/ngx-dsfr/password';
import { DsfrButtonModule } from '@betagouv/ngx-dsfr/button';
import { of } from 'rxjs';

/**
 * Internal imports
 */
import { LoginFormComponent } from './login-form.component';
import { AuthenticationService } from '../../services/authentication.service';
import { AuthenticationServiceMock } from '../../mocks/authentication.service.mock';

describe('LoginFormComponent', () => {
  let component: LoginFormComponent;
  let fixture: ComponentFixture<LoginFormComponent>;
  let authenticationService: AuthenticationServiceMock;
  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        ReactiveFormsModule,
        DsfrInputModule,
        DsfrPasswordModule,
        DsfrButtonModule
      ],
      declarations: [LoginFormComponent],
      providers: [
        { provide: AuthenticationService, useClass: AuthenticationServiceMock }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(LoginFormComponent);
    authenticationService = TestBed.inject(
      AuthenticationService
    ) as unknown as AuthenticationServiceMock;
    router = TestBed.inject(Router);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should call auth service login when form is valid and submitted', () => {
    const form = fixture.debugElement.query(By.css('form'));
    jest.spyOn(component, 'onSubmit');
    authenticationService.login.mockReturnValue(of(null));
    router.navigate = jest.fn();
    component.loginForm.controls.email.setValue('test@gouv.fr');
    component.loginForm.controls.password.setValue('azerty12345+');
    component.loginForm.controls.honey.setValue('');
    form.triggerEventHandler('submit', null);
    expect(component.loginForm.valid).toBeTruthy();
    expect(component.onSubmit).toHaveBeenCalled();
    expect(authenticationService.login).toHaveBeenCalledWith({
      email: 'test@gouv.fr',
      password: 'azerty12345+'
    });
    expect(router.navigate).toHaveBeenCalled();
  });
});
