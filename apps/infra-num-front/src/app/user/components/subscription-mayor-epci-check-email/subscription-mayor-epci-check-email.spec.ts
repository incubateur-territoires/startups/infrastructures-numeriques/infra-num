/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';

/**
 * Internal imports
 */
import { SubscriptionMayorEpciCheckEmailComponent } from './subscription-mayor-epci-check-email.component';

/**
 * 3rd-party imports
 */
import { DsfrStepperModule } from '@betagouv/ngx-dsfr/stepper';
import { DsfrButtonModule } from '@betagouv/ngx-dsfr/button';
import { DsfrAlertModule } from '@betagouv/ngx-dsfr/alert';

describe('SubscriptionMayorEpciCheckEmailComponent', () => {
  let component: SubscriptionMayorEpciCheckEmailComponent;
  let fixture: ComponentFixture<SubscriptionMayorEpciCheckEmailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        DsfrStepperModule,
        DsfrButtonModule,
        DsfrAlertModule
      ],
      declarations: [SubscriptionMayorEpciCheckEmailComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(SubscriptionMayorEpciCheckEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
