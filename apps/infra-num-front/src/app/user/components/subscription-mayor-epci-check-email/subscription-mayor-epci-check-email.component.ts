/**
 * Angular imports
 */
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

/**
 * 3rd-party imports
 */
import { AlertType } from '@betagouv/ngx-dsfr/alert';
import { ElementSize } from '@betagouv/ngx-dsfr';
import { ButtonHtmlType, ButtonType } from '@betagouv/ngx-dsfr/button';
import { JurisdictionType, Role, UserToCreate } from '@infra-num/common';

/**
 * Internal imports
 */
import { SubscriptionData, SubscriptionStepComponent } from '../../subscription-common';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'infra-num-subscription-mayor-epci-check-email',
  templateUrl: './subscription-mayor-epci-check-email.component.html',
  styleUrls: ['./subscription-mayor-epci-check-email.component.scss']
})
export class SubscriptionMayorEpciCheckEmailComponent implements OnInit, SubscriptionStepComponent {

  @Input() subscriptionData: SubscriptionData | undefined;

  @Output() stepSubmitted = new EventEmitter<SubscriptionData>();
  @Output() goingBack = new EventEmitter<void>();

  alertType = AlertType;
  buttonSize = ElementSize.MEDIUM;
  buttonType: typeof ButtonHtmlType = ButtonHtmlType;
  buttonDesign = ButtonType.SECONDARY;
  noEmailFound: boolean = false;
  environment = environment;
  currentRole: Role;
  currentJurisdictionType: JurisdictionType;
  currentZoom: number;

  ngOnInit(): void {
    this.noEmailFound = !this.subscriptionData?.locality.localityInformation.adresse_courriel;
  }

  onBackButtonClicked(): void {
    this.goingBack.emit();
  }

  onSubmit(): void {
    this.setRoleAndJurisdictionType();
    const userToCreate: UserToCreate = {
      roles: [this.currentRole],
      email: this.subscriptionData.email,
      password: this.subscriptionData.password,
      hasSubscribedToNewsletter: this.subscriptionData.newsletter.includes('subscribed'),
      jurisdictions: [{
        type: this.currentJurisdictionType,
        initialZoom: this.currentZoom,
        code: this.subscriptionData.locality.originalObject.code ?? undefined,
        name: this.subscriptionData.locality.originalObject.nom,
        referenceCoordinates: this.subscriptionData.locality.originalObject.centre.coordinates.reverse() as [number, number]
      }]
    };
    this.stepSubmitted.emit({
      user: userToCreate,
      localityEmail: this.subscriptionData.locality.localityInformation.adresse_courriel
    });
  }

  private setRoleAndJurisdictionType(): void {
    switch(this.subscriptionData.locality.originalObject.type) {
      case 'city':
        this.currentJurisdictionType = JurisdictionType.City;
        this.currentRole = Role.Mayor;
        this.currentZoom = 13;
        break;
      case 'epci':
        this.currentJurisdictionType = JurisdictionType.EPCI;
        this.currentRole = Role.EPCI;
        this.currentZoom = 10;
        break;
    }
  }
}
