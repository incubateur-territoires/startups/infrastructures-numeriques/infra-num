/**
 * Angular imports
 */
import { Component } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

/**
 * 3rd-party imports
 */
import {
  ACCOUNT_ALREADY_EXISTS_ERROR,
  ALLOWED_ACCOUNT_NOT_FOUND, EMAIL_ALLOWED_ACCOUNT_NOT_FOUND_ERROR,
  EMAIL_ALREADY_EXISTS_ERROR, INTERNAL_SERVER_ERROR,
  UserToCreate
} from '@infra-num/common';

/**
 * Internal imports
 */
import { UsersService } from '../../services/users.service';
import { SubscriptionData } from '../../subscription-common';

@Component({
  templateUrl: './subscription-others.component.html',
  styleUrls: ['./subscription-others.component.scss']
})
export class SubscriptionOthersComponent {
  isAllowedAccountEmailVerified: boolean = false;
  disallowedEmail: string;
  userEmail: string;
  serverErrorMessage: string | undefined;

  private takeUntilDestroyed = takeUntilDestroyed();

  constructor(private usersService: UsersService) {}

  onStepSubmitted(subscriptionData: SubscriptionData) {
    const userToCreate: UserToCreate = {
      email: subscriptionData.email.toLowerCase(),
      password: subscriptionData.password,
      hasSubscribedToNewsletter: subscriptionData.newsletter.includes('subscribed')
    };
    this.usersService.createUser(userToCreate)
      .pipe(this.takeUntilDestroyed)
      .subscribe({
        next: () => {
          this.userEmail = userToCreate.email;
          this.isAllowedAccountEmailVerified = true;
        },
        error: (errors: HttpErrorResponse) => {
          this.disallowedEmail = undefined;
          switch (errors.error.message) {
            case ALLOWED_ACCOUNT_NOT_FOUND:
              this.serverErrorMessage = EMAIL_ALLOWED_ACCOUNT_NOT_FOUND_ERROR;
              this.disallowedEmail = subscriptionData.email;
              break;
            case EMAIL_ALREADY_EXISTS_ERROR:
              this.serverErrorMessage = ACCOUNT_ALREADY_EXISTS_ERROR;
              break;
            default:
              this.serverErrorMessage = INTERNAL_SERVER_ERROR
              break;
          }
        }
      }
    );
  }
}
