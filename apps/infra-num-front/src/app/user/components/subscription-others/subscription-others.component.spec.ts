/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';

/**
 * 3rd-party imports
 */
import { DsfrButtonModule } from '@betagouv/ngx-dsfr/button';
import { DsfrCheckboxModule } from '@betagouv/ngx-dsfr/checkbox';
import { DsfrPasswordModule } from '@betagouv/ngx-dsfr/password';
import { DsfrInputModule } from '@betagouv/ngx-dsfr/input';
import { DsfrAlertModule } from '@betagouv/ngx-dsfr/alert';

/**
 * Internal imports
 */
import { SubscriptionOthersComponent } from './subscription-others.component';
import { UsersService } from '../../services/users.service';
import { UsersServiceMock } from '../../mocks/users.service.mock';
import { SubscriptionOthersCredentialsComponent } from '../subscription-others-credentials/subscription-others-credentials.component';

describe('SubscriptionOthersComponent', () => {
  let component: SubscriptionOthersComponent;
  let fixture: ComponentFixture<SubscriptionOthersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        DsfrButtonModule,
        DsfrCheckboxModule,
        DsfrPasswordModule,
        DsfrInputModule,
        DsfrAlertModule
      ],
      declarations: [
        SubscriptionOthersComponent,
        SubscriptionOthersCredentialsComponent
      ],
      providers: [{ provide: UsersService, useClass: UsersServiceMock }]
    }).compileComponents();

    fixture = TestBed.createComponent(SubscriptionOthersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
