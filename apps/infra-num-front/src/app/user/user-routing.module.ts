/**
 * Angular imports
 */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

/**
 * Internal imports
 */
import { UserRootComponent } from './user-root.component';
import { SubscriptionUserTypeComponent } from './components/subscription-user-type/subscription-user-type.component';
import { SubscriptionMayorEpciComponent } from './components/subscription-mayor-epci/subscription-mayor-epci.component';
import { SubscriptionOthersComponent } from './components/subscription-others/subscription-others.component';
import { SubscriptionAccountConfirmationComponent } from './components/subscription-account-confirmation/subscription-account-confirmation.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { isUserNotAuthenticated } from './guards/auth.guard';

/**
 * TypeScript entities and constants
 */
const routes: Routes = [
  {
    path: '',
    component: UserRootComponent,
    canActivateChild: [isUserNotAuthenticated],
    children: [
      {
        path: 'inscription',
        component: SubscriptionUserTypeComponent
      },
      {
        path: 'inscription/mairie-epci',
        component: SubscriptionMayorEpciComponent
      },
      {
        path: 'inscription/autres',
         component: SubscriptionOthersComponent
      },
      {
        path: 'validation-compte',
        component: SubscriptionAccountConfirmationComponent
      },
      {
        path: 'oubli-mdp',
        component: ForgotPasswordComponent
      },
      {
        path: 'nouveau-mdp',
        component: ResetPasswordComponent
      },
      {
        path: 'connexion',
        component: LoginFormComponent
      }
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {}
