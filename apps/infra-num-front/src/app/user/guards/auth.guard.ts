/**
 * Angular imports
 */
import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';

/**
 * 3rd-party imports
 */
import { catchError, map, of } from 'rxjs';

/**
 * Internal imports
 */
import { AuthenticationService } from '../services/authentication.service';

export const isUserNotAuthenticated: CanActivateFn = () => {
  /*
   * We need to inject the router here because
   * inside the `map` operator is not an allowed
   * injection context ( see https://angular.io/errors/NG0203 )
   */
  const router = inject(Router);

  return inject(AuthenticationService).authenticatedUser.pipe(
    map((authenticatedUser) => {
      if (authenticatedUser !== null) {
        return router.parseUrl('/');
      }

      return true;
    }),
    catchError(() => {
      return of(false);
    })
  );
};

export const isUserAuthenticated: CanActivateFn = () => {
  /*
   * We need to inject the router here because
   * inside the `map` operator is not an allowed
   * injection context ( see https://angular.io/errors/NG0203 )
   */
  const router = inject(Router);

  return inject(AuthenticationService).authenticatedUser.pipe(
    map((authenticatedUser) => {
      if (authenticatedUser === null) {
        return router.parseUrl('/connexion');
      }
      return true;
    }),
    catchError(() => {
      return of(false);
    })
  );
};
