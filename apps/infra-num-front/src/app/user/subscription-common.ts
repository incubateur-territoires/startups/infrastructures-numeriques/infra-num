/**
 * Angular imports
 */
import { EventEmitter } from '@angular/core';
import { City, Epci, ServicePublicApiProperties, UserToCreate } from '@infra-num/common';

/**
 * TypeScript entities and constants
 */
export interface SubscriptionData {
  email?: string;
  password?: string;
  newsletter?: string[];
  locality?: { id: string, label: string, originalObject: City | Epci, localityInformation?: ServicePublicApiProperties };
  user?: UserToCreate;
  localityEmail?: string;
}

export interface SubscriptionStepComponent {
  stepSubmitted: EventEmitter<SubscriptionData>;
  goingBack?: EventEmitter<void>;
}

export type SubscriptionMayorStep = 1 | 2 | 3 | 4;
