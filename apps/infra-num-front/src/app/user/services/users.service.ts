/**
 * Angular imports
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

/**
 * Internal imports
 */
import { environment } from '../../../environments/environment';

/**
 * 3rd party imports
 */
import { Observable } from 'rxjs';
import { AllowedAccount, SafeUser, TokenObject, UserToCreate } from '@infra-num/common';

@Injectable()
export class UsersService {
  constructor(private http: HttpClient) {}

  createUser(user: UserToCreate, cityHallEmail?: string): Observable<void> {
    return this.http.post<void>(`${environment.apiUrl}/users`, {user, ...(cityHallEmail && {cityHallEmail})});
  }

  sendAccountConfirmationEmail(sendTo: string, sendFor: string): Observable<null> {
    return this.http.post<null>(`${environment.apiUrl}/users/account-confirmation-email`, {sendTo, sendFor});
  }

  checkIfEmailAlreadyExists(email: string): Observable<null> {
    return this.http.get<null>(`${environment.apiUrl}/users/check-email/${email}`);
  }

  confirmUserAccount(tokenObject: TokenObject): Observable<SafeUser> {
    return this.http.put<SafeUser>(
      `${environment.apiUrl}/users/confirm-account`,
      tokenObject
    );
  }

  forgotPassword(email: string): Observable<null> {
    return this.http.post<null>(`${environment.apiUrl}/users/forgot-password`, { email });
  }

  verifyResetPasswordToken(tokenObject: TokenObject): Observable<null> {
    return this.http.post<null>(`${environment.apiUrl}/users/verify`, tokenObject);
  }

  resetPassword(password: string, token: string): Observable<null> {
    return this.http.put<null>(`${environment.apiUrl}/users/reset-password`, { password, token });
  }

  getPointsOfContactInformation(region: string, department: string): Observable<AllowedAccount[]> {
    return this.http.get<AllowedAccount[]>(`${environment.apiUrl}/users/contacts?region=${region}&department=${department}`,
      {
        withCredentials: true
      }
    );
  }
}
