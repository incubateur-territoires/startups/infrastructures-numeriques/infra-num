/**
 * Angular imports
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

/**
 * 3rd-party imports
 */
import { SafeUser } from '@infra-num/common';
import { BehaviorSubject, catchError, Observable, of, tap } from 'rxjs';

/**
 * Internal imports
 */
import { environment } from '../../../environments/environment';

export interface Credentials {
  email: string;
  password: string;
}

@Injectable()
export class AuthenticationService {
  private authenticatedUser$ = new BehaviorSubject<SafeUser>(null);

  get authenticatedUser(): Observable<SafeUser> {
    return this.authenticatedUser$.asObservable();
  }

  constructor(private http: HttpClient) {}

  login(credentials: Credentials): Observable<SafeUser> {
    return this.http
      .post<SafeUser>(`${environment.apiUrl}/login`, credentials, {
        withCredentials: true
      })
      .pipe(
        tap((user) => {
          this.authenticatedUser$.next(user);
        }),
        catchError((error) => {
          this.authenticatedUser$.next(null);
          throw error;
        })
      );
  }

  logout(): Observable<void> {
    return this.http
      .get<void>(`${environment.apiUrl}/logout`, { withCredentials: true })
      .pipe(
        tap(() => {
          this.authenticatedUser$.next(null);
        })
      );
  }

  updateAuthenticatedUser(): Observable<SafeUser> {
    return this.http
      .get<SafeUser>(`${environment.apiUrl}/user-information`, {
        withCredentials: true
      })
      .pipe(
        tap((user) => {
          this.authenticatedUser$.next(user);
        }),
        catchError(() => {
          this.authenticatedUser$.next(null);
          return of(null);
        })
      );
  }
}
