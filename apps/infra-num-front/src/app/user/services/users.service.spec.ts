/**
 * Angular imports
 */
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

/**
 * 3rd-party imports
 */
import { JurisdictionType, Role, UserToCreate } from '@infra-num/common';

/**
 * Internal imports
 */
import { environment } from '../../../environments/environment';
import { UsersService } from './users.service';

describe('UsersService', () => {
  let service: UsersService;
  let httpController: HttpTestingController;

  const userMock: UserToCreate = {
    roles: [Role.Mayor],
    email: 'test@test.fr',
    password: 'Azerty1.',
    hasSubscribedToNewsletter: false,
    jurisdictions: [{
      type: JurisdictionType.City,
      name: 'Ailleville',
      initialZoom: 12,
      referenceCoordinates: [4.6911, 48.2562]
    }]
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UsersService]
    });
    service = TestBed.inject(UsersService);
    httpController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpController.verify();
  });

  it('should use POST method to create a user and send only the user when the cityHallEmail is not provided', async () => {
    service.createUser(userMock).subscribe();
    const req = httpController.expectOne(`${environment.apiUrl}/users`);
    expect(req.request.method).toEqual('POST');
    expect(req.request.body).toMatchObject({ user: userMock });
    expect(req.request.body.cityHallEmail).toBeUndefined();
  });

  it('should use POST method to create a user and send the user and the cityHallEmail when the cityHallEmail is provided', async () => {
    service.createUser(userMock, 'fakeCityHallEmail').subscribe();
    const req = httpController.expectOne(`${environment.apiUrl}/users`);
    expect(req.request.method).toEqual('POST');
    expect(req.request.body).toMatchObject({
      user: userMock,
      cityHallEmail: 'fakeCityHallEmail'
    });
    expect(req.request.body.cityHallEmail).toEqual('fakeCityHallEmail');
  });

  it('should use PUT method to confirm a user account', async () => {
    service.confirmUserAccount({ token: 'aZhjbvDE12delezvyMCsvf' }).subscribe();
    const req = httpController.expectOne(
      `${environment.apiUrl}/users/confirm-account`
    );
    expect(req.request.method).toEqual('PUT');
  });

  it('should use POST method to send confirmation email', async () => {
    service
      .sendAccountConfirmationEmail('test@test.fr', 'test@test.fr')
      .subscribe();
    const req = httpController.expectOne(
      `${environment.apiUrl}/users/account-confirmation-email`
    );
    expect(req.request.method).toEqual('POST');
  });
});
