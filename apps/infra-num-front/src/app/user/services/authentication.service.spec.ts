/**
 * Angular imports
 */
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

/**
 * Internal imports
 */
import { environment } from '../../../environments/environment';
import { AuthenticationService } from './authentication.service';

describe('AuthenticationService', () => {
  let authService: AuthenticationService;
  let httpController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AuthenticationService]
    });
    authService = TestBed.inject(AuthenticationService);
    httpController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpController.verify();
  });

  it('should use POST method to login the user', async () => {
    authService
      .login({ email: 'test@test.fr', password: 'password' })
      .subscribe();
    const req = httpController.expectOne(`${environment.apiUrl}/login`);
    expect(req.request.method).toEqual('POST');
  });

  it('should use GET method to logout the user', async () => {
    authService.logout().subscribe();
    const req = httpController.expectOne(`${environment.apiUrl}/logout`);
    expect(req.request.method).toEqual('GET');
  });
});
