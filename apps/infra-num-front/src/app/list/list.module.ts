/**
 * Angular imports
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

/**
 * 3rd-party imports
 */
import { DsfrTabModule } from '@betagouv/ngx-dsfr/tab';
import { DSFR_SEARCH_BAR_SERVICE_TOKEN, DsfrSearchBarModule } from '@betagouv/ngx-dsfr/search-bar';

/**
 * Internal imports
 */
import { ListViewComponent } from './components/list-view/list-view.component';
import { ListRoutingModule } from './list-routing.module';
import { DynamicBoxModule } from '../dynamic-box/dynamic-box.module';
import { LocalitySearchService } from '../api-gouv/services/locality-search.service';

@NgModule({
  imports: [
    CommonModule,
    DsfrTabModule,
    DsfrSearchBarModule,
    DynamicBoxModule,
    ListRoutingModule
  ],
  declarations: [ListViewComponent],
  exports: [ListViewComponent],
  providers: [
    {
      provide: DSFR_SEARCH_BAR_SERVICE_TOKEN,
      useClass: LocalitySearchService
    }
  ]
})
export class ListModule {}
