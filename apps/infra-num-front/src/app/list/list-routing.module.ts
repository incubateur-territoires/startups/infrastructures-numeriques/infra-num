/**
 * Angular imports
 */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

/**
 * Internal imports
 */
import { ListViewComponent } from './components/list-view/list-view.component';
import { ReportsListComponent } from '../mobile/report/components/reports-list/reports-list.component';
import { isUserAuthenticated } from '../user/guards/auth.guard';

/**
 * TypeScript entities and constants
 */
const routes: Routes = [
  {
    path: '',
    component: ListViewComponent,
    canActivateChild: [isUserAuthenticated],
    children: [
      {
        path: 'signalements',
        component: ReportsListComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListRoutingModule {}
