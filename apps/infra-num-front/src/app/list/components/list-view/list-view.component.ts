/**
 * Angular imports
 */
import { Component } from '@angular/core';

/**
 * 3rd-party imports
 */
import { ElementSize } from '@betagouv/ngx-dsfr';
import { RoutedTabDefinition } from '@betagouv/ngx-dsfr/tab';
import { ItemResult } from '@betagouv/ngx-dsfr/search-bar';
import { ReportsService } from '../../../mobile/report/services/reports.service';

@Component({
  selector: 'infra-num-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss']
})
export class ListViewComponent {
  routedTabs: RoutedTabDefinition[] = [
    {
      id: 'reports',
      label: 'Signalements',
      icon: 'fr-icon-warning-line',
      route: 'signalements'
    }
  ];
  elementSize: typeof ElementSize = ElementSize;

  constructor(private reportsService: ReportsService) {}

  onItemSelected(value: ItemResult) {
    this.reportsService.onSearchLocality(value);
  }
}
