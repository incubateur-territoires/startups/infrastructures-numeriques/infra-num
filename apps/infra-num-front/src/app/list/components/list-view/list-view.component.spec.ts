/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

/**
 * 3rd-party imports
 */
import { DsfrTabModule } from '@betagouv/ngx-dsfr/tab';
import { DsfrSearchBarModule } from '@betagouv/ngx-dsfr/search-bar';

/**
 * Internal imports
 */
import { ListViewComponent } from './list-view.component';
import { DynamicBoxModule } from '../../../dynamic-box/dynamic-box.module';

jest.mock('maplibre-gl', () => ({
  Map: jest
    .fn()
    .mockReturnValue({
      addControl: jest.fn(),
      on: jest.fn(),
      remove: jest.fn()
    }),
  NavigationControl: jest.fn(),
  ScaleControl: jest.fn(),
  GeoJSONSource: jest.fn(),
  MapGeoJSONFeature: jest.fn()
}));

describe('ListViewComponent', () => {
  let component: ListViewComponent;
  let fixture: ComponentFixture<ListViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DsfrTabModule, DsfrSearchBarModule, DynamicBoxModule, HttpClientTestingModule, RouterTestingModule],
      declarations: [ListViewComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(ListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
