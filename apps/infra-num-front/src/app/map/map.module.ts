/**
 * Angular imports
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

/**
 * 3rd-party imports
 */
import { DsfrAlertModule } from '@betagouv/ngx-dsfr/alert';
import {
  DSFR_SEARCH_BAR_SERVICE_TOKEN,
  DsfrSearchBarModule
} from '@betagouv/ngx-dsfr/search-bar';
import { DsfrBadgeModule } from '@betagouv/ngx-dsfr/badge';
import { DsfrInputModule } from '@betagouv/ngx-dsfr/input';
import { DsfrButtonModule } from '@betagouv/ngx-dsfr/button';

/**
 * Internal imports
 */
import { MapComponent } from './components/map/map.component';
import { MapViewComponent } from './components/map-view/map-view.component';
import { DynamicBoxModule } from '../dynamic-box/dynamic-box.module';
import { MapRoutingModule } from './map-routing.module';
import { GeographicSearchService } from '../api-gouv/services/geographic-search.service';
import { MapFiltersComponent } from './components/map-filters/map-filters.component';
import { MapFilterComponent } from './components/map-filter/map-filter.component';
import { MapFilterArrowComponent } from './components/map-filter-arrow/map-filter-arrow.component';
import { CoordinatesToolComponent } from './components/coordinates-tool/coordinates-tool.component';

@NgModule({
  imports: [
    CommonModule,
    DsfrSearchBarModule,
    DsfrBadgeModule,
    DsfrInputModule,
    DsfrAlertModule,
    DsfrButtonModule,
    DynamicBoxModule,
    ReactiveFormsModule,
    MapRoutingModule
  ],
  declarations: [MapViewComponent, MapComponent, MapFiltersComponent, MapFilterComponent, MapFilterArrowComponent, CoordinatesToolComponent],
  exports: [MapViewComponent],
  providers: [
    {
      provide: DSFR_SEARCH_BAR_SERVICE_TOKEN,
      useClass: GeographicSearchService
    }
  ]
})
export class MapModule {}
