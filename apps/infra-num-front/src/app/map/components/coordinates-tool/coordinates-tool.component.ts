/**
 * Angular imports
 */
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output
} from '@angular/core';
import { FormArray, NonNullableFormBuilder, Validators } from '@angular/forms';
import { Clipboard } from '@angular/cdk/clipboard';

/**
 * 3rd-party imports
 */
import {
  GPSToLambert93,
  lambert93ToGPS,
  PossibleCoordinates
} from '@infra-num/common';
import { Marker } from 'maplibre-gl';
import { ButtonType } from '@betagouv/ngx-dsfr/button';
import { ElementSize, ThemeColor } from '@betagouv/ngx-dsfr';

/**
 * TypeScript entities and constants
 */

/*
 * Coordinates of a point, regardless of the coordinates type
 * that might be used ( whether GPS or Lambert93 ).
 *
 * c1 = X = Longitude
 * c2 = Y = Latitude
 */
interface AgnosticCoordinates {
  c1: string;
  c2: string;
}

@Component({
  selector: 'infra-num-coordinates-tool',
  templateUrl: './coordinates-tool.component.html',
  styleUrls: ['./coordinates-tool.component.scss']
})
export class CoordinatesToolComponent implements OnChanges {
  @Input() coordinates: Marker;
  @Input() disabled: boolean = false;

  @Output() toolToggled = new EventEmitter<boolean>();
  @Output() coordinatesValuesChanged = new EventEmitter<[number, number][]>();

  buttonTypes: typeof ButtonType = ButtonType;
  themeColor: typeof ThemeColor = ThemeColor;
  elementSizes: typeof ElementSize = ElementSize;
  isToolDisplayed: boolean = false;
  possibleCoordinates: typeof PossibleCoordinates = PossibleCoordinates;
  selectedCoordinatesType: PossibleCoordinates = PossibleCoordinates.Lambert93;
  displayOtherCoordinatesType: boolean = false;
  isTooltipDisplayed: boolean = false;
  tooltipStyle: string;

  coordinatesForm = this.formBuilder.group({
    coordinates: this.formBuilder.array([
      this.formBuilder.group({
        c1: ['', Validators.required],
        c2: ['', Validators.required]
      })
    ])
  });

  get coordinatesFormArray() {
    return this.coordinatesForm.controls['coordinates'] as FormArray;
  }

  constructor(
    private formBuilder: NonNullableFormBuilder,
    private clipboard: Clipboard
  ) {}

  ngOnChanges(): void {
    this.setFormValue();
  }

  private setFormValue(): void {
    if (this.coordinates) {
      /*
       * Because of maplibre.js, MapComponent will only provide us
       * with GPS coordinates
       */
      const newCoordinates: AgnosticCoordinates = {
        c1: this.coordinates._lngLat.lng.toFixed(5).toString(),
        c2: this.coordinates._lngLat.lat.toFixed(5).toString()
      };

      this.coordinatesFormArray.setValue([
        ...this.coordinatesFormArray.value.slice(0, -1),
        this.selectedCoordinatesType === PossibleCoordinates.Lambert93
          ? this.convertCoordinatesValue(
            newCoordinates,
            PossibleCoordinates.Lambert93
          )
          : newCoordinates
      ]);
    }
  }

  toggleCoordinatesTool(value: boolean): void {
    this.isToolDisplayed = value;
    this.toolToggled.emit(this.isToolDisplayed);
  }

  onSelectCoordinatesType(): void {
    this.selectedCoordinatesType =
      this.selectedCoordinatesType === this.possibleCoordinates.GPS
        ? this.possibleCoordinates.Lambert93
        : this.possibleCoordinates.GPS;
    this.displayOtherCoordinatesType = false;

    this.coordinatesFormArray.setValue(
      this.coordinatesFormArray.value.map(
        (coordinates: AgnosticCoordinates) => {
          return coordinates.c1 !== '' && coordinates.c2 !== ''
            ? this.convertCoordinatesValue(
              coordinates,
              this.selectedCoordinatesType
            )
            : coordinates;
        }
      )
    );
  }

  private convertCoordinatesValue(
    coordinates: AgnosticCoordinates,
    requestedCoordinatesType: PossibleCoordinates
  ): AgnosticCoordinates {
    if (requestedCoordinatesType === this.possibleCoordinates.GPS) {
      const GPSCoordinates = lambert93ToGPS(+coordinates.c1, +coordinates.c2);

      return {
        c1: GPSCoordinates.lng.toString(),
        c2: GPSCoordinates.lat.toString()
      };
    } else {
      const lambertCoordinates = GPSToLambert93(
        +coordinates.c1,
        +coordinates.c2
      );

      return {
        c1: lambertCoordinates.lambertE.toString(),
        c2: lambertCoordinates.lambertN.toString()
      };
    }
  }

  addPoint(): void {
    if (this.coordinatesForm.valid) {
      const pointCoordinatesForm = this.formBuilder.group({
        c1: ['', Validators.required],
        c2: ['', Validators.required]
      });
      this.coordinatesFormArray.push(pointCoordinatesForm);

      this.emitNewCoordinates();
    }
  }

  deletePoint(index: number): void {
    this.coordinatesFormArray.removeAt(index);

    this.emitNewCoordinates();
  }

  /**
   * Emit value with empty last element to allow
   * MapComponent to add a marker, when relevant.
   *
   * Also, because of maplibre.js, we must send GPS
   * coordinates to MapComponent
   */
  private emitNewCoordinates(): void {
    const coordinatesToEmit: [number, number][] = [];
    this.coordinatesFormArray.value.forEach(
      (coordinates: AgnosticCoordinates) => {
        if (coordinates.c1 === '' && coordinates.c2 === '') {
          // We've just added a blank point in the form
          coordinatesToEmit.push([null, null]);
        } else {
          coordinates =
            this.selectedCoordinatesType === PossibleCoordinates.Lambert93
              ? this.convertCoordinatesValue(
                coordinates,
                PossibleCoordinates.GPS
              )
              : coordinates;
          coordinatesToEmit.push([+coordinates.c1, +coordinates.c2]);
        }
      }
    );

    this.coordinatesValuesChanged.emit(coordinatesToEmit);
  }

  copyCoordinates(event: Event, type: 'c1' | 'c2'): void {
    if (this.coordinatesForm.valid) {
      let coordinatesToCopy: string = '';
      this.coordinatesFormArray.value.forEach(
        (coordinates: AgnosticCoordinates, index: number) => {
          const isLast =
            index === this.coordinatesFormArray.controls.length - 1;
          coordinatesToCopy += `${coordinates[type]}`;
          if (!isLast) {
            coordinatesToCopy += ' \n';
          }
        }
      );
      this.clipboard.copy(coordinatesToCopy);
      this.displayTooltip(event);
    }
  }

  private displayTooltip(event: Event): void {
    const labelClicked: HTMLElement = (event.target as HTMLElement)
      .parentElement.parentElement;
    const labelCoordinates = labelClicked.getBoundingClientRect();

    const tooltipTop = labelCoordinates.y - labelCoordinates.height - 8;
    let tooltipLeft = labelCoordinates.x;

    this.tooltipStyle = `top: ${tooltipTop}px; left: ${tooltipLeft}px`;
    this.isTooltipDisplayed = true;

    setTimeout(() => {
      this.isTooltipDisplayed = false;
    }, 2000);
  }
}
