/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';

/**
 * Internal imports
 */
import { CoordinatesToolComponent } from './coordinates-tool.component';

/**
 * 3rd-party imports
 */
import { DsfrButtonModule } from '@betagouv/ngx-dsfr/button';

jest.mock('maplibre-gl', () => ({
  Map: jest.fn().mockReturnValue({
    addControl: jest.fn(),
    on: jest.fn(),
    remove: jest.fn()
  }),
  NavigationControl: jest.fn(),
  ScaleControl: jest.fn(),
  GeoJSONSource: jest.fn(),
  MapGeoJSONFeature: jest.fn()
}));

describe('CoordinatesToolComponent', () => {
  let component: CoordinatesToolComponent;
  let fixture: ComponentFixture<CoordinatesToolComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DsfrButtonModule],
      declarations: [CoordinatesToolComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(CoordinatesToolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
