/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';

/**
 * Internal imports
 */
import { MapFiltersComponent } from './map-filters.component';
import { ReportsServiceMock } from '../../../mobile/report/services/reports.service.mock';
import { ReportsService } from '../../../mobile/report/services/reports.service';
import { MapFilterComponent } from '../map-filter/map-filter.component';
import { MapFilterArrowComponent } from '../map-filter-arrow/map-filter-arrow.component';

describe('MapFiltersComponent', () => {
  let component: MapFiltersComponent;
  let fixture: ComponentFixture<MapFiltersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [],
      declarations: [
        MapFiltersComponent,
        MapFilterComponent,
        MapFilterArrowComponent
      ],
      providers: [
        {
          provide: ReportsService,
          useClass: ReportsServiceMock
        }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(MapFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

