/**
 * Angular imports
 */
import { Component } from '@angular/core';

/**
 * Internal imports
 */
import {
  ReportsFiltersConfiguration,
  ReportsService
} from '../../../mobile/report/services/reports.service';
import { ReportsFiltersComponent } from '../../../mobile/report/components/reports-filters/reports-filters.component';

@Component({
  selector: 'infra-num-map-filters',
  templateUrl: './map-filters.component.html',
  styleUrls: ['./map-filters.component.scss']
})
export class MapFiltersComponent {
  isReportFilterSelected = this.reportsService.isAFilterSelected;
  // isLocationFilterSelected = signal(false);
  // isAntennaFilterSelected = signal(false);
  isReportFilterActivated = true;
  // isLocationFilterActivated = false;
  // isAntennaFilterActivated = false;

  reportFilterComponent = ReportsFiltersComponent;

  constructor(private readonly reportsService: ReportsService) {}

  toggleReportsFilter(isReportFilterActivated: boolean): void {
    this.reportsService.reportsFilters.update(
      (filtersConfig: ReportsFiltersConfiguration) => {
        return {
          ...filtersConfig,
          displayReports: isReportFilterActivated
        };
      }
    );
  }

  // toggleLocationFilter(isLocationFilterActivated: boolean): void {
  //   this.isLocationFilterActivated = isLocationFilterActivated;
  // }
  //
  // toggleAntennaFilter(isAntennaFilterActivated: boolean): void {
  //   this.isAntennaFilterActivated = isAntennaFilterActivated;
  // }
}
