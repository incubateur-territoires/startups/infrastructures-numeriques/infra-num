/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';

/**
 * Internal imports
 */
import { MapFilterArrowComponent } from './map-filter-arrow.component';

describe('MapFilterArrowComponent', () => {
  let component: MapFilterArrowComponent;
  let fixture: ComponentFixture<MapFilterArrowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MapFilterArrowComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(MapFilterArrowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
