/**
 * Angular imports
 */
import { Component, Input } from '@angular/core';

@Component({
  selector: 'infra-num-map-filter-arrow',
  templateUrl: './map-filter-arrow.component.html',
  styleUrls: ['./map-filter-arrow.component.scss']
})
export class MapFilterArrowComponent {
  @Input({ required: true }) isFilterActivated!: boolean;
  @Input({ required: true }) isFilterSelected!: boolean;
}
