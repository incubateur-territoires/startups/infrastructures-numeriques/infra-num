/**
 * Angular imports
 */
import {
  Component,
  effect,
  ElementRef,
  EventEmitter,
  Inject,
  Injector,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';

/**
 * 3rd-party imports
 */
import {
  Map,
  MapMouseEvent,
  Marker,
  NavigationControl,
  ScaleControl
} from 'maplibre-gl';

/**
 * Internal imports
 */
import { MAP_SOURCES, MapSource } from '../../map-source';
import { MapLocation, MapService } from '../../map.service';

@Component({
  selector: 'infra-num-map',
  template: ` <div
    class="map-container"
    [ngClass]="{
      edit: editMode,
      'edit--by-coordinates': coordinatesToolOpen
    }"
    #mapContainer></div>`,
  styleUrls: ['./map.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MapComponent implements OnChanges, OnInit, OnDestroy {
  @Input({ required: true }) initialPositioning!: MapLocation;
  @Input({ required: true }) editMode!: boolean;
  @Input({ required: true }) coordinatesToolOpen!: boolean;
  @Input({ required: true }) markersCoordinates!: [number, number][];

  @Output() markerAddedOrMoved = new EventEmitter<Marker>();

  @ViewChild('mapContainer', { static: true })
  private mapContainer: ElementRef<HTMLElement>;

  private map: Map;
  private currentAddedMarker: Marker;
  private markersAddedByCoordinatesTool: Marker[] = [];

  constructor(
    private readonly domRenderer: Renderer2,
    @Inject(MAP_SOURCES) private readonly mapSources: MapSource[],
    private readonly mapService: MapService,
    private readonly injector: Injector
  ) {}

  ngOnChanges(): void {
    /*
     * If we're leaving the editMode or closing
     * the Coordinates tool
     */
    if (
      (!this.editMode && this.currentAddedMarker) ||
      !this.coordinatesToolOpen
    ) {
      this.currentAddedMarker?.remove();
      this.markersAddedByCoordinatesTool.forEach(
        (marker: Marker) => marker?.remove()
      );
    }

    for (const mapSource of this.mapSources) {
      mapSource.editMode = this.editMode;
    }

    /*
     * If we're using the Coordinates Tool in the direction :
     * Coordinates Tool -> Map
     * ( i.e. we're manually inputting data in its form )
     * OR
     * we're simply re-opening the Coordinates Tool
     */
    if (this.markersCoordinates.length && this.coordinatesToolOpen) {
      this.renderMarkersForCoordinatesTool(this.markersCoordinates);
    }
  }

  ngOnInit(): void {
    this.initMap();
    this.observeNewLocation();
  }

  private initMap(): void {
    this.map = new Map({
      container: this.mapContainer.nativeElement,
      style: `assets/map-styles/base.json`,
      center: [
        this.initialPositioning?.longitude,
        this.initialPositioning?.latitude
      ],
      zoom: this.initialPositioning?.zoom
    });

    this.map.addControl(new ScaleControl({}), 'bottom-right');
    this.map.addControl(
      new NavigationControl({ showCompass: false }),
      'bottom-right'
    );

    this.map.on('load', () => {
      this.mapService.addPMTilesCapacity(this.map);

      for (const mapSource of this.mapSources) {
        mapSource.initMapForThisSource(this.map);
      }

      this.map.on('click', (event: MapMouseEvent) => {
        if (this.editMode) {
          if (this.coordinatesToolOpen) {
            /*
             * We're using the Coordinates Tool in the direction :
             * Map -> Coordinates Tool
             */
            this.renderMarkersForCoordinatesTool(event);
          } else {
            /*
             * We're choosing a location on the map when creating or
             * updating an entity
             */
            this.addNewMarkerForEntity(event);
          }
        }
      });
    });
  }

  private renderMarkersForCoordinatesTool(
    newCoordinates: MapMouseEvent | [number, number][]
  ): void {
    this.markersAddedByCoordinatesTool.forEach(
      (marker: Marker) => marker?.remove()
    );

    if (Array.isArray(newCoordinates)) {
      /*
       * This method has been called while using the Coordinates Tool
       * in this direction : Coordinates Tool -> Map
       */
      this.markersCoordinates = newCoordinates;
    } else {
      /*
       * This method has been called while using the Coordinates Tool
       * in this direction : Map -> Coordinates Tool
       */
      this.markersCoordinates[this.markersCoordinates.length - 1] = [
        newCoordinates.lngLat.lng,
        newCoordinates.lngLat.lat
      ];
    }

    this.markersCoordinates.forEach((coordinates, index) => {
      const singleIconTextSuffix =
        this.markersCoordinates.length > 1 ? '' : '-single';
      const element = this.domRenderer.createElement('div');
      const textToDisplay = this.markersCoordinates.length > 1 ? index + 1 : '';
      this.domRenderer.setStyle(
        element,
        'background',
        `url('assets/illustrations/marker-coordinates-tool${singleIconTextSuffix}.svg') no-repeat`
      );
      this.domRenderer.addClass(element, 'tool-coordinates-marker');
      this.domRenderer.setProperty(
        element,
        'innerHTML',
        `<span>${textToDisplay}</span>`
      );

      const addedMarker = new Marker({ element, offset: [-1, -16] })
        .setLngLat(coordinates)
        .addTo(this.map);
      this.markersAddedByCoordinatesTool.push(addedMarker);
    });

    if (!Array.isArray(newCoordinates)) {
      /*
       * We emit only the last value added by clicking on the map
       * so that the Coordinates Tool can be aware it needs to add
       * a new set of coordinates to its form
       */
      this.markerAddedOrMoved.emit(
        this.markersAddedByCoordinatesTool[
          this.markersAddedByCoordinatesTool.length - 1
        ]
      );
    }
  }

  private addNewMarkerForEntity(event: MapMouseEvent): void {
    if (this.currentAddedMarker) {
      this.currentAddedMarker.remove();
    }

    const element = this.domRenderer.createElement('div');
    this.domRenderer.setStyle(
      element,
      'background',
      "url('assets/illustrations/marker-edit-mode.svg') no-repeat"
    );
    this.domRenderer.setStyle(element, 'width', '50px');
    this.domRenderer.setStyle(element, 'height', '60px');

    this.currentAddedMarker = new Marker({
      element,
      offset: [-2, -8]
    })
      .setLngLat([event.lngLat.lng, event.lngLat.lat])
      .addTo(this.map);

    this.markerAddedOrMoved.emit(this.currentAddedMarker);
  }

  private observeNewLocation(): void {
    effect(
      () => {
        const newLocation: MapLocation | null = this.mapService.location();
        if (newLocation) {
          this.map.flyTo({
            center: [newLocation.longitude, newLocation.latitude],
            zoom: newLocation.zoom
          });
        }
      },
      { injector: this.injector }
    );
  }

  ngOnDestroy(): void {
    for (const mapSource of this.mapSources) {
      mapSource.cleanUp();
    }
    this.map.remove();
  }
}
