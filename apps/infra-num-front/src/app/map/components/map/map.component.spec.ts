/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';

/**
 * Internal imports
 */
import { MapComponent } from './map.component';
import { MapService } from '../../map.service';
import { MAP_SOURCES } from '../../map-source';
import { MapServiceMock } from '../../mocks/map.service.mock';
import { MapSourceMock } from '../../mocks/map-source.mock';

jest.mock('maplibre-gl', () => ({
  Map: jest
    .fn()
    .mockReturnValue({
      addControl: jest.fn(),
      on: jest.fn(),
      remove: jest.fn()
    }),
  NavigationControl: jest.fn(),
  ScaleControl: jest.fn(),
  GeoJSONSource: jest.fn(),
  MapGeoJSONFeature: jest.fn()
}));

describe('MapComponent', () => {
  let component: MapComponent;
  let fixture: ComponentFixture<MapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MapComponent],
      providers: [
        {
          provide: MapService,
          useClass: MapServiceMock
        },
        {
          provide: MAP_SOURCES,
          multi: true,
          useClass: MapSourceMock
        }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(MapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
