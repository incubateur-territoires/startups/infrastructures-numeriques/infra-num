/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';

/**
 * Internal imports
 */
import { MapFilterComponent } from './map-filter.component';
import { MapFilterArrowComponent } from '../map-filter-arrow/map-filter-arrow.component';

describe('MapFilterComponent', () => {
  let component: MapFilterComponent;
  let fixture: ComponentFixture<MapFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MapFilterComponent, MapFilterArrowComponent]
    }).compileComponents();


    fixture = TestBed.createComponent(MapFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
