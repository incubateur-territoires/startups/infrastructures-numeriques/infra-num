/**
 * Angular imports
 */
import {
  Component,
  ElementRef, EventEmitter,
  Injector, Input,
  OnInit, Output,
  ViewChild
} from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal, ComponentType } from '@angular/cdk/portal';

/**
 * Internal imports
 */
import { COMPONENT_PORTAL_DATA } from '../../../common';
import { DynamicBoxMobileDisplay, DynamicBoxService } from '../../../dynamic-box/dynamic-box.service';

@Component({
  selector: 'infra-num-map-filter',
  templateUrl: './map-filter.component.html',
  styleUrls: ['./map-filter.component.scss']
})
export class MapFilterComponent implements OnInit {
  @Input({ required: true }) isFilterActivated!: boolean;
  @Input({ required: true }) isFilterSelected!: boolean;
  @Input({ required: true }) filterLabel!: string;
  @Input({ required: true }) filterComponent!: ComponentType<any>;

  @Output() filterToggled = new EventEmitter<boolean>();

  private filtersOverlay: OverlayRef = this.overlayService.create({
    maxWidth: 398
  });

  @ViewChild('filtersOverlayTrigger', {static: true})
  private filtersOverlayTrigger: ElementRef;

  private takeUntilDestroyed = takeUntilDestroyed();

  constructor(
    private readonly overlayService: Overlay,
    private readonly dynamicBoxService: DynamicBoxService
  ) {}

  ngOnInit(): void {
    // Creating the filters overlay for desktop
    this.createFiltersOverlay(
      this.filtersOverlay,
      this.filtersOverlayTrigger,
      140
    );
  }

  private createFiltersOverlay(
    overlayRef: OverlayRef,
    overlayTrigger: ElementRef,
    offsetToTheLeft: number
  ): void {
    overlayRef.updatePositionStrategy(
      this.overlayService
        .position()
        .flexibleConnectedTo(overlayTrigger)
        .withPositions([
          {
            originX: 'center',
            originY: 'bottom',
            overlayX: 'start',
            overlayY: 'top',
            offsetX: -offsetToTheLeft,
            offsetY: 4
          }
        ])
    );

    overlayRef
      .outsidePointerEvents()
      .pipe(this.takeUntilDestroyed)
      .subscribe({
        next: (event: MouseEvent) => {
          // If we're clicking outside and NOT on the trigger, close the overlay
          if (!overlayTrigger.nativeElement.contains(event.target)) {
            overlayRef.detach();
          }
        }
      });
  }

  toggleFilter(): void {
    this.isFilterActivated = !this.isFilterActivated;
    this.filterToggled.emit(this.isFilterActivated);
  }

  toggleFiltersOverlay(): void {
    if (this.filtersOverlay.hasAttached()) {
      this.filtersOverlay.detach();
    } else {
      const portalInjector = Injector.create({
        providers: [
          {provide: COMPONENT_PORTAL_DATA, useValue: {view: 'Map'}}
        ]
      });
      this.filtersOverlay.attach(
        new ComponentPortal(this.filterComponent, null, portalInjector)
      );
    }
  }

  openFiltersBox(): void {
    this.dynamicBoxService.showBoxWithContent(
      this.filterComponent,
      null,
      {
        display: true,
        displayOnMobile: DynamicBoxMobileDisplay.FullHeight,
        showCloseOnMobile: true
      }
    );
  }
}
