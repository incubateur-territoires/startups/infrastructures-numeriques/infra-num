/**
 * Angular imports
 */
import {
  Component,
  DestroyRef,
  inject,
  OnInit,
  Signal,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

/**
 * 3rd-party imports
 */
import { AlertType } from '@betagouv/ngx-dsfr/alert';
import { ElementSize } from '@betagouv/ngx-dsfr';
import { Marker } from 'maplibre-gl';
import { City, Jurisdiction, JurisdictionType, Role, SafeUser } from '@infra-num/common';
import { ItemResult } from '@betagouv/ngx-dsfr/search-bar';

/**
 * Internal imports
 */
import { AuthenticationService } from '../../../user/services/authentication.service';
import { CitySearchService } from '../../../api-gouv/services/city-search.service';
import {
  DynamicBoxOptions,
  DynamicBoxService
} from '../../../dynamic-box/dynamic-box.service';
import { MapLocation, MapService } from '../../map.service';
import { ReportsService } from '../../../mobile/report/services/reports.service';

/**
 * TypeScript entities and constants
 */
export const OUTSIDE_HEXAGON_ALERT: string =
  'Localisation invalide : veuillez choisir une position\n' +
  'qui est située dans l\'hexagone';
export const OUTSIDE_MAYOR_JURISDICTION_ALERT: string =
  'La position que vous avez choisie sur la carte n’est pas située sur la\n' +
  'commune de votre mairie. Le marqueur est-il bien placé ?';
export const OUTSIDE_EPCI_JURISDICTION_ALERT: string =
  'La position que vous avez choisie sur la carte n’est pas située sur l\'une\n' +
  'des communes de votre intercommunalité. Le marqueur est-il bien placé ?';
export const OUTSIDE_PROJECT_TEAM_JURISDICTION_ALERT: string =
  'La position que vous avez choisie sur la carte n’est pas située sur le\n' +
  'territoire dont vous vous occupez. Le marqueur est-il bien placé ?';

@Component({
  selector: 'infra-num-map-view',
  templateUrl: './map-view.component.html',
  styleUrls: ['./map-view.component.scss']
})
export class MapViewComponent implements OnInit {
  @ViewChild('alertSlot', {
    read: ViewContainerRef
  }) alertSlot: ViewContainerRef;

  @ViewChild('alertTemplate', {
    read: TemplateRef
  }) alertTemplate: TemplateRef<any>;

  alertType: AlertType;
  alertTypes: typeof AlertType = AlertType;
  elementSize: typeof ElementSize = ElementSize;

  alertDescription: string;
  mapInitialPositioning: MapLocation;
  boxOptions: Signal<DynamicBoxOptions> = this.dynamicBoxService.dynamicBoxOptions;

  isCoordinatesToolOpen: boolean = false;
  coordinatesToolMarker: Marker;
  markersCoordinates: [number, number][] = [[null, null]];

  private destroyRef = inject(DestroyRef);
  private user: SafeUser;

  constructor(
    private readonly authenticationService: AuthenticationService,
    private readonly citySearchService: CitySearchService,
    private readonly dynamicBoxService: DynamicBoxService,
    private readonly mapService: MapService,
    private readonly reportsService: ReportsService
  ) {}

  ngOnInit(): void {
    this.getUser();
  }

  private getUser(): void {
    this.authenticationService.authenticatedUser
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((authenticatedUser) => {
        this.user = authenticatedUser;
        if (this.user) {
          this.initMapPositioning();
        }
      });
  }

  private initMapPositioning(): void {
    const filters = this.reportsService.reportsFilters();
    const hexagonInitialPositioning = {
      latitude: 46.603354,
      longitude: 1.8883335,
      zoom: 5.3
    };

    if (filters.geographic.regions.length > 0) {
      this.mapInitialPositioning =
        filters.geographic.regions.length === 1
          ? {
            latitude: filters.geographic.regions[0].centre.coordinates[1],
            longitude: filters.geographic.regions[0].centre.coordinates[0],
            zoom: filters.geographic.regions[0].zoom
          }
          : hexagonInitialPositioning;
      return;
    }
    if (filters.geographic.departments.length > 0) {
      this.mapInitialPositioning =
        filters.geographic.departments.length === 1
          ? {
            latitude: filters.geographic.departments[0].centre.coordinates[1],
            longitude:
              filters.geographic.departments[0].centre.coordinates[0],
            zoom: filters.geographic.departments[0].zoom
          }
          : hexagonInitialPositioning;
      return;
    }
    if (filters.geographic.cities.length > 0) {
      this.mapInitialPositioning =
        filters.geographic.cities.length === 1
          ? {
            latitude: filters.geographic.cities[0].centre.coordinates[1],
            longitude: filters.geographic.cities[0].centre.coordinates[0],
            zoom: filters.geographic.cities[0].zoom
          }
          : hexagonInitialPositioning;
      return;
    }

    this.mapInitialPositioning =
      this.user?.jurisdictions?.length === 1
        ? {
          latitude: this.user.jurisdictions[0]?.referenceCoordinates[0],
          longitude: this.user.jurisdictions[0]?.referenceCoordinates[1],
          zoom: this.user.jurisdictions[0]?.initialZoom
        }
        : hexagonInitialPositioning;
  }

  onMarkerAddedOrMoved(marker: Marker) {
    if (this.isCoordinatesToolOpen) {
      this.coordinatesToolMarker = marker;
    } else {
      this.setCurrentReport(marker);
    }
  }

  private setCurrentReport(marker: Marker): void {
    this.citySearchService
      .getCityByGPSCoordinates(marker.getLngLat().lng, marker.getLngLat().lat)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (city: City) => {
          // Clear alert every marker changes
          this.clearAlert();

          /*
           * Making a Report outside metropolitan France is not allowed
           * for the Mobile New deal.
           * TODO: We'll have to update this rule for the FTTH use-case
           */
          if (city === null || parseInt(city.codeDepartement) > 95) {
            this.alertType = this.alertTypes.ERROR;
            return this.displayAlert(OUTSIDE_HEXAGON_ALERT);
          }

          if (this.hasUserClickedOutsideOfJurisdiction(city)) {
            this.alertType = this.alertTypes.WARNING;
            if (this.user.roles.includes(Role.Mayor)) {
              this.displayAlert(OUTSIDE_MAYOR_JURISDICTION_ALERT);
            } else if (this.user.roles.includes(Role.EPCI)) {
              this.displayAlert(OUTSIDE_EPCI_JURISDICTION_ALERT);
            } else if (this.user.roles.includes(Role.ProjectTeam)) {
              this.displayAlert(OUTSIDE_PROJECT_TEAM_JURISDICTION_ALERT);
            }
          }

          this.mapService.selectedCity.set({marker, city});
        }
      });
  }

  private displayAlert(alertDescription: string) {
    this.alertDescription = alertDescription;
    this.alertSlot.clear();
    this.alertSlot.createEmbeddedView(this.alertTemplate);
  }

  private clearAlert(): void {
    this.alertDescription = undefined;
    this.alertSlot.clear();
  }

  private hasUserClickedOutsideOfJurisdiction(city: City): boolean {
    let eligibleJurisdictions: Jurisdiction[] = [];

    if (this.user.roles.includes(Role.ANCT)) {
      return false;
    }

    if (this.user.roles.includes(Role.ProjectTeam)) {
      eligibleJurisdictions = this.user.jurisdictions.filter(
        (jurisdiction: Jurisdiction) => {
          return jurisdiction.type === JurisdictionType.Department;
        }
      );

      return !eligibleJurisdictions.some(
        ({ name }) => name === city.nomDepartement
      );
    }

    if (this.user.roles.includes(Role.EPCI)) {
      eligibleJurisdictions = this.user.jurisdictions.filter(
        (jurisdiction: Jurisdiction) => {
          return jurisdiction.type === JurisdictionType.EPCI;
        }
      );

      return !eligibleJurisdictions[0].epciCities.filter(
        (epciCity: Jurisdiction) => epciCity.name === city.nom
      ).length;
    }

    if (this.user.roles.includes(Role.Mayor)) {
      eligibleJurisdictions = this.user.jurisdictions.filter(
        (jurisdiction: Jurisdiction) => {
          return jurisdiction.type === JurisdictionType.City;
        }
      );

      return eligibleJurisdictions[0].name !== city.nom;
    }

    return false;
  }


  onItemSelected(value: ItemResult) {
    const coordinates: [number, number] = value.originalObject.centre
      ? value.originalObject.centre.coordinates
      : value.originalObject.coordinates;
    this.mapService.location.set({
      zoom: value.originalObject.zoom,
      longitude: coordinates[0],
      latitude: coordinates[1]
    });
    this.reportsService.onSearchLocality(value);
  }
}
