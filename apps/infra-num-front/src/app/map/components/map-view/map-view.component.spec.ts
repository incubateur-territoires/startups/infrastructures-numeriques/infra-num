/**
 * Angular imports
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

/**
 * 3rd-party imports
 */
import { DsfrButtonModule } from '@betagouv/ngx-dsfr/button';

/**
 * Internal imports
 */
import { MapViewComponent } from './map-view.component';
import { MapModule } from '../../map.module';
import { AuthenticationService } from '../../../user/services/authentication.service';
import { AuthenticationServiceMock } from '../../../user/mocks/authentication.service.mock';
import { MAP_SOURCES } from '../../map-source';
import { MapSourceMock } from '../../mocks/map-source.mock';

jest.mock('maplibre-gl', () => ({
  Map: jest.fn().mockReturnValue({
    addControl: jest.fn(),
    on: jest.fn(),
    remove: jest.fn()
  }),
  NavigationControl: jest.fn(),
  ScaleControl: jest.fn(),
  GeoJSONSource: jest.fn(),
  MapGeoJSONFeature: jest.fn()
}));

describe('MapViewComponent', () => {
  let component: MapViewComponent;
  let fixture: ComponentFixture<MapViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MapModule, DsfrButtonModule, HttpClientTestingModule],
      declarations: [MapViewComponent],
      providers: [
        { provide: AuthenticationService, useClass: AuthenticationServiceMock }
      ]
    })
      .overrideProvider(MAP_SOURCES, {
        useFactory: () => {
          return [new MapSourceMock()];
        },
        multi: true
      })
      .compileComponents();

    fixture = TestBed.createComponent(MapViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
