/**
 * Angular imports
 */
import {
  DestroyRef,
  EffectRef,
  inject,
  InjectionToken,
  Injector,
  Signal,
  signal,
  WritableSignal
} from '@angular/core';

/**
 * 3rd-party imports
 */
import { Map } from 'maplibre-gl';
import { GeoJSON } from 'geojson';

/**
 * TypeScript entities and constants
 */
export const MAP_SOURCES = new InjectionToken<MapSource[]>(
  'An array ( so use multi: true ) of Services extending the MapSource abstract class'
);

/**
 * This abstract class is to be extended by any Service which purpose will
 * be to add a new source of data to the maplibre-gl's Map instance,
 * displayed in the MapComponent.
 * Then it will also have to instruct the Map instance how to display
 * the data and how to react when the user will interact with markers
 * associated with it.
 * See https://maplibre.org/maplibre-style-spec/sources/ and
 * https://maplibre.org/maplibre-gl-js-docs/api/sources/
 */
export abstract class MapSource {
  editMode: boolean = false;

  protected map: Map;
  protected sourceData: Signal<GeoJSON> = signal(null);
  protected destroyRef = inject(DestroyRef);
  protected effectsReferences: EffectRef[] = [];
  protected selectedMarkerId: string;
  protected selectedMarkerImageLoaded: WritableSignal<boolean> = signal(false);

  protected constructor(protected readonly injector: Injector) {}

  /**
   * This method should be called within MapComponent and be given its Map
   * instance.
   *
   * From now on, in the implementations of the other methods in this class,
   * you'll be able to use this.map
   *
   * @param mapInstance
   */
  initMapForThisSource(mapInstance: Map): void {
    this.map = mapInstance;

    this.initMap();
  }

  /**
   * Implement this method to add a new source of data to the Map instance.
   * Initialize it at first with empty data and then plug it to a signal of
   * GeoJSON for the given source.
   */
  protected abstract initMap(): void;

  /**
   * Implement this method to instruct the Map how to display an individual item
   * of the source of data.
   *
   * You should call it inside initMap
   *
   * @protected
   */
  protected abstract initDisplayForIndividualItems(): void;

  /**
   * Implement this method to instruct the Map how to display a cluster of
   * several items of the source of data.
   *
   * You should call it inside initMap
   *
   * @protected
   */
  protected abstract initDisplayForClusters(): void;

  /**
   * Implement this method to instruct the Map how to react when the user
   * clicks on a cluster.
   *
   * You should call it inside initMap
   *
   * @protected
   */
  protected abstract initClusterInspection(): void;

  /**
   * Implement this method to instruct the Map how to change its appearance
   * on specific events happening on particular layers.
   *
   * You should call it inside initMap
   *
   * @protected
   */
  protected abstract initStyleForLayers(): void;

  /**
   * Implement this method to tell the source how to inject the first batch
   * of data into the sourceData signal.
   *
   * You should call it AT THE END of initMap
   *
   * @protected
   */
  protected abstract displayInitialData(): void;

  /**
   * This method will be called inside the ngOnDestroy of MapComponent
   * to make sure we don't have any memory leaking and the main resources
   * taken by this Service will be liberated
   */
  cleanUp(): void {
    this.map = null;
    this.selectedMarkerImageLoaded.set(false);
    this.effectsReferences.forEach((effectRef: EffectRef) =>
      effectRef.destroy()
    );
  }
}
