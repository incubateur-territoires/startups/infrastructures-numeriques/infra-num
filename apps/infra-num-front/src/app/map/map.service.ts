/**
 * Angular imports
 */
import { Injectable, signal, WritableSignal } from '@angular/core';

/**
 * 3rd-party imports
 */
import { default as MapLibreGL, Map, Marker } from 'maplibre-gl';
import { StyleSpecification } from '@maplibre/maplibre-gl-style-spec';
import { Protocol } from 'pmtiles';
import { City } from '@infra-num/common';

/**
 * Internal imports
 */
import { default as railroadsStyle } from '../../assets/map-styles/railroads.json';

/**
 * TypeScript entities and constants
 */
export interface MarkerWithCity {
  marker: Marker;
  city: City;
}

export interface MapLocation {
  longitude: number;
  latitude: number;
  zoom: number;
}

@Injectable({
  providedIn: 'root'
})
export class MapService {
  selectedCity: WritableSignal<MarkerWithCity> = signal(null);
  location: WritableSignal<MapLocation> = signal(null);

  private mapInstance: Map;
  private pmTilesProtocol: Protocol;

  /**
   * This public method is to be called within the MapComponent to enable
   * the Map instance from MapLibre-GL to handle PMTiles.
   * 🚨Must be called ONCE the Map instance is loaded ( on the 'load'
   * event )
   *
   * @param mapInstance : Map, the instance of the Map we want to add layers to
   */
  addPMTilesCapacity(mapInstance: Map): void {
    this.mapInstance = mapInstance;

    this.pmTilesProtocol = new Protocol();
    MapLibreGL.addProtocol('pmtiles', this.pmTilesProtocol.tile);

    // Add HERE all the PMTiles that the Map should be able to display
    //this.addSourcesAndLayersfromStyle(railroadsStyle as StyleSpecification);
  }

  private addSourcesAndLayersfromStyle(style: StyleSpecification): void {
    for (const [sourceKey, sourceSpec] of Object.entries(style.sources)) {
      this.mapInstance.addSource(sourceKey, sourceSpec);
    }
    for (const styleLayer of style.layers) {
      this.mapInstance.addLayer({
        ...styleLayer,
        layout: {
          ...styleLayer.layout,
          visibility: 'none'
        }
      });
    }
  }

  toggleVisibilityFor(layerId: string): void {
    const currentVisibility: 'visible' | 'none' =
      this.mapInstance.getLayoutProperty(layerId, 'visibility');
    this.mapInstance.setLayoutProperty(
      layerId,
      'visibility',
      currentVisibility === 'visible' ? 'none' : 'visible'
    );
  }
}
