export class MapSourceMock {
  initMapForThisSource: jest.Mock = jest.fn();
  displayNewData: jest.Mock = jest.fn();
  cleanUp: jest.Mock = jest.fn();
}
