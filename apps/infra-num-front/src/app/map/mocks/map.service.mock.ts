import { signal, WritableSignal } from '@angular/core';

export class MapServiceMock {
  selectedCity: jest.Mock = jest.fn();
  location: WritableSignal<null> = signal(null);
  setNewSelectedCity: jest.Mock = jest.fn();
  flyToNewLocation: jest.Mock = jest.fn();
}
