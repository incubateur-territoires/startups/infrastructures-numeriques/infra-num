/**
 * Angular imports
 */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

/**
 * Internal imports
 */
import { MapViewComponent } from './components/map-view/map-view.component';
import { isUserAuthenticated } from '../user/guards/auth.guard';

/**
 * TypeScript entities and constants
 */
const routes: Routes = [
  {
    path: '',
    canActivate: [isUserAuthenticated],
    component: MapViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MapRoutingModule {}
