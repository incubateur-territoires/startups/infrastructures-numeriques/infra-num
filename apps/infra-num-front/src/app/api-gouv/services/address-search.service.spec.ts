/**
 * Angular imports
 */
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

/**
 * Internal imports
 */
import { environment } from '../../../environments/environment';
import { AddressSearchService } from './address-search.service';

describe('AddressSearchService', () => {
  let service: AddressSearchService;
  let httpController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AddressSearchService]
    });
    service = TestBed.inject(AddressSearchService);
    httpController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpController.verify();
  });

  it('should use GET to retrieve search addresses list', async () => {
    service.search('8 bd du pont').subscribe();
    const req = httpController.expectOne(`${environment.apiUrl}/addresses?search=8+bd+du+pont`);
    expect(req.request.method).toEqual('GET');
  });
});
