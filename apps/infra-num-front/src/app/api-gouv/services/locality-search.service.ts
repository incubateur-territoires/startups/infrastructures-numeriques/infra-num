/**
 * Angular imports
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

/**
 * 3rd party imports
 */
import {
  DsfrSearchBarService,
  ItemResult
} from '@betagouv/ngx-dsfr/search-bar';
import { GeographicSearchResult } from '@infra-num/common';
import { map, Observable } from 'rxjs';

/**
 * Internal imports
 */
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LocalitySearchService implements DsfrSearchBarService {

  constructor(private http: HttpClient) {}

  search(query: string): Observable<ItemResult[]> {
    const formattedQuery: string = query.trim().replace(/[ ]/g, '+');
    return this.http
      .get<GeographicSearchResult>(`${environment.apiUrl}/localities?search=${formattedQuery}`)
      .pipe<ItemResult[]>(
        map((localities: GeographicSearchResult) =>
        localities.map((locality, index) => {
          let label: string;
          switch(locality.type) {
            case 'city':
              label = `${locality.nom} (${locality.codePostal})`;
              break;
            case 'department':
              label = `${locality.nom} (${locality.code})`;
              break;
            case 'region':
              label = `${locality.nom}`;
              break;
          };

          return {
            id: index.toString(),
            label: label,
            originalObject: locality
          }
        })
        )
      );
  }
}
