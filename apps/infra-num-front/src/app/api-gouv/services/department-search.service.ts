/**
 * Angular imports
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

/**
 * 3rd party imports
 */
import { Department } from '@infra-num/common';
import { Observable } from 'rxjs';

/**
 * Internal imports
 */
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DepartmentSearchService {
  constructor(private http: HttpClient) {}

  search(query: string): Observable<Department[]> {
    return this.http.get<Department[]>(`${environment.apiUrl}/departments?search=${query}`);
  }
}
