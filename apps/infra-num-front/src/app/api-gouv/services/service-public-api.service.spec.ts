/**
 * Angular imports
 */
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

/**
 * Internal imports
 */
import { environment } from '../../../environments/environment';
import { ServicePublicApiService } from './service-public-api.service';

describe('ServicePublicApiService', () => {
  let service: ServicePublicApiService;
  let httpController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ServicePublicApiService]
    });
    service = TestBed.inject(ServicePublicApiService);
    httpController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpController.verify();
  });

  it('should use GET to retrieve city hall information', async () => {
    service.getCityHallInformationByInseeCode('10002').subscribe();
    const req = httpController.expectOne(`${environment.apiUrl}/city-hall/10002`);
    expect(req.request.method).toEqual('GET');
  });
});
