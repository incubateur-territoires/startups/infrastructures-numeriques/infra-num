/**
 * Angular imports
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

/**
 * 3rd party imports
 */
import {
  DsfrSearchBarService,
  ItemResult
} from '@betagouv/ngx-dsfr/search-bar';
import { City } from '@infra-num/common';
import { map, Observable } from 'rxjs';

/**
 * Internal imports
 */
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CitySearchService implements DsfrSearchBarService {
  constructor(private http: HttpClient) {}

  search(query: string): Observable<ItemResult[]> {
    return this.http
      .get<City[]>(`${environment.apiUrl}/cities?search=${query}`)
      .pipe<ItemResult[]>(
        map((cities: City[]) =>
          cities.map((city) => ({
            id: `${city.nom}-${city.codePostal}`,
            label: `${city.nom} (${city.codePostal})`,
            originalObject: city
          }))
        )
      );
  }

  getCityByGPSCoordinates(
    longitude: number,
    latitude: number
  ): Observable<City> {
    return this.http.get<City>(
      `${environment.apiUrl}/cities?lon=${longitude}&lat=${latitude}`
    );
  }
}
