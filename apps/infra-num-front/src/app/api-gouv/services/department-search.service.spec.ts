import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';

import { TestBed } from '@angular/core/testing';

import { DepartmentSearchService } from './department-search.service';

describe('DepartmentSearchService', () => {
  let service: DepartmentSearchService;
  let httpController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [DepartmentSearchService]
    });
    service = TestBed.inject(DepartmentSearchService);
    httpController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpController.verify();
  });

  it('should return an unmodified response right from the API', async () => {
    service.search('12345').subscribe((result) => {
      expect(result.length).toBe(0);
    });
    let response = [];

    const req = httpController.expectOne(
      'http://localhost:3333/api/departments?search=12345'
    );
    expect(req.request.method).toEqual('GET');
    req.flush(response);
  });
});
