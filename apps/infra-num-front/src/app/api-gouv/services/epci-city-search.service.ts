/**
 * Angular imports
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

/**
 * 3rd party imports
 */
import { City, Epci } from '@infra-num/common';
import { ItemResult } from '@betagouv/ngx-dsfr/search-bar';
import { combineLatest, map, Observable } from 'rxjs';

/**
 * Internal imports
 */
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EpciCitySearchService {
  constructor(private http: HttpClient) {}

  search(query: string): Observable<ItemResult[]> {

    return combineLatest([
        this.http.get<City[]>(`${environment.apiUrl}/cities?search=${query}`),
        this.http.get<Epci[]>(`${environment.apiUrl}/epcis?search=${query}`)
      ]
    ).pipe<ItemResult[]>(
      map((results: [City[], Epci[]]) =>
        [...results[0], ...results[1]].map((locality: City | Epci, index) => {
          let label: string;
          switch(locality.type) {
            case 'epci':
              label = locality.nom;
              break;
            case 'city':
              label = `${locality.nom} (${locality.codePostal})`;
              break;
          }

          return {
            id: index.toString(),
            label: label,
            originalObject: locality
          };
        })
      )
    );
  }
}
