/**
 * Angular imports
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

/**
 * 3rd party imports
 */
import {
  DsfrSearchBarService,
  ItemResult
} from '@betagouv/ngx-dsfr/search-bar';
import { Address, City, Department, GeographicSearchResult, Region } from '@infra-num/common';
import { combineLatest, map, Observable } from 'rxjs';

/**
 * Internal imports
 */
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GeographicSearchService implements DsfrSearchBarService {

  constructor(private http: HttpClient) {}

  search(query: string): Observable<ItemResult[]> {
    const formattedQuery: string = query.trim().replace(/[ ]/g, '+');

    return combineLatest([
      this.http.get<GeographicSearchResult>(`${environment.apiUrl}/localities?search=${formattedQuery}`),
      this.http.get<Address[]>(`${environment.apiUrl}/addresses?search=${formattedQuery}`)
    ]
    ).pipe<ItemResult[]>(
      map((results: [GeographicSearchResult, Address[]]) =>
      [...results[0], ...results[1]].map((locality: Region | Department | City | Address, index) => {
          let label: string;
          switch(locality.type) {
            case 'address':
              label = locality.label;
              break;
            case 'city':
              label = `${locality.nom} (${locality.codePostal})`;
              break;
            case 'department':
              label = `${locality.nom} (${locality.code})`;
              break;
            case 'region':
              label = locality.nom;
              break;
          };

          return {
            id: index.toString(),
            label: label,
            originalObject: locality
          };
        })
      )
    );
  }
}
