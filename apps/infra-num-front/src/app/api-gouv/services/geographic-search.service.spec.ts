import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';

import { TestBed } from '@angular/core/testing';

import { GeographicSearchService } from './geographic-search.service';
import { ItemResult } from '@betagouv/ngx-dsfr/search-bar';

describe('GeographicSearchService', () => {
  let service: GeographicSearchService;
  let httpController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [GeographicSearchService]
    });
    service = TestBed.inject(GeographicSearchService);
    httpController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpController.verify();
  });

  it('should return a list of ItemResult objects when being asked to search for a locality', async () => {
    const querySearch: string = '10200';
    const response = [{
      codePostal: "10200",
      code: "10002",
      nom: "Ailleville",
      centre: {
        type: "Point",
        coordinates: [4.6911, 48.2562]
      },
      codeDepartement: "10",
      nomDepartement: "Aube",
      codeRegion: "44",
      nomRegion: "Grand Est",
      type: "city",
      zoom: 13
    }];

    const isItemResult = (data: ItemResult[]): data is ItemResult[] => {
      return (<ItemResult>data[0]).label !== undefined;
    };

    service.search(querySearch).subscribe((result) => {
      expect(isItemResult(result)).toBeTruthy();
    });

    const req = httpController.expectOne(
      'http://localhost:3333/api/addresses?search=10200'
    );

    const req2 = httpController.expectOne(
      'http://localhost:3333/api/localities?search=10200'
    );

    expect(req.request.method).toEqual('GET');
    req.flush(response);
  });

});
