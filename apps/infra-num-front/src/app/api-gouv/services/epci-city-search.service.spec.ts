/**
 * Angular imports
 */
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

/**
 * Internal imports
 */
import { EpciCitySearchService } from './epci-city-search.service';
import { environment } from '../../../environments/environment';

describe('EpciCitySearchService', () => {
  let service: EpciCitySearchService;
  let httpController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [EpciCitySearchService]
    });
    service = TestBed.inject(EpciCitySearchService);
    httpController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpController.verify();
  });

  it('should return an array of ItemResult objects', async () => {

    service.search('Aube').subscribe({
      next: (result) => {
        expect(Array.isArray(result)).toBe(true);
        result.forEach(item => {
          expect(item).toHaveProperty('id');
          expect(item).toHaveProperty('label');
          expect(item).toHaveProperty('originalObject');
        });
      }
    });

    // CITIES Endpoint
    const reqCities = httpController.expectOne(`${environment.apiUrl}/cities?search=Aube`);
    expect(reqCities.request.method).toEqual('GET');
    reqCities.flush([
      {
        codePostal: '59249',
        code: '59025',
        nom: 'Aubers',
        centre: {
          type: 'Point',
          coordinates: [
            2.8187,
            50.5995
          ]
        },
        codeDepartement: '59',
        nomDepartement: 'Nord',
        codeRegion: '32',
        nomRegion: 'Hauts-de-France',
        type: 'city',
        zoom: 13
      },
      {
        codePostal: '52210',
        code: '52022',
        nom: 'Aubepierre-sur-Aube',
        centre: {
          type: 'Point',
          coordinates: [
            4.9552,
            47.9145
          ]
        },
        codeDepartement: '52',
        nomDepartement: 'Haute-Marne',
        codeRegion: '44',
        nomRegion: 'Grand Est',
        type: 'city',
        zoom: 13
      }
    ]);

    // EPCIS Endpoint
    const reqEpcis = httpController.expectOne(`${environment.apiUrl}/epcis?search=Aube`);
    expect(reqEpcis.request.method).toEqual('GET');
    reqEpcis.flush([
      {
        code: '200070126',
        nom: 'CC Seine et Aube',
        centre: {
          type: 'Point',
          coordinates: [
            3.9466,
            48.5216
          ]
        },
        type: 'epci',
        zoom: 10
      },
      {
        code: '241000405',
        nom: 'CC de la Région de Bar sur Aube',
        centre: {
          type: 'Point',
          coordinates: [
            4.7019,
            48.1924
          ]
        },
        type: 'epci',
        zoom: 10
      }
    ]);
  });
});
