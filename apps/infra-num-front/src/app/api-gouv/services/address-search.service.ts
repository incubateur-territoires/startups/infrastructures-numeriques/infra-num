/**
 * Angular imports
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

/**
 * 3rd party imports
 */
import {
  DsfrSearchBarService,
  ItemResult
} from '@betagouv/ngx-dsfr/search-bar';
import { Address } from '@infra-num/common';
import { map, Observable } from 'rxjs';

/**
 * Internal imports
 */
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AddressSearchService implements DsfrSearchBarService {

  constructor(private http: HttpClient) {}

  search(query: string): Observable<ItemResult[]> {
    const formattedQuery: string = query.trim().replace(/[ ]/g, '+');
    return this.http
      .get<Address[]>(`${environment.apiUrl}/addresses?search=${formattedQuery}`)
      .pipe<ItemResult[]>(
        map((addresses: Address[]) =>
        addresses.map((address) => ({
            id: address.id,
            label: address.label,
            originalObject: address
          }))
        )
      );
  }
}
