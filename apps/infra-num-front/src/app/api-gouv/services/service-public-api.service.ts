/**
 * Angular imports
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

/**
 * Internal imports
 */
import { environment } from '../../../environments/environment';

/**
 * 3rd party imports
 */
import { Observable } from 'rxjs';
import { ServicePublicApiProperties } from '@infra-num/common';

@Injectable({
  providedIn: 'root'
})
export class ServicePublicApiService {

  constructor(private http: HttpClient) { }
  getLocalityInformation(
    type: 'epci' | 'city',
    code: string
  ): Observable<ServicePublicApiProperties> {
    switch (type) {
      case 'city':
        return this.getCityHallInformationByInseeCode(code);
      case 'epci':
        return this.getEpciInformationBySirenCode(code);
    }
  }

  getEpciInformationBySirenCode(
    siren: string
  ): Observable<ServicePublicApiProperties> {
    return this.http.get<ServicePublicApiProperties>(`${environment.apiUrl}/epci/${siren}`);
  }

  getCityHallInformationByInseeCode(
    codeInsee: string
  ): Observable<ServicePublicApiProperties> {
    return this.http.get<ServicePublicApiProperties>(`${environment.apiUrl}/city-hall/${codeInsee}`);
  }
}
