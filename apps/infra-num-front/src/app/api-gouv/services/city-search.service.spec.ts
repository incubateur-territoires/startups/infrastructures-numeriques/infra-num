import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';

import { TestBed } from '@angular/core/testing';

import { CitySearchService } from './city-search.service';

describe('CitySearchService', () => {
  let service: CitySearchService;
  let httpController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CitySearchService]
    });
    service = TestBed.inject(CitySearchService);
    httpController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpController.verify();
  });

  it('should return an empty list of cities when the API does so', async () => {
    service.search('12345').subscribe((result) => {
      expect(result.length).toBe(0);
    });
    let response = [];

    const req = httpController.expectOne(
      'http://localhost:3333/api/cities?search=12345'
    );
    expect(req.request.method).toEqual('GET');
    req.flush(response);
  });
});
