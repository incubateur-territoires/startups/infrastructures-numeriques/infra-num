export const environment = {
  production: true,
  apiUrl: '/api',
  emailAnctContact: 'tous-connectes@anct.gouv.fr',
  servicePublicPhoneBook:
    'https://lannuaire.service-public.fr/navigation/mairie',
  appName: 'Toutes et tous connecté·e·s',
  breakpointDesktop: '62em'
};
