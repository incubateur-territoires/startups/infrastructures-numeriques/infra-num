# InfraNum Front

This project is part of the **InfraNum** MonoRepo, managed via [Nx](https://nx.dev). It holds the Front-End part
of the "Infrastructures Numériques" Web app, as a **Single Page Application**, built with [Angular](https://angular.io/) and served with [AWS Lambda](https://aws.amazon.com/fr/lambda/?nc2=h_ql_prod_fs_lbd),
through the [serverless](https://www.serverless.com/) framework ( and the [serverless-finch](https://www.npmjs.com/package/serverless-finch) plugin specifically 😉 ).

## How to use this project ? 🤓

### Develop locally
Tap this in your Terminal, _while being at the MonoRepo's root_:
```shell
nx serve infra-num-front
```
Now, you can update the code and your app will be automatically
updated in your browser as well.

### Make a deployment on AWS Lambda
Tap this in your Terminal, _while being at the MonoRepo's root_:
 ```shell
 nx deploy infra-num-front
 ```

<blockquote>
  🚨 Once it's done, the output in your Terminal will give you
the proper URL to use to view your Front-End app 🚨
</blockquote>

### Remove your current deployment on AWS Lambda
Tap this in your Terminal, _while being at the MonoRepo's root_:
 ```shell
 nx remove infra-num-front
 ```

<blockquote>
  ⚠️ DO NOT FORGET to remove a deployment on AWS Lambda that was meant only to test your
implementation. In the end, it will take up resources and might cost money for nothing 😉
</blockquote>

### Extend the set of icons compatible with the DSFR
1. Add the icon as a `.svg` file into `apps/infra-num-front/src/assets/icons`


2. Go to `apps/infra-num-front/src/assets/styling/_icons-extension.scss` and add this :
```scss
@include mix.extend-icon-set(<icon-class-name>, <icon-svg-filename>);
```
<blockquote>
  ⚠️ DO NOT FORGET that <code>icon-class-name</code> must be a string that begins with <code>fr-icon</code> in order to be
compatible with the system ⚠️
</blockquote>

<blockquote>
  ⚠️ <code>icon-svg-filename</code> is the name of the svg file WITHOUT the <code>.svg</code> extension ⚠️
</blockquote>

For instance :
```scss
@include mix.extend-icon-set("fr-icon-pushpin-line", "pushpin-line");
```

3. Now you can use `icon-class-name` ( _remember, with `fr-icon` at the start_ 😉 ), wherever you would
normally use icon classes from the DSFR
