/**
 * 3rd-party imports
 */
import type { Serverless } from 'serverless/aws';

/**
 * To know what to put into the custom property,
 * see https://github.com/fernando-mc/serverless-finch#configuration-parameters
 */
const serverlessConfiguration: Serverless = {
  provider: {
    name: 'aws',
    region: 'eu-west-3',
    profile: 'sls-infra-num',
    runtime: 'nodejs16.x'
  },
  service: 'infra-num-front',
  plugins: [
    'serverless-finch', // Use this to be able to deploy a static website ( like an SPA ) to an S3 bucket
    'serverless-offline'
  ],
  custom: {
    client: {
      bucketName: "s3-for-infra-num-front-${opt:stage, 'dev'}",
      distributionFolder: '../../dist/apps/infra-num-front',
      errorDocument: 'index.html'
    }
  }
};

module.exports = serverlessConfiguration;
